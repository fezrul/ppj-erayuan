let mix = require('laravel-mix');
require('dotenv').config();

const PROXY_URL = process.env.APP_URL || 'localhost';
const WATCH_FILES = [
    'public/css/all.css',
    'resources/assets/js/app.js',
    'resources/views/vendor/**/*.php',
    'packages/ekompaun/**/*.php',
];

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.scripts([
    './node_modules/jquery/dist/jquery.min.js',
    './node_modules/tether/dist/js/tether.min.js',
    './node_modules/popper.js/dist/umd/popper.min.js',
    './node_modules/bootstrap/dist/js/bootstrap.min.js',
    'resources/assets/js/app.js'
], 'public/js/all.js');

mix.sass('resources/assets/sass/app.scss', 'public/css');

mix.styles([
    './node_modules/font-awesome/css/font-awesome.min.css',
    'public/css/app.css',
], 'public/css/all.css');

mix.copy('./node_modules/font-awesome/fonts', 'public/fonts');

mix.browserSync({
    files: WATCH_FILES,
    proxy: PROXY_URL
});
