<?php

namespace Ekompaun\My;

use Ekompaun\Systemconfig\Enum\Permission;
use Illuminate\Support\ServiceProvider;

class MyServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'my');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'my');
        $this->bootMenu();
    }

    public function register()
    {
        include __DIR__.'/../routes/web.php';
    }

    protected function bootMenu()
    {
        app('menu')
            ->add('Kemas Kini Profil', route('my::profile.edit'))
            ->data('icon', 'fa fa-users')
            ->data('permission', Permission::EDIT_MY_PROFILE);

        app('menu')
            ->add('Keputusan Rayuan', route('my::appeal.index'))
            ->data('icon', 'fa fa-legal')
            ->data('permission', Permission::VIEW_MY_RAYUAN);

        app('menu')
            ->add('Senarai Kompaun', route('my::kompaun.index'))
            ->data('icon', 'fa fa-search')
            ->data('permission', Permission::VIEW_KOMPAUN);

        app('menu')
            ->add('Rekod Pembayaran', route('my::payment.index'))
            ->data('icon', 'fa fa-money')
            ->data('permission', Permission::VIEW_MY_PAYMENT_HISTORY);
    }
}
