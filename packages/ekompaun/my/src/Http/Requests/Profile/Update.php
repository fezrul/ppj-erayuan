<?php

namespace Ekompaun\My\Http\Requests\Profile;

use App\User;
use Ekompaun\Systemconfig\Model\UserProfile;
use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = app(User::class)->getTable();
        $profile = app(UserProfile::class)->getTable();

        return [
            'name'                      => 'required',
            'email'                     => "required|email|unique:$user",
            'icno'                      => "required|unique:$profile,user_referenceid",
            'new_password'              => 'required|min:6|confirmed',
            'new_password_confirmation' => 'required|min:6',
            'fk_lkp_state'              => 'required',
            'fk_lkp_country'            => 'required',
        ];
    }
}
