<?php

namespace Ekompaun\My\Http\Requests\Password;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // custom validator
        Validator::extend(
            'password', function () {
                $password = Input::get('old_password');
                return Hash::check($password, \Auth::user()->password);
            }
        );

        return [
            'old_password'              => 'required|password',
            'new_password'              => 'required|min:8|max:14|confirmed',
            'new_password_confirmation' => 'required|min:8|max:14',
        ];
    }

    public function messages()
    {
        return [
            'old_password.password'             => 'Sandi tidak sama',
        ];
    }
}
