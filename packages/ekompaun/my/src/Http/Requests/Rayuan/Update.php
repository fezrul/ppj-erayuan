<?php

namespace Ekompaun\My\Http\Requests\Rayuan;

class Update extends Store
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'appl_email'            => 'required|email',
            'appl_phonenumber'      => 'required|numeric',
            'file.*'                => 'file|max:2048',
        ];
    }

}
