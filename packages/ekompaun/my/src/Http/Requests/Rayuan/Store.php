<?php

namespace Ekompaun\My\Http\Requests\Rayuan;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->user();
        return [
            // 'user_mobileno'         => 'required|numeric',
            // 'email'                 => 'required|email|unique:users,email,'.$user->id,
            'appl_email'            => 'required|email',
            'appl_phonenumber'      => 'required|numeric',
            'appl_reason'           => 'required',
            'file.*'                => 'file|max:2048',
        ];
    }
}
