<?php

namespace Ekompaun\My\Http\Controllers;

use Ekompaun\Systemconfig\Enum\UserCategory;
use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Enum\Citizenship;
use Ekompaun\Systemconfig\Model\Lookup\State;
use Ekompaun\Systemconfig\Model\Lookup\Country;
use Ekompaun\Systemconfig\Services\MyProfileService;
use Ekompaun\Systemconfig\Http\Requests\Profile\UpdateProfileRequest;

class MyProfileController extends Controller
{

    protected $service;

    /**
     * RegistrationController constructor.
     *
     * @param $service
     */
    public function __construct(MyProfileService $service)
    {
        $this->service = $service;
    }

    public function edit()
    {
        $user = auth()->user();
        $states = State::dropdown();
        $countries = Country::dropdown();
        $citizenship = Citizenship::dropdown();
        $categories = UserCategory::dropdown();
        return view('my::profile.edit')
        ->with(compact('user', 'states', 'countries', 'citizenship', 'categories'));
    }

    public function update(UpdateProfileRequest $form)
    {
        try {
            $this->service->update(auth()->user(), $form->all());

            return redirect()->back()->withSuccess(__("Berjaya Mengubah Profil"));
        } catch (\Exception $e) {
            return redirect()->back()->withError($e->getMessage())->withInput();
        }
    }
}
