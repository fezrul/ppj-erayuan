<?php

namespace Ekompaun\My\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Enum\StatusType;

class MyPaymentController extends Controller
{
    public function index(Request $request)
    {
        $applications = $request->user()
            ->userApplication()
            ->with('transaction','paymentstatus')
            ->whereHas('payment', function ($query) {
            $query->where('pymt_status', '=', 1);
        })
            ->where('appl_status', StatusType::PAID)

            ->paginate();

            // dd($applications);
        return view('my::payment.index', compact('applications'));
    }
}
