<?php

namespace Ekompaun\My\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Appeal\Model\Transaction;
use Ekompaun\My\Http\Requests\Rayuan\Store;
use Ekompaun\My\Http\Requests\Rayuan\Update;
use Ekompaun\Appeal\Services\ApplicationFlowService;
use App\Events\Application\ApplicationSubmitEvent;
use App\Events\Application\ApplicationViewEvent;
use App\Events\Application\ApplicationStatusChangeEvent;
use App\Events\Mail\SendMailRayuanKeduaEvent;
use Ekompaun\Sms\Http\Controllers\SmsController;
use Ekompaun\Sap\Http\Controllers\SapController;

class MyAppealController extends Controller
{

    public function __construct(SmsController $sms,SapController $sap)
    {

        $this->sms = $sms;
        $this->sap = $sap;

    }

    public function index(Request $request)
    {
        $applications = $request->user()
            ->userApplication()
            ->typeOfRayuanKedua()
            ->with('transaction')
            ->paginate();

        return view('my::appeal.index', compact('applications'));
    }

    public function create(Transaction $transaction, Request $request)
    {
        $user = $request->user();
        return view('my::appeal.create', compact('user', 'transaction'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Store $request, ApplicationFlowService $service, Transaction $transaction)
    {
        // try {

            $check = Application::where('appl_transid', '=', $transaction->tran_id)->count();
            if($check > 0)
            {

                $checks = Application::where('appl_transid', '=', $transaction->tran_id)->first();
                $checkstatus = $checks->appl_status;

                if(($checkstatus == 8) OR ($checkstatus == 9) OR ($checkstatus == 14)) 
                {

                    
                    $application = $service->rayuanKedua($transaction, $request);
           
                    event(
                        new ApplicationSubmitEvent(
                            $request->user(),
                            $application->appl_id,
                            __('Permohonan Rayuan Ke-2')
                        )
                    );

                    $applicationdata = Application::with(
                    'transaction.user',
                    'transaction.offence'
                     )->findOrFail($application->appl_id);
                    $return = $this->sms->getSend('2', $application->appl_id);
                    event(new SendMailRayuanKeduaEvent($application->appl_id));

                    return redirect()->home()->withSuccess(__("Berjaya mengajukan rayuan"));
                }

                return redirect()->home()->withSuccess(__("Kompaun tidak boleh atau teleh dibuat aduan"));

                   


            }

            $application = $service->rayuanKedua($transaction, $request);
           
                    event(
                        new ApplicationSubmitEvent(
                            $request->user(),
                            $application->appl_id,
                            __('Permohonan Rayuan Ke-2')
                        )
                    );

                    $applicationdata = Application::with(
                    'transaction.user',
                    'transaction.offence'
                     )->findOrFail($application->appl_id);
                    $return = $this->sms->getSend('2', $application->appl_id);
                    event(new SendMailRayuanKeduaEvent($application->appl_id));

                    return redirect()->home()->withSuccess(__("Berjaya mengajukan rayuan"));


            
        // } catch (\Exception $e) {
        //     return redirect()->back()->withError($e->getMessage())->withInput();
        // }
    }

    public function edit(Request $request, $transaction, $id)
    {
        $application = Application::with(
            'transaction.user',
            'transaction.offence',
            'status',
            'attachments'
        )->findOrFail($id);

        event(new ApplicationViewEvent($request->user(), $id));
        return view('my::appeal.edit', compact('application', 'transaction'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update(Update $request, ApplicationFlowService $service, Transaction $transaction)
    {
        $application = Application::findOrFail($request->get('appl_id'));

        $this->authorize('updateRayuanKedua', $application);

        try {
            $service->rayuanKedua($transaction, $request, $application);

            event(new ApplicationStatusChangeEvent($request->user(), $request->get('appl_id')));
            $return = $this->sms->getSend('2', $request->get('appl_id'));
            return redirect()->route('my::appeal.index')->withSuccess(__("Berjaya mengajukan rayuan"));
        } catch (\Exception $e) {
            return redirect()->back()->withError($e->getMessage())->withInput();
        }
    }

    public function destroy(Request $request, ApplicationFlowService $service, $id)
    {
        $application = Application::findOrFail($id);

        try {
            $service->cancel($application);

            event(new ApplicationStatusChangeEvent($request->user(), $id));

            return redirect()->back()->withSuccess(__("Berjaya Membatalkan Rayuan"));
        } catch (\Exception $e) {
            return redirect()->back()->withError($e->getMessage())->withInput();
        }
    }
}
