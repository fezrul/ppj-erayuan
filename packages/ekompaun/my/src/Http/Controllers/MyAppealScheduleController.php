<?php

namespace Ekompaun\My\Http\Controllers;

use App\Events\AuditEvent;
use Ekompaun\Systemconfig\Model\ScheduleRate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ekompaun\Appeal\Model\Transaction;
use Ekompaun\Appeal\Services\ApplicationFlowService as Service;
use App\Events\Application\ApplicationSubmitEvent;
use App\Events\Mail\SendMailMengikutRayuanEvent;
use Ekompaun\Sms\Http\Controllers\SmsController;
use Ekompaun\Sap\Http\Controllers\SapController;
use Ekompaun\Appeal\Model\Application;

class MyAppealScheduleController extends Controller
{
    public function __construct(SmsController $sms,SapController $sap)
    {

        $this->sms = $sms;
        $this->sap = $sap;

    }
    public function index(Request $request, Transaction $transaction, ScheduleRate $scheduleRate)
    {
        $amaunKadarRayuan = $scheduleRate->getAmaunKadarRayuan($transaction);
        $user = $request->user();

        return view('my::appeal-schedule.index', compact('user', 'transaction', 'amaunKadarRayuan'));
    }

    public function store(Request $request, Transaction $transaction, Service $service)
    {


        try {
            $application = $service->rayuanIkutJadwal($transaction, $request->user());
            event(
                new ApplicationSubmitEvent(
                    $request->user(),
                    $application->appl_id,
                    __('Rayuan Mengikut Jadual')
                )
            );

            $applicationdata = Application::with(
            'transaction.user',
            'transaction.offence'
             )->findOrFail($application->appl_id);

            $compno = $applicationdata->transaction->tran_compoundno;
            $price = $applicationdata->appl_campamount;

            $return = $this->sap->getFunction('2', '1',  $compno, $price);
            $return = $this->sms->getSend('1', $application->appl_id);
            //recommit

            event(new SendMailMengikutRayuanEvent($application->appl_id));



            return redirect()->route('my::kompaun.show', $transaction)->withSuccess(__("Berjaya"));
        } catch (\Exception $e) {
            return redirect()->back()->withError($e->getMessage())->withInput();
        }
    }
}
