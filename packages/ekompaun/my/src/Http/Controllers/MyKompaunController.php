<?php

namespace Ekompaun\My\Http\Controllers;

use Ekompaun\Systemconfig\Services\CampaignService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ekompaun\Appeal\Model\Transaction;
use Ekompaun\Systemconfig\Model\Lookup\SearchParameter;
use Ekompaun\Sap\Http\Controllers\SapController;
use Ekompaun\Systemconfig\Model\UserProfile;
use App\User;
use Config;
use DB;
use Auth;
class MyKompaunController extends Controller
{
    public function __construct(SapController $sap)
    {

        $this->sap = $sap;

    }

    public function index(Request $request)
    {
        $sap = Config::get('ppj.sap');
        $user = $request->user();
        $users = User::with('profile')->find($user->id);
        $types = request('parameter');
        $inputs = request('search');
        $uid = $user->id;
        // dd($uid);

        $icno = $user->icno;
        $ssm = $users['profile']->user_companyssmno;
        $vehicle1 = $users['profile']->user_vehicleno1;
        $vehicle2 = $users['profile']->user_vehicleno2;
        $vehicle3 = $users['profile']->user_vehicleno3;


        $allapplist = DB::SELECT("SELECT COUNT(*) as counts FROM `ek_transaction` WHERE `tran_id` NOT IN (SELECT `appl_transid` FROM `ek_application` WHERE created_by = $uid) AND fk_userid = $uid");
         // dd($allapplist);

        if($allapplist[0]->counts > 0)
        {
            $remove = DB::SELECT("SELECT * FROM `ek_transaction` WHERE `tran_id` NOT IN (SELECT `appl_transid` FROM `ek_application` WHERE created_by = $uid) AND fk_userid = $uid");

            foreach ($remove as $key => $value) 
            {
                $tid = $value->tran_id;
                $update = DB::UPDATE("UPDATE ek_transaction SET fk_userid = 1 where tran_id = $tid ");
            }


        }

        $notyoursbutsolved = DB::SELECT("SELECT COUNT(*) AS counts FROM ek_application a,ek_transaction b,`users` c ,`user_profiles` d
                                        WHERE a.`appl_transid` = b.`tran_id`
                                        AND c.`id` = d.`fk_users`
                                        AND a.`created_by` = c.`id`
                                        AND a.created_by = '90'
                                        AND ( b.`tran_offendericno` != c.`icno` OR b.`tran_offendericno` != d.`user_companyssmno`
                                        OR b.`tran_vehicleno` != d.`user_vehicleno1`
                                        OR b.`tran_vehicleno` != d.`user_vehicleno2`
                                        OR b.`tran_vehicleno` != d.`user_vehicleno3`
                                        ) AND a.`appl_status` IN ('4','5','8','9','12','14')

                                        ");

        if($notyoursbutsolved[0]->counts > 0)
        {

                $removeyoursbutsolved = DB::SELECT("SELECT b.* FROM ek_application a,ek_transaction b,`users` c ,`user_profiles` d
                                        WHERE a.`appl_transid` = b.`tran_id`
                                        AND c.`id` = d.`fk_users`
                                        AND a.`created_by` = c.`id`
                                        AND a.created_by = '90'
                                        AND ( b.`tran_offendericno` != c.`icno` OR b.`tran_offendericno` != d.`user_companyssmno`
                                        OR b.`tran_vehicleno` != d.`user_vehicleno1`
                                        OR b.`tran_vehicleno` != d.`user_vehicleno2`
                                        OR b.`tran_vehicleno` != d.`user_vehicleno3`
                                        ) AND a.`appl_status` IN ('4','5','8','9','12','14')

                                        ");

                foreach ($removeyoursbutsolved as $key => $value) {

                   $tid = $value->tran_id;
                   $update = DB::UPDATE("UPDATE ek_transaction SET fk_userid = 1 where tran_id = $tid ");
                }


        }



        if($sap == 1) {

            if($inputs == null) {

                if($vehicle1 == null) {
                }else{$return = $this->sap->getFunction('1', '3', $vehicle1, $vehicle1);
                }
                if($vehicle2 == null) {
                }else{$return = $this->sap->getFunction('1', '3', $vehicle2, $vehicle2);
                }
                if($vehicle3 == null) {
                }else{$return = $this->sap->getFunction('1', '3', $vehicle3, $vehicle3);
                }
                if($ssm == null) {
                }else{$return = $this->sap->getFunction('1', '2', $ssm, $ssm);
                }

                $return = $this->sap->getFunction('1', '2', $icno, $icno);


            }else
            {

                if($vehicle1 == null) {
                }else{$return = $this->sap->getFunction('1', '3', $vehicle1, $vehicle1);
                }
                if($vehicle2 == null) {
                }else{$return = $this->sap->getFunction('1', '3', $vehicle2, $vehicle2);
                }
                if($vehicle3 == null) {
                }else{$return = $this->sap->getFunction('1', '3', $vehicle3, $vehicle3);
                }
                if($ssm == null) {
                }else{$return = $this->sap->getFunction('1', '2', $ssm, $ssm);
                }

                $return = $this->sap->getFunction('1', '2', $icno, $icno);

                if(($types == 1) OR ($types == 3) OR ($types == 4) OR ($types == 6) OR ($types == 7)) {

                    $return = $this->sap->getFunction('1', '2', $inputs, $inputs);

                }elseif($types == 5) {

                    $return = $this->sap->getFunction('1', '3', $inputs, $inputs);

                }else{

                    $return = $this->sap->getFunction('1', '4', $inputs, $inputs);
                }

            }

        }else{

                $users = Auth::user();
                $uid = $users->id;


                if($vehicle1 == null) {
                }else{

                    $checkappl = DB::SELECT("SELECT COUNT(*) as counts from ek_application where appl_transid = (SELECT tran_id from ek_transaction where  tran_vehicleno = '$vehicle1')");
                    if($checkappl[0]->counts > 0)
                    {


                    }else{

                        $update = DB::UPDATE("UPDATE ek_transaction SET fk_userid = $uid where tran_vehicleno = '$vehicle1'");
                    }
                }
                if($vehicle2 == null) {
                }else{


                    $checkappl = DB::SELECT("SELECT COUNT(*) as counts from ek_application where appl_transid = (SELECT tran_id from ek_transaction where  tran_vehicleno = '$vehicle2')");
                    if($checkappl[0]->counts > 0)
                    {


                    }else{

                        $update = DB::UPDATE("UPDATE ek_transaction SET fk_userid = $uid where tran_vehicleno = '$vehicle2'");
                    }


                }
                if($vehicle3 == null) {
                }else{


                    $checkappl = DB::SELECT("SELECT COUNT(*) as counts from ek_application where appl_transid = (SELECT tran_id from ek_transaction where  tran_vehicleno = '$vehicle3')");
                    if($checkappl[0]->counts > 0)
                    {


                    }else{

                        $update = DB::UPDATE("UPDATE ek_transaction SET fk_userid = $uid where tran_vehicleno = '$vehicle3'");
                    }


                }
                if($ssm == null) {
                }else{


                    $ek_transssm = DB::SELECT("SELECT * from ek_transaction where tran_offendericno = '$ssm' and tran_id not in(select appl_transid from ek_application)");

                    foreach ($ek_transssm as $key => $value) {

                        $update = DB::UPDATE("UPDATE ek_transaction SET fk_userid = $uid where tran_id = $value->tran_id");
                    }



                }
                if($icno == null) {
                }else{

                    $ek_transssm = DB::SELECT("SELECT * from ek_transaction where tran_offendericno = '$icno' and tran_id not in(select appl_transid from ek_application)");

                    foreach ($ek_transssm as $key => $value) {

                        $update = DB::UPDATE("UPDATE ek_transaction SET fk_userid = $uid where tran_id = $value->tran_id");
                    }




                }

        }

        $parameters = SearchParameter::whereParamStatus(1)->pluck('param_name', 'param_id');
        $transactionsQuery = Transaction::byKeyword(request('search'))
        ->with('offence', 'statusSap', 'lastApplication.status');

        if (!request('search')) {
            $transactionsQuery->byUser(auth()->id());
        }

        $transactions = $transactionsQuery->paginate()->appends(['search'=>request('search')]);

        return view('my::kompaun.index', compact('transactions', 'user', 'parameters'));
    }

    public function show(Request $request, $id, CampaignService $campaignService)
    {
        $user = $request->user();
        $transaction = Transaction::with('offence', 'statusSap', 'vehicle', 'lastApplication')->findOrFail($id);
        $activeCampaign = $campaignService->getCampaignFor($transaction);
        $amountAfterDiscount = $campaignService->calculateDiscount($transaction->tran_compoundamount, $activeCampaign->cdet_amount, $activeCampaign->type);

        return view('my::kompaun.show', compact('transaction', 'user', 'activeCampaign', 'amountAfterDiscount'));
    }
}
