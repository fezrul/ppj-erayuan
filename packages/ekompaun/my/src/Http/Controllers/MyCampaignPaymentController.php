<?php

namespace Ekompaun\My\Http\Controllers;

use Ekompaun\Appeal\Model\Transaction;
use App\Http\Controllers\Controller;
use Ekompaun\Appeal\Services\ApplicationFlowService;
use Ekompaun\Systemconfig\Services\CampaignService;
use Ekompaun\Sms\Http\Controllers\SmsController;
use Ekompaun\Sap\Http\Controllers\SapController;

class MyCampaignPaymentController extends Controller
{
	public function __construct(SmsController $sms,SapController $sap)
    {

        $this->sms = $sms;
        $this->sap = $sap;

    }
    public function update($id, ApplicationFlowService $service, CampaignService $campaignService)
    {
        $transaction = Transaction::findOrFail($id);

        $activeCampaign = $campaignService->getCampaignFor($transaction);
        $amountAfterDiscount = $campaignService->calculateDiscount($transaction->tran_compoundamount, $activeCampaign->cdet_amount, $activeCampaign->type);

        $service->bayarKempen($transaction, $activeCampaign, $amountAfterDiscount, auth()->user());

             $compno = $transaction->tran_compoundno;
             $price  = $amountAfterDiscount;

             $appl_status = $transaction->lastApplication->appl_status;
             if ($appl_status !== 16)
            {

                $return = $this->sap->getFunction('2', '3', $compno, $price);

            }


                // $return = $this->sms->getSend('4', $id);
           
         

        return redirect()->route('payment::sambungan.index',$transaction);
    }
}
