<?php

namespace Ekompaun\My\Http\Controllers;

use App\Http\Controllers\Controller;
use Ekompaun\My\Http\Requests\Password\Update;

class MyPasswordController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit()
    {
        $user = auth()->user();

        return view('my::password.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @return Response
     */
    public function update(Update $request)
    {
        auth()->user()->setPassword($request->new_password);

        return redirect()->back()->withSuccess(__("Berjaya Mengubah Kata Laluan"));
    }
}
