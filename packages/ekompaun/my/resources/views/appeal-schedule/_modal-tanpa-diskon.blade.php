<!-- Modal -->
<div id="modalTanpaDiskon" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body text-center" style="padding: 20px">
                <p>Kompaun anda telah melebihi tempoh diskaun. Sila klik butang "Tidak Setuju" untuk membuat rayuan.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

@push('end')
    <script>
        $('#modalTanpaDiskon').modal('show');
    </script>
@endpush
