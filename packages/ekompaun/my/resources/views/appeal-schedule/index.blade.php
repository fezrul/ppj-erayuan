@extends('layouts.app')
@section('content')
    <div class="container-fluid mt-5">
        <div class="card">
            <div class="card-header">
                <h5 class="mb-0">{{ __("Maklumat Kompaun Selepas Diskaun") }}</h5>
                <div class="card-actions"></div>
            </div>
            <div class="card-body">
                <div class="row">
                	<div class="col-sm-3">{{ __("ID Pengguna") }} </div>
                    <div class="col-sm-9">: {{ $transaction->tran_offendericno }}</div>
                    <div class="col-sm-3">{{ __("Nama Pelanggan") }} </div>
                    <div class="col-sm-9">: {{ $transaction->tran_offendername }}</div>
                    <div class="col-sm-3">{{ __("No Kompaun") }} </div>
                    <div class="col-sm-9">: {{ $transaction->tran_compoundno }}</div>
                    <div class="col-sm-3">{{ __("No Kenderaan") }} </div>
                    <div class="col-sm-9">: {{ $transaction->tran_vehicleno }}</div>
                    <div class="col-sm-3">{{ __("No Telefon") }} </div>
                    <div class="col-sm-9">: {{ $user->profile->user_mobileno }}</div>
                    <div class="col-sm-3">{{ __("E-mel") }} </div>
                    <div class="col-sm-9">: {{ $user->email }}</div>
                    <div class="col-sm-3">{{ __("Amaun Asal (RM)") }} </div>
                    <div class="col-sm-9">: {{format_money($transaction->tran_compoundamount)}}</div>
                    <div class="col-sm-3">{{ __("Amaun Perlu Bayar (RM)") }} </div>
                    <div class="col-sm-9">: <strong>{{format_money($amaunKadarRayuan)}}</strong></div>
                    <div class="col-sm-3">{{ __("Status") }} </div>
                    <div class="col-sm-9">: {!! $transaction->lastApplication->present_status !!}</div>

                    <br>
                    <br>
                    <div class="col-sm-12">
                        <div style="font-size: 1.25em; font-weight:bold">
                            @if($amaunKadarRayuan == $transaction->present_compoundamount)
                                {{ __('Nota : Kompaun anda telah melebihi tempoh diskaun. Sila klik butang "Tidak Setuju" untuk membuat rayuan.') }}
                                @include('my::appeal-schedule._modal-tanpa-diskon')
                            @elseif($amaunKadarRayuan < $transaction->present_compoundamount)
                                {{ __('Nota : Sekiranya tidak bersetuju dengan "Amaun Perlu Bayar" selepas diskaun, klik "Tidak Setuju" untuk membuat rayuan.') }}
                            @endif
                        </div>
                    </div>

                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-md-3">
                        <a href="{{ route('my::kompaun.index') }}" class="btn btn-primary">{{ __("Kembali") }}</a>
                        <a href="{{ route('my::appeal.create', $transaction) }}" class="btn btn-primary">{{ __("Tidak Setuju") }}</a>
                    </div>
                    <div class="col-md-3">
		            	{{ html()->form('POST', route('my::appeal-schedule.store',$transaction))->open() }}

                            @if($amaunKadarRayuan == $transaction->present_compoundamount)
                              
                            @elseif($amaunKadarRayuan < $transaction->present_compoundamount)
                                 {{ html()->input('submit', '')->value(__("Setuju"))->addClass('btn btn-primary') }}
                            @endif
                       
                        {{ html()->form()->close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
