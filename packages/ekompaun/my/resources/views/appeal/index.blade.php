@extends('layouts.app')
@section('content')
<div class="my-5">
    <div class="card" style="overflow-x: auto;">
        <div class="card-header">
            <h5 class="mb-0">{{ __("Senarai Keputusan Rayuan") }}</h5>
            <div class="card-actions">

            </div>
        </div>
        <table class="table table-striped mb-0 table-responsive">
            <thead class="thead-light">
                <tr>
                    <th>{{ __("Bil") }}</th>
                    <th>{{ __("No Kompaun") }}</th>
                    <th>{{ __("Jenis Kesalahan") }}</th>
                    <th>{{ __("Tarikh Kompaun") }}</th>
                    <th>{{ __("Tarikh Mohon") }}</th>
                    <th>{{ __("Status Kompaun") }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($applications as $application)
                <?php 

                    if($application->status->status_id == 12){


                    }else{

                ?>
                <tr>
                    <td>{{ numbering($application, $applications) }}</td>
                    @if($application->status->status_id == 6)
                    <td><a href="{{route('my::appeal.edit', [$application->appl_transid, $application]) }}">
                        {{ $application->transaction->tran_compoundno }}
                    </a></td>
                    @else
                    <td><a href="{{route('my::kompaun.show', $application->transaction)}}" target="blank">
                        {{ $application->transaction->tran_compoundno }}
                    </a></td>
                    @endif
                    <td>{{ $application->transaction->offence->offence_name }}</td>
                    <td>{{ format_date($application->transaction->tran_compounddate) }}</td>
                    <td>{{ format_date($application->appl_date) }}</td>
                    <td>{{ $application->status->status_name }}</td>
                </tr>
                <?php } ?>
                @endforeach
            </tbody>
        </table>
        <div class="card-footer">

        </div>
    </div>
</div>

@endsection
