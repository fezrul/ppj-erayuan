@extends('layouts.app')

@section('content')
    <div>
        <div class="card">
            <div class="card-header">
                <h5 class="mb-0">{{ __("Permohonan Rayuan - Mohon Dokumen Tambahan") }}</h5>
                <div class="card-actions">

                </div>
            </div>
            <div class="card-body">
                @can('updateRayuanKedua', $application)
                {{ html()->form('PUT', route('my::appeal.update',[$transaction,$application->getKey()]))->attribute("enctype='multipart/form-data'")->open() }}
                @endcan

                <div class="container-fluid mb-3">
                    <div class="row">
                        <div class="col-6">
                            <div class="pr-5">
                                {{ html()->hidden('appl_id',$application->appl_id)}}
                                {{ html()->formGroup(__("Nama Pelanggan"), html()->text('name', $application->transaction->user->name)->attribute('disabled')) }}
                                {{ html()->formGroup(__("No. Kompaun"), html()->text('icno', $application->transaction->tran_compoundno)->attribute('disabled')) }}
                                {{ html()->formGroup(__("No. Telefon"), html()->text('appl_phonenumber', $application->appl_phonenumber)) }}
                                {{ html()->formGroup(__("Emel"), html()->email('appl_email', $application->appl_email)) }}
                                {{ html()->formGroup(__("Amaun Asal (RM)"), html()->text('amount', $application->transaction->present_compoundamount)->attribute('disabled')) }}
                                <div class="row">
                                <div class="col-sm-4">
                                    <label class="col-sm-label">{{ __("Dokumen Sokongan")}}</label>
                                </div>
                                <div class='col-sm-8'>
                                    @if($application->attachments->count() > 0)
                                        @foreach($application->attachments as $attachment)
                                            <a href="{{asset('storage/attachments/'.$attachment->attc_filename)}}" target="_blank">{{ __("Dokumen ".$loop->iteration) }}&nbsp;&nbsp;</a>
                                        @endforeach
                                    @else
                                        -
                                    @endif
                                </div>
                                </div>
                                {{ html()->formGroup(__("Ulasan Pegawai"), html()->textarea('ulasan', $application->transaction->lastApplication->appl_reason)->attribute('disabled')) }}
                                {{ html()->hidden('appl_reason',$application->transaction->lastApplication->appl_reason)}}
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="pl-5">
                                <div class="row form-group">
                                    <div class="col-sm-5">
                                        <label class="col-sm-label">{{ __("Maklumat tambahan (jika ada) bagi menyokong rayuan")}}</label>
                                    </div>
                                    <div class="col-sm-7">
                                        @foreach(range(0, 2) as $i)
                                            <div class="file-upload">
                                                <div class="file-select">
                                                    <div class="file-select-name"><i class="fa fa-upload"></i> {{ __('Muatnaik') }}</div>
                                                    {{ html()->file("file[$i]") }}
                                                </div>
                                            </div>
                                        @endforeach
                                        <small class="form-text text-muted">{{ __("format fail jpg, jpeg, pdf sahaja") }}</small>
                                        <small class="form-text text-muted">{{ __("Max 1 MB") }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="form-group row mx-3 justify-content-center">
                    @can('updateRayuanKedua', $application)
                    <button type="submit" class="btn btn-primary">{{ __("Hantar") }}</button>
                    @endcan
                    <a href="{{ route('my::appeal.index') }}" class="btn btn-link">{{ __("Kembali") }}</a>
                </div>

                @can('updateRayuanKedua', $application)
                {{ html()->form()->close() }}
                @endcan

            </div>
        </div>
    </div>
@endsection

@push('head')
    <style>
        .file-upload{display:block;text-align:center;cursor:pointer;margin-bottom: 5px;}
        .file-upload .file-select{cursor:pointer;display:block;border: 1px solid #bdbbb5; border-radius: 0.25rem; color: #34495e;height:40px;line-height:40px;text-align:left;background:#FFFFFF;overflow:hidden;position:relative;}
        .file-upload .file-select .file-select-name{cursor:pointer;line-height:40px; overflow:hidden; white-space: nowrap; text-overflow: ellipsis; display:block;padding:0 10px;}
        .file-upload.active .file-select{border-color:#3fa46a;background-color:#3fa46a;color:#ffffff;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
        .file-upload .file-select input[type=file]{z-index:100;cursor:pointer;position:absolute;height:100%;width:100%;top:0;left:0;opacity:0;filter:alpha(opacity=0);}
        .file-upload .file-select.file-select-disabled:hover .file-select-name{line-height:40px;display:inline-block;padding:0 10px;}
    </style>
@endpush

@push('end')
    <script>
        $('.file-upload').bind('change', 'input[type=file]', function (e) {
            var input = $(e.target);
            var wrapper = $(e.currentTarget);
            var filename = input.val();

            if (/^\s*$/.test(filename)) {
                wrapper.removeClass('active');
                wrapper.find('.file-select-name').html("<i class=\"fa fa-upload\"></i> Muatnaik");
            }
            else {
                wrapper.addClass('active');
                wrapper.find('.file-select-name').html(filename.replace("C:\\fakepath\\", ""));
            }
        });
    </script>
@endpush
