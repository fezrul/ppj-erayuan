@extends('layouts.app')

@section('content')
    <div class="my-5">
        @component('components.panel')
            @slot('title', 'Maklumat Pelanggan')

            @component('components.form', ['action' => route('my::profile.update')])
                {{ method_field('PUT') }}
                <div class="container-fluid mb-3">
                    <div class="row">
                        <div class="col-md-6">
                                @component('components.fields_equal', ['fields' => [
                                    ['label' => __("Nama"), 'name' => 'name', 'value' => $user->name],
                                    ['label' => __('Kewarganegaraan'), 'type' => 'radio', 'name' => 'user_citizenship', 'options' => $citizenship, 'value' => $user->profile->user_citizenship, 'id' => 'citizenshipSelector'],
                                    ['label' => __("No Kad Pengenalan"), 'name' => 'icno', 'value' => $user->icno,'help' => 'Tanpa tanda "-"', 'attributes' => ['data-citizenship' => \Ekompaun\Systemconfig\Enum\Citizenship::RESIDENT]],
                                    ['label' => __("No Pasport"), 'name' => 'icno', 'value' => $user->icno,'help' => 'Tanpa tanda "-"', 'attributes' => ['data-citizenship' => \Ekompaun\Systemconfig\Enum\Citizenship::NON_RESIDENT]],
                                    ['type' => 'radio', 'options' => $categories, 'label' => __("Kategori"), 'name' => 'user_category', 'value' => $user->profile->user_category],
                                    ['label' => __("Syarikat SSM No"), 'name' => 'user_companyssmno', 'value' => $user->profile->user_companyssmno, 'attributes' => ['class' => 'field-for-company']],
                                    ['label' => __("Premis No"), 'name' => 'user_premisno', 'value' => $user->profile->user_premisno, 'attributes' => ['class' => 'field-for-company']],
                                    ['label' => __("No. Kenderaan"), 'name' => 'user_vehicleno1','value' => $user->profile->user_vehicleno1],
                                    ['label' => '', 'name' => 'user_vehicleno2','value' => $user->profile->user_vehicleno2],
                                    ['label' => '', 'name' => 'user_vehicleno3','value' => $user->profile->user_vehicleno3],
                                ]])
                                @endcomponent
                        </div>
                        <div class="col-md-6">
                                @component('components.fields_equal', ['fields' => [
                                    ['label' => __("Emel"), 'name' => 'email','value' => $user->email],
                                    ['label' => __("No. Telefon Bimbit"), 'name' => 'user_mobileno','value' => $user->profile->user_mobileno],
                                    ['label' => __("No. Telefon Rumah"), 'name' => 'user_phoneno','value' => $user->profile->user_phoneno],
                                    ['label' => __("No. Telefon Pejabat"), 'name' => 'user_officeno','value' => $user->profile->user_officeno],
                                    ['label' => __("Alamat"), 'name' => 'user_address1','value' => $user->profile->user_address1],
                                    ['label' => '', 'name' => 'user_address2','value' => $user->profile->user_address2],
                                    ['label' => '', 'name' => 'user_address3','value' => $user->profile->user_address3],
                                    ['label' => __("Poskod"), 'name' => 'user_postcode','value' => $user->profile->user_postcode],
                                    ['type' => 'select', 'options' => $states, 'label' => __("Negeri"), 'name' => 'fk_lkp_state', 'value'=>$user->profile->fk_lkp_state, 'attributes' => ['data-citizenship' => \Ekompaun\Systemconfig\Enum\Citizenship::RESIDENT]],
                                    ['type' => 'select', 'options' => $countries, 'label' => __("Negara"), 'name' => 'fk_lkp_country', 'value'=>$user->profile->fk_lkp_country, 'attributes' => ['data-citizenship' => \Ekompaun\Systemconfig\Enum\Citizenship::NON_RESIDENT]],
                                ]])
                                @endcomponent
                        </div>
                    </div>
                </div>

                <div class="form-group row mx-3 justify-content-center">
                    <button type="submit" class="btn btn-primary">{{ __("Hantar") }}</button>
                </div>

            @endcomponent

        @endcomponent
    </div>
@endsection

@push('end')
    <script>
        $(function(){
           $('#citizenshipSelector input:radio').on('change', function(e){
               if($(this).is(':checked')) {
                   var citizenship = $(this).val();

                   $('[data-citizenship]').hide();
                   $('[data-citizenship] .form-control').attr('disabled', 'disabled');

                   $('[data-citizenship='+citizenship+']').show();
                   $('[data-citizenship='+citizenship+'] .form-control').removeAttr('disabled');
               }
           }).trigger('change');

            $('input[name=user_category]').on('change', function(e){
                if ($(this).is(':checked')) {
                    $('.field-for-company').hide();
                    var category = $(this).val();

                    if(category == {{ \Ekompaun\Systemconfig\Enum\UserCategory::COMPANY }}) {
                        $('.field-for-company').show();
                    }
                }
            }).trigger('change');

        });
    </script>
@endpush
