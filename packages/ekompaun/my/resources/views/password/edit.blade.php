@extends('layouts.app')

@section('content')
    <div class="my-5">
        @component('components.panel')
            @slot('title', 'Tukar Kata Laluan')

            <div class="p-5">
                @component('components.form', ['action' => route('my.password.update')])
                    {{ method_field('PUT') }}

                    @component('components.fields', ['fields' => [
                        ['type' => 'password', 'label' => __("Kata Laluan Lama"), 'name' => 'old_password', 'help' => __("Minimum delapan aksara")],
                        ['type' => 'password', 'label' => __("Kata Laluan Baharu"), 'name' => 'new_password', 'help' => __("Minimum delapan aksara")],
                        ['type' => 'password', 'label' => __("Pengesahan Kata Laluan Baharu"), 'name' => 'new_password_confirmation', 'help' => __("Minimum delapan aksara")],
                    ]])
                    @endcomponent

                    <div class="form-group row">
                        <div class="col-md-2"></div>
                        <div class="col-md-10">
                            <button type="submit" class="btn btn-primary">{{__("Simpan")}}</button>
                        </div>
                    </div>

                @endcomponent

                @endcomponent
            </div>
    </div>
@endsection('content')
