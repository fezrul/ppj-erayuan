@extends('layouts.app')
@section('content')
<div class="my-5">
    <div class="card" style="overflow-x: auto;">
        <div class="card-header">
            <h5 class="mb-0">{{ __("Senarai Kompaun") }}</h5>
            <div class="card-actions">
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                {{ html()->form('GET', route('my::kompaun.index', ['parameter'=> request('parameter'), 'search'=>request('search')]))->class(['container-fluid'])->open() }}
                    <div {{ html()->class(['row']) }}>
                        <div {{ html()->class(['col-md-5']) }}>
                            {{ html()->select('parameter', $parameters, request('parameter'))->class(['form-control']) }}
                        </div>
                        <div {{ html()->class(['col-md-5']) }}>
                            {{ html()->input('text', 'search', request('search'))->style('text-transform:uppercase')->class(['form-control'])->placeholder(__("Carian Untuk")) }}
                        </div>
                        <div {{ html()->class(['col-md-2']) }}>
                            {{ html()->submit(__("Carian"))->class(['btn btn-secondary btn-block']) }}
                        </div>
                    </div>
                {{ html()->form()->close() }}
            </div>
            <br>
            <div class="row">
                <div class="col-sm-3">{{ __("No Kad Pengenalan") }} </div>
                <div class="col-sm">: {{$user->icno}}</div>
            </div>
        </div>
        <table class="table table-striped mb-0 table-responsive">
            <thead class="thead-light">
                <tr>
                    <th>{{ __("Bil") }}</th>
                    <th>{{ __("No Kompaun") }}</th>
                    <th>{{ __("No Kenderaan") }}</th>
                    <th>{{ __("Jenis Kesalahan") }}</th>
                    <th>{{ __("Tarikh Kompaun") }}</th>
                    <th>{{ __("Amaun Asal (RM)") }}</th>
                    <th>{{ __("Amaun Kempen (RM)") }}</th>
                    <th>{{ __("Status") }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($transactions as $transaction)
                <tr>
                    <td>{{ numbering($transaction, $transactions) }}</td>
                    <td><a href="{{route('my::kompaun.show', $transaction)}}" target="blank">
                        {{ $transaction->tran_compoundno }}
                    </a></td>
                    <td>{{ $transaction->tran_vehicleno }}</td>
                    <td>{{ $transaction->offence->offence_name }}</td>
                    <td>{{ format_date($transaction->tran_compounddate) }}</td>
                    <td>{{ $transaction->tran_compoundamount }}</td>
                    <td>{{ $transaction->lastApplication->present_active_campamoun }}</td>
                    <td>{!! $transaction->lastApplication->present_status !!}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="card-footer">
            {{ $transactions->links() }}
        </div>
    </div>
</div>

@endsection
