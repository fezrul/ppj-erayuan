@extends('layouts.app')
@section('content')
    <div class="container-fluid mt-5">
        <div class="card">
            <div class="card-header">
                <h5 class="mb-0">{{ __("Detail Kompaun") }}</h5>
                <div class="card-actions"></div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col"><h4>{{ __('Butiran Kompaun') }}</h4></div>
                </div>
                <div class="row">
                    <div class="col-sm-2">{{ __("Nama Pelanggan") }} </div>
                    <div class="col-sm">: {{$transaction->tran_offendername}}</div>
                    <div class="col-sm-2">{{ __("Tarikh Kompaun") }} </div>
                    <div class="col-sm">: {{ format_date($transaction->tran_compounddate) }}</div>
                    <div class="w-100"></div>
                    <div class="col-sm-2">{{ __("No Kad Pengenalan") }} </div>
                    <div class="col-sm">: {{$transaction->tran_offendericno}}</div>
                    <div class="col-sm-2">{{ __("No Kompaun") }} </div>
                    <div class="col-sm">: {{$transaction->tran_compoundno}}</div>
                    <div class="w-100"></div>
                    <div class="col-sm-2">{{ __("Kod Kesalahan") }} </div>
                    <div class="col-sm">: {{$transaction->offence->offence_code}}</div>
                    <div class="col-sm-2">{{ __("No Rujukan") }} </div>
                    <div class="col-sm">: {{$transaction->tran_refno}}</div>
                    <div class="w-100"></div>
                    <div class="col-sm-2">{{ __("Jenis Kesalahan") }} </div>
                    <div class="col-sm">: {{$transaction->offence->offence_name}}</div>
                    <div class="col-sm-2">{{ __("Amaun Kompaun (RM)") }} </div>
                    <div class="col-sm">:
                        @if($activeCampaign->exists)
                            {{$amountAfterDiscount}}
                        @else
                            {{$transaction->amount}}
                        @endif
                    </div>
                    <div class="w-100"></div>
                    <div class="col-sm-2">{{ __("Status") }} </div>
                    <div class="col-sm">: {!! $transaction->lastApplication->present_status !!}</div>
                </div>
                <div class="row pt-3">
                    <div class="col"><h4>Maklumat Kesalahan</h4></div>
                </div>
                <div class="row">
                    <div class="col-sm-2">{{ __("Tempat") }} </div>
                    <div class="col-sm">: {{$transaction->tran_place}}</div>
                    <div class="col-sm-2">{{ __("No Kenderaan") }} </div>
                    <div class="col-sm">: {{$transaction->tran_vehicleno}}</div>
                    <div class="w-100"></div>
                    <div class="col-sm-2">{{ __("Tarikh") }} </div>
                    <div class="col-sm">: {{format_date($transaction->tran_compounddate)}}</div>
                    <div class="col-sm-2">{{ __("Jenis") }} </div>
                    <div class="col-sm">: {{$transaction->vehicle->vehicle_name}}</div>
                    <div class="w-100"></div>
                    <div class="col-sm-2">{{ __("Masa") }} </div>
                    <div class="col-sm">: {{format_time($transaction->tran_compounddate)}}</div>
                    <div class="col-sm-2">{{ __("Warna") }} </div>
                    <div class="col-sm">: {{$transaction->tran_vehiclecolor}}</div>
                    <div class="w-100"></div>
                </div>
            </div>
            <table class="table table-striped mb-0">
                <thead class="thead-light">
                    <tr>
                        <th>{{ __("Keterangan") }}</th>
                        <th>{{ __("Jumlah") }}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{$transaction->offence->offence_name}}</td>
                        @if($activeCampaign->exists)
                        <td>{{$amountAfterDiscount}}</td>
                        @else
                        <td>{{$transaction->amount}}</td>
                        @endif
                    </tr>
                    <tr>
                        <td>GST:ZRL:Kadar Sifar</td>
                        <td>{{$transaction->lastApplication->appl_gst}}</td>
                    </tr>
                </tbody>
                <tfoot class="thead-light">
                    <tr>
                        @if($activeCampaign->exists)
                            @if($transaction->lastApplication->is_bayaran_selesai)
                                <th>{{ __("Jumlah Bayaran (Kempen)") }}</th>
                            @else
                                <th>{{ __("Jumlah Perlu Dibayar (Kempen)") }}</th>
                            @endif
                            <th>{{$amountAfterDiscount + $transaction->lastApplication->appl_gst}}</th>
                        @else
                            @if($transaction->lastApplication->is_bayaran_selesai)
                                <th>{{ __("Jumlah Bayaran") }}</th>
                            @else
                                <th>{{ __("Jumlah Perlu Dibayar") }}</th>
                            @endif
                            <th>{{$transaction->total_amount}}</th>
                        @endif
                    </tr>
                </tfoot>
            </table>
            <div class="card-footer">
                <div class="row">

                    <div class="col-md-3">
                        <a href="{{ route('my::kompaun.index') }}" class="btn btn-primary">{{ __("Kembali") }}</a>
                    </div>

                    @can('submitRayuanIkutJadwal', $transaction)
                        <div class="col-md-3">
                            <a href="{{ route('my::appeal-schedule.index', $transaction) }}" class="btn btn-primary">{{ __("Mohon Diskaun") }}</a>
                        </div>
                    @endcan

                    @can('submitRayuanKedua', $transaction)
                        <div class="col-md-3">
                            <a href="{{ route('my::appeal.create', $transaction) }}" class="btn btn-primary">{{ __("Ke Skrin Rayuan") }}</a>
                        </div>
                    @endcan

                    @can('bayarFpx', $transaction)
                        @if($activeCampaign->exists)
                            <div class="col-md-3">
                                {{ html()->form('PUT', route('my::campaign-payment.update', $transaction))->open() }}
                                {{ html()->submit(__("Bayar FPX"))->class(['btn btn-primary']) }}
                                {{ html()->form()->close() }}
                            </div>
                        @else
                             @if($transaction->lastApplication->is_bayaran_selesai)
                            <div class="col-md-3" style="display:none">
                                <a href="{{ route('payment::sambungan.index',$transaction) }}" class="btn btn-primary">{{ __("Bayar FPX") }}</a>
                            </div>
                            @else
                             <div class="col-md-3">
                                <a href="{{ route('payment::sambungan.index',$transaction) }}" class="btn btn-primary">{{ __("Bayar FPX") }}</a>
                            </div>
                             @endif

                        @endif
                    @endcan

                    @can('rundingcara', $transaction)
                        <div class="col-md-3">
                            <a href="{{ route('my::rundingcara.index', $transaction) }}" class="btn btn-primary">{{ __("Rundingcara") }}</a>
                        </div>
                    @endcan

                    @can('batalRayuanIkutJadwal', $transaction)
                        <div class="col-md-3">
                            {{ html()->form('DELETE', route('my::appeal.destroy',$transaction->lastApplication))->open() }}
                            {{ html()->input('submit', '')->value(__("Batal Diskaun"))->addClass('btn btn-primary') }}
                            {{ html()->form()->close() }}
                        </div>
                    @endcan

                </div>
            </div>
        </div>
    </div>

@endsection
