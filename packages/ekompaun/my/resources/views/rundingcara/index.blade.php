@extends('layouts.app')
@section('content')
    <div class="container-fluid mt-5">
        <div class="card">
            <div class="card-header">
                <h5 class="mb-0">{{ __("Rundingcara") }}</h5>
                <div class="card-actions"></div>
            </div>
            <div class="card-body">
                <h5 style="line-height: 2em; text-transform: uppercase">
                    Untuk kes mahkamah, sila berhubung terus dengan pegawai jabatan undang-undang,
                    <br>
                    Perbadanan Putrajaya untuk rundingcara di talian 03-88877078 / 7499 / 7290 / 7610
                    <br>
                    pada waktu pejabat sahaja, Isnin - Jumaat 8 Pagi - 5 Petang
                    <br>
                    <br>
                    Terima Kasih
                </h5>
            </div>
            <div class="card-footer">
                <a href="{{ route('my::kompaun.show', $transaction) }}" class="btn btn-primary">{{ __('Kembali') }}</a>
            </div>
        </div>
    </div>

@endsection
