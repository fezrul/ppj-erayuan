@extends('layouts.app')
@section('content')
    <div class="my-5">
    <div class="card" style="overflow-x: auto;">
        <div class="card-header">
            <h5 class="mb-0">{{ __("Rekod Pembayaran Melalui e-Rayuan Kompaun") }}</h5>
            <div class="card-actions">

            </div>
        </div>
        <table class="table table-striped mb-0 table-responsive">
            <thead class="thead-light">
                <tr>
                    <th>{{ __("Bil") }}</th>
                    <th>{{ __("No Kompaun") }}</th>
                    <th>{{ __("No Kenderaan") }}</th>
                    <th>{{ __("Jenis Kesalahan") }}</th>
                    <th>{{ __("Tarikh Kompaun") }}</th>
                    <th>{{ __("Tarikh Bayaran") }}</th>
                    <th>{{ __("Jumlah Bayaran(RM)") }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($applications as $application)
                <tr>
                    <td>{{ numbering($application, $applications) }}</td>
                    <td><a href="{{ route('payment::pay-resit.show', $application->paymentstatus->getKey()) }}">{{ $application->transaction->tran_compoundno }}</a></td>
                    <td>{{ $application->transaction->tran_vehicleno }}</td>
                    <td>{{ $application->transaction->offence->offence_name }}</td>
                    <td>{{ format_date($application->transaction->tran_compounddate) }}</td>
                    <td>{{ format_date($application->payment->pymt_date) }}</td>
                    <td>{{ $application->appl_approveamount }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="card-footer">

        </div>
    </div>
</div>
@endsection
