<?php

Route::group(
    [
        'prefix'     => LaravelLocalization::setLocale().'/my',
        'middleware' => ['web', 'auth', 'localeSessionRedirect', 'localizationRedirect'],
        'namespace'  => 'Ekompaun\My\Http\Controllers',
        'as'         => 'my::',
    ],
    function () {
        Route::get('rundingcara/{transaction}', function($transaction){
            return view('my::rundingcara.index', compact('transaction'));
        })->name('rundingcara.index');

        Route::get('password', ['uses' => 'MyPasswordController@edit', 'as' => 'password.edit']);
        Route::put('password', ['uses' => 'MyPasswordController@update', 'as' => 'password.update']);

        Route::get('profile', ['as' => 'profile.edit', 'uses' => 'MyProfileController@edit'])
        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::EDIT_MY_PROFILE);

        Route::put('profile', ['as' => 'profile.update', 'uses' => 'MyProfileController@update']);
        Route::resource('appeal', 'MyAppealController', ['only' => ['index', 'destroy']])
        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_MY_RAYUAN);

        Route::resource('transaction/{transaction}/appeal', 'MyAppealController', ['except' => ['index', 'show', 'destroy']]);

        Route::resource('transaction/{transaction}/appeal-schedule', 'MyAppealScheduleController', ['only' => ['index', 'store']]);

        Route::resource('payment', 'MyPaymentController', ['only' => 'index'])
        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_MY_PAYMENT_HISTORY);

        Route::resource('campaign-payment', 'MyCampaignPaymentController', ['only' => 'update']);

        Route::resource('kompaun', 'MyKompaunController', ['only' => ['index', 'show']])
        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_KOMPAUN);
    }
);
