<?php

namespace Ekompaun\Payment\Events;

use Illuminate\Queue\SerializesModels;

class PaymentLog
{
    use SerializesModels;

    public $paylog;

    /**
     * Create a new event instance.
     *
     * @param  Bid $bid
     * @return void
     */
    public function __construct($paylog)
    {
        $this->paylog = $paylog;
    }
    
}
