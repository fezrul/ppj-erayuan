<?php namespace Ekompaun\Payment\Http\Processor;

use Illuminate\Http\Request;
use Orchestra\Support\Facades\Foundation;
use Carbon\Carbon;
use Ekompaun\Payment\Data\Repositories\PaymentRepo;
use Ekompaun\Appeal\Model\Application;
use Illuminate\Database\Connection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Redirect;
use Ekompaun\Payment\Events\PaymentLog;
use App\Events\Payment\PaymentLogEvent;
use Mail;
use Ekompaun\Sms\Http\Controllers\SmsController;
use Ekompaun\Sap\Http\Controllers\SapController;

/**
 * 
 *
 * @package default
 * @author 
 **/
class PaymentProcessor
{
    // protected $repo;

    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    public function __construct(PaymentRepo $repo,SmsController $sms,SapController $sap)
    {
        $this->repo = $repo;
        $this->sms = $sms;
        $this->sap = $sap;
        
    }
    
    public function createpay($transaction,$user)
    {  
       
        return $this->repo->createpay($transaction, $user);
    }
    public function getpayfpx($fpxtransid,$status,$fpx_serial_no,$fpx_trans_date,$namabank)
    {  
       
          $data=$this->repo->datafpx($fpxtransid);
          $updatefpx = $this->repo->updatefpx($data->pymt_id, $fpxtransid, $status, $fpx_serial_no, $fpx_trans_date, $namabank);
          $status=substr($status, 1);


        if($status==1) {
            //$payment = $this->repo->updatelicense($data->id);
            //$emailpay = $this->emailpay($data,1);
            $task='Bayaran FPX Berjaya Indirect -'.$fpx_serial_no;
            event(new PaymentLogEvent([\Auth::user()->id,$data->pymt_id,$task]));
            //update status
            $updatestatus=$this->repo->updatestatus($data->fk_applid);


            $applicationdata = Application::with(
            'transaction.user',
            'transaction.offence'
             )->findOrFail($data->fk_applid);

            $compno = $applicationdata->transaction->tran_compoundno;
            $price1 = $applicationdata->appl_approveamount;
            $price2 = $applicationdata->appl_campamount;
            $type = $applicationdata->appl_type;

            // if($type == 1){$return = $this->sap->getFunction('2', '1', $compno, $price1);}
            // if($type == 2){$return = $this->sap->getFunction('2', '2', $compno, $price1);}
            // if($type == 4){$return = $this->sap->getFunction('2', '4', $compno, $price2);}

            return redirect()->route('payment::pay.index', $data)->withSuccess(__("Bayaran FPX Berjaya dibuat"));
            
        }else{
            $task='Bayaran FPX Gagal Indirect';
            event(new PaymentLogEvent([\Auth::user()->id,$data->pymt_id,$task]));

            return redirect()->route('my::kompaun.show', $data->fk_tranid)->withError(__("Bayaran FPX Gagal dibuat"));
        
        }      
          
      
    }
    public function connectionfpx($fpxtransid,$status,$fpx_serial_no,$fpx_trans_date,$namabank)
    { 
          $data=$this->repo->datafpx($fpxtransid); 
          $updatefpx = $this->repo->updatefpx($data->pymt_id, $fpxtransid, $status, $fpx_serial_no, $fpx_trans_date, $namabank);
          $status=substr($status, 1);
          

        if($status==1) {
            //$updatelicense = $this->repo->updatelicense($data->id);
            $task='Bayaran FPX Berjaya Redirect -'.$fpx_serial_no;
            
            event(new PaymentLogEvent([\Auth::user()->id,$data->pymt_id,$task]));

              //update status
            $updatestatus=$this->repo->updatestatus($data->fk_applid);

            $applicationdata = Application::with(
            'transaction.user',
            'transaction.offence'
             )->findOrFail($data->fk_applid);

            $compno = $applicationdata->transaction->tran_compoundno;
            $price1 = $applicationdata->appl_approveamount;
            $price2 = $applicationdata->appl_campamount;
            $type = $applicationdata->appl_type;

            // if($type == 1){$return = $this->sap->getFunction('2', '1', $compno, $price1);}
            // if($type == 2){$return = $this->sap->getFunction('2', '2', $compno, $price1);}
            // if($type == 4){$return = $this->sap->getFunction('2', '4', $compno, $price2);}

        }else{
            $task='Bayaran FPX Gagal Redirect';
            event(new PaymentLogEvent([\Auth::user()->id,$data->pymt_id,$task]));

        }      
      
    }
    public function datapay($data)
    {  
       
        return $this->repo->datapay($data);
    }

      
} // END