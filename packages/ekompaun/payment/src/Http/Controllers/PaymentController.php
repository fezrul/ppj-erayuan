<?php

namespace Ekompaun\Payment\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Ekompaun\Appeal\Model\Transaction;
use Ekompaun\Payment\Data\Model\Payment;
use Ekompaun\Payment\Http\Processor\PaymentProcessor;

class PaymentController extends Controller
{
    public function __construct(PaymentProcessor $processor)
    {
        $this->processor = $processor;

    }
    public function index(Transaction $transaction, Request $request)
    {

        $this->authorize('bayarFpx', $transaction);

        $user = $request->user();
        $createpay= $this->processor->createpay($transaction, $user);
        return view('payment::sambungan.index', compact('transaction', 'createpay'));
    }

    public function getpayfpx($fpxtransid,$status,$fpx_serial_no,$fpx_trans_date,$namabank)
    {
        return $this->processor->getpayfpx($fpxtransid, $status, $fpx_serial_no, $fpx_trans_date, $namabank);
    }
    public function getConnectionfpx($fpxtransid,$status,$fpx_serial_no,$fpx_trans_date,$namabank)
    {
        return $this->processor->getConnectionfpx($fpxtransid, $status, $fpx_serial_no, $fpx_trans_date, $namabank);
    }


}
