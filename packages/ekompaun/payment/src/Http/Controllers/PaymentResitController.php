<?php

namespace Ekompaun\Payment\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Ekompaun\Appeal\Model\Transaction;
use Ekompaun\Payment\Data\Model\Payment;
use Ekompaun\Payment\Http\Processor\PaymentProcessor;

class PaymentResitController extends Controller
{
    public function __construct(PaymentProcessor $processor)
    {
        $this->processor = $processor;
        
    }    
    public function index($data)
    {
        //
    }
    public function show($id)
    {
        $datapay=$this->processor->datapay($id);
        return view('payment::payment-resit.show', compact('datapay'));
    }
   
    
}
