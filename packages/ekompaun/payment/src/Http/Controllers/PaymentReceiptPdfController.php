<?php

namespace Ekompaun\Payment\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Ekompaun\Appeal\Model\Transaction;
use Ekompaun\Payment\Data\Model\Payment;
use Ekompaun\Payment\Http\Processor\PaymentProcessor;
use PDF;

class PaymentReceiptPdfController extends Controller
{
    public function __construct(PaymentProcessor $processor)
    {
        $this->processor = $processor;
        
    }    
    public function index($data)
    {
        //
    }
    public function show($id)
    {
        $datapay=$this->processor->datapay($id);
        $pdf = PDF::loadview('payment::payment-resit-pdf.show', compact('datapay'))->setPaper('a4', 'potrait');
        return $pdf->download(__('Resit-'.data_get($datapay, 'pymt_refno').'.pdf'));
    }
   
    
}
