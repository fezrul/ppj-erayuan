<?php

namespace Ekompaun\Payment\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Ekompaun\Appeal\Model\Transaction;
use Ekompaun\Payment\Data\Model\Payment;
use Ekompaun\Payment\Http\Processor\PaymentProcessor;

class PaymentInfoController extends Controller
{
    public function __construct(PaymentProcessor $processor)
    {
        $this->processor = $processor;
        
    }    
    public function index($data)
    {
        $datapay=$this->processor->datapay($data);
        //dd($datapay);
        return view('payment::payment.index', compact('datapay'));
    }
    public function show($id)
    {
        dd('xxx');
    }
   
    
}
