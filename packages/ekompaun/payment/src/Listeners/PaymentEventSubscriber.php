<?php

namespace Ekompaun\Payment\Listeners;


class PaymentEventSubscriber
{
   
    /**
     * Register the listeners for the subscriber.
     *
     * @param Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Ekompaun\Payment\Events\PaymentLog',
            'Ekompaun\Payment\Handlers\Events\PayTransaction',
        );

       
    }
}
