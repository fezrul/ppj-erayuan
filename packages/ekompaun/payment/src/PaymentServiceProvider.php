<?php

namespace Ekompaun\Payment;

use Illuminate\Support\ServiceProvider;
use Ekompaun\Systemconfig\Enum\Permission;

class PaymentServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    //  protected $listen = [
       
    //     'Ekompaun\Payment\Events\PaymentLog' => [
    //         'Ekompaun\Payment\Handlers\Events\PayTransaction'
    //     ]
    // ];

    protected $subscribe = [
        'Ekompaun\Payment\Listeners\PaymentEventSubscriber',
    ];

    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'payment');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'payment');
        // $this->bootMenu();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/../routes/web.php';
    }


    
}
