<?php 
namespace Ekompaun\Payment\Data\Model;

use Ekompaun\Platform\Model\BaseModel;

class Users extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Has many and belongs to relationship with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
  
    /**
     * Has One relationship with Elesen\Model\Data\Model\Application.
     */
  
   public function usersprofiles()
   {
        return $this->hasOne('Ekompaun\Payment\Data\Model\UserProfiles','fk_users');
   }
   
   
}
