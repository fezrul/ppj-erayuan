<?php 
namespace Ekompaun\Payment\Data\Model;

use Ekompaun\Platform\Model\BaseModel;
use Ekompaun\Systemconfig\Enum\PaymentStatus;
use Illuminate\Database\Eloquent\Builder;
class Payment extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ek_payment';
    protected $primaryKey = 'pymt_id';
    protected $appends = ['paystatus'];

    /**
     * Has many and belongs to relationship with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
  
    /**
     * Has One relationship with Elesen\Model\Data\Model\Application.
     */
  
  
   public function transaction()
   {
        return $this->belongsTo('Ekompaun\Appeal\Model\Transaction','fk_tranid');
   }
   public function paymod()
   {
        return $this->belongsTo('Ekompaun\Payment\Data\Model\LkpPaymentMethod','fk_paymentmethod');
   }
   public function users()
   {
        return $this->belongsTo('Ekompaun\Payment\Data\Model\Users','fk_users');
   }
   public function getPaystatusAttribute()
   {
           return ($this->pymt_status==1) ?
            __("enum.".PaymentStatus::class.".".PaymentStatus::BERJAYA) :
            __("enum.".PaymentStatus::class.".".PaymentStatus::TIDAK_BERJAYA);    



        
    }
    public function scopeByKeyword(Builder $query, $keyword = null)
    {
        if ($keyword) {
            $query->where(function (Builder $query) use ($keyword) {  
              $query->where('pymt_compoundno',  "$keyword")
                    ->orWhere('pymt_refno', "$keyword");
               
            });
        }

          
    }
   
   
}
