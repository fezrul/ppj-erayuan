<?php namespace Ekompaun\Payment\Data\Repositories;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Ekompaun\Payment\Data\Model\Payment;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Payment\Data\Model\RunningNo;
use DB;
/**
 * 
 *
 * @package default
 * @author 
 **/
class PaymentRepo
{
    /**
     * 
     *
     * @return Illuminate\Pagination\LengthAwarePaginator
     **/

    public function createpay($transaction,$user)
    {  
            $fk_applid = $transaction->lastApplication->appl_id;       
            $refno = $this->generateId();
           
            $type = $transaction->lastApplication->appl_type; 

            if($type == 4){$price = $transaction->lastApplication->appl_campamount;}
            if($type == 2){$price = $transaction->lastApplication->appl_approveamount;}
            if($type == 1){$price = $transaction->lastApplication->appl_campamount;}

                

            // dd($price);

            $paymentinfo = new Payment;
            $paymentinfo->fk_paymentmethod=1;
            $paymentinfo->fk_applid=$fk_applid;
            $paymentinfo->fk_users=$user->id;
            $paymentinfo->fk_tranid = data_get($transaction, 'tran_id');
            $paymentinfo->pymt_compoundno=data_get($transaction, 'tran_compoundno');
            $paymentinfo->pymt_refno='ERK'.date('Ymd').$refno;//no resit
            $paymentinfo->pymt_totalamount=$price;
            $paymentinfo->pymt_received=$price;
            $paymentinfo->pymt_date=date('Y-m-d H:i:s');
            $paymentinfo->pymt_status=0;
            $paymentinfo->created_by=$user->id;
          

            // if($type==1){//fpx

            // //$paymentinfo->fpx_serial_no='FPX001';
            // $paymentinfo->fpx_date=date('Y-m-d H:i:s');
            // $paymentinfo->fpx_status=0;

            // } 
            $paymentinfo->save();

        

             return $paymentinfo;
    }
    public  function generateId()
    {
        $data = RunningNo::first();


        $no = $data->trans_no;

        if(($no < 99999999)) {
            $run_no = $no+1;

            /*-----------update running_no-----------*/
            $data->trans_no = $run_no;
            $data->save();
            /*-----------end update running_no-----------*/

        }else{
            $run_no = 1;
            /*-----------update running_no-----------*/
            $data->trans_no = $run_no;
            $data->save();
            /*-----------end update running_no-----------*/
        }

        while(strlen($run_no) <= 7){
            $run_no = '0' . $run_no;
        }

        return $run_no;

    }
    public function role($userid)
    {
        return UserRole::where('user_id', ($userid))
                         ->pluck('role_id')->toArray();
    }
    public function datafpx($transid)
    {
         $datafpx = Payment::where('pymt_refno', $transid)
                             ->first();

          return $datafpx;
    }
    public function updatefpx($id,$fpxtransid,$status,$fpx_serial_no,$fpx_trans_date,$namabank)
    {
                $data = Payment::find($id);
                $datefpx = strtotime($fpx_trans_date);
                $datetrans = date('Y-m-d H:i:s', $datefpx);
                $status=substr($status, 1);
            
                $data->pymt_status=$status;
                $data->fpx_serial_no=$fpx_serial_no;
                $data->fpx_trans_date=$datetrans;
                $data->pymt_bank=$namabank;
                $data->save();  

    }
    public function datapay($data)
    {
        return Payment::where('pymt_id', ($data))
                        ->with('transaction', 'transaction.user', 'transaction.offence', 
                         'paymod', 'users','users.usersprofiles','transaction.offence.ztrans',
                         'transaction.offence.ztrans.maintrans')
                        ->first();
    }
    public function updatestatus($fk_applid)
    {
                $data = Application::find($fk_applid);
                $data->appl_status=12;
                $data->save();  

    }
    
  

} // END class 