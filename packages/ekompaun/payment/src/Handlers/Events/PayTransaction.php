<?php

namespace Ekompaun\Payment\Handlers\Events;

use Ekompaun\Payment\Events\PaymentLog;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Ekompaun\Data\Model\Payment\PaymentLog;

class PayTransaction
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PaymentLog $event
     * @return void
     */
    public function handle($payment)
    {
        dd($payment);
        $paylog= new PaymentLogs;
        $paylog->fk_users=$payment->paylog[0];
        $paylog->fk_payment=$payment->paylog[1];
        $paylog->task=$payment->paylog[2];
        $paylog->save();
    }
}
