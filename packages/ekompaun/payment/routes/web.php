<?php

Route::group(
    [
        'prefix'     => LaravelLocalization::setLocale().'/payment',
        'middleware' => ['web', 'auth', 'localeSessionRedirect', 'localizationRedirect'],
        'namespace'  => 'Ekompaun\Payment\Http\Controllers',
        'as'         => 'payment::',
    ],
    function () {
        Route::resource('transaction/{transaction}/sambungan', 'PaymentController', ['only' => 'index']);
        Route::resource('data/{data}/pay', 'PaymentInfoController', ['only' => 'index']);
        Route::resource('pay-resit', 'PaymentResitController');
        Route::resource('pay-resit-pdf', 'PaymentReceiptPdfController');
        
    }
);



 Route::group(
     [
        'prefix'     => LaravelLocalization::setLocale().'/fpx',
        'middleware' => ['web'],
        'namespace'  => 'Ekompaun\Payment\Http\Controllers',
        'as'         => 'fpx::',
     ],
     function () {
        Route::any('payfpx/{fpxtransid}/{status}/{fpx_serial_no}/{fpx_trans_date}/{namabank}', ['uses' => 'PaymentController@getpayfpx']);
        Route::any('connectionfpx/{fpxtransid}/{status}/{fpx_serial_no}/{fpx_trans_date}/{namabank}', ['uses' => 'PaymentController@getConnectionfpx']);
        
     }
 );


 