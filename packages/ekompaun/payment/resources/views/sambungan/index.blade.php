@extends('layouts.app')
@section('content')
    <div class="container-fluid mt-5">
        <div class="card">
            <div class="card-header">
                <h5 class="mb-0">Bayaran Fpx</h5>
                <div class="card-actions"></div>
            </div>
            <div class="card-body">
                 <?php $url = 'http://'.$_SERVER['HTTP_HOST'];
                 ?>
 <!--url-->
  @if($url=='http://erayuan.ppj.gov.my' || $url=='http://10.10.32.33' || $url=='https://erayuan.ppj.gov.my')
  {{ html()->form('POST', url('https://fpx.ppj.gov.my/eBayar/confirmation.php'))->attribute("enctype='multipart/form-data'")->open() }}
  @else
  {{ html()->form('POST', url('https://fpx.ppj.gov.my/uat/confirmation.php'))->attribute("enctype='multipart/form-data'")->open() }}
  @endif
              
            	<div class="container-fluid mb-3">
            	 <div class="row">
                    <div class="col"><h4>Maklumat Bayaran</h4></div>
                </div>
                    <div class="row">
                        <div class="col-8">
                            <div class="pr-2">
                                {{ html()->formGroup(__("No Kompaun"), html()->text('TxnDesc', $transaction->tran_compoundno)->attribute('readonly')) }}
                                {{ html()->formGroup(__("No Transaksi FPX"), html()->text('TxnOrderNo', $createpay->pymt_refno)->attribute('readonly')) }}
                                {{ html()->formGroup(__("Jumlah Bayaran (RM)"), html()->text('TxnAmount', number_format($createpay->pymt_totalamount,2))->attribute('readonly')) }}
                                
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
             <div class="card-footer">
                <div class="col-sm-offset-2 col-sm-12 text-right">
                <a href="{{route('my::kompaun.show', $transaction)}}" class="btn btn-primary">Kembali</a>
		         <button class="btn btn-primary" type="submit"> 
		         Teruskan
		         </button>
                </div>
                {{ html()->form()->close() }}
            </div>  
        </div>
                   
    </div>

@endsection