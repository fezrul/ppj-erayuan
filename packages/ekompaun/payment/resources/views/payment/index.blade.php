@extends('layouts.app')
@section('content')
    <div class="my-5">
        <div class="card" style="overflow-x: auto;">
            <div class="card-header">
                <h5 class="mb-0">Maklumat Bayaran</h5>
                <div class="card-actions"></div>
            </div>
<div class="card-body">

                 <div class="basic_bootstrap_tbl">
                <div class="bootstrap-table basic_table table-responsive">
                    <table class="table table-bordered table-hover table-sm table-responsive">
                        <thead>
                            <tr>
                                <th colspan="4" class="">
                                    Butiran Kompaun
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="table-active text-capitalize text-xs-left" width="22%">
                                    <b>{{ __("Nama Pelanggan") }}</b>
                                </td>
                                <td class="text-capitalize" width="26%">
                                   {{data_get($datapay,'transaction.tran_offendername')}}
                                </td>
                                 <td class="table-active text-capitalize text-xs-left" width="22%">
                                    <b>{{  __("Tarikh Kompaun") }}</b>
                                </td>
                                <td class="text-capitalize" width="26%">
                                   {{ format_date(data_get($datapay,'transaction.tran_compounddate'))}}
                                </td>
                               
                            </tr>
                             <tr>
                                <td class="table-active text-capitalize text-xs-left" width="22%">
                                    <b>{{ __("No Kad Pengenalan") }}</b>
                                </td>
                                <td class="text-capitalize" width="26%">
                                   {{data_get($datapay,'transaction.tran_offendericno')}}
                                </td>
                                 <td class="table-active text-capitalize text-xs-left" width="22%">
                                    <b>{{ __("No Kompaun") }}</b>
                                </td>
                                <td class="text-capitalize" width="26%">
                                   {{data_get($datapay,'transaction.tran_compoundno')}}
                                </td>
                               
                            </tr>
                            <tr>
                                <td class="table-active text-capitalize text-xs-left" width="22%">
                                    <b>{{  __("Kod Kesalahan") }}</b>
                                </td>
                                <td class="text-capitalize" width="26%">
                                  {{data_get($datapay,'transaction.offence.offence_code')}}
                                </td>
                                 <td class="table-active text-capitalize text-xs-left" width="22%">
                                    <b>{{__("No Rujukan")}}</b>
                                </td>
                                <td class="text-capitalize" width="26%">
                                    {{data_get($datapay,'transaction.tran_refno')}}
                                </td>
                               
                            </tr>
                            <tr>
                                <td class="table-active text-capitalize text-xs-left" width="22%">
                                    <b>{{ __("Jenis Kesalahan") }}</b>
                                </td>
                                <td class="text-capitalize" width="26%">
                                  {{data_get($datapay,'transaction.offence.offence_name')}}
                                </td>
                                 <td class="table-active text-capitalize text-xs-left" width="22%">
                                    <b>{{ __("Amaun Asal Kompaun(RM)") }}</b>
                                </td>
                                <td class="text-capitalize" width="26%">
                                    {{number_format(data_get($datapay,'pymt_totalamount'),2)}}
                                </td>
                               
                            </tr>
                           
                        </tbody>
                    </table>
                </div>
            </div>
  <br>
  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
   <div class="table-responsive"> 
         <table class="table table-bordered table-striped table-condensed">
         <thead> 
            <tr>
              <th align="center"><b>Bil.</td></b>
              <th style="text-align: center"><b>No Kompaun</td></b>
              <th style="text-align: center"><b>Amaun (RM)</b></td>   
              <th style="text-align: center"><b>Total (RM)</b></td>
             
           </tr> 
        </thead>
         <tbody> 
           <tr>
              <td>
                 1.
              </td>
               <td align="center">
                 {{data_get($datapay,'pymt_compoundno')}}
              </td>
              <td align="center">
                 {{number_format(data_get($datapay,'pymt_received'),2)}}
              </td>
               <td align="center">
                 {{number_format(data_get($datapay,'pymt_totalamount'),2)}}
              </td>
             
           </tr>
            <tr><td colspan='3' align="right"><b>Jumlah (RM)</b></td>
              <td colspan='4' align="center"><b>{{number_format(data_get($datapay,'pymt_totalamount'),2)}}</b></td>
            </tr>
   
            
        </tbody>                           
       
      </table>
 
    </div>
  </div>
  <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-xs-right"> 
       <div class="form-group">
         <div class="col-sm-offset-2 col-sm-12 text-right">
           <a href="{{route('payment::pay-resit.show', data_get($datapay,'pymt_id'))}}" class="btn btn-primary">Cetak Resit</a>
          
        </div>
      </div>     
    </div>
</div>
</div>
          </div>
           
        </div>
                   
    </div>

@endsection