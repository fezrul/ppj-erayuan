@extends('layouts.pdf.master')

@push('head')
    <style type="text/css">
       body { background: white !important; } 
       .left{
            margin-right: 5px;
            padding: 5px;
            float: left;
            width: 50%;
       }
       .right{
            margin-left: 5px;
            padding: 5px;
            float: left;
            width: 45%;
       }
       .clearfix{
            clear: both;
       }
        #back{

   width:100%; 
   height:55%; 
   background-image: url('{{ url('/img/Transparent_Logo_2.png')}}');
   background-position: center;
   background-repeat: no-repeat;
  /*background-attachment:fixed;*/
      
  }
   #title{
      font-size:16px;
      font-family: Arial, Helvetica, sans-serif;
    }
    </style>
@endpush

@section('title', 'Resit Rasmi')
@section('content')
<!--  <table  width="100%" border="0" cellpadding="0">
  <tr>
    <th width="80%" rowspan="5" scope="col" align="center"><img src="{{ url('/img/logo.png') }}" width="100" height="100" /></th>
  </tr>
</table> -->
<div id="back">
<h4 class="mb-0" style="text-align: center" id="title">{{ __("Resit Rasmi") }}</h5>

                        <table  id="title" width="100%" cellpadding="2" border="0" cellspacing="0"> 
                            <tr>
                                <th>{{ __("No. Resit") }}</th>
                                <th>{{ __("Tarikh") }}</th>
                                <th>{{ __("MOD") }}</th>
                                <th>{{ __("No. Dokumen") }}</th>
                                <th>{{ __("Jumlah") }} (RM)</th>
                            </tr>
                            <tr>
                                <td>{{data_get($datapay,'pymt_refno')}}</td>
                                <td>{{date('d-m-Y H:i:s',strtotime(data_get($datapay,'fpx_trans_date')))}}</td>
                                <td>{{data_get($datapay,'paymod.paymethod_name')}}</td>
                                <td>{{data_get($datapay,'pymt_compoundno')}}</td>
                                <td>{{number_format(data_get($datapay,'pymt_totalamount'),2)}}</td>
                            </tr>
                        </table>

<br>
<table  id="title" width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="50%">
                <b>{{ __("No. ID Pelanggan ") }}</b>
            </td>
             <td width="70%"></td>
             <td width="30%">
                <b>{{ __("Rujukan :") }}</b>
            </td>
             <td width="70%">{{data_get($datapay,'fpx_serial_no')}}</td>
            
        </tr>
            <tr>
                 <td>
                    {{data_get($datapay,'transaction.tran_offendericno')}}
                </td>
                 <td>&nbsp;</td>
                 <td width="40%">
                    <b>{{ __("No. Kenderaan :") }}</b>
                </td>
                 <td width="70%">{{data_get($datapay,'transaction.tran_vehicleno')}}</td>
            </tr>
            <!-- <th>{{ __("No. ID Pelanggan :") }}</th>
            <td>{{data_get($datapay,'users.name')}}</td><tr>
            <th>{{ __("FPX ID :") }}</th>
            <td>{{data_get($datapay,'fpx_serial_no')}}</td><tr>
            <th>{{ __("Approval Code :") }}</th>
            <td></td><tr>
            <th>{{ __("Nama Bank :") }}</th>
            <td>{{data_get($datapay,'pymt_bank')}}</td><tr> -->
</table>
<br>
 <table id="title" width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            {{data_get($datapay,'transaction.tran_offendername')}}
        </td>
         
    </tr>
    <tr>
         <td>
            <!-- {{data_get($datapay,'users.usersprofiles.user_address1')}} -->
        </td>
   </tr>
   <tr>
        <td>
            <!-- {{data_get($datapay,'users.usersprofiles.user_address2')}} -->
        </td> 
    </tr>
 <tr>
       <td>
            <!-- {{data_get($datapay,'users.usersprofiles.user_address3')}} -->
        </td> 
    </tr>
 <tr>
       <td>
            <!-- {{data_get($datapay,'users.usersprofiles.user_postcode')}} -->
        </td> 
    </tr>
</table>
      
<br>
<div>
    <table id="title" width="100%" cellpadding="2" border="0" cellspacing="0">
        <thead>
            <tr>
                <th>{{ __("Untuk Bayaran") }}</th>
                <th>{{ __("KOD GST") }}</th>
                <th>{{ __("Amaun(RM)") }}</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{data_get($datapay,'transaction.offence.ztrans.maintrans.main_tdesc')}} {{data_get($datapay,'transaction.offence.ztrans.description')}}</td>
                <td>{{ __("ZRL") }}</td>
                <td>{{number_format(data_get($datapay,'pymt_totalamount'),2)}}</td>
            </tr>
        </tbody>
    </table>
</div>
<br>
<br>
<div id="title">
    <small>{{ __("Resit ini cetakan komputer, tiada tanda tangan diperlukan.") }}</small>
</div>
</div>
@endsection