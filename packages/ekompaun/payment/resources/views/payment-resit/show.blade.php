@extends('layouts.app')
@section('content')
<style>
    #back{

   width:100%; 
   height:100%; 
   background-image: url('{{ url('/img/Transparent_Logo_2.png')}}');
   background-position: center;
   background-repeat: no-repeat;
  /*background-attachment:fixed;*/
      
  }
  </style>

    <div class="my-5">
        <div class="card">
            <div class="card-header">
                <h5 class="mb-0">{{ __("Resit Rasmi") }}</h5>
                <div class="card-actions">
                     <a href="{{route('payment::pay-resit-pdf.show', data_get($datapay,'pymt_id'))}}" target="_blank">Cetak Resit
                      <i class="fa fa-file-pdf-o"></i> {{ __("PDF") }}
                     </a>
          
                </div>
            </div>
            <div>
            &nbsp;
            </div>
              <div>
            &nbsp;
            </div>
            <div id="back">
            <div class="container">
            <div class="col-md-12"><center>
                <b>Resit Rasmi</b>

          <!-- <img src="https://spt.ppj.gov.my/packages/threef/entree/img/logo.png" style="max-width:100px"/> -->
         <!--  <img src="{{ url('/img/logo.png') }}" style="max-width:100px"> -->
                </center>
               </div>
               &nbsp;
                <div class="row">
                    <div class="col-md-12">
                        <div class="bootstrap-table basic_table table-responsive">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
                            <tr>
                                <th>{{ __("No. Resit") }}</th>
                                <th>{{ __("Tarikh") }}</th>
                                <th>{{ __("MOD") }}</th>
                                <th>{{ __("No. Dokumen") }}</th>
                                <th>{{ __("Jumlah") }} (RM)</th>
                            </tr>
                            <tr>
                                <td>{{data_get($datapay,'pymt_refno')}}</td>
                                <td>{{date('d-m-Y H:i:s',strtotime(data_get($datapay,'fpx_trans_date')))}}</td>
                                <td>{{data_get($datapay,'paymod.paymethod_name')}}</td>
                                <td>{{data_get($datapay,'pymt_compoundno')}}</td>
                                <td>{{number_format(data_get($datapay,'pymt_totalamount'),2)}}</td>
                            </tr>
                        </table>
                        </div>
                    </div>
                </div>
                </div>
                &nbsp;
                &nbsp;
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <b>{{ __("No. ID Pelanggan") }}</b>
                                    </td>
                                     <td></td>
                                     <td>
                                        <b>{{ __("Rujukan :") }}</b>
                                    </td>
                                     <td>{{data_get($datapay,'fpx_serial_no')}}</td>
                                    
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                     <td>
                                        {{data_get($datapay,'transaction.tran_offendericno')}}
                                    </td>
                                     <td>&nbsp;</td>
                                     <td>
                                        <b>{{ __("No. Kenderaan :") }}</b>
                                    </td>
                                     <td>{{data_get($datapay,'transaction.tran_vehicleno')}}</td>
                                </tr>
                                <!-- <th>{{ __("No. ID Pelanggan :") }}</th>
                                <td>{{data_get($datapay,'users.name')}}</td><tr>
                                <th>{{ __("FPX ID :") }}</th>
                                <td>{{data_get($datapay,'fpx_serial_no')}}</td><tr>
                                <th>{{ __("Approval Code :") }}</th>
                                <td></td><tr>
                                <th>{{ __("Nama Bank :") }}</th>
                                <td>{{data_get($datapay,'pymt_bank')}}</td><tr> -->
                            </table>
                        </div>
                        <!-- <div class="col-md-6">
                            <table class="table table-bordered">
                                <th>{{ __("Rujukan :") }}</th>
                                <td></td><tr>
                                
                            </table>
                        </div> -->
                    </div>
                </div>
                &nbsp;
            <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        {{data_get($datapay,'transaction.tran_offendername')}}
                                    </td>
                                     
                                </tr>
                                <tr>
                                     <td>
                                        <!-- {{data_get($datapay,'users.usersprofiles.user_address1')}} -->
                                    </td>
                               </tr>
                               <tr>
                                    <td>
                                        <!-- {{data_get($datapay,'users.usersprofiles.user_address2')}} -->
                                    </td> 
                                </tr>
                             <tr>
                                   <td>
                                        <!-- {{data_get($datapay,'users.usersprofiles.user_address3')}} -->
                                    </td> 
                                </tr>
                             <tr>
                                   <td>
                                        <!-- {{data_get($datapay,'users.usersprofiles.user_postcode')}} -->
                                    </td> 
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                &nbsp;
                &nbsp;
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>{{ __("Untuk Bayaran") }}</th>
                                        <th>{{ __("KOD GST") }}</th>
                                        <th>{{ __("Amaun(RM)") }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{data_get($datapay,'transaction.offence.ztrans.maintrans.main_tdesc')}} {{data_get($datapay,'transaction.offence.ztrans.description')}}</td>
                                        <td>{{ __("ZRL") }}</td>
                                        <td>{{number_format(data_get($datapay,'pymt_totalamount'),2)}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div>
                    &nbsp;
                </div>
                <div>
                    &nbsp;
                </div>
                 <div>
                    &nbsp;
                </div>
                 <div>
                    &nbsp;
                </div>
                <div class="col-md-4" style="text-align: center;">
                    <small>{{ __("Resit ini cetakan komputer, tiada tanda tangan diperlukan.") }}</small>
                </div>
                <div class="card-footer">
                </div>
            </div>
        </div>
    </div>
@endsection
