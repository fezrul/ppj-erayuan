<?php
Route::group(
    [
        'middleware' => ['web', 'localeSessionRedirect', 'localizationRedirect'],
        'namespace' => 'Ekompaun\Systemconfig\Http\Controllers',
        'prefix' => LaravelLocalization::setLocale()
    ], function () {

        Route::group(
            ['middleware' => ['guest']], function () {
                Route::resource('registration', 'RegistrationController', ['only' => ['create', 'store']]);
                Route::resource('registration/citizenship', 'RegistrationCitizenshipController', ['only' => ['index']]);
            }
        );

        Route::group(
            ['middleware' => ['auth']], function () {
                Route::resource('profile', 'ProfileController', ['only' => ['edit', 'update']]);

                Route::resource('variable', 'VariableController', ['only' => 'index'])
                ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::MANAGE_SYSTEM);

                Route::resource('holiday', 'HolidayController', ['except' => 'show'])
                ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::MANAGE_HOLIDAY);

                Route::resource('campaign', 'CampaignController', ['except' => 'show'])
                ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::MANAGE_CAMPAIGN);

                Route::resource('offence', 'OffenceController', ['except' => 'show']);

                // Route::delete('campaign-detail', 'CampaignDeleteDetailController@destroy')->name('campaign-detail.destroy');

                Route::resource('schedule-rate', 'ScheduleRateController', ['except' => 'show'])
                ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::MANAGE_SCHEDULE_RATE);

                Route::resource('customer', 'CustomerController', ['only' => ['index', 'show', 'edit', 'update']])
                ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::MANAGE_CUSTOMER);

                Route::resource('customer-password', 'CustomerPasswordController', ['only' => ['edit', 'update']])->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::MANAGE_CUSTOMER);

                Route::get('setting', ['uses' => 'SettingController@edit', 'as' => 'setting.edit'])
                ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::MANAGE_SYSTEM);

                Route::put('setting', ['uses' => 'SettingController@update', 'as' => 'setting.update'])
                ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::MANAGE_SYSTEM);

                Route::group(
                    ['namespace' => 'Pdf'], function () {
                        Route::resource('campaign-pdf', 'CampaignPdfController', ['only' => ['index']])
                        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::MANAGE_CAMPAIGN);

                        Route::resource('customer-pdf', 'CustomerPdfController', ['only' => ['index']])
                        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::MANAGE_CUSTOMER);

                        Route::resource('schedule-rate-pdf', 'ScheduleRatePdfController', ['only' => ['index']])
                        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::MANAGE_SCHEDULE_RATE);
                    }
                );

                Route::group(
                    ['prefix'=>'csv', 'namespace' => 'Csv'], function () {
                        Route::get('schedule-rate', 'CsvScheduleRateController@index')->name('csv.schedule-rate')
                        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::MANAGE_SCHEDULE_RATE);

                        Route::get('customer', 'CsvCustomerController@index')->name('csv.customer')
                        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::MANAGE_CUSTOMER);
                        
                        Route::get('campaign', 'CsvCampaignController@index')->name('csv.campaign')
                        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::MANAGE_CAMPAIGN);
                    }
                );

            }
        );
    }
);
