<?php

return [
    'attributes' => [
        'compound_status' => 'Compound Status',
        'content'         => 'Content',
        'end_date'        => 'End Date',
        'image'           => 'Image',
        'name'            => 'Name',
        'rate_type'       => 'Type',
        'start_date'      => 'Start Date',
        'status'          => 'Status',
    ],
    'action'     => [

    ],
];
