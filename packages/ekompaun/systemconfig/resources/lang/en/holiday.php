<?php

return [
    'attributes' => [
        'no'        => 'No',
        'name'      => 'Name',
        'date_from' => 'Date From',
        'date_to'   => 'Date To',
        'state'     => 'State',
        'status'    => 'Status',
    ],
    'action'     => [

    ],
];
