<?php

return [
    'campaign'     => 'Campaign',
    'customer'     => 'Customer',
    'holiday'      => 'Holiday',
    'variable'     => 'Custom Variable',
    'user'         => 'Users (Staf)',
    'role'         => 'Roles',
    'rate'         => 'Schedule Rate',
    'setting'      => 'Setting',
    'systemconfig' => 'System',
];
