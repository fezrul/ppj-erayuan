@extends('layouts.app')
@section('content')
<div class="container-fluid mt-5">
    <div class="card">
        <div class="card-header">
            <h5 class="mb-0">Tetapan Sistem</h5>
        </div>
        <div class="card-body">
            {{ html()->form('PUT', route('setting.update'))->open() }}
                {{ html()->formGroup(__("Nama"), html()->input('text', 'set_name', setting_fallback('site.name'))->class(['form-control']) ) }}
                {{ html()->formGroup(__("Keterangan"), html()->input('text', 'set_description', setting_fallback('site.description'))->class(['form-control']) ) }}

                {{ html()->formGroup(__("Hak Cipta"), html()->input('text', 'set_copyright', setting_fallback('site.copyright'))->class(['form-control']) ) }}
                {{ html()->formGroup(__("Peranan Lalai"), html()->select('set_default_role', $roles, setting_fallback('site.default_role'))->class(['form-control'])) }}
                <div {{ html()->class(['form-group row mb-0']) }}>
                    <div {{ html()->class(['col-sm-10']) }}>
                        {{ html()->submit(__("Hantar"))->class(['btn btn-primary']) }}
                        {{ html()->a("#",__("Batal"))->class(['btn btn-link']) }}
                    </div>
                </div>
            {{ html()->form()->close() }}
        </div>
        <div class="card-footer">
        </div>
    </div>
</div>
@endsection
