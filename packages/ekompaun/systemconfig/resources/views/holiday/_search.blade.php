{{ html()->form('GET', "")->open() }}
    <div {{ html()->class(['row']) }}>
        <div {{ html()->class(['col-md-5']) }}>
            {{ html()->select('year', $years, request('year'))->class(['form-control']) }}
        </div>
        <div {{ html()->class(['col-md-5']) }}>
            {{ html()->input('text', 'search', request('search'))->class(['form-control'])->placeholder(__("Carian Untuk")) }}
        </div>
        <div {{ html()->class(['col-md-2']) }}>
            {{ html()->submit(__("Carian"))->class(['btn btn-secondary btn-block']) }}
        </div>
    </div>
{{ html()->form()->close() }}
