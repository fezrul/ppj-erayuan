@extends('layouts.app')
@section('content')

    <div class="my-5">
        <div class="card" style="overflow-x: auto;">
            <div class="card-header">
                <h5 class="mb-0">{{ __("Senarai Cuti") }}</h5>
                <div class="card-actions">
                    <a href="{{ route('holiday.create') }}" class=""><i class="fa fa-plus"></i> {{ __("Tambah") }}</a>
                </div>
            </div>
            <div class="card-header">
                @include('systemconfig::holiday._search')
            </div>
            @if ($holidays->count() > 0 )
                <table class="table table-striped mb-0">
                    <thead class="thead-light">
                    <tr>
                        <th>{{ __("No") }}</th>
                        <th>{{ __("Nama") }}</th>
                        <th>{{ __("Tarikh") }}</th>
{{--                        <th>{{ __("Tarikh Hingga") }}</th>--}}
                        <th>{{ __("Status") }}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        @foreach($holidays as $h)
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $h->hday_name }}</td>
                            <td>{{ $h->hday_date_from }}</td>
                            {{--<td>{{ $h->hday_date_to }}</td>--}}
                            @if($h->hday_status === 2)
                                <td>{{ $h->hday_status = __("Aktif") }}</td>
                            @else
                                <td>{{ $h->hday_status = __("Tidak Aktif") }}</td>
                            @endif
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-secondary btn-sm dropdown-toggle" type="button"
                                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                        {{ __("Tindakan") }}
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item"
                                           href="{{route('holiday.edit',$h->hday_id)}}">{{ __("Kemas Kini") }}</a>
                                        <div class="dropdown-divider"></div>
                                        {{ html()->form("DELETE", route('holiday.destroy',$h->hday_id))->open() }}
                                        {{ html()->submit(__("Padam") )->class(['dropdown-item padam']) }}
                                        {{ html()->form()->close() }}
                                    </div>
                                </div>
                            </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                @include('components.empty')
            @endif
            {{--<div class="card-footer">--}}
                {{--{{ $holidays->links() }}--}}
            {{--</div>--}}
        </div>
    </div>
    @include('components.modal-delete-confirmation', ['label' => __('Padam data cuti')])
@endsection

@push('end')
<script>
    $(function(){
       var modal = $("#modal-delete-confirmation");
       var submit = $("#submit");
       var padam = $(".padam");
       padam.click(function (e) {
            var form = $(this).closest('form');
            e.preventDefault();
            modal.modal('show');

            submit.click(function () {
                form.submit();
            });
       })
    });
</script>
@endpush
