@extends('layouts.app')

@push('head')
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
@endpush

@section('content')
<div class="container-fluid mt-5">
    <div class="card">
        <div class="card-header">
            <h5 class="mb-0">{{ __("Tambah Cuti") }}</h5>
        </div>
        <div class="card-body">
            {{ html()->form('POST', route('holiday.store'))->open() }}
            {{ html()->formGroup(__("Nama"), html()->input('text', 'hday_name')->placeholder(__("Nama"))->class(['form-control'])) }}
            {{ html()->formGroup(__("Tarikh"), html()->input('text', 'hday_date_from')->class(['form-control'])) }}
{{--            {{ html()->formGroup(__("Tarikh Hingga"), html()->input('text', 'hday_date_to')->class(['form-control'])) }}--}}
            {{ html()->formGroup(__("Status"), html()->select('hday_status', $status, request('hday_status'))->class(['form-control'])) }}
        </div>
        <div class="card-footer">
            {{ html()->submit(__("Hantar"))->class(['btn btn-primary']) }}
            {{ html()->a(route('holiday.index'),__("Batal"))->class(['btn btn-link']) }}
        </div>
            {{ html()->form()->close() }}
    </div>
</div>
@endsection('content')

@push('end')
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

<script type="text/javascript">
$(function() {
    $('input[name="hday_date_from"]').daterangepicker({
        locale: {
          format: 'DD-MM-YYYY'
        },
        singleDatePicker: true,
        showDropdowns: true
    },
    function(start, end, label) {
    });
});
</script>

<script type="text/javascript">
$(function() {
    $('input[name="hday_date_to"]').daterangepicker({
        locale: {
          format: 'DD-MM-YYYY'
        },
        singleDatePicker: true,
        showDropdowns: true
    },
    function(start, end, label) {
    });
});
</script>
@endpush
