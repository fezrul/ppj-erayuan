@extends('layouts.app')

@section('content')
    <div class="my-5">
        @component('components.panel')
            @slot('title', __("Kemas Kini Pelanggan"))

            @component('components.form', ['action' => route('customer.update',$user->id)])
                {{ method_field('PUT') }}
               
                    <div class="row">
                        <div class="col-md-6">
                                @component('components.fields_equal', ['fields' => [
                                    ['type' => 'hidden', 'name' => 'id','value' => $user->id],
                                    ['label' => __("Nama"), 'name' => 'name', 'value' => $user->name],
                                    ['label' => __('Kewarganegaraan'), 'type' => 'radio', 'name' => 'user_citizenship', 'options' => $citizenship, 'value' => $user->profile->user_citizenship, 'id' => 'citizenshipSelector'],
                                    ['type' => 'text', 'label' => __("No. Kad Pengenalan"), 'name' => 'icno', 'value' => $user->icno,'help' => 'Tanpa tanda "-"', 'attributes' => ['data-citizenship' => \Ekompaun\Systemconfig\Enum\Citizenship::RESIDENT]],
                                    ['label' => __("No Pasport"), 'name' => 'icno', 'value' => $user->icno,'help' => 'Tanpa tanda "-"', 'attributes' => ['data-citizenship' => \Ekompaun\Systemconfig\Enum\Citizenship::NON_RESIDENT]],
                                    ['type' => 'radio', 'options' => $categories, 'label' => __("Kategori"), 'name' => 'user_category', 'value' => $user->profile->user_category],
                                    ['label' => __("Syarikat SSM No"), 'name' => 'user_companyssmno', 'value' => $user->profile->user_companyssmno],
                                    ['label' => __("No Premis"), 'name' => 'user_premisno', 'value' => $user->profile->user_premisno],
                                    ['label' => __("No Kenderaan"), 'name' => 'user_vehicleno1','value' => $user->profile->user_vehicleno1],
                                    ['label' => '', 'name' => 'user_vehicleno2','value' => $user->profile->user_vehicleno2],
                                    ['label' => '', 'name' => 'user_vehicleno3','value' => $user->profile->user_vehicleno3],
                                ]])
                                @endcomponent
                        </div>
                        <div class="col-md-6">
                                @component('components.fields_equal', ['fields' => [
                                    ['label' => __("Emel"), 'name' => 'email','value' => $user->email],
                                    ['type' => 'number', 'label' => __("No. Telefon Bimbit"), 'name' => 'user_mobileno','value' => $user->profile->user_mobileno],
                                    ['type' => 'number', 'label' => __("No. Telefon Rumah"), 'name' => 'user_phoneno','value' => $user->profile->user_phoneno],
                                    ['type' => 'number', 'label' => __("No. Telefon Pejabat"), 'name' => 'user_officeno','value' => $user->profile->user_officeno],
                                    ['label' => __("Alamat"), 'name' => 'user_address1','value' => $user->profile->user_address1],
                                    ['label' => '', 'name' => 'user_address2','value' => $user->profile->user_address2],
                                    ['label' => '', 'name' => 'user_address3','value' => $user->profile->user_address3],
                                    ['type' => 'number', 'label' => __("Poskod"), 'name' => 'user_postcode','value' => $user->profile->user_postcode],
                                    ['type' => 'select', 'options' => $states, 'label' => __("Negeri"), 'name' => 'fk_lkp_state', 'value'=>$user->profile->fk_lkp_state, 'attributes' => ['data-citizenship' => \Ekompaun\Systemconfig\Enum\Citizenship::RESIDENT]],
                                    ['type' => 'select', 'options' => $countries, 'label' => __("Negara"), 'name' => 'fk_lkp_country', 'value'=>$user->profile->fk_lkp_country, 'attributes' => ['data-citizenship' => \Ekompaun\Systemconfig\Enum\Citizenship::NON_RESIDENT]],
                                    //['type' => 'select', 'options' => $status, 'label' => __("Status"), 'name' => 'status', 'value'=>$user->status],
                                ]])
                                @endcomponent
                            </div>
                    </div>
            
                </div>
                <div class="panel-footer">
                    <div class="form-group row mx-3 justify-content-center">
                        <button type="submit" class="btn btn-primary">{{ __("Hantar") }}</button>
                        <a href="{{ route('customer.index') }}" class="btn btn-link">
                            {{ __("Kembali") }}
                        </a>
                    </div>
                </div>
            @endcomponent
        @endcomponent
    </div>
@endsection

@push('end')
    <script>
        $(function(){
            $('#citizenshipSelector input:radio').on('change', function(e){
                if($(this).is(':checked')) {
                    var citizenship = $(this).val();

                    $('[data-citizenship]').hide();
                    $('[data-citizenship] .form-control').attr('disabled', 'disabled');

                    $('[data-citizenship='+citizenship+']').show();
                    $('[data-citizenship='+citizenship+'] .form-control').removeAttr('disabled');
                }
            }).trigger('change');
        });
    </script>
@endpush
