@extends('layouts.app')

@section('content')
    <div class="my-5">

        <div class="mb-3">
            <a href="{{ route('customer.index') }}" class="btn btn-sm btn-secondary">{{ __("Kembali") }}</a>
        </div>
        @component('components.panel')
            @slot('title', __("Maklumat Pelanggan"))

            <div class="container-fluid mb-3">
                <div class="row">
                    <div class="col-6">
                        <div class="pr-5">
                            <dl>
                                <dt>{{ __('Nama') }}</dt>
                                <dd>{{ $user->name }}</dd>
                                <dt>{{ __('Kewarganegaraan') }}</dt>
                                <dd>{{ $user->profile->present_citizenship }}</dd>
                                <dt>{{ __('No Kad/Pengenalan') }}</dt>
                                <dd>{{ $user->icno }}</dd>
                                <dt>{{ __('No Pasport') }}</dt>
                                <dd>{{ $user->icno }}</dd>
                                <dt>{{ __('Kategori') }}</dt>
                                <dd>{{ $user->profile->present_category }}</dd>
                                <dt>{{ __('Syarikat SSM No') }}</dt>
                                <dd>{{ $user->profile->user_companyssmno }}</dd>
                                <dt>{{ __('No Premis') }}</dt>
                                <dd>{{ $user->profile->user_premisno }}</dd>
                                <dt>{{ __('No Kenderaan') }}</dt>
                                <dd>{{ $user->profile->user_vehicleno1 or '-' }}</dd>
                                <dd>{{ $user->profile->user_vehicleno2 or '-'  }}</dd>
                                <dd>{{ $user->profile->user_vehicleno3 or '-'  }}</dd>
                            </dl>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="pl-5">
                            <dt>{{ __('Emel') }}</dt>
                            <dd>{{ $user->email }}</dd>
                            <dt>{{ __('No. Telefon Bimbit') }}</dt>
                            <dd>{{ $user->profile->user_mobileno }}</dd>
                            <dt>{{ __('No. Telefon Rumah') }}</dt>
                            <dd>{{ $user->profile->user_phoneno}}</dd>
                            <dt>{{ __('No. Telefon Pejabat') }}</dt>
                            <dd>{{ $user->profile->user_officeno }}</dd>
                            <dt>{{ __('Alamat') }}</dt>
                            <dd>{{ $user->profile->user_address1 }}</dd>
                            <dd>{{ $user->profile->user_address2 }}</dd>
                            <dd>{{ $user->profile->user_address3 }}</dd>
                            <dt>{{ __('Poskod') }}</dt>
                            <dd>{{ $user->profile->user_postcode }}</dd>
                            <dt>{{ __('Negeri') }}</dt>
                            <dd>{{ $user->profile->present_negeri }}</dd>
                            <dt>{{ __('Negara') }}</dt>
                            <dd>{{ $user->profile->present_negara }}</dd>
                            <dt>{{ __('Status') }}</dt>
                            <dd>{{ $user->present_status }}</dd>
                        </div>
                    </div>
                </div>
            </div>
        @endcomponent
    </div>
@endsection
