@extends('layouts.app')
@section('content')

    <div class="my-5">
        <div class="card">
            <div class="card-header">
                
                <div class="card-actions">
                    <a href="{{ route('customer-pdf.index', ['search'=>request('search')]) }}" target="blank"><i class="fa fa-file-pdf-o"></i> {{ __("Print PDF") }}</a>
                    <a href="{{ route('csv.customer', ['search'=>request('search')]) }}" target="blank"><i class="fa fa-file-excel-o"></i> {{ __("Eksport CSV") }}</a>
                </div>
                 <p></p><br>
                 <h5 class="mb-0">{{ __("Pelanggan") }}</h5>
            </div>
            <div class="card-header">
                {{ html()->form('GET', route('customer.index'))->open() }}
                    <div {{ html()->class(['input-group']) }}>
                        {{ html()->input('text', 'search', $search)->placeholder(__("Carian Untuk"))->class(['form-control']) }}
                        <span class="input-group-btn">
                            {{ html()->submit(__("Carian"))->class(['btn btn-secondary']) }}
                        </span>
                    </div>
                {{ html()->form()->close() }}
            </div>
            @if ($user->count() > 0 )
            <table class="table table-striped table-responsive table-bordered mb-0">
                <thead class="thead-light text-center">
                <tr>
                    <th>{{ __("No") }}</th>
                    <th>{{ __("Nama") }}</th>
                    <th>{{ __("Emel") }}</th>
                    <th>{{ __("Kewarganegaraan") }}</th>
                    <th>{{ __("Kategori") }}</th>
                    <th>{{ __("No Kad Pengenalan/No Passport") }}</th>
                    <th>{{ __("Tarikh Daftar") }}</th>
                    <th>{{ __("Status") }}</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                @foreach($user as $u)
                    <td>{{ numbering($u, $user) }}</td>
                    <td>{{ $u->name }}</td>
                    <td>{{ $u->email }}</td>
                    <td>{{ $u->profile->present_citizenship }}</td>
                    <td>{{ $u->profile->present_category }}</td>
                    <td>{{ $u->icno }}</td>
                    <td>{{ $u->created_at->format('j F Y') }}</td>
                    <td>{{ $u->status }}</td>
                    <td>
                        <div class="dropdown">
                            <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ __("Tindakan") }}
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{ route('customer.show', $u->id) }}">{{ __("Maklumat Pelanggan") }}</a>
                                <a class="dropdown-item" href="{{ route('customer.edit', $u->id) }}">{{ __("Kemas Kini") }}</a>
                                <a class="dropdown-item" href="{{ route('customer-password.edit', $u) }}">{{ __("Tukar Kata Laluan") }}</a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                @endforeach
                </tbody>
            </table>
            @else
                @include('components.empty')
            @endif
            <div class="card-footer">
                <!-- {{ $user->links() }} -->
                <?php 

                    if(isset($_GET['search'])) {$par = $_GET['search'];}else{$par = '';}


                ?>
                {!! str_replace('/?', '?', 

                        $user->appends(['search' => "{$par}"])->render()) 
                !!}
            </div>
        </div>
    </div>
@endsection
