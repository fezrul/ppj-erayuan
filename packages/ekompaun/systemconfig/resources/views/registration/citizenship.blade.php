@extends('layouts.auth')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card-group mb-0">
            <div class="card p-4">
                <div class="card-body">
                    <h1>{{ __("Kewarganegaraan") }}</h1>
                    <p class="text-muted">{{ __("Pilih Kewarganegaraan") }}</p>
                    <div class="row">
                        <div {{ html()->class("col-md-6") }}>
                            {{ html()->form('GET', route('registration.create'))->open() }}
                            {{ html()->hidden('citizenship', 1) }}
                            {{ html()->submit(__("Warganegara"))->class(['btn btn-block btn-primary']) }}
                            {{ html()->form()->close() }}
                        </div>
                        <div {{ html()->class("col-md-6") }}>
                            {{ html()->form('GET', route('registration.create'))->open() }}
                            {{ html()->hidden('citizenship', 2) }}
                            {{ html()->submit(__("Bukan Warganegara"))->class(['btn btn-block btn-primary']) }}
                            {{ html()->form()->close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
