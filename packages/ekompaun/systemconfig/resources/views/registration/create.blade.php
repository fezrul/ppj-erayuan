@extends('layouts.auth')
<style>
.uppercase input[type=text] {text-transform:uppercase}
</style>
@section('content')
    <div>
        @component('components.panel')
            @slot('title', 'Daftar Pelanggan Baharu')

            @component('components.form', ['action' => route('registration.store')])
                {{ html()->hidden('citizenship', $citizenship) }}
                <div class="container-fluid mb-3">
                    <div class="row">
                        <div class="col-12">
                            <div class="">
                                @if ($citizenship == 1)
                                    @component('components.fields_equal', ['fields' => [
                                        ['label' => __("Nama"), 'name' => 'name', 'required' => true,'attributes' => ['class' => 'uppercase']],
                                        ['type' => 'radio', 'options' => $category, 'value' => \Ekompaun\Systemconfig\Enum\UserCategory::INDIVIDUAL, 'label' => __("Kategori"), 'name' => 'user_category'],
                                        ['type' => 'text', 'label' => __("No. Kad Pengenalan"), 'name' => 'icno', 'required' => true],
                                        ['label' => __("Syarikat SSM No"), 'name' => 'companyssmno', 'attributes' => ['class' =>'field-for-company uppercase']],
                                        ['label' => __("Premis No"), 'name' => 'premisno', 'attributes' => ['class' => 'field-for-company uppercase']],
                                        ['type' => 'password', 'label' => __("Kata Laluan"), 'name' => 'new_password', 'help' => 'Minimum 8 aksara', 'required' => true],
                                        ['type' => 'password', 'label' => __("Pengesahan Kata Laluan"), 'name' => 'new_password_confirmation', 'help' => 'Minimum 8 aksara', 'required' => true],
                                        ['label' => __("No. Kenderaan"), 'name' => 'user_vehicleno1','attributes' => ['class' => 'uppercase']],
                                        ['label' => '', 'name' => 'user_vehicleno2','attributes' => ['class' => 'uppercase']],
                                        ['label' => '', 'name' => 'user_vehicleno3','attributes' => ['class' => 'uppercase']],
                                    ]])
                                    @endcomponent
                                @elseif($citizenship == 2)
                                    @component('components.fields_equal', ['fields' => [
                                        ['label' => __("Nama"), 'name' => 'name', 'required' => true,'attributes' => ['class' => 'uppercase']],
                                        ['type' => 'radio', 'options' => $category, 'value' => \Ekompaun\Systemconfig\Enum\UserCategory::COMPANY, 'label' => __("Kategori"), 'name' => 'user_category'],
                                        ['type' => 'text', 'label' => __("No Pasport"), 'name' => 'icno','attributes' => ['class' => 'uppercase']],
                                        ['label' => __("Syarikat SSM No"), 'name' => 'companyssmno','attributes' => ['class' => 'field-for-company uppercase']],
                                        ['label' => __("Premis No"), 'name' => 'premisno','attributes' => ['class' => 'field-for-company uppercase']],
                                        ['type' => 'password', 'label' => __("Kata Laluan"), 'name' => 'new_password', 'help' => 'Minimum 8 aksara', 'required' => true],
                                        ['type' => 'password', 'label' => __("Pengesahan kata Laluan"), 'name' => 'new_password_confirmation', 'help' => 'Minimum 8 aksara', 'required' => true],
                                        ['label' => __("No. Kenderaan"), 'name' => 'user_vehicleno1','attributes' => ['class' => 'uppercase']],
                                        ['label' => '', 'name' => 'user_vehicleno2','attributes' => ['class' => 'uppercase']],
                                        ['label' => '', 'name' => 'user_vehicleno3','attributes' => ['class' => 'uppercase']],
                                    ]])
                                    @endcomponent
                                @endif
                            
                                @if ($citizenship == 1)
                                    @component('components.fields_equal', ['fields' => [
                                        ['label' => __("Emel"), 'name' => 'email', 'required' => true],
                                        ['type' => 'number', 'label' => __("No. Telefon Bimbit"), 'name' => 'user_mobileno', 'required' => true],
                                        ['type' => 'number', 'label' => __("No. Telefon Rumah"), 'name' => 'user_phoneno'],
                                        ['type' => 'number', 'label' => __("No. Telefon Pejabat"), 'name' => 'user_officeno'],
                                        ['label' => __("Alamat"), 'name' => 'user_address1', 'required' => true,'attributes' => ['class' => 'uppercase']],
                                        ['label' => '', 'name' => 'user_address2','attributes' => ['class' => 'uppercase']],
                                        ['label' => '', 'name' => 'user_address3','attributes' => ['class' => 'uppercase']],
                                        ['type' => 'number', 'label' => __("Poskod"), 'name' => 'user_postcode', 'required' => true],
                                        ['type' => 'select', 'options' => $states, 'label' => __("Negeri"), 'name' => 'fk_lkp_state', 'required' => true],
                                    ]])
                                    @endcomponent
                                @elseif ($citizenship == 2)
                                    @component('components.fields_equal', ['fields' => [
                                        ['label' => __("Emel"), 'name' => 'email', 'required' => true],
                                        ['type' => 'number', 'label' => __("No. Telefon Bimbit"), 'name' => 'user_mobileno', 'required' => true],
                                        ['type' => 'number', 'label' => __("No. Telefon Rumah"), 'name' => 'user_phoneno'],
                                        ['type' => 'number', 'label' => __("No. Telefon Pejabat"), 'name' => 'user_officeno'],
                                        ['label' => __("Alamat"), 'name' => 'user_address1', 'required' => true,'attributes' => ['class' => 'uppercase']],
                                        ['label' => '', 'name' => 'user_address2','attributes' => ['class' => 'uppercase']],
                                        ['label' => '', 'name' => 'user_address3','attributes' => ['class' => 'uppercase']],
                                        ['type' => 'number', 'label' => __("Poskod"), 'name' => 'user_postcode', 'required' => true],
                                        ['type' => 'select', 'options' => $countries, 'label' => __("Negara"), 'name' => 'fk_lkp_country', 'required' => true],
                                    ]])
                                    @endcomponent
                                @endif

                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group row mx-3 justify-content-center">
                    <button type="submit" class="btn btn-primary">{{ __("Hantar") }}</button>
                    <a href="{{ route('login') }}" class="btn btn-link">{{ __("Kembali") }}</a>
                </div>

            @endcomponent

        @endcomponent
    </div>
@endsection('content')

@push('end')
   <script>
        $(function(){
           $('input[name=user_category]').on('change', function(e){
               if ($(this).is(':checked')) {
                   $('.field-for-company').hide();
                   var category = $(this).val();

                   if(category == {{ \Ekompaun\Systemconfig\Enum\UserCategory::COMPANY }}) {
                       $('.field-for-company').show();
                   }
               }
           }).trigger('change');
        });
    </script>
@endpush
