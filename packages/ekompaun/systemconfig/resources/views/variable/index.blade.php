@extends('layouts.app')
@section('content')

    <div class="my-5">
        <div class="card" style="overflow-x: auto;">
            <div class="card-header">
                <h5 class="mb-0">{{ __("Konfigurasi Sistem") }}</h5>
            </div>
            @if ($customs->count() > 0 )
            <table class="table table-striped mb-0">
                <thead class="thead-light">
                <tr>
                    <th>{{ __("No") }}</th>
                    <th>{{ __("Kod Tersuai") }}</th>
                    <th>{{ __("Nama") }}</th>
                    <th>{{ __("Tempoh") }}</th>
                    <th>{{ __("Status") }}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                @foreach($customs as $c)
                    <td>{{ numbering($c, $customs) }}</td>
                    <td>{{ $c->custom_code }}</td>
                    <td>{{ $c->custom_name }}</td>
                    <td>{{ $c->custom_period }}</td>
                    @if($c->custom_status === 2)
                        <td>{{ $c->custom_status = __("Aktif") }}</td>
                    @else
                        <td>{{ $c->custom_status = __("Tidak Aktif") }}</td>
                    @endif
                </tr>
                <tr>
                @endforeach
                </tbody>
            </table>
            @else
                @include('components.empty')
            @endif
            <div class="card-footer">
            </div>
        </div>
    </div>
</script>
@endsection
