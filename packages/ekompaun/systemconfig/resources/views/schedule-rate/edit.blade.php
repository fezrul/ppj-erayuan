@extends('layouts.app')

@section('content')
<div class="container-fluid mt-5">
    <div class="card">
        <div class="card-header">
            <h5 class="mb-0">{{ __("Kemas Kini Jadual Kadar Rayuan") }}</h5>
        </div>
        <div class="card-body">
            {{ html()->form('PUT', route('schedule-rate.update', $scheduleRate->getKey()))->open() }}
            {{ html()->formGroup(__("Kesalahan"), html()->select('fk_lkp_offence', $offences, $scheduleRate->fk_lkp_offence)->attribute('disabled')->class(['form-control'])) }}
            {{ html()->formGroup(__("Tempoh masa (Hari)"), html()->select('fk_lkp_period', $periods, $scheduleRate->fk_lkp_period)->attribute('disabled')->class(['form-control'])) }}
            {{ html()->formGroup(__("Jenis Kenderaan"), html()->select('fk_lkp_vehicle', $vehicles, $scheduleRate->fk_lkp_vehicle)->attribute('disabled')->class(['form-control'])) }}
            {{ html()->formGroup(__("Pengguna"), html()->select('schd_usercategory', $vehiclesType, $scheduleRate->schd_usercategory)->attribute('disabled')->class(['form-control'])) }}
            {{ html()->formGroup(__("Jumlah"), html()->input('text', 'schd_amount', $scheduleRate->schd_amount)->placeholder(__("Jumlah"))->class(['form-control'])) }}
        </div>
        <div class="card-footer">
            {{ html()->submit(__("Hantar"))->class(['btn btn-primary']) }}
            {{ html()->a(route('schedule-rate.index'),__("Batal"))->class(['btn btn-link']) }}
        </div>
            {{ html()->form()->close() }}
    </div>
</div>
@endsection
