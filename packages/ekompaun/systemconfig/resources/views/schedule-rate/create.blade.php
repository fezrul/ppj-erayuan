@extends('layouts.app')

@section('content')
<div class="container-fluid mt-5">
    <div class="card">
        <div class="card-header">
            <h5 class="mb-0">{{ __("Tambah Jadual Kadar Rayuan") }}</h5>
        </div>
        <div class="card-body">
            {{ html()->form('POST', route('schedule-rate.store'))->open() }}
            {{ html()->formGroup(__("Akta"), html()->select('akta', $acts, request('akta'))->class(['form-control'])) }}
            {{ html()->formGroup(__("Kesalahan"), html()->selectKesalahan('fk_lkp_offence', $offences, request('fk_lkp_offence'))->class(['form-control'])) }}
            {{ html()->formGroup(__("Tempoh masa (Hari)"), html()->select('fk_lkp_period', $periods, request('fk_lkp_period'))->class(['form-control'])) }}
            {{ html()->formGroup(__("Jenis Kenderaan"), html()->select('fk_lkp_vehicle', $vehicles, request('fk_lkp_vehicle'))->class(['form-control'])) }}
            {{ html()->formGroup(__("Pengguna"), html()->select('schd_usercategory', $vehiclesType, request('schd_usercategory'))->class(['form-control'])) }}
            {{ html()->formGroup(__("Jumlah"), html()->input('text', 'schd_amount')->placeholder(__("Jumlah"))->class(['form-control'])) }}
        </div>
        <div class="card-footer">
            {{ html()->submit(__("Hantar"))->class(['btn btn-primary']) }}
            {{ html()->a(route('schedule-rate.index'),__("Batal"))->class(['btn btn-link']) }}
        </div>
            {{ html()->form()->close() }}
    </div>
</div>
@endsection

@push('end')
    <script type="text/javascript">
        $(document).ready(function() {
            $('select[name="akta"]').on('change', function() {
                var aktaID = $(this).val();
                var kesalahan = $('select[name="fk_lkp_offence"]');
                var firstValue = kesalahan.find('option[data-act='+aktaID+']:first').attr('value');
                if (firstValue) {
                    kesalahan.val(firstValue);
                    kesalahan.prop('disabled', false);
                } else {
                    kesalahan.val('').prop('disabled', true);
                }
                kesalahan.find('option[data-act!='+aktaID+']').hide();
                kesalahan.find('option[data-act='+aktaID+']').show();
            }).change();
        });
    </script>
@endpush

