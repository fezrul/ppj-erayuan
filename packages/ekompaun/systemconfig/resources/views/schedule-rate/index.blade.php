@extends('layouts.app')
@section('content')
    <div class="my-5">
        <div class="card">
            <div class="card-header">
                
                <div class="card-actions">
                    <a style="margin-left: -15px !important;" href="{{ route('schedule-rate.create') }}"><i style="margin-left: -15px !important;" class="fa fa-plus"></i> {{ __("Tambah") }}</a>
                    <a style="margin-left: -15px !important;" href="{{ route('schedule-rate-pdf.index', ['act'=>request('act'), 'search'=>request('search')]) }}" target="blank"><i style="margin-left: -15px !important;" class="fa fa-file-pdf-o"></i> {{ __("Print PDF") }}</a>
                    <a style="margin-left: -15px !important;" href="{{ route('csv.schedule-rate', ['act'=>request('act'), 'search'=>request('search')]) }}" target="blank"><i style="margin-left: -15px !important;" class="fa fa-file-excel-o"></i> {{ __("Eksport CSV") }}</a>
                </div>
                <p></p><br>
                <h5 class="mb-0">{{ __("Kadar Rayuan Kompaun") }}</h5>
            </div>

            <div class="card-header">
                {{ html()->form('GET', route('schedule-rate.index'))->open() }}
                    <div class="row">
                        <div {{ html()->class(['col-md-4']) }}>
                            {{ html()->select('act', $acts, request('act'))->class(['form-control']) }}
                        </div>
                        <div {{ html()->class(['col-md-4']) }}>
                            {{ html()->select('type', $type, request('type'))->class(['form-control']) }}
                        </div>
                        <div {{ html()->class(['col-md-3']) }}>
                            {{ html()->input('text', 'search', request('search'))->placeholder(__("Carian Untuk"))->class(['form-control']) }}
                        </div>
                        <div {{ html()->class(['col-md-1']) }}>
                            <div class="btn-group pull-right" role="group" aria-label="Basic example">
                              {{ html()->submit(__("Carian"))->class(['btn btn-secondary']) }}
                            </div>
                        </div>
                    </div>
                {{ html()->form()->close() }}
            </div>

            <table class="table table-responsive table-bordered mb-0 table-responsive">
                <thead class="thead-light text-center">
                <tr>
                    <th colspan="3">Tempoh masa (Hari)</th>
                    @foreach($periods as $period)
                        <th colspan="{{ count($vehiclesType) }}">{{ $period->period_name }}</th>
                    @endforeach
                </tr>
                <tr>
                    <th>{{ __('Kod') }}</th>
                    <th>{{ __('Kesalahan') }}</th>
                    <th></th>
                    @foreach($periods as $period)
                        @foreach($vehiclesType as $type)
                            @if($actType == \Ekompaun\Systemconfig\Enum\ActType::VEHICLE)
                                <th>{{ ucfirst($type->vehicle_name) }}</th>
                            @elseif($actType == \Ekompaun\Systemconfig\Enum\ActType::USER_CATEGORY)
                                <th>{{ ucfirst(strtolower($type)) }}</th>
                            @endif
                        @endforeach
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach($offences as $offence)
                    <tr>
                        <td>{{ $offence->offence_code?:$loop->iteration}}</td>
                        <td>{{ $offence->text }}</td>
                        <td>
                            <div class="dropdown">
                            <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ __("Tindakan") }}
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{route('offence.edit',$offence->offence_id)}}">{{ __("Kemas Kini") }}</a>
                                {{ html()->form('DELETE', route('offence.destroy',$offence->offence_id))->open() }}
                                {{ html()->submit(__("Padam") )->class(['dropdown-item padam']) }}
                                {{ html()->form()->close() }}
                            </div>
                        </div>
                        </td>
                        @foreach($periods as $period)
                            @if($actType == \Ekompaun\Systemconfig\Enum\ActType::VEHICLE)
                                @foreach($vehiclesType as $type)
                                    <td>{{ $offence->getAmount($period->getKey(), $type->getKey()) }}
                                    @if ($offence->getAmount($period->getKey(), $type->getKey()) != null)
                                    <a href="{{ route('schedule-rate.edit', $offence->getId($period->getKey(), $type->getKey())) }}"><i class="fa fa-edit"></i>
                                    </a>
                                    {{ html()->form("DELETE", route('schedule-rate.destroy',$offence->getId($period->getKey(), $type->getKey())))->open() }}
                                    <a href="#" class="padam"><i class="fa fa-trash"></i></a>
                                    {{ html()->form()->close() }}
                                    @endif
                                    </td>
                                @endforeach
                            @elseif($actType == \Ekompaun\Systemconfig\Enum\ActType::USER_CATEGORY)
                                @foreach($vehiclesType as $key=>$value)
                                    <td>{{ $offence->getAmount($period->getKey(), $key) }}
                                    @if ($offence->getAmount($period->getKey(), $key) != null)
                                    <a href="{{ route('schedule-rate.edit', $offence->getId($period->getKey(), $key)) }}"><i class="fa fa-edit"></i>
                                    </a>
                                    {{ html()->form("DELETE", route('schedule-rate.destroy',$offence->getId($period->getKey(), $key)))->open() }}
                                    <a href="#" class="padam"><i class="fa fa-trash"></i></a>
                                    {{ html()->form()->close() }}
                                    @endif
                                    </td>
                                @endforeach
                            @endif
                        @endforeach
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="card-footer">
                {{ $offences->links() }}
            </div>

        </div>
    </div>
    @include('components.modal-delete-confirmation', ['label' => __('Padam data')])
@endsection

@push('end')
<script>
    $(function(){
       var modal = $("#modal-delete-confirmation");
       var submit = $("#submit");
       var padam = $(".padam");
       padam.click(function (e) {
            var form = $(this).closest('form');
            e.preventDefault();
            modal.modal('show');

            submit.click(function () {
                form.submit();
            });
       })
    });
</script>
@endpush
