{{ __("Tuan/Puan") }}
<br>
<br>

{{ __("Tahniah, anda telah berjaya mendaftar :application dengan butiran berikut :application", ['application' => config('site.name')]) }}
<br>{{ __("ID Pengguna: "). $user->profile->user_referenceid ?? '' }}
<br>{{ __("Kata Laluan: "). $password }}

<br>
<br>
{{ __("Sila klik pautan untuk log masuk ke dalam sistem :application", ['application' => config('site.name')]) }}.
<br>{{ route('auth::login', ['id' => $user->id ?? '']) }}

<br>
<br>
<br>


<hr>
Sir/Madam,
<br>
<br>

Congratulation, you have successfully registered to {{ config('site.name') }} system with details :
<br>Username: {{ $user->id ?? '' }}
<br>Password: {{ $password }}

<br>
<br>
Please click link below to log in to {{ config('site.name') }} system.
<br>{{ route('auth::login', ['id' => $user->id ?? '']) }}
