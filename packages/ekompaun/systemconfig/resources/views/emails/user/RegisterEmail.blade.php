@extends('layouts.mail.master')
@section('content')

<table>
	<tbody>
		<tr>
			<td><center>
				<h3>MAKLUMAN AKAUN </h3>
			</center>
			<div class="divider hidden"></div>
			<div class="divider"></div>
			<div class="divider hidden"></div>
			<div class="detail"><b>Tuan/Puan,</b>
				<div class="divider hidden"></div>
				<p>Adalah dengan ini dimaklumkan bahawa pendaftaran akaun anda telah berjaya<br>Sila log masuk ke sistem dengan menggunakan maklumat :</p>
				<div class="divider hidden"></div>
				<p>
					<table class="content-table">
						<tbody>
							<tr>
								<td>Nama Pengguna / Username</td>
								<td>{{$username}}</td>
							</tr>
							<tr>
								<td>Kata laluan / Password</td>
								<td>{{$password}}</td>
							</tr>
						</tbody>	
					</table>
				</p>

				<div class="divider hidden"></div>
				<p>Sila log masuk di {{ route('login') }}</p>

					<br /> <br />
					<p><b>Sekian terima kasih.</b></p>

					<br /> <br />
					<p><b>E-Rayuan Perbadanan Putrajaya</b></p>
				</div>
			</td>
		</tr>
	</tbody>
</table>

@endsection