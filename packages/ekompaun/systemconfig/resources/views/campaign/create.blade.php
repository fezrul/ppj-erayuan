@extends('layouts.app')

@push('head')
<link rel="stylesheet" type="text/css" href="{{ asset('lib/bootstrap-daterangepicker/daterangepicker.css') }}" />
<style>
    .file-upload{display:block;text-align:center;cursor:pointer;margin-bottom: 5px;}
    .file-upload .file-select{cursor:pointer;display:block;border: 1px solid #bdbbb5; border-radius: 0.25rem; color: #34495e;height:40px;line-height:40px;text-align:left;background:#FFFFFF;overflow:hidden;position:relative;}
    .file-upload .file-select .file-select-name{cursor:pointer;line-height:40px; overflow:hidden; white-space: nowrap; text-overflow: ellipsis; display:block;padding:0 10px;}
    .file-upload.active .file-select{border-color:#3fa46a;background-color:#3fa46a;color:#ffffff;transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;}
    .file-upload .file-select input[type=file]{z-index:100;cursor:pointer;position:absolute;height:100%;width:100%;top:0;left:0;opacity:0;filter:alpha(opacity=0);}
    .file-upload .file-select.file-select-disabled:hover .file-select-name{line-height:40px;display:inline-block;padding:0 10px;}
</style>
@endpush

@section('content')
<div class="my-5">
    <div class="card" style="overflow-x: auto;">
        <div class="card-header">
            <h5 class="mb-0">{{ __("Tambah kempen") }}</h5>
        </div>

        <div class="card-body">
            {{ html()->form('POST', route('campaign.store'))->acceptsFiles()->open() }}
            <div {{ html()->class(['row']) }}>
                <div {{ html()->class(['col-sm-6']) }}>
                    {{ html()->formGroup(__("Nama Kempen"), html()->input('text', 'camp_name')->placeholder(__("Nama"))) }}
                    {{ html()->formGroup(__("Tarikh Dari"), html()->input('text', 'camp_startdate')) }}
                    {{ html()->formGroup(__("Tarikh Hingga"), html()->input('text', 'camp_enddate')) }}
                    {{ html()->formGroup(__("Jenis Kadar"), html()->radioGroup('camp_ratetype', $campaignRates)) }}
                </div>
                <div {{ html()->class(['col-sm-6']) }}>
                    {{ html()->formGroup(__("Info"), html()->textarea('camp_content')->placeholder(__("Info")) ) }}
                    <div {{ html()->class(['row']) }}>
                        <div {{ html()->class(['col-sm-4']) }}>{{ __("Gambar") }}</div>
                        <div {{ html()->class(['col-sm-8']) }}>
                            <div class="file-upload">
                                <div class="file-select">
                                    <div class="file-select-name"><i class="fa fa-upload"></i> {{ __('Muatnaik') }}</div>
                                    {{ html()->file('picture')->acceptImage()->addClass('form-control') }}
                                </div>
                            </div>

                            <small class="form-text text-muted">
                                {{ __("Format fail png, jpg, jpeg sahaja, Max 2 MB") }}
                            </small>
                        </div>
                    </div>
                    <div {{ html()->class(['row']) }}>
                        <div {{ html()->class(['col-sm-4']) }}></div>
                        <div {{ html()->class(['col-sm-8']) }}>
                            <img id="prev_picture" src="{{ asset('img/no_image.jpg') }}" class="img-thumbnail img-responsive" width="50%">
                        </div>
                    </div>
                </div>
            </div>

            <table class="table table-striped table-bordered">
                <thead class="thead-light text-center">
                    <tr>
                        <th rowspan="2" style="width: 100px">{{ __("Tahun Dari") }}</th>
                        <th rowspan="2" style="width: 100px">{{ __("Tahun Hingga") }}</th>
                        <th rowspan="2">{{ __("Jenis Kesilapan") }}</th>
                        <th colspan="{{ count($campaignTypes) }}">{{ __("Bayaran/Diskaun") }}</th>
                    </tr>
                    <tr>
                        @foreach($campaignTypes as $type)
                        <th style="width: 150px">{{ $type }}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach(range(0, 5) as $i)
                    <tr>
                        <td><input type="number" name="details[{{ $i }}][cdet_offencedate_from]" class="form-control"  value="{{ old("details.$i.cdet_offencedate_from") }}" min="1900" max="9999" style="width: 80px"></td>
                        <td><input type="number" name="details[{{ $i }}][cdet_offencedate_to]" class="form-control"  value="{{ old("details.$i.cdet_offencedate_to") }}" min="1900" max="9999" style="width: 80px"></td>
                        <td>@include('components.jenis-kesalahan.control', ['name' => "details[$i][cdet_offenceid]", 'value' => explode(',', old("details.$i.cdet_offenceid", ''))])</td>
                        @foreach($campaignTypes as $key => $value)
                        <td><input type="number" name="details[{{ $i }}][cdet_amount][{{ $key }}]" class="form-control" value="{{ old("details.$i.cdet_amount.$key") }}"></td>
                        @endforeach
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div {{ html()->class(['form-group text-center']) }}>
                {{ html()->submit( __("Hantar") )->class(['btn btn-primary']) }}
                {{ html()->a(route('campaign.index'),__("Batal"))->class(['btn btn-link']) }}
            </div>
            {{ html()->form()->close() }}
        </div>
    </div>
</div>
@endsection('content')

@push('end')
<script type="text/javascript" src="{{ asset('lib/bootstrap-daterangepicker/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('lib/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

<script type="text/javascript">
    $(function() {
        $('input[name="camp_startdate"]').daterangepicker({
            timePicker: true,
            timePicker24Hour: true,
            locale: {
                format: 'DD-MM-YYYY H:mm'
            },
            singleDatePicker: true,
            showDropdowns: true
        },
        function(start, end, label) {
        });
    });
</script>

<script type="text/javascript">
    $(function() {
        $('input[name="camp_enddate"]').daterangepicker({
            timePicker: true,
            timePicker24Hour: true,
            locale: {
                format: 'DD-MM-YYYY H:mm'
            },
            singleDatePicker: true,
            showDropdowns: true
        },
        function(start, end, label) {
        });
    });

    function readURL(input, id) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $(id).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $('[name="picture"]').change(function(){
        readURL(this, '#prev_picture');
    });
</script>


@endpush
