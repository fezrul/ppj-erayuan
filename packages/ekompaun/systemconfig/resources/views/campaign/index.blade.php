@extends('layouts.app')
@section('content')

    <div class="my-5">
        <div class="card" style="overflow-x: auto;">
            <div class="card-header">
                
                <div class="card-actions">
                    <a style="margin-left: -15px !important;" href="{{ route('campaign.create') }}"><i style="margin-left: -15px !important;" class="fa fa-plus"></i> {{ __("Tambah") }}</a>
                    <a style="margin-left: -15px !important;" href="{{ route('campaign-pdf.index') }}" target="blank"><i style="margin-left: -15px !important;" class="fa fa-file-pdf-o"></i> {{ __("Print PDF") }}</a>
                    <a style="margin-left: -15px !important;" href="{{ route('csv.campaign') }}" target="blank"><i style="margin-left: -15px !important;" class="fa fa-file-excel-o"></i> {{ __("Eksport CSV") }}</a>
                </div>
                <p></p><br>
                <h5 class="mb-0">{{ __("Senarai Kempen") }}</h5>

            </div>
            @if ($campaign->count() > 0 )
            <table class="table table-striped mb-0 table-responsive">
                <thead class="thead-light">
                <tr>
                    <th>{{ __("No") }}</th>
                    <th>{{ __("Nama Kempen") }}</th>
                    <th>{{ __("Tarikh Kempen") }}</th>
                    <th>{{ __("Jenis Kadar") }}</th>
                    <th>{{ __("Status") }}</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                @foreach($campaign as $c)
                    <td>{{ ++$no }}</td>
                    <td>{{ $c->camp_name }}</td>
                    <td>{{ $c->camp_startdate }} - {{ $c->camp_enddate }}</td>
                    @if($c->camp_ratetype === 1)
                        <td>{{ $c->camp_ratetype = __("Kadar Tetap") }}</td>
                    @else
                        <td>{{ $c->camp_ratetype = __("Diskaun") }}</td>
                    @endif
                    @if ($c->is_active)
                        <td>{{ __("Aktif") }}</td>
                    @else
                        <td>{{ __("Tidak Aktif") }}</td>
                    @endif
                    <td>
                        <div class="dropdown">
                            <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ __("Tindakan") }}
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{route('campaign.edit',$c->camp_id)}}">{{ __("Kemas Kini") }}</a>
                                {{ html()->form('DELETE', route('campaign.destroy',$c->camp_id))->open() }}
                                {{ html()->submit(__("Padam") )->class(['dropdown-item padam']) }}
                                {{ html()->form()->close() }}
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                @endforeach
                </tbody>
            </table>
            @else
                @include('components.empty')
            @endif
            <div class="card-footer">
                {{ $campaign->links() }}
            </div>
        </div>
    </div>
    @include('components.modal-delete-confirmation', ['label' => __('Padam data Kempen')])
@endsection

@push('end')
<script>
    $(function(){
       var modal = $("#modal-delete-confirmation");
       var submit = $("#submit");
       var padam = $(".padam");
       padam.click(function (e) {
            var form = $(this).closest('form');
            e.preventDefault();
            modal.modal('show');

            submit.click(function () {
                form.submit();
            });
       })
    });
</script>
@endpush
