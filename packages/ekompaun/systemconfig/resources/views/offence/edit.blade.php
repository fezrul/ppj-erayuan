@extends('layouts.app')
@section('content')
<div class="container-fluid mt-5">
    <div class="card">
        <div class="card-header">
            <h5 class="mb-0">{{ __("Pinda Kesalahan") }}</h5>
        </div>

        <div class="card-body">
            {{ html()->form('PUT', route('offence.update', $offence->offence_id))->open() }}
            {{ html()->formGroup(__("Kesalahan"), html()->input('text', 'offence_name', $offence->offence_name)->class(['form-control'])) }}
        </div>
        <div class="card-footer">
            {{ html()->submit(__("Hantar"))->class(['btn btn-primary']) }}
            {{ html()->a(route('schedule-rate.index'), __("Batal"))->class(['btn btn-link']) }}
        </div>
            {{ html()->form()->close() }}
    </div>
</div>
@endsection
