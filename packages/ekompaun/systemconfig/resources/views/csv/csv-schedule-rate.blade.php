<!DOCTYPE html>
<html>
<body>
	<table>
        <thead>
        <tr>
            <th>Tempoh masa (Hari)</th>
            @foreach($periods as $period)
                <th colspan="{{ count($vehiclesType) }}">{{ $period->period_name }}</th>
            @endforeach
        </tr>
        <tr>
            <th>Seksyen</th>
            @foreach($periods as $period)
                @foreach($vehiclesType as $type)
                    @if($actType == \Ekompaun\Systemconfig\Enum\ActType::VEHICLE)
                        <th>{{ ucfirst($type->vehicle_name) }}</th>
                    @elseif($actType == \Ekompaun\Systemconfig\Enum\ActType::USER_CATEGORY)
                        <th>{{ ucfirst(strtolower($type)) }}</th>
                    @endif
                @endforeach
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach($offences as $offence)
            <tr>
                <td>{{ $offence->text }}</td>
                @foreach($periods as $period)
                    @if($actType == \Ekompaun\Systemconfig\Enum\ActType::VEHICLE)
                        @foreach($vehiclesType as $type)
                            <td>{{ $offence->getAmount($period->getKey(), $type->getKey()) }}</td>
                        @endforeach
                    @elseif($actType == \Ekompaun\Systemconfig\Enum\ActType::USER_CATEGORY)
                        @foreach($vehiclesType as $key=>$value)
                            <td>{{ $offence->getAmount($period->getKey(), $key) }}</td>
                        @endforeach
                    @endif
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
</body>
</html>