<!DOCTYPE html>
<html>
<body>
	<table>
        <thead>
        <tr>
            <th>{{ __("No") }}</th>
            <th>{{ __("Nama Kempen") }}</th>
            <th>{{ __("Tarikh Kempen") }}</th>
            <th>{{ __("Jenis Kadar") }}</th>
            <th>{{ __("Status") }}</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        @foreach($campaigns as $campaign)
            <td>{{ $loop->iteration }}</td>
            <td>{{ $campaign->camp_name }}</td>
            <td>{{ $campaign->camp_startdate }} - {{ $campaign->camp_enddate }}</td>
            @if($campaign->camp_ratetype === 1)
                <td>{{ $campaign->camp_ratetype = __("Kadar Tetap") }}</td>
            @else
                <td>{{ $campaign->camp_ratetype = __("Diskaun") }}</td>
            @endif
            @if ($campaign->is_active)
                <td>{{ __("Aktif") }}</td>
            @else
                <td>{{ __("Tidak Aktif") }}</td>
            @endif
        </tr>
        <tr>
        @endforeach
        </tbody>
    </table>
</body>
</html>
