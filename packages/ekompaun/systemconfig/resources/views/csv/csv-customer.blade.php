<!DOCTYPE html>
<html>
<body>
	<table>
        <thead>
        <tr>
            <th>{{ __("No") }}</th>
            <th>{{ __("Nama") }}</th>
            <th>{{ __("Emel") }}</th>
            <th>{{ __("Kewarganegaraan") }}</th>
            <th>{{ __("Kategori") }}</th>
            <th>{{ __("No Kad Pengenalan/No Passport") }}</th>
            <th>{{ __("Tarikh Daftar") }}</th>
            <th>{{ __("Status") }}</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        @foreach($users as $user)
            <td>{{ $loop->iteration }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->profile->present_citizenship }}</td>
            <td>{{ $user->profile->present_category }}</td>
            <td>{{ $user->icno }}</td>
            <td>{{ $user->created_at->format('j F Y') }}</td>
            <td>{{ $user->status }}</td>
        </tr>
        <tr>
        @endforeach
        </tbody>
    </table>
</body>
</html>