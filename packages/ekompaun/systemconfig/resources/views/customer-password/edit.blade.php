@extends('layouts.app')

@section('content')
<div class="my-5">
    @component('components.form', ['action' => route('customer-password.update', $user)])
        @component('components.panel')
            @slot('title', 'Tukar Kata Laluan')

            <div class="p-5">
                {{ method_field('PUT') }}
                @component('components.fields', ['fields' => [
                    ['type' => 'password', 'label' => __("Kata Laluan Baharu"), 'name' => 'new_password', 'help' => __("Minimum lapan aksara")],
                    ['type' => 'password', 'label' => __("Pengesahan Kata Laluan Baharu"), 'name' => 'new_password_confirmation', 'help' => __("Minimum lapan aksara")],
                ]])
                @endcomponent
            </div>
            <div class="panel-footer">
                <div class="form-group row mx-3 justify-content-center">
                    <button type="submit" class="btn btn-primary">{{ __("Hantar") }}</button>
                    <a href="{{ route('customer.index') }}" class="btn btn-link">
                        {{ __("Kembali") }}
                    </a>
                </div>
            </div>
        @endcomponent
    @endcomponent
</div>
@endsection('content')
