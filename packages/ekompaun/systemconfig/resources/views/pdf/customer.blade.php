@extends('layouts.pdf.master')

@section('title', 'Customer')
@section('content')

    <div style="font-family:Arial; font-size:12px;">
       <div class="col-md-3">
           <img src="img/logo-ppj.png" height="100px">
      </div>
      <div class="col-md-9">
            <h2 style="text-align: center">{{ __("Pelanggan") }}</h2>
      </div>
  </div>
  <br>
  <br>
  <table class="tg">
    <tr>
        <th class="tg-3wr7" width="1">{{ __("No") }}<br></th>
        <th class="tg-3wr7">{{ __("Nama") }}<br></th>
        <th class="tg-3wr7">{{ __("Emel") }}<br></th>
        <th class="tg-3wr7">{{ __("Kewarganegaraan") }}<br></th>
        <th class="tg-3wr7">{{ __("Kategori") }}<br></th>
        <th class="tg-3wr7">{{ __("No Kad Pengenalan/No Passport") }}<br></th>
        <th class="tg-3wr7">{{ __("Tarikh Daftar") }}<br></th>
        <th class="tg-3wr7">{{ __("Status") }}<br></th>
    </tr>
    @if ($user->isNotEmpty() )
    @foreach($user as $u)
    <tr>
        <td class="tg-rv4w" width="1">{{ $loop->iteration }}</td>
        <td class="tg-ti5e">{{ $u->name }}</td>
        <td class="tg-ti5e">{{ $u->email }}</td>
        <td class="tg-rv4w">{{ $u->profile->present_citizenship }}</td>
        <td class="tg-rv4w">{{ $u->profile->present_category }}</td>
        <td class="tg-ti5e">{{ $u->icno }}</td>
        <td class="tg-rv4w">{{ $u->created_at->format('j F Y') }}</td>
        <td class="tg-rv4w">{{ $u->status }}</td>
    </tr>
    @endforeach
    @else
    <tr>
      <td colspan='8' class="tg-rv4w">{{ __("Maaf, tiada data tersedia") }}</td>
    </tr>
    @endif
  </table>
@endsection