@extends('layouts.pdf.master')

@section('title', 'Kadar Rayuan Kompaun')
@section('content')
  <div style="font-family:Arial; font-size:10px;">
      <h2 style="text-align: center">{{ __("Kadar Rayuan Kompaun") }}</h2>
  </div>
  <br>
  <table class="tg" cellpadding="0" cellspacing="0">
    <tr>
        @if($actType == \Ekompaun\Systemconfig\Enum\ActType::USER_CATEGORY)
            <th style="font-size:11px;">Tempoh masa (Hari)</th>
        @else
            <th style="font-size:6px;">Tempoh masa (Hari)</th>
        @endif
        @foreach($periods as $period)
        @if($actType == \Ekompaun\Systemconfig\Enum\ActType::USER_CATEGORY)
            <th style="font-size:11px;" colspan="{{ count($vehiclesType) }}">{{ $period->period_name }}</th>
        @else
            <th style="font-size:6px;" colspan="{{ count($vehiclesType) }}">{{ $period->period_name }}</th>
        @endif
        @endforeach
    </tr>
    <tr>
        <th style="font-size:7px;">Seksyen</th>
        @foreach($periods as $period)
            @foreach($vehiclesType as $type)
                @if($actType == \Ekompaun\Systemconfig\Enum\ActType::VEHICLE)
                    <th style="font-size:7px;">{{ ucfirst($type->vehicle_name) }}</th>
                @elseif($actType == \Ekompaun\Systemconfig\Enum\ActType::USER_CATEGORY)
                    <th style="font-size:7px;">{{ ucfirst(strtolower($type)) }}</th>
                @endif
            @endforeach
        @endforeach
    </tr>

    @foreach($offences as $offence)
        <tr>
            @if($actType == \Ekompaun\Systemconfig\Enum\ActType::USER_CATEGORY)
              <td style="font-size:10px;">
                  <strong>
                      {{ $offence->section->act->act_name }}
                      /
                      {{ $offence->section->section_name }}
                      /
                      {{ $offence->section->section_type_label }}

                  </strong>
                  {{ $offence->offence_name }}
              </td>
            @else
              <td style="font-size:5px;">
                  <strong>
                      {{ $offence->section->act->act_name }}
                      /
                      {{ $offence->section->section_name }}
                      /
                      {{ $offence->section->section_type_label }}

                  </strong>
                  {{ $offence->offence_name }}
              </td>
            @endif
            @foreach($periods as $period)
                @if($actType == \Ekompaun\Systemconfig\Enum\ActType::VEHICLE)
                    @foreach($vehiclesType as $type)
                        <td style="font-size:6px;font-weight:bold;">{{ $offence->getAmount($period->getKey(), $type->getKey()) }}</td>
                    @endforeach
                @elseif($actType == \Ekompaun\Systemconfig\Enum\ActType::USER_CATEGORY)
                    @foreach($vehiclesType as $key=>$value)
                        <td style="font-size:10px;font-weight:bold;">{{ $offence->getAmount($period->getKey(), $key) }}</td>
                    @endforeach
                @endif
            @endforeach
        </tr>
    @endforeach
  </table>
@endsection
