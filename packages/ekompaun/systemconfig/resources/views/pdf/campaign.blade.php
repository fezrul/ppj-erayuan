@extends('layouts.pdf.master')

@section('title', 'Campaign')
@section('content')
  <div style="font-family:Arial; font-size:12px;">
      <h2 style="text-align: center">{{ __("Senarai Kempen") }}</h2>
  </div>
  <br>
  <table class="tg">
    <tr>
      <th class="tg-3wr7" width="1">{{ __("No") }}</th>
      <th class="tg-3wr7">{{ __("Nama Kempen") }}<br></th>
      <th class="tg-3wr7">{{ __("Tarikh Kempen") }}<br></th>
      <th class="tg-3wr7">{{ __("Jenis Kempen") }}<br></th>
      <th class="tg-3wr7">{{ __("Status") }}<br></th>
    </tr>
    @if ($campaignMaster->isNotEmpty() )
    @foreach ($campaignMaster as $cm)
    <tr>
      <td class="tg-rv4w" width="1">{{ $loop->iteration }}</td>
      <td class="tg-ti5e">{{ $cm->camp_name }}</td>
      <td class="tg-rv4w">{{ $cm->camp_startdate }} - {{ $cm->camp_enddate }}</td>
      @if($cm->camp_status == 1)
          <td class="tg-rv4w">{{ $cm->camp_status = __("Kadar Tetap") }}</td>
      @else
          <td class="tg-rv4w">{{ $cm->camp_status = __("Diskaun") }}</td>
      @endif
      @if ($cm->is_active)
          <td class="tg-rv4w">{{ __("Aktif") }}</td>
      @else
          <td class="tg-rv4w">{{ __("Tidak Aktif") }}</td>
      @endif
    </tr>
    @endforeach
    @else
    <tr>
      <td colspan='5' class="tg-rv4w">{{ __("Maaf, tiada data tersedia") }}</td>
    </tr>
    @endif
  </table>
@endsection
