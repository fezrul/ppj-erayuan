<?php

namespace Ekompaun\Systemconfig\Rules;

use Ekompaun\Systemconfig\Enum\Status;
use Illuminate\Contracts\Validation\Rule;
use Ekompaun\Systemconfig\Model\ScheduleRate;

class ScheduleRateValidationRule implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute

     * @param mixed  $value

     * @return bool
     */

    public function passes($attribute, $value)
    {
        $scheduleRate = ScheduleRate::where(
            [
                ['fk_lkp_offence', '=', request()->fk_lkp_offence],
                ['fk_lkp_vehicle', '=', request()->fk_lkp_vehicle],
                ['fk_lkp_period', '=', request()->fk_lkp_period],
                ['schd_usercategory', '=', request()->schd_usercategory],
                ['schd_status', '=', Status::ACTIVE()],
            ]
        );
        return !$scheduleRate->exists();
    }

    public function message()
    {
        return __("Jadual Kadar Rayuan Sudah ada!");
    }
}
