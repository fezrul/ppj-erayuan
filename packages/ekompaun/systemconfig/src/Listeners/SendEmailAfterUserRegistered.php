<?php

namespace Ekompaun\Systemconfig\Listeners;

use Ekompaun\Systemconfig\Events\UserRegistered;
use Ekompaun\Systemconfig\Mail\UserRegistered as UserRegisteredMail;
use Illuminate\Support\Facades\Mail;

class SendEmailAfterUserRegistered
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        Mail::to($event->user)->queue(new UserRegisteredMail($event->user, $event->password));
    }
}
