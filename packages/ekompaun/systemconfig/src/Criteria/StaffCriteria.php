<?php

namespace Ekompaun\Systemconfig\Criteria;

use Ekompaun\Systemconfig\Enum\UserType;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class StaffCriteria implements CriteriaInterface
{
    public function apply($model, RepositoryInterface $repository)
    {
        $model->whereType(UserType::STAF);

        return $model;
    }
}
