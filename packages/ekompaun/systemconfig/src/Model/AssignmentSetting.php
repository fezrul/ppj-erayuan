<?php

namespace Ekompaun\Systemconfig\Model;

use App\User;
use Ekompaun\Platform\Model\BaseModel;

class AssignmentSetting extends BaseModel
{
    protected $table = 'ek_assignment_setting';

    protected $primaryKey = 'id';

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'asgn_pic_userid')->withDefault();
    }
}
