<?php

namespace Ekompaun\Systemconfig\Model;

use App\User;
use Ekompaun\Platform\Model\BaseModel;
use Ekompaun\Systemconfig\Enum\Citizenship;
use Ekompaun\Systemconfig\Model\Lookup\Country;
use Ekompaun\Systemconfig\Model\Lookup\State;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class UserProfile extends BaseModel implements AuditableContract
{
    use Auditable;
    
    protected $table = 'user_profiles';

    protected $primaryKey = 'id';

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'fk_users');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'fk_lkp_country');
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'fk_lkp_state');
    }

    public function getPresentCategoryAttribute()
    {
        return trans("enum.user_category.{$this->user_category}");
    }

    public function getPresentCitizenshipAttribute()
    {
        return trans(sprintf("enum.%s.%s", Citizenship::class, $this->user_citizenship));
    }

    public function getPresentNegeriAttribute()
    {
        return $this->state->state_name ?? '-';
    }

    public function getPresentNegaraAttribute()
    {
        return $this->country->country_name ?? '-';
    }
}
