<?php

namespace Ekompaun\Systemconfig\Model\Traits;

use Carbon\Carbon;

trait CampaignMasterAttribute
{
    public function setCampStartdateAttribute($dates)
    {
        $this->attributes['camp_startdate'] = Carbon::createFromFormat('d-m-Y H:i', $dates)->toDateTimeString();
    }

    public function setCampEnddateAttribute($dates)
    {
        $this->attributes['camp_enddate'] = Carbon::createFromFormat('d-m-Y H:i', $dates)->toDateTimeString();
    }

    public function getCampStartdateAttribute()
    {
        return Carbon::parse($this->attributes['camp_startdate'])
                     ->format('d-m-Y H:i');
    }

    public function getCampEnddateAttribute()
    {
        return Carbon::parse($this->attributes['camp_enddate'])
                     ->format('d-m-Y H:i');
    }

    public function getPictureAttribute()
    {
        $file = 'storage/campaign/'.$this->attributes['camp_filename'];
        if (is_file(public_path($file))) {
            return $file;
        }
        return 'img/no_image.jpg';
    }

    public function getIsActiveAttribute()
    {
        $now = Carbon::now()->second('0');
        $start = Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['camp_startdate']);
        $end = Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['camp_enddate']);

        if ($now->between($start, $end, $inclusive = true)) {
            return true ;
        }

        return false ;
    }
}
