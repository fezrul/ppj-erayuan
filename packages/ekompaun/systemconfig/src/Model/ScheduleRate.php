<?php

namespace Ekompaun\Systemconfig\Model;

use Ekompaun\Appeal\Model\Transaction;
use Ekompaun\Platform\Model\BaseModel;
use Ekompaun\Systemconfig\Enum\Status;
use Illuminate\Database\Eloquent\Builder;
use Ekompaun\Systemconfig\Model\Lookup\Period;
use Illuminate\Support\Collection;

class ScheduleRate extends BaseModel
{
    protected $table = 'ek_schedule_rate';

    protected $primaryKey = 'schd_id';

    protected $guarded = [];

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(
            'status', function (Builder $builder) {
                $builder->where('schd_status', Status::ACTIVE);
            }
        );
    }

    public function period()
    {
        return $this->belongsTo(Period::class, 'fk_lkp_period')->withDefault();
    }

    public function scopeByVehicle($query, $vehicle)
    {
        if ($vehicle) {
            return $query->where('fk_lkp_vehicle', $vehicle);
        }
    }

    public function scopeByUserCategory($query, $category)
    {
        if ($category) {
            return $query->where('schd_usercategory', $category);
        }
    }

    public function scopeByOffence($query, $offence)
    {
        if ($offence) {
            return $query->where('fk_lkp_offence', $offence);
        }
    }

    public function getAmaunKadarRayuan(Transaction $transaction)
    {
        $days = $transaction->days_since_offence;

        /**
         * @var $rates Collection
         */
        $rates = $this->with('period')
                      ->byVehicle($transaction->fk_lkp_vehicletype)
                      // ->byUserCategory($transaction->user->profile->user_category)
                      ->byOffence($transaction->offence->getKey())
                      ->get();

        if ($rates->isEmpty()) {
            return config('site.max_saman');
        }

        // ambil data schedule rate yang sesuai dengan periode kompaun
        $rate = $rates->first(
            function ($item) use ($days) {
                if ($item->period->start_date <= $days && $item->period->end_date >= $days) {
                    return $item;
                }
            }
        );

        // max saman
        $maxRate = $rates->first(function ($item) {
            return $item->fk_lkp_period == 99;
        });

        // Kembalikan amaun sesuai jadual kadar rayuan atau ambil denda maksimal (Saman)
        return $rate->schd_amount ?? $maxRate->schd_amount ?? config('site.max_saman');
    }
}
