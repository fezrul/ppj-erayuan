<?php

namespace Ekompaun\Systemconfig\Model;

use Carbon\Carbon;
use Ekompaun\Platform\Model\BaseModel;
use Illuminate\Database\Eloquent\Builder;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Holiday extends BaseModel implements AuditableContract
{
    use Auditable;

    const ACTIVE_USER = 'Active';
    const INACTIVE_USER = 'Inactive';

    protected $table = 'ek_holiday';

    protected $primaryKey = 'hday_id';

    protected $guarded = [];

    protected static $days = null;

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(
            'orderByDefault', function (Builder $builder) {
                $builder->orderBy('hday_date_from', 'ASC');
            }
        );
    }

    public static function allDays()
    {
        if (static::$days === null) {
            $holidays = static::all();
            $dates = collect();
            foreach ($holidays as $day) {
                $dates = $dates->merge($day->toDatesArray());
            }

            static::$days = $dates->unique()->transform(function($date){
                return $date->format('d-m-Y');
            })->toArray();
        }

        return static::$days;
    }

    public function scopeByYear(Builder $query, $year)
    {
        if ($year) {
            $query->where(
                function ($query2) use ($year) {
                    $query2
                        ->whereRaw("YEAR(hday_date_from) = $year")
                        ->orWhereRaw("YEAR(hday_date_to) = $year");
                }
            );
        }
    }

    public function scopeByKeyword(Builder $query, $keyword)
    {
        if ($keyword) {
            $query->where('hday_name', 'LIKE', "%$keyword%");
        }
    }

    public function isActive()
    {
        return $this->hday_status == User::ACTIVE_USER;
    }

    public function setHdayDateFromAttribute($dates)
    {
        $this->attributes['hday_date_from'] = Carbon::createFromFormat('d-m-Y', $dates)->toDateString();
    }

    public function setHdayDateToAttribute($dates)
    {
        $this->attributes['hday_date_to'] = Carbon::createFromFormat('d-m-Y', $dates)->toDateString();
    }

    public function getHdayDateFromAttribute()
    {
        return Carbon::parse($this->attributes['hday_date_from'])
        ->format('d-m-Y');
    }

    public function getHdayDateToAttribute()
    {
        return Carbon::parse($this->attributes['hday_date_to'])
        ->format('d-m-Y');
    }

    public static function dropdownYear()
    {
        $years = range(2017, date('Y') + 1);

        return array_prepend(array_combine($years, $years), __("Semua"), '');
    }

    public function toDatesArray()
    {
        $start = Carbon::createFromFormat('Y-m-d', $this->attributes['hday_date_from']);
        $finish = Carbon::createFromFormat('Y-m-d', $this->attributes['hday_date_to']);

        $dates = [];
        while($start <= $finish) {
            $dates[] = $start;
            $start = $start->addDay();
        }
        return $dates;
    }
}
