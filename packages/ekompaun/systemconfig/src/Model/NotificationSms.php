<?php

namespace Ekompaun\Systemconfig\Model;

use App\User;
use Ekompaun\Platform\Model\BaseModel;
use Ekompaun\Appeal\Model\Application;

class NotificationSms extends BaseModel
{
    protected $table = 'ek_notification_sms';

    protected $primaryKey = 'nsms_id';

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'fk_userid');
    }

    public function application()
    {
        return $this->belongsTo(Application::class, 'fk_applid');
    }

    public function scopeByBetweenDate($query, $dateFrom, $dateTo)
    {
        return $query->whereBetween(
            'created_date',
            [
                format_date($dateFrom, 'Y-m-d', 'Y-m-d 00:00:00'),
                format_date($dateTo, 'Y-m-d', 'Y-m-d 23:59:59'),
            ]
        );
    }
}
