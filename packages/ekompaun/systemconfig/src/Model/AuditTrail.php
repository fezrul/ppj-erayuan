<?php

namespace Ekompaun\Systemconfig\Model;

use App\User;
use Ekompaun\Platform\Model\BaseModel;
use Ekompaun\Systemconfig\Model\Lookup\Task;

class AuditTrail extends BaseModel
{

    protected $table = 'ek_audittrail';

    protected $primaryKey = 'audit_id';

    protected $guarded = [];

    public function tableRef()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'fk_users');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function lkpTask()
    {
        return $this->belongsTo(Task::class, 'fk_lkp_task');
    }
}
