<?php

namespace Ekompaun\Systemconfig\Model;

use Ekompaun\Platform\Model\BaseModel;

class Custom extends BaseModel
{
    protected $table = 'lkp_custom_period';

    protected $primaryKey = 'custom_id';
}
