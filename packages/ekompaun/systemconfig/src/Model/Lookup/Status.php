<?php

namespace Ekompaun\Systemconfig\Model\Lookup;

use Ekompaun\Platform\Model\BaseModel;

class Status extends BaseModel
{
    protected $table = 'lkp_status';

    protected $primaryKey = 'status_id';

    protected $displayField = 'status_name';

    public function scopeByType($query, $type)
    {
        if ($type) {
            return $query->whereStatusType($type);
        }
    }
}
