<?php

namespace Ekompaun\Systemconfig\Model\Lookup;

use Ekompaun\Platform\Model\BaseModel;
use Ekompaun\Systemconfig\Enum\SectionType;

class Section extends BaseModel
{
    protected $table = 'lkp_section';

    protected $primaryKey = 'section_id';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(
            'active', function ($builder) {
                $builder->where('section_status', '1');
            }
        );
    }

    public function act()
    {
        return $this->belongsTo(Act::class, 'fk_lkp_actid')->withDefault();
    }

    public function offences()
    {
        return $this->hasMany(Offence::class, 'fk_lkp_sectionid');
    }

    public function getSectionTypeLabelAttribute()
    {
        return __("enum.".SectionType::class.".".$this->section_type);
    }
}
