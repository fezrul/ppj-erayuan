<?php

namespace Ekompaun\Systemconfig\Model\Lookup;

use Ekompaun\Platform\Model\BaseModel;

class Ztranstype extends BaseModel
{
    protected $table = 'lkp_ztranstype';

    public function maintrans()
    {
       return $this->belongsTo('Ekompaun\Systemconfig\Model\Lookup\Maintrans','fk_maintrans_id');
           
    }

}
