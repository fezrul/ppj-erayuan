<?php

namespace Ekompaun\Systemconfig\Model\Lookup;

use Ekompaun\Platform\Model\BaseModel;

class PaymentMethod extends BaseModel
{
    protected $table = 'lkp_payment_method';

    protected $primaryKey = 'paymethod_id';

    protected $displayField = 'paymethod_name';
}
