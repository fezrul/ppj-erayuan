<?php

namespace Ekompaun\Systemconfig\Model\Lookup;

use Ekompaun\Platform\Model\BaseModel;
use Ekompaun\Systemconfig\Model\ScheduleRate;
use Illuminate\Database\Eloquent\Builder;

class Offence extends BaseModel
{
    protected $table = 'lkp_offence';

    protected $primaryKey = 'offence_id';

    protected $displayField = 'offence_name';

    protected $guarded = [];

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(
            'orderByName', function (Builder $builder) {
                $builder->orderBy('offence_name');
            }
        );

        static::addGlobalScope(
            'active', function ($builder) {
                $builder->where('offence_status', '1')->where('offence_code', '<>', '');
            }
        );
    }

    public function section()
    {
        return $this->belongsTo(Section::class, 'fk_lkp_sectionid')->withDefault();
    }
    public function ztrans()
    {
       return $this->belongsTo('Ekompaun\Systemconfig\Model\Lookup\Ztranstype','fk_lkp_ztranstypeid');
           
    }

    public function rates()
    {
        return $this->hasMany(ScheduleRate::class, 'fk_lkp_offence');
    }

    public function scopeByKeyword(Builder $query, $keyword = null)
    {
        $keywordSection = $keywordOffence = $keyword;

        $sections = Section::all()->pluck('section_name')->toArray();
        foreach ($sections as $sectionName) {
            if (starts_with(strtolower($keyword), strtolower($sectionName))) {
                $keywordSection = $sectionName;
                $keywordOffence = str_after($keyword, $sectionName);
                break;
            }
        }

        if ($keyword) {
            $query->with('section')->where(
                function (Builder $query2) use ($keywordSection, $keywordOffence) {
                    $query2
                        ->where('offence_name', 'like', "%$keywordOffence%")
                        ->orWhereHas(
                            'section', function ($query3) use ($keywordSection) {
                                $query3->where('section_name', 'like', "%$keywordSection%");
                            }
                        );
                }
            );
        }
    }

    public function scopeByType(Builder $query, $type)
    {
        if ($type) {
            $query->whereHas('section', function ($query2) use ($type) {
                $query2->where('section_type', $type);
            });
        }
    }

    public function scopeByAct(Builder $query, $act)
    {
        if ($act) {
            $query->where('fk_lkp_actid', '=', $act);
        }
    }

    public function getTextAttribute()
    {
        $output = '';

        if ($this->section) {
            $output .= $this->section->section_name;
            $output .= ' ';
        }

        $output .= $this->offence_name;

        return ucfirst(strtolower($output));
    }

    public function getAmount($period, $vehicle)
    {
        $rate = $this->rates->first(
            function ($item) use ($period, $vehicle) {
                return ($item->fk_lkp_period == $period) && ($item->fk_lkp_vehicle == $vehicle);
            }
        );

        if ($rate) {
            return $rate->schd_amount;
        }

        return null;
    }

    public function getId($period, $vehicle)
    {
        $rate = $this->rates->first(
            function ($query) use ($period, $vehicle) {
                return ($query->fk_lkp_period == $period) && ($query->fk_lkp_vehicle == $vehicle);
            }
        );

        if ($rate) {
            return $rate->schd_id;
        }

        return null;
    }
}
