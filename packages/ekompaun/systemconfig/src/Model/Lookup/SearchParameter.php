<?php

namespace Ekompaun\Systemconfig\Model\Lookup;

use Ekompaun\Platform\Model\BaseModel;
use Illuminate\Database\Eloquent\Builder;

class SearchParameter extends BaseModel
{
    protected $table = 'lkp_search_parameter';

    protected $primaryKey = 'param_id';

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(
            'orderByName', function (Builder $builder) {
                $builder->orderBy('param_name');
            }
        );
    }
}
