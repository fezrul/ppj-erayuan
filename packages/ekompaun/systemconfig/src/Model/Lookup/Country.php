<?php

namespace Ekompaun\Systemconfig\Model\Lookup;

use Ekompaun\Platform\Model\BaseModel;
use Illuminate\Database\Eloquent\Builder;

class Country extends BaseModel
{
    protected $table = 'lkp_country';

    protected $primaryKey = 'country_id';

    protected $displayField = 'country_name';

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(
            'orderByDefault', function (Builder $builder) {
                $builder->orderBy('country_name');
            }
        );
    }
}
