<?php

namespace Ekompaun\Systemconfig\Model\Lookup;

use Ekompaun\Platform\Model\BaseModel;

class Task extends BaseModel
{
    protected $table = 'lkp_task';

    protected $primaryKey = 'task_id';

    protected $displayField = 'task_name';

    protected $guarded = [];

    public function scopeByTaskType($query, $type)
    {
        if ($type) {
            return $query->whereTaskType($type);
        }
    }
}
