<?php

namespace Ekompaun\Systemconfig\Model\Lookup;

use Ekompaun\Platform\Model\BaseModel;

class State extends BaseModel
{
    protected $table = 'lkp_state';

    protected $primaryKey = 'state_id';

    protected $displayField = 'state_name';

    public function holidays()
    {
        return $this->hasMany(State::class);
    }
}
