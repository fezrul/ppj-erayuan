<?php

namespace Ekompaun\Systemconfig\Model\Lookup;

use Ekompaun\Platform\Model\BaseModel;
use Illuminate\Database\Eloquent\Builder;
use Ekompaun\Systemconfig\Enum\Status;

class Period extends BaseModel
{
    protected $table = 'lkp_period';

    protected $primaryKey = 'period_id';

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(
            'orderByDefault', function (Builder $builder) {
                $builder->orderBy('period_id');
            }
        );
    }

    public function scopePeriodType(Builder $query, $type)
    {
        $query->where('period_acttype', '=', $type);
    }

    public function getStartDateAttribute()
    {
        $dates = explode("-", $this->period_name);
        if (is_numeric($dates[0])) {
            return $dates[0];
        }
    }

    public function getEndDateAttribute()
    {
        $dates = explode("-", $this->period_name);
        if (is_numeric($dates[0])) {
            return $dates[1];
        }
    }
}
