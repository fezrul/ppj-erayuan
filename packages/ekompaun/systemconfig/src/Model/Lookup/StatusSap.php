<?php

namespace Ekompaun\Systemconfig\Model\Lookup;

use Ekompaun\Platform\Model\BaseModel;

class StatusSap extends BaseModel
{
    protected $table = 'lkp_status_sap';

    protected $primaryKey = 'status_id';

    public function scopeByAppealType($query, $type)
    {
        if ($type) {
            return $query->whereAppealType($type);
        }
    }
}
