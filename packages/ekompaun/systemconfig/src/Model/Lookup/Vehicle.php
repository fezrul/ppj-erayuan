<?php

namespace Ekompaun\Systemconfig\Model\Lookup;

use Ekompaun\Platform\Model\BaseModel;
use Illuminate\Database\Eloquent\Builder;

class Vehicle extends BaseModel
{
    protected $table = 'lkp_vehicle';

    protected $primaryKey = 'vehicle_id';

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(
            'orderByDefault', function (Builder $builder) {
                $builder->orderBy('vehicle_id');
            }
        );
    }

    public function scopeVisible(Builder $query)
    {
        $query->where('vehicle_status', '=', 1);
    }
}
