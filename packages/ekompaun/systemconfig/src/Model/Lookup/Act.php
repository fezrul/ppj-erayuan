<?php

namespace Ekompaun\Systemconfig\Model\Lookup;

use Ekompaun\Platform\Model\BaseModel;

class Act extends BaseModel
{
    protected $table = 'lkp_act';

    protected $primaryKey = 'act_id';

    protected $displayField = 'act_name';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(
            'active', function ($builder) {
                $builder->where('act_status', '1');
            }
        );
    }

    public function sections()
    {
        return $this->hasMany(Section::class, 'fk_lkp_actid');
    }
}
