<?php

namespace Ekompaun\Systemconfig\Model;

use Ekompaun\Platform\Model\BaseModel;
use Ekompaun\Systemconfig\Enum\CampaignRateType;

class CampaignDetail extends BaseModel
{
    protected $table = 'ek_campaign_detail';

    protected $primaryKey = 'cdet_id';

    protected $guarded = [];

    protected $casts = [
        'cdet_offenceid' => 'array',
        'cdet_amount'    => 'int',
    ];

    public function campaignMaster()
    {
        return $this
            ->belongsTo(CampaignMaster::class, 'fk_campid')
            ->withDefault(['camp_retetype' => CampaignRateType::KADAR_TETAP]);
    }

    public function getTypeAttribute()
    {
        return $this->campaignMaster->camp_ratetype;
    }
}
