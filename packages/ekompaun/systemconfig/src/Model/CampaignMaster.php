<?php

namespace Ekompaun\Systemconfig\Model;

use Carbon\Carbon;
use Ekompaun\Systemconfig\Enum\Status;
use Ekompaun\Platform\Model\BaseModel;
use Ekompaun\Systemconfig\Enum\AppealStatusType;
use Illuminate\Http\UploadedFile;
use Ekompaun\Systemconfig\Model\Traits\CampaignMasterAttribute;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class CampaignMaster extends BaseModel implements AuditableContract
{
    use CampaignMasterAttribute,
        Auditable;

    protected $table = 'ek_campaign_master';

    protected $primaryKey = 'camp_id';

    protected $guarded = [];

    public function details()
    {
        return $this->hasMany(CampaignDetail::class, 'fk_campid');
    }

    public function savePicture(UploadedFile $picture)
    {
        $picture->store('campaign', 'public');
        $this->camp_filename = $picture->hashName();

        return $this->save();
    }

    public function getDetailsGroupByYear()
    {
        $types = AppealStatusType::dropdown();

        $grouped = $this->details->groupBy(
            function ($item) {
                return sprintf('%s-%s', $item->cdet_offencedate_from, $item->cdet_offencedate_to);
            }
        );

        $result = [];
        foreach ($grouped as $details) {
            $detail = [
                'from'     => $details->first()->cdet_offencedate_from,
                'to'       => $details->first()->cdet_offencedate_to,
                'offences' => $details->first()->cdet_offenceid,
                'items'    => [],
            ];

            foreach (array_keys($types) as $key) {
                $item = $details->first(
                    function ($item) use ($key) {
                        return $item->cdet_compoundstatus == $key;
                    }
                );

                if (!$item) {
                    $item = new CampaignDetail();
                }

                $detail['items'][$key] = ['id' => $item->getKey(), 'amount' => (int)$item->cdet_amount];
            }
            $result[] = $detail;
        }

        return $result;
    }

    public function scopeActive($query)
    {
        $now = Carbon::now()->second(0)->format('Y-m-d H:i:s');
        return $query->where('camp_startdate', '<=', $now)
            ->where('camp_enddate', '>=', $now);
    }

    public function scopeByStatus($query, $status)
    {
        if ($status) {
            return $query->where('camp_status', $status);
        }
    }

    public function unpublish()
    {
        $this->camp_status = Status::INACTIVE;

        return $this->save();
    }
}
