<?php

namespace Ekompaun\Systemconfig\Model;

use App\User;
use Illuminate\Database\Eloquent\Builder;
use OwenIt\Auditing\Models\Audit as Auditing;

class Audit extends Auditing
{
    

    protected static function boot()
    {
        parent::boot();
     
        // Order by created_at DESC
        static::addGlobalScope(
            'order', function (Builder $builder) {
                $builder->orderByDesc('created_at');
            }
        );
    }

    public function scopeByKeyword(Builder $query, $keyword = null)
    {
        if ($keyword) {
            $keyword = explode(", ", $keyword);
            $query->with('user')->where(
                function (Builder $query2) use ($keyword) {
                    $query2
                        ->whereIn('auditable_type', $keyword)
                        ->orWhereIn('event', $keyword)
                        ->orWhereHas(
                            'user', function ($query3) use ($keyword) {
                                foreach ($keyword as $key => $word) {
                                    if ($key == 0) {
                                        $query3->where('name', 'like', "%$word%");
                                        continue;
                                    }
                                    $query3->orWhere('name', 'like', "%$word%");
                                }
                            }
                        );
                }
            );
        }
    }
}
