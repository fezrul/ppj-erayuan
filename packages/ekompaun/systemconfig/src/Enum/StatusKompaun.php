<?php

namespace Ekompaun\Systemconfig\Enum;

use Ekompaun\Platform\Enum\BaseEnum;

class StatusKompaun extends BaseEnum
{
    // Special status untuk transaction yang belum pernah di-create applicationnya
    const BELUM_BAYAR           = 0;

    const MENUNGGU_KELULUSAN    = 1;
    const LULUS                 = 2;
    const AMARAN                = 3;
    const DITOLAK               = 4;
    const PEMBATALAN            = 5;
    const DOKUMEN               = 6;
    const MENGIKUT_JADWAL       = 7;
    const BATAL_AUTOMATIK       = 8;
    const BATAL_PERMOHONAN      = 9;
    const ASSIGNMENT            = 10;
    const SUBMIT                = 11;
    const PAID                  = 12;
    const DOKUMEN_LENGKAP       = 13;
    const RESET                 = 14;
    const TIDAK_SELESAI         = 15;
    const PENDING_PAYMENT       = 16;
}
