<?php

namespace Ekompaun\Systemconfig\Enum;

use MyCLabs\Enum\Enum;

class SemakSapStatus extends Enum
{
    const HANTAR = 1;
    const RETURN = 2;
}
