<?php

namespace Ekompaun\Systemconfig\Enum;

class Year
{
    const FROM = 2017;

    public static function dropdown($from = null, $to = null)
    {
        $from = $from ?? self::FROM;
        $to = $to ?? date('Y');
        $years = range($from, $to);

        return collect(array_combine($years, $years));
    }
}
