<?php

namespace Ekompaun\Systemconfig\Enum;

use MyCLabs\Enum\Enum;

class Status extends Enum
{
    const INACTIVE = 0;
    const ACTIVE = 1;
}
