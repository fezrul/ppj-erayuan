<?php

namespace Ekompaun\Systemconfig\Enum;

use MyCLabs\Enum\Enum;

class UserType extends Enum
{
    const PELANGGAN = 1;
    const STAF = 2;
}
