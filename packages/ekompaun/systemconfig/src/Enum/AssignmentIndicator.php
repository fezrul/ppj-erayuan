<?php

namespace Ekompaun\Systemconfig\Enum;

use MyCLabs\Enum\Enum;

class AssignmentIndicator extends Enum
{
    const AUTO_ASSIGN = 1;
    const MANUAL_ASSIGN = 2;
}
