<?php

namespace Ekompaun\Systemconfig\Enum;

use MyCLabs\Enum\Enum;

class ActType extends Enum
{
    const VEHICLE = 1;
    const USER_CATEGORY = 2;
}
