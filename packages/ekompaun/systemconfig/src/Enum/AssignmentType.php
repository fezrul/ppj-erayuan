<?php

namespace Ekompaun\Systemconfig\Enum;

use MyCLabs\Enum\Enum;

class AssignmentType extends Enum
{
    const NORMAL = 0;
    const REASSIGN = 1;
}
