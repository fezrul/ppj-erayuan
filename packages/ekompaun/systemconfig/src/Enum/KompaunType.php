<?php

namespace Ekompaun\Systemconfig\Enum;

use Ekompaun\Platform\Enum\BaseEnum;

class KompaunType extends BaseEnum
{
    const NOTIS_KOMPAUN = 1;
    const KES_MAHKAMAH = 2;
    const KES_SELESAI = 3;
}
