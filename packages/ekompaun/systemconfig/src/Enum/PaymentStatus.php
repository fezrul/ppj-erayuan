<?php

namespace Ekompaun\Systemconfig\Enum;

use Ekompaun\Platform\Enum\BaseEnum;

class PaymentStatus extends BaseEnum
{
    const BERJAYA = 1;
    const TIDAK_BERJAYA = 2;
}
