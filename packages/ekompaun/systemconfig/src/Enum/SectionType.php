<?php

namespace Ekompaun\Systemconfig\Enum;

use Ekompaun\Platform\Enum\BaseEnum;

class SectionType extends BaseEnum
{
    const SECTION = 1;
    const KAEDAH = 2;
    const PERINTAH = 3;
}
