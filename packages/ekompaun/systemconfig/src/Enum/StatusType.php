<?php

namespace Ekompaun\Systemconfig\Enum;

use Ekompaun\Platform\Enum\BaseEnum;

class StatusType extends BaseEnum
{
    const SUBMIT        = 1;
    const ASSIGNED      = 2;
    const DECISION      = 3;
    const AGGREEMENT    = 4;
    const PAID          = 12;
}
