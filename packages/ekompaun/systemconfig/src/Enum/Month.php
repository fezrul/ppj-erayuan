<?php

namespace Ekompaun\Systemconfig\Enum;

use Jenssegers\Date\Date;

class Month
{
    public static function dropdown($format = 'F')
    {
        $months = [];
        foreach (range(1, 12) as $i) {
            $months[$i] = Date::createFromDate(200, $i, 1)->format($format);
        }

        return collect($months);
    }
}
