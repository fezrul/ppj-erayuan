<?php

namespace Ekompaun\Systemconfig\Enum;

use MyCLabs\Enum\Enum;

class UserIdentity extends Enum
{
    const IC = 1;
    const PASSPORT = 2;
}
