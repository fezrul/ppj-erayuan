<?php

namespace Ekompaun\Systemconfig\Enum;

use Ekompaun\Platform\Enum\BaseEnum;

class UserCategory extends BaseEnum
{
    const INDIVIDUAL = 1;
    const COMPANY = 2;
}
