<?php

namespace Ekompaun\Systemconfig\Enum;

use Ekompaun\Platform\Enum\BaseEnum;

class AppealType extends BaseEnum
{
    const MENGIKUTI_JADUAL = 1;
    const RAYUAN_KEDUA = 2;
    const KES_MAHKAMAH = 3;
    const KEMPEN = 4;
}
