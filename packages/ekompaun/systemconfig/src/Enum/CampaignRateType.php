<?php

namespace Ekompaun\Systemconfig\Enum;

use Ekompaun\Platform\Enum\BaseEnum;

class CampaignRateType extends BaseEnum
{
    const KADAR_TETAP = 1;
    const KADAR_DISKAUN = 2;
    const DISKAUN = 3;
}
