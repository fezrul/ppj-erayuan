<?php

namespace Ekompaun\Systemconfig\Enum;

use Ekompaun\Platform\Enum\BaseEnum;

class AppealChannel extends BaseEnum
{
    const ONLINE = 1;
    const SMS = 2;
}
