<?php

namespace Ekompaun\Systemconfig\Enum;

use Ekompaun\Platform\Enum\BaseEnum;

class TaskType extends BaseEnum
{
    const INDEX_APPLICATION     = 1;
    const SHOW_APPLICATION      = 2;
    const STORE_APPLICATION     = 3;
    const EDIT_APPLICATION      = 4;
    const UPDATE_APPLICATION    = 5;
    const DELETE_APPLICATION    = 6;
    const INDEX_TRANSACTION     = 7;
    const SHOW_TRANSACTION      = 8;
    const STORE_TRANSACTION     = 9;
    const EDIT_TRANSACTION      = 10;
    const UPDATE_TRANSACTION    = 11;
    const DELETE_TRANSACTION    = 12;
    const INDEX_PAYMENT         = 13;
    const SHOW_PAYMENT          = 14;
    const STORE_PAYMENT         = 15;
    const EDIT_PAYMENT          = 16;
    const UPDATE_PAYMENT        = 17;
    const DELETE_PAYMENT        = 18;
}
