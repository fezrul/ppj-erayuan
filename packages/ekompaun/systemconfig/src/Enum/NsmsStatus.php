<?php

namespace Ekompaun\Systemconfig\Enum;

use MyCLabs\Enum\Enum;

class NsmsStatus extends Enum
{
    const NOT_SENT = 0;
    const SENT = 1;
}
