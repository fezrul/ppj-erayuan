<?php

namespace Ekompaun\Systemconfig\Enum;

use Ekompaun\Platform\Enum\BaseEnum;

class AppealProgress extends BaseEnum
{
    const BELUM_SELESAI = 1;
    const SELESAI = 2;
}
