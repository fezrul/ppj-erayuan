<?php

namespace Ekompaun\Systemconfig\Enum;

use MyCLabs\Enum\Enum;

class Permission extends Enum
{
    const VIEW_MY_RAYUAN = 'my::appeal';
    const VIEW_MY_PAYMENT_HISTORY = 'my::payment-history';
    const VIEW_MY_CAMPAIGN = 'my::campaign';
    const EDIT_MY_PROFILE = 'my::edit-profile';

    const SUBMIT_RAYUAN = 'appeal::submit-rayuan';
    const ASSIGN_RAYUAN = 'appeal::assign-rayuan';
    const PROCESS_RAYUAN = 'appeal::process-rayuan';
    const VIEW_RAYUAN_ALL = 'appeal::view-rayuan-all';
    const VIEW_RAYUAN_PENGGUNA = 'appeal::view-rayuan-pengguna';

    const VIEW_KOMPAUN = 'appeal::view-kompaun';

    const PRINT_RESIT = 'appeal::print-resit';

    const VIEW_LAPORAN_RAYUAN = 'report::rayuan';
    const VIEW_LAPORAN_RAYUAN_SMS = 'report::rayuan-sms';
    const VIEW_LAPORAN_TERIMAAN_HARIAN = 'report::terimaan-harian';
    const VIEW_LAPORAN_PAYMENT_DETAIL = 'report::payment-detail';
    const VIEW_LAPORAN_PAYMENT_SUMMARY = 'report::payment-summary';
    const VIEW_LAPORAN_PRESTASI_PEGAWAI_DETAIL = 'report::prestasi-pegawai-detail';
    const VIEW_LAPORAN_PRESTASI_PEGAWAI_SUMMARY = 'report::prestasi-pegawai-summary';
    const VIEW_LAPORAN_KOMPAUN = 'report::kompaun';
    const VIEW_LAPORAN_AUDIT_TRAIL = 'report::audit-trail';
    const VIEW_LAPORAN_AUDIT_TRAIL_RAYUAN = 'report::audit-trail-rayuan';

    const MANAGE_CAMPAIGN = 'systemconfig::manage-campaign';
    const MANAGE_CUSTOMER = 'systemconfig::manage-customer';
    const MANAGE_HOLIDAY = 'systemconfig::manage-holiday';
    const MANAGE_SCHEDULE_RATE = 'systemconfig::manage-schedule-rate';
    const MANAGE_SYSTEM = 'platform::manage-system';
}
