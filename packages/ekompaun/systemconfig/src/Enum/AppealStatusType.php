<?php

namespace Ekompaun\Systemconfig\Enum;

use Ekompaun\Platform\Enum\BaseEnum;

class AppealStatusType extends BaseEnum
{
    const NOTIS_KOMPAUN = 1;
    const KES_MAHKAMAH = 2;
}
