<?php

namespace Ekompaun\Systemconfig\Enum;

use Ekompaun\Platform\Enum\BaseEnum;

class UserStatus extends BaseEnum
{
    const INACTIVE = 'INACTIVE';
    const ACTIVE = 'ACTIVE';
    const PENDING = 'PENDING';
}
