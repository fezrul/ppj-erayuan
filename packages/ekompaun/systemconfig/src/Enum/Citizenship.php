<?php

namespace Ekompaun\Systemconfig\Enum;

use Ekompaun\Platform\Enum\BaseEnum;

class Citizenship extends BaseEnum
{
    const RESIDENT = 1;
    const NON_RESIDENT = 2;
}
