<?php

namespace Ekompaun\Systemconfig;

use Ekompaun\Systemconfig\Enum\Permission;
use Illuminate\Support\ServiceProvider;

class SystemconfigServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */

    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'systemconfig');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'systemconfig');
        $this->bootMenu();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/../routes/web.php';
    }

    protected function bootMenu()
    {
        $menu = app('menu')
            ->add(__("Sistem"))
            ->data('icon', 'fa fa-gears');

        $menu->add(__("Tetapan Cuti"), route('holiday.index'))
            ->data('icon', 'fa fa-calendar')
            ->data('permission', Permission::MANAGE_HOLIDAY);

        $menu->add(__("Tetapan Kempen"), url('campaign'))
            ->data('icon', 'fa fa-gift')
            ->data('permission', Permission::MANAGE_CAMPAIGN);

        $menu->add(__("Jadual Kadar Rayuan"), route('schedule-rate.index'))
            ->data('icon', 'fa fa-tasks')
            ->data('permission', Permission::MANAGE_SCHEDULE_RATE);

        // $menu->add(__("Konfigurasi Sistem"), route('variable.index'))
        //     ->data('icon', 'fa fa-sliders')
        //     ->data('permission', Permission::MANAGE_SYSTEM);

        $menu->add(__("Pelanggan"), route('customer.index'))
            ->data('icon', 'fa fa-users')
            ->data('permission', Permission::MANAGE_CUSTOMER);

        $menu->add(__("Pengguna Dalaman"), route('epicentrum::users.index'))
            ->data('icon', 'fa fa-users')
            ->data('permission', \Laravolt\Epicentrum\Permission::MANAGE_USER);

        $menu->add(__("Peranan"), route('epicentrum::roles.index'))
            ->data('icon', 'fa fa-id-card')
            ->data('permission', \Laravolt\Epicentrum\Permission::MANAGE_ROLE);

        $menu->add(__("Tetapan Sistem"), route('setting.edit'))
            ->data('icon', 'fa fa-gear')
            ->data('permission', Permission::MANAGE_SYSTEM);
    }
}
