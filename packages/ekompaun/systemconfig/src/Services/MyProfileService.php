<?php

namespace Ekompaun\Systemconfig\Services;

use App\User;
use Ekompaun\Systemconfig\Enum\UserCategory;
use Ekompaun\Systemconfig\Enum\UserStatus;
use Ekompaun\Systemconfig\Enum\UserType;
use Ekompaun\Systemconfig\Model\UserProfile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class MyProfileService
{
    public function update($user, $data)
    {
        $data = collect($data);

        DB::transaction(
            function () use ($user, $data) {
                $user = $this->updateUser($user, $data);
                $this->updateProfile($user, $data);

                return $user;
            }
        );

        return false;
    }

    protected function updateUser($user, Collection $data)
    {
        $userData = [
            'name'     => $data->get('name'),
            'email'    => $data->get('email'),
            'icno'     => $data->get('icno'),
        ];

        if ($data->has('status')) {
            $userData['status'] = $data->get('status');
        }

        $user->update($userData);

        return $user;
    }

    protected function updateProfile($user, Collection $data)
    {
        $profile = $data->only(
            [
                'user_category',
                'user_citizenship',
                'user_companyssmno',
                'user_premisno',
                'user_address1',
                'user_address2',
                'user_address3',
                'user_vehicleno1',
                'user_vehicleno2',
                'user_vehicleno3',
                'user_postcode',
                'user_mobileno',
                'user_phoneno',
                'user_officeno',
                'fk_lkp_state',
                'fk_lkp_country',
            ]
        )->toArray();

        $user->profile()->updateOrCreate(['fk_users' => $user->getKey()], $profile);

        return $user->save();
    }
}
