<?php

namespace Ekompaun\Systemconfig\Services;

use App\Mail\SendMailDokumenLengkap;
use Mail;
use App\User;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Systemconfig\Enum\StatusKompaun;
use App\Mail\SendMailApplication;
use App\Mail\SendMailRayuanIkutJadwal;
use App\Mail\SendMailRayuanKedua;
use App\Mail\SendMailDokumenTambahan;
use App\Mail\SendMailRayuanLulus;
use App\Mail\SendMailRayuanDitolak;
use App\Mail\SendMailRayuanAmaran;
use App\Mail\SendMailKetuaPegawaiJu;
use App\Mail\SendMailApplicationAssignment;
use App\Mail\SendMailApplicationIdle;

class MailService
{
    public function mailSubmitRayuanIkutJadwal($applId)
    {
        $application = Application::with('transaction')->findOrFail($applId);
        Mail::to($application->user)
        ->send(new SendMailRayuanIkutJadwal($application));
    }

    public function mailSubmitRayuanKedua($applId)
    {
        $application = Application::with('transaction')->findOrFail($applId);
        Mail::to($application->user)
        ->send(new SendMailRayuanKedua($application));
        $this->mailToKetuaPegawai($applId);
    }

    public function mailToKetuaPegawai($applId)
    {
        $application = Application::with('transaction')->findOrFail($applId);
        $users = User::byRolesName('Ketua Pegawai J(U)')->get()->toArray();

        Mail::to($users)
        ->send(new SendMailKetuaPegawaiJu($application));
    }

    public function mailApplicationStatusChange($applId)
    {
        $application = Application::with('transaction')->findOrFail($applId);
        if ($application->appl_status == StatusKompaun::DOKUMEN) {
            Mail::to($application->user)
            ->send(new SendMailDokumenTambahan($application));
        }

        if ($application->appl_status == StatusKompaun::LULUS) {
            Mail::to($application->user)
            ->send(new SendMailRayuanLulus($application));
        }

        if ($application->appl_status == StatusKompaun::DITOLAK) {
            Mail::to($application->user)
            ->send(new SendMailRayuanDitolak($application));
        }

        if ($application->appl_status == StatusKompaun::AMARAN) {
            Mail::to($application->user)
            ->send(new SendMailRayuanAmaran($application));
        }
    }

    public function mailApplicationAssigned($applId)
    {
        $application = Application::with('transaction')->findOrFail($applId);
        Mail::to($application->personInCharge)
        ->send(new SendMailApplicationAssignment($application));
    }

    public function mailApplicationIdle($applId)
    {
        $application = Application::with('transaction')->findOrFail($applId);
        Mail::to($application->personInCharge)
        ->send(new SendMailApplicationIdle($application));
    }

    public function mailApplicationReverted($applId)
    {
        $application = Application::with('transaction')->findOrFail($applId);
        Mail::to($application->user)
        ->send(new SendMailApplication($application));
    }

    public function mailDokumenLengkap($application)
    {
        Mail::to($application->personInCharge)->send(new SendMailDokumenLengkap($application));
    }
}
