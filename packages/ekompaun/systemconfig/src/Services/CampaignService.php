<?php

namespace Ekompaun\Systemconfig\Services;

use Carbon\Carbon;
use Ekompaun\Appeal\Model\Transaction;
use Ekompaun\Systemconfig\Enum\CampaignRateType;
use Ekompaun\Systemconfig\Enum\KompaunType;
use Ekompaun\Systemconfig\Model\CampaignDetail;
use Ekompaun\Systemconfig\Model\CampaignMaster;
use Ekompaun\Systemconfig\Model\Lookup\Act;
use Ekompaun\Systemconfig\Model\Lookup\Offence;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;

class CampaignService
{
    public function getTypeOfMistakes()
    {
        $acts = Act::with('sections', 'sections.offences')->get();

        return $acts;
    }

    public function create($master, $details, $picture)
    {
        // filter data yang tidak lengkap
        $details = collect($details)->filter(
            function ($item) {
                return $item['cdet_offencedate_from'] && $item['cdet_offencedate_to'];
            }
        );

        // prepare campaign detail data for insertion
        $campaignDetail = [];
        foreach ($details as $detail) {
            foreach ($detail['cdet_amount'] as $type => $amount) {
                $campaignDetail[] = [
                    'cdet_offencedate_from' => $detail['cdet_offencedate_from'],
                    'cdet_offencedate_to' => $detail['cdet_offencedate_to'],
                    'cdet_offenceid' => explode(',', $detail['cdet_offenceid']),
                    'cdet_compoundstatus' => $type,
                    'cdet_amount' => $amount ?? 0,
                ];
            }
        }

        DB::transaction(
            function () use ($master, $campaignDetail, $picture) {
                $campaignMaster = CampaignMaster::create($master);
                $campaignMaster->details()->createMany($campaignDetail);

                if ($picture instanceof UploadedFile) {
                    $campaignMaster->savePicture($picture);
                }
            }
        );
    }

    public function update($model, $master, $details, $picture)
    {
        // filter data yang tidak lengkap
        $details = collect($details)->filter(
            function ($item) {
                return $item['cdet_offencedate_from'] && $item['cdet_offencedate_to'];
            }
        );

        // prepare campaign detail data for insertion
        $campaignDetail = [];
        $where = [];
        foreach ($details as $detail) {
            foreach ($detail['cdet_amount'] as $type => $amount) {
                $campaignDetail[] = [
                    'cdet_offencedate_from' => $detail['cdet_offencedate_from'],
                    'cdet_offencedate_to' => $detail['cdet_offencedate_to'],
                    'cdet_offenceid' => explode(',', $detail['cdet_offenceid']),
                    'cdet_compoundstatus' => $type,
                    'cdet_amount' => $amount ?? 0,
                    'deleted' => isset($detail['deleted']),
                ];

                $where[] =  [
                    'cdet_offencedate_from' => $detail['cdet_offencedate_from_org']??$detail['cdet_offencedate_from'],
                    'cdet_offencedate_to' => $detail['cdet_offencedate_to_org']??$detail['cdet_offencedate_to'],
                    'cdet_compoundstatus' => $type,
                ];
            }
        }

        DB::transaction(
            function () use ($model, $master, $campaignDetail, $picture, $where) {
                $model->update($master);

                foreach ($campaignDetail as $key => $detail) {
                    if ($detail['deleted']) {
                        $model->details()->where($where[$key])->delete();
                        continue;
                    }
                    unset($detail['deleted']);
                    $model->details()->updateOrCreate($where[$key], $detail);
                }

                if ($picture instanceof UploadedFile) {
                    $model->savePicture($picture);
                }
            }
        );
    }

    public function calculateDiscount($initialAmount, $discount, $type)
    {
        $amount = 0;

        switch ($type) {
            case CampaignRateType::KADAR_TETAP:
                $amount = $discount;
                break;
            case CampaignRateType::KADAR_DISKAUN:
                $amount = $initialAmount - $discount;
                break;
            case CampaignRateType::DISKAUN:
                $amount = $initialAmount - ($discount / 100 * $initialAmount);
                break;
        }

        // prevent minus amount
        return max(0, $amount);
    }

    public function calculateAmountAfterDiscount($initialAmount, $discount, $type)
    {
        $amount = $initialAmount;

        switch ($type) {
            case CampaignRateType::KADAR_TETAP:
                $amount = $discount;
                break;
            case CampaignRateType::KADAR_DISKAUN:
                $amount = $initialAmount - $discount;
                break;
            case CampaignRateType::DISKAUN:
                $amount = $initialAmount - ($discount / 100 * $initialAmount);
                break;
        }

        // prevent minus amount
        return max(0, $amount);
    }

    public function getCampaignFor(Transaction $transaction)
    {
        $activeCampaign = CampaignMaster::active()->first();

        if (!$activeCampaign) {
            return new CampaignDetail();
        }

        $matchedCampaign = $activeCampaign->details->first( function($item) use ($transaction) {
            $compoundDate = Carbon::createFromFormat('Y-m-d H:i:s', $transaction->tran_compounddate);

            /**
             * @var $codes Collection
             */
            $codes = Offence::whereIn('offence_id', $item->cdet_offenceid)->pluck('offence_code');
            $isOffenceMatched = $codes->search($transaction->fk_lkp_offencecode) !== false;
            // $isVehicleMatched = $item->cdet_vehicletype == $transaction->fk_lkp_vehicletype;
            $isVehicleMatched = true;
            $isYearMatched = ($compoundDate->year >= $item->cdet_offencedate_from) && ($compoundDate->year <= $item->cdet_offencedate_to);

            $type = KompaunType::NOTIS_KOMPAUN;
            if ($transaction->is_kes_mahkamah) {
                $type = KompaunType::KES_MAHKAMAH;
            }
            $isTypeMatched = $item->cdet_compoundstatus == $type;

            return $isOffenceMatched && $isVehicleMatched && $isYearMatched && $isTypeMatched;

        });

        if (!$matchedCampaign) {
            $matchedCampaign = new CampaignDetail();
        }
        return $matchedCampaign;
    }

}
