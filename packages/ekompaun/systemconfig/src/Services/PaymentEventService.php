<?php

namespace Ekompaun\Systemconfig\Services;

use App\User;
use Ekompaun\Payment\Data\Model\PaymentLogs;


class PaymentEventService
{

    public function __construct(PaymentLogs $payment)
    {
        $this->payment = $payment;
    }

    public function addpaylog($payment)
    {
        $this->payment= new PaymentLogs;
        $this->payment->fk_users=$payment->paylog[0];
        $this->payment->fk_payment=$payment->paylog[1];
        $this->payment->task=$payment->paylog[2];
        $this->payment->save();
    }

   
}
