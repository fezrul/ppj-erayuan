<?php

namespace Ekompaun\Systemconfig\Services;

use App\User;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Systemconfig\Model\AuditTrail;
use Ekompaun\Systemconfig\Enum\TaskType;

class ApplicationEventService
{
    protected $audit;

    public function __construct(AuditTrail $audit)
    {
        $this->audit = $audit;
    }

    public function applicationViewed(User $user, $applId)
    {
        $this->audit->fill(['task'=>__('Maklumat Kompaun')]);
        $this->audit->user()->associate($user);
        $this->audit->createdBy()->associate($user);
        $this->audit->lkpTask()->associate(TaskType::SHOW_APPLICATION);
        $application = Application::find($applId);
        $application->auditrail()->save($this->audit);
    }

    public function applicationStatusChange(User $user, $applId)
    {
        $this->audit->fill(['task'=>__('Mengubah Kompaun')]);
        $this->audit->user()->associate($user);
        $this->audit->createdBy()->associate($user);
        $this->audit->lkpTask()->associate(TaskType::UPDATE_APPLICATION);
        $application = Application::find($applId);
        $application->auditrail()->save($this->audit);
    }

    public function applicationAssign(User $user, $applId)
    {
        $this->audit->fill(['task'=>__('Assign Kompaun')]);
        $this->audit->user()->associate($user);
        $this->audit->createdBy()->associate($user);
        $this->audit->lkpTask()->associate(TaskType::UPDATE_APPLICATION);
        $application = Application::find($applId);
        $application->auditrail()->save($this->audit);
    }

    public function applicationSubmit(User $user, $applId, $task)
    {
        $this->audit->fill(['task'=>$task]);
        $this->audit->user()->associate($user);
        $this->audit->createdBy()->associate($user);
        $this->audit->lkpTask()->associate(TaskType::STORE_APPLICATION);
        $application = Application::find($applId);
        $application->auditrail()->save($this->audit);
    }
}
