<?php

namespace Ekompaun\Systemconfig\Services;

use App\User;
use Ekompaun\Systemconfig\Enum\UserStatus;
use Ekompaun\Systemconfig\Enum\UserType;
use Ekompaun\Systemconfig\Events\UserRegistered;
use Ekompaun\Systemconfig\Model\UserProfile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class RegistrationService
{
    public function register($data)
    {
        $data = collect($data);

        DB::transaction(
            function () use ($data) {
                $user = $this->createUser($data);

                $profile = $this->makeProfile($data);
                $profile->user()->associate($user);
                $profile->save();

                $user->save();

                $user->assignRole('Pelanggan');

                event(new UserRegistered($user, $data->get('new_password')));

                return $user;
            }
        );

        return false;
    }

    protected function createUser(Collection $data)
    {
        $userData = [
            'name'     => strtoupper($data->get('name')),
            'email'    => $data->get('email'),
            'icno'     => strtoupper($data->get('icno')),
            'password' => bcrypt($data->get('new_password')),
            'timezone' => config('app.timezone'),
            'status'   => UserStatus::ACTIVE,
            'type'     => UserType::PELANGGAN,
        ];

        return User::create($userData);
    }

    protected function makeProfile(Collection $data)
    {
        $profile = $data->only(
            [
               
                'user_postcode',
                'user_mobileno',
                'user_phoneno',
                'user_officeno',
                'fk_lkp_state',
                'fk_lkp_country',
            ]
        )->toArray();

        $profile['user_address1'] = strtoupper($data->get('user_address1'));
        $profile['user_address2'] = strtoupper($data->get('user_address2'));
        $profile['user_address3'] = strtoupper($data->get('user_address3'));
        $profile['user_vehicleno1'] = strtoupper($data->get('user_vehicleno1'));
        $profile['user_vehicleno2'] = strtoupper($data->get('user_vehicleno2'));
        $profile['user_vehicleno3'] = strtoupper($data->get('user_vehicleno3'));
        $profile['user_companyssmno'] = strtoupper($data->get('companyssmno'));

        $profile['user_category'] = $data->get('user_category');
        $profile['user_citizenship'] = $data->get('citizenship');
        $profile['user_companyssmno'] = strtoupper($data->get('companyssmno'));
        $profile['user_premisno'] = strtoupper($data->get('premisno'));

        return UserProfile::make($profile);
    }
}
