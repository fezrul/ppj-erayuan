<?php

namespace Ekompaun\Systemconfig\Services;

use Ekompaun\Systemconfig\Enum\Status;
use Ekompaun\Systemconfig\Enum\ActType;
use Ekompaun\Systemconfig\Model\Lookup\Act;
use Ekompaun\Systemconfig\Enum\UserCategory;
use Ekompaun\Systemconfig\Model\ScheduleRate;
use Ekompaun\Systemconfig\Model\Lookup\Period;
use Ekompaun\Systemconfig\Model\Lookup\Offence;
use Ekompaun\Systemconfig\Model\Lookup\Vehicle;

class ScheduleRateService
{
    /**
     * ScheduleRateService constructor.
     */
    public function __construct()
    {
    }

    public function index($act = null, $type = null, $keyword = null)
    {
        $act = $this->act($act);
        return array_prepend($this->scheduleRate($act), $this->offences($act, $type, $keyword)->paginate());
    }

    public function export($act = null, $keyword = null)
    {
        $act = $this->act($act);
        return array_prepend($this->scheduleRate($act), $this->offences($act, $keyword)->get());
    }

    public function store($request)
    {
        $act_offence = Offence::findOrFail($request->fk_lkp_offence);
        $dataScheduleRate = [
            'schd_status' => Status::ACTIVE,
            'fk_lkp_act'  => $act_offence->fk_lkp_actid,
            'fk_lkp_period' => $request->fk_lkp_period,
            'fk_lkp_offence' => $request->fk_lkp_offence,
            'fk_lkp_vehicle' => $request->fk_lkp_vehicle,
            'schd_usercategory' => $request->schd_usercategory,
            'schd_amount' => $request->schd_amount,
        ];
        ScheduleRate::create($dataScheduleRate);
    }

    public function update($request, $id)
    {
        $scheduleRate = ScheduleRate::findOrFail($id);
        $scheduleRate->update($request->all());
    }

    private function act($act)
    {
        if (!$act) {
            return Act::first();
        }
        return Act::find($act);
    }

    private function sectionType($type)
    {
        if (!$type) {
            return SectionType::first();
        }
        return SectionType($type);
    }

    private function offences($act, $type, $keyword = null)
    {
        return Offence::with(['rates', 'section'])->byAct($act->getKey())->byType($type)->byKeyword($keyword);
    }

    private function scheduleRate($act)
    {
        $periods  = Period::periodType($act->act_type)->get();
        if ($act->act_type == ActType::VEHICLE) {
            $vehiclesType = Vehicle::visible()->get();
        }
        if ($act->act_type == ActType::USER_CATEGORY) {
            $vehiclesType = UserCategory::dropdown();
        }
        return [$periods, $vehiclesType, $act->act_type];
    }
}
