<?php

namespace Ekompaun\Systemconfig\Http\Requests\Customer;

use App\User;
use Ekompaun\Systemconfig\Model\UserProfile;
use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = app(User::class)->getTable();
        return [
            'name'                      => 'required|max:45',
            'user_category'             => 'required',
            'email'                     => "required|email|unique:$user,email,".$this->id,
            'icno'                      => "required|max:45|unique:$user,icno,".$this->id,
            'companyssmno'              => 'max:10',
            'user_vehicleno1'           => 'max:30',
            'user_vehicleno2'           => 'max:30',
            'user_vehicleno3'           => 'max:30',
            'user_address1'             => 'required|max:50',
            'user_address2'             => 'max:50',
            'user_address3'             => 'max:50',
            'user_mobileno'             => 'required|numeric|digits_between:1,15',
            'user_officeno'             => 'digits_between:0,15',
            'user_phoneno'              => 'digits_between:0,15',
            'user_postcode'             => 'required|numeric|digits_between:5,10',
            'fk_lkp_state'              => 'required_if:citizenship,==,1',
        ];
    }

    public function messages()
    {
        return [
            'fk_lkp_state.required_if' => 'Pilih Negeri'
        ];
    }
}
