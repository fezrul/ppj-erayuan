<?php

namespace Ekompaun\Systemconfig\Http\Requests\Campaign;

use Illuminate\Foundation\Http\FormRequest;

class StoreCampaignRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'camp_name'                         => 'required|max:100',
            'camp_enddate'                      => 'required|date|after_or_equal:camp_startdate',
            'camp_ratetype'                     => 'required',
            'picture'                           => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'camp_content'                      => 'required|max:500',
            'details.*.cdet_offencedate_from'   => 'nullable|numeric|digits:4',
            'details.*.cdet_offencedate_to'     => 'nullable|numeric|digits:4',
            'details.*.cdet_amount.*'           => 'nullable|numeric',
        ];

        return $rules;
    }

    public function attributes()
    {

        $attributes = [];
        foreach ($this->request->get('details') as $key => $details) {
            for ($i=0; $i < count($details); $i++) {
                $attributes['details.'.$key.'.cdet_offencedate_from'] = __("Tarikh Dari");
                $attributes['details.'.$key.'.cdet_offencedate_to'] = __("Tarikh Untuk");
                $attributes['details.'.$key.'.cdet_amount.1'] = __("Notis Kompaun");
                $attributes['details.'.$key.'.cdet_amount.2'] = __("Kes Mahkamah");
            }
        }
        
        return $attributes;
    }
}
