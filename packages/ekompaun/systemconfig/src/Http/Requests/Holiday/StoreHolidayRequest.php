<?php

namespace Ekompaun\Systemconfig\Http\Requests\Holiday;

use Illuminate\Foundation\Http\FormRequest;

class StoreHolidayRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hday_name'             => 'required|max:100',
            'hday_date_from'        => 'required|date_format:d-m-Y',
            // 'hday_date_to'          => 'required|date_format:d-m-Y|after_or_equal:hday_date_from',
            'hday_status'           => 'required',
        ];
    }
}
