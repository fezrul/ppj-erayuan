<?php

namespace Ekompaun\Systemconfig\Http\Requests\Profile;

use App\User;
use Ekompaun\Systemconfig\Model\UserProfile;
use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = app(User::class)->getTable();
        $profile = app(UserProfile::class)->getTable();

        return [
            'name'                      => 'required',
            'user_category'             => 'required',
            'email'                     => "required|email|unique:$user,email,".auth()->user()->id,
            'icno'                      => "required|unique:$profile,user_referenceid",
            'user_address1'             => 'required',
            'user_mobileno'             => 'required|numeric',
            'user_postcode'             => 'required|numeric',
            'fk_lkp_state'              => 'required_if:user_citizenship,==,1',
        ];
    }

    public function messages()
    {
        return [
            'fk_lkp_state.required_if' => 'Pilih Negeri'
        ];
    }
}
