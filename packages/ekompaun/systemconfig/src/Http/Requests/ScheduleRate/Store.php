<?php

namespace Ekompaun\Systemconfig\Http\Requests\ScheduleRate;

use Illuminate\Foundation\Http\FormRequest;
use Ekompaun\Systemconfig\Rules\ScheduleRateValidationRule;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fk_lkp_offence'        => 'required',
            'fk_lkp_period'         => ["required", new ScheduleRateValidationRule],
            'fk_lkp_vehicle'        => 'required',
            'schd_usercategory'     => 'required',
            'schd_amount'           => 'required',
        ];
    }
}
