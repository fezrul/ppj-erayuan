<?php

namespace Ekompaun\Systemconfig\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Model\Holiday;
use Ekompaun\Systemconfig\Model\Lookup\State;
use Ekompaun\Systemconfig\Model\Lookup\Status;
use Ekompaun\Systemconfig\Http\Requests\Holiday\StoreHolidayRequest;
use Ekompaun\Systemconfig\Http\Requests\Holiday\UpdateHolidayRequest;

class HolidayController extends Controller
{
    protected $model;
    protected $state;

    public function __construct()
    {
        $this->model = Holiday::class;
        $this->state = State::class;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function index(Request $request)
    {
        $holidays = Holiday::byYear($request->year)->byKeyword($request->search)->get();
        $years = Holiday::dropdownYear();

        return view('systemconfig::holiday.index', compact('holidays', 'years'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $status = [1 => __('Tidak Aktif'), 2 => __('Aktif')];
        return view('systemconfig::holiday.create', compact('status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(StoreHolidayRequest $request)
    {
        $this->model::create($request->all());
        return redirect()->route('holiday.index')->withSuccess(__("Cuti Berjaya Ditambah"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $status = [1 => __('Tidak Aktif'), 2 => __('Aktif')];
        $holiday = $this->model::find($id);
        return view('systemconfig::holiday.edit', compact('holiday', 'status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update(UpdateHolidayRequest $request, $id)
    {
        $holiday = $this->model::find($id);
        $holiday->update($request->all());
        return redirect()->route('holiday.index')->withSuccess(__("Cuti Berjaya Ditukar"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $holiday = $this->model::find($id);
        $holiday->delete();
        return redirect()->back()->withSuccess(__("Cuti Berjaya Dipadamkan"));
    }
}
