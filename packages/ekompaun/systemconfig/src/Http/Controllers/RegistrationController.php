<?php

namespace Ekompaun\Systemconfig\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Ekompaun\Systemconfig\Enum\Citizenship;
use Ekompaun\Systemconfig\Enum\UserCategory;
use Ekompaun\Systemconfig\Model\Lookup\State;
use Ekompaun\Systemconfig\Model\Lookup\Country;
use Ekompaun\Systemconfig\Services\RegistrationService;
use Ekompaun\Systemconfig\Http\Requests\Registration\Store;
use Mail;

class RegistrationController extends Controller
{

    protected $service;

    /**
     * RegistrationController constructor.
     *
     * @param $service
     */
    public function __construct(RegistrationService $service)
    {
        $this->service = $service;
    }

    public function create()
    {
        $citizenship = Input::get('citizenship', Citizenship::RESIDENT);
        $states = State::dropdown();
        $countries = Country::dropdown();
        $category = UserCategory::dropdown();

        return view('systemconfig::registration.create', compact('states', 'countries', 'category', 'citizenship'));
    }

    public function store(Store $form)
    {

        try {
            $this->service->register($form->all());

            $email = $form->email;

                Mail::send(
                    'systemconfig::emails.user.RegisterEmail', array('username' =>$form->icno,'password' => $form->new_password ), function ($message) use ($email) {

                        $message->to($email)->subject(__('Maklumat Pendaftaran Akaun Baru'));

                    }
                );

            return redirect()->route('login')->withSuccess(__("Pendaftaran Berjaya"));
        } catch (\Swift_TransportException $e) {
            return redirect()->back()->withError($e->getMessage())->withInput();
        }
    }
}
