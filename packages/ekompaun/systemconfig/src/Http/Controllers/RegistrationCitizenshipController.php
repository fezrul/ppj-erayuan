<?php

namespace Ekompaun\Systemconfig\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegistrationCitizenshipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function index()
    {
        return view('systemconfig::registration.citizenship');
    }
}
