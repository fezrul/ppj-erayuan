<?php

namespace Ekompaun\Systemconfig\Http\Controllers;

use File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Model\CampaignDetail;

class CampaignDeleteDetailController extends Controller
{
    protected $modelDetail;
    
    public function __construct()
    {
        $this->modelDetail = CampaignDetail::class;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy(Request $request)
    {
        foreach ($request['cdet_id'] as $id) {
            $campaignDetail = $this->modelDetail::find($id);
            $campaignDetail->delete();
        }
    }
}
