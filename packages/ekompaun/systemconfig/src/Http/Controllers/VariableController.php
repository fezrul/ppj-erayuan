<?php

namespace Ekompaun\Systemconfig\Http\Controllers;

use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Model\Custom;

class VariableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function index()
    {
        $customs = Custom::paginate();

        return view('systemconfig::variable.index', compact('customs'));
    }
}
