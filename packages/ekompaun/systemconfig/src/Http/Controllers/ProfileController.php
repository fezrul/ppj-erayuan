<?php

namespace Ekompaun\Systemconfig\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Enum\Citizenship;
use Ekompaun\Systemconfig\Model\UserProfile;
use Ekompaun\Systemconfig\Model\Lookup\State;
use Ekompaun\Systemconfig\Model\Lookup\Country;
use Ekompaun\Systemconfig\Services\MyProfileService;
use Ekompaun\Systemconfig\Http\Requests\Profile\UpdateProfileRequest;

class ProfileController extends Controller
{

    protected $service;

    /**
     * RegistrationController constructor.
     *
     * @param $service
     */
    public function __construct(MyProfileService $service)
    {
        $this->service = $service;
    }

    public function edit($id)
    {
        $user = User::find($id);
        $states = State::dropdown();
        $countries = Country::dropdown();
        return view('systemconfig::profile.edit', compact('user', 'states', 'countries'));
    }

    public function update(UpdateProfileRequest $form, $id)
    {
        try {
            $this->service->update($form->all(), $id);

            return redirect()->back()->withSuccess(__("Berjaya Mengubah Profil"));
        } catch (\Exception $e) {
            return redirect()->back()->withError($e->getMessage())->withInput();
        }
    }
}
