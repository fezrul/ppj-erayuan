<?php

namespace Ekompaun\Systemconfig\Http\Controllers\Pdf;

use PDF;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerPdfController extends Controller
{
    public function index(Request $request)
    {
        $user = User::customer()->search($request->search)->get();

        $pdf = PDF::loadview('systemconfig::pdf.customer', compact('user'))
                  ->setPaper('a4', 'landscape');

        return $pdf->download('Pelanggan.pdf');
    }
}
