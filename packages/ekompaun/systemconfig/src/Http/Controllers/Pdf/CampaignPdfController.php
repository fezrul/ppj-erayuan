<?php

namespace Ekompaun\Systemconfig\Http\Controllers\Pdf;

use PDF;
use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Model\CampaignMaster;

class CampaignPdfController extends Controller
{
    public function index()
    {
        $campaignMaster = CampaignMaster::all();
        $pdf = PDF::loadview('systemconfig::pdf.campaign', compact('campaignMaster'))->setPaper('a4');

        return $pdf->download(__('Senarai Kempen.pdf'));
    }
}
