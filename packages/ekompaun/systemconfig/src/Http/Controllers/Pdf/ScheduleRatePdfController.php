<?php

namespace Ekompaun\Systemconfig\Http\Controllers\Pdf;

use PDF;
use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Services\ScheduleRateService;

class ScheduleRatePdfController extends Controller
{
    public function __construct(ScheduleRateService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $dataExport = $this->service->export(request('act'), request('search'));
        list($offences, $periods, $vehiclesType, $actType) = $dataExport;
        $pdf = PDF::loadview(
            'systemconfig::pdf.scheduleRate',
            compact('offences', 'periods', 'vehiclesType', 'actType')
        )->setPaper('a4', 'landscape');
        return $pdf->download(__('Kadar Rayuan Kompaun.pdf'));
    }
}
