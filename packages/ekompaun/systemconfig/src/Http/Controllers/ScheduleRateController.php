<?php

namespace Ekompaun\Systemconfig\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Enum\Status;
use Ekompaun\Systemconfig\Enum\SectionType;
use Illuminate\Database\Query\paginate;
use Ekompaun\Systemconfig\Model\Lookup\Act;
use Ekompaun\Systemconfig\Enum\UserCategory;
use Ekompaun\Systemconfig\Model\ScheduleRate;
use Ekompaun\Systemconfig\Model\Lookup\Period;
use Ekompaun\Systemconfig\Model\Lookup\Offence;
use Ekompaun\Systemconfig\Model\Lookup\Vehicle;
use Ekompaun\Systemconfig\Services\ScheduleRateService;
use Ekompaun\Systemconfig\Http\Requests\ScheduleRate\Store;
use Ekompaun\Systemconfig\Http\Requests\ScheduleRate\Update;
use Ekompaun\Sap\Http\Controllers\SapController;

class ScheduleRateController extends Controller
{
    protected $service;

    /**
     * ScheduleRateController constructor.
     */
    public function __construct(ScheduleRateService $service,SapController $sap)
    {
        $this->service = $service;
        $this->sap = $sap;
    }

    public function index()
    {
        list($offences, $periods, $vehiclesType, $actType) = $this->service
        ->index(request('act'), request('type'), request('search'));
        $acts = Act::dropdown()->except('');
        $type = SectionType::dropdown();
        $offences->appends(['act'=>request('act'), request('type'), 'search'=>request('search')]);
        return view(
            'systemconfig::schedule-rate.index',
            compact('offences', 'periods', 'vehiclesType', 'acts', 'actType', 'type')
        );
    }

    public function create()
    {
        $offences = Offence::all();
        $periods = Period::pluck('period_name', 'period_id');
        $vehicles = Vehicle::pluck('vehicle_name', 'vehicle_id');
        $vehiclesType = UserCategory::dropdown();
        $acts = Act::dropdown()->except('');
        return view(
            'systemconfig::schedule-rate.create',
            compact('offences', 'periods', 'vehicles', 'vehiclesType', 'acts')
        );
    }

    public function store(Store $request)
    {
        $this->service->store($request);
        return redirect()->route('schedule-rate.index')
        ->withSuccess(__("Jadual Kadar Rayuan Berjaya Ditambah"));
    }

    public function edit($id)
    {
        $scheduleRate = ScheduleRate::findOrFail($id);
        $offences = Offence::pluck('offence_name', 'offence_id');
        $periods = Period::pluck('period_name', 'period_id');
        $vehicles = Vehicle::pluck('vehicle_name', 'vehicle_id');
        $vehiclesType = UserCategory::dropdown();
        $acts = Act::dropdown()->except('');


        //put sap function here
        // $return = $this->sap->getJadual('2', etc,etc);
        return view(
            'systemconfig::schedule-rate.edit',
            compact('scheduleRate', 'offences', 'periods', 'vehicles', 'vehiclesType', 'acts')
        );
    }

    public function update(Update $request, $id)
    {
        $this->service->update($request, $id);
        return redirect()->route('schedule-rate.index')
        ->withSuccess(__("Jadual Kadar Rayuan Berjaya Dikemaskini"));
    }

    public function destroy($id)
    {
        $scheduleRate = ScheduleRate::findOrFail($id);
        $scheduleRate->schd_status = Status::INACTIVE;
        $scheduleRate->save();
        return redirect()->route('schedule-rate.index')
        ->withSuccess(__("Jadual Kadar Rayuan Berjaya Dipadam"));
    }
}
