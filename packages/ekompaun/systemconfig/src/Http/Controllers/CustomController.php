<?php

namespace Ekompaun\Systemconfig\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Model\Custom;

class CustomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function index()
    {
        $customs = Custom::orderBy('lkp_custom_period.custom_id', 'asc')->paginate();

        return view('systemconfig::custom.index', compact('customs'));
    }
}
