<?php

namespace Ekompaun\Systemconfig\Http\Controllers\Csv;

use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Model\Lookup\Act;
use Ekompaun\Systemconfig\Services\ScheduleRateService;
use Illuminate\Database\Query\paginate;
use Excel;

class CsvScheduleRateController extends Controller
{
    protected $service;

    /**
     * ScheduleRateController constructor.
     */
    public function __construct(ScheduleRateService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        Excel::create(
            'Kadar Rayuan Kompaun', function ($excel) {
                $excel->sheet(
                    'New sheet', function ($sheet) {
                        $dataExport = $this->service->export(request('act'), request('search'));
                        list($offences, $periods, $vehiclesType, $actType) = $dataExport;
                        $sheet->loadView(
                            'systemconfig::csv.csv-schedule-rate',
                            compact('offences', 'periods', 'vehiclesType', 'actType')
                        );
                    }
                );
            }
        )->export('csv');
    }
}
