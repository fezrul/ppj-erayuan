<?php

namespace Ekompaun\Systemconfig\Http\Controllers\Csv;

use Ekompaun\Systemconfig\Model\CampaignMaster;
use App\Http\Controllers\Controller;
use Excel;

class CsvCampaignController extends Controller
{

    public function index()
    {
        $campaigns = CampaignMaster::get();
        Excel::create(
            'Campaign', function ($excel) use ($campaigns) {
                $excel->sheet(
                    'New sheet', function ($sheet) use ($campaigns) {
                        $sheet->loadView('systemconfig::csv.csv-campaign', compact('campaigns'));
                    }
                );
            }
        )->export('csv');
    }
}
