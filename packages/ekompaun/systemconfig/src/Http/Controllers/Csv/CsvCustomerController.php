<?php

namespace Ekompaun\Systemconfig\Http\Controllers\Csv;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Excel;

class CsvCustomerController extends Controller
{

    public function index(Request $request)
    {
        Excel::create(
            'Pelanggan', function ($excel) use ($request) {
                $excel->sheet(
                    'New sheet', function ($sheet) use ($request) {
                        $users = User::customer()->search($request->search)->get();
                        $sheet->loadView('systemconfig::csv.csv-customer', compact('users'));
                    }
                );
            }
        )->export('csv');
    }
}
