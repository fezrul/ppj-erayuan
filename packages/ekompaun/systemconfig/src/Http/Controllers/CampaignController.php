<?php

namespace Ekompaun\Systemconfig\Http\Controllers;

use Ekompaun\Systemconfig\Services\CampaignService;
use File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Query\paginate;
use Ekompaun\Systemconfig\Model\Lookup\Status;
use Ekompaun\Systemconfig\Model\CampaignDetail;
use Ekompaun\Systemconfig\Model\CampaignMaster;
use Ekompaun\Systemconfig\Enum\Status as EnumStatus;
use Ekompaun\Systemconfig\Enum\CampaignRateType;
use Ekompaun\Systemconfig\Enum\AppealStatusType;
use Ekompaun\Systemconfig\Services\TypeOfMistakeService;
use Ekompaun\Systemconfig\Http\Requests\Campaign\StoreCampaignRequest;
use Ekompaun\Systemconfig\Http\Requests\Campaign\UpdateCampaignRequest;

class CampaignController extends Controller
{

    public function __construct()
    {
        $this->modelMaster = CampaignMaster::class;
        $this->modelDetail = CampaignDetail::class;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function index()
    {
        $limit = 5;
        $now = Carbon::now();
        $campaign = $this->modelMaster::byStatus(EnumStatus::ACTIVE)->paginate($limit);
        $no = $limit * ($campaign->currentPage() - 1);
        return view('systemconfig::campaign.index', compact('campaign', 'no', 'now'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $campaignTypes = AppealStatusType::dropdown();
        $campaignRates = CampaignRateType::dropdown();

        return view('systemconfig::campaign.create', compact('campaignTypes', 'campaignRates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(StoreCampaignRequest $request, CampaignService $service)
    {
        $service->create(
            // campaign master
            $request->only(['camp_name', 'camp_startdate', 'camp_enddate', 'camp_ratetype', 'camp_content']),
            // campaign detail
            $request->get('details'),
            // campaign file
            $request->file('picture')
        );

        return redirect()->route('campaign.index')->withSuccess(__("Kempen Berjaya Dibuat"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $campaignMaster = CampaignMaster::findOrFail($id);
        $campaignDetail = $campaignMaster->getDetailsGroupByYear();
        $campaignTypes = AppealStatusType::dropdown();
        $campaignRates = CampaignRateType::dropdown();

        // if ($campaignMaster->is_active) {
        //     return redirect()->back()->withError('kempen sedang berjalan');
        // }

        return view('systemconfig::campaign.edit')
        ->with(compact('campaignMaster', 'campaignDetail', 'campaignTypes', 'campaignRates'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, UpdateCampaignRequest $request, CampaignService $service)
    {
        $model = CampaignMaster::findOrFail($id);
        $service->update(
            $model,
            // campaign master
            $request->only(['camp_name', 'camp_startdate', 'camp_enddate', 'camp_ratetype', 'camp_content']),
            // campaign detail
            $request->get('details'),
            // campaign file
            $request->file('picture')
        );
        return redirect()->route('campaign.index')->withSuccess(__("Kempen Berjaya Ditukar"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy(CampaignMaster $campaign)
    {
        if ($campaign->is_active) {
            return redirect()->back()->withError(__("Kempen Sedang Berjalan"));
        }
        $campaign->unpublish();
        return redirect()->back()->withSuccess(__("Berjaya Memadamkan Kempen"));
    }
}
