<?php

namespace Ekompaun\Systemconfig\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Http\Requests\CustomerPassword\Update;

class CustomerPasswordController extends Controller
{
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view('systemconfig::customer-password.edit', compact('user'));
    }

    public function update(Update $request, $id)
    {
        User::findOrFail($id)->setPassword($request->input('new_password'));

        return redirect()->route('customer.index')->withSuccess(__("Berjaya Mengubah Kata Laluan"));
    }
}
