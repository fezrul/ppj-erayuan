<?php

namespace Ekompaun\Systemconfig\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Enum\UserStatus;
use Ekompaun\Systemconfig\Enum\Citizenship;
use Ekompaun\Systemconfig\Enum\UserCategory;
use Ekompaun\Systemconfig\Model\Lookup\State;
use Ekompaun\Systemconfig\Model\Lookup\Country;
use Ekompaun\Systemconfig\Services\MyProfileService;
use Ekompaun\Systemconfig\Http\Requests\Customer\Update;

class CustomerController extends Controller
{
    protected $service;

    public function __construct(MyProfileService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $search = $request->search;
        $user = User::customer()->search($request->search)->paginate();

        return view('systemconfig::customer.index', compact('user', 'search'));
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        $states = State::dropdown();
        $countries = Country::dropdown();
        $citizenship = Citizenship::dropdown();
        $categories = UserCategory::dropdown();
        $status = UserStatus::dropdown();

        return view(
            'systemconfig::customer.show',
            compact('user', 'states', 'countries', 'citizenship', 'categories', 'status')
        );
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        $states = State::dropdown();
        $countries = Country::dropdown();
        $citizenship = Citizenship::dropdown();
        $categories = UserCategory::dropdown();
        $status = UserStatus::dropdown();

        return view(
            'systemconfig::customer.edit',
            compact('user', 'states', 'countries', 'citizenship', 'categories', 'status')
        );
    }

    public function update(Update $form, $id)
    {
        try {
            $user = User::findOrFail($id);
            $this->service->update($user, $form->all());

            return redirect()->back()->withSuccess(__("Berjaya Mengubah Profil"));
        } catch (\Exception $e) {
            return redirect()->back()->withError($e->getMessage())->withInput();
        }
    }
}
