<?php

namespace Ekompaun\Systemconfig\Http\Controllers;

use Setting;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit()
    {
        $roles = app(config("laravolt.epicentrum.models.role"))->pluck("name", "id");
        $pegawai = User::pegawai()->pluck("name", "id");
        return view('systemconfig::setting.edit', compact('roles', 'pegawai'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @return Response
     */
    public function update(Request $request)
    {
        Setting::set('site.name', $request->set_name);
        Setting::set('site.description', $request->set_description);
        Setting::set('site.copyright', $request->set_copyright);
        Setting::set('site.default_role', $request->set_default_role);
        Setting::save();

        return redirect()->back()->withSuccess(__('Berjaya mengubah tetapan sistem'));
    }
}
