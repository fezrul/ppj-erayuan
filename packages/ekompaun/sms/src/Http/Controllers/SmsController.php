<?php

namespace Ekompaun\Sms\Http\Controllers;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Config;

class SmsController extends Controller
{
    public function getSend($type,$appid)
    {


        $created     = (date('Y-m-d H:i:s'));
        $users       = Auth::user();
        $uid         = $users->id;

        $smsoption     = Config::get('ppj.sms');
        if($smsoption == 0) {

            return 2;

        }


        $info = DB::SELECT("SELECT * FROM vw_user_sms where appid = $appid");
        if ($info == []) {
            return 0;
        }


        $phoneno = $info[0]->user_mobileno;
        $compaun = $info[0]->no_kompaun;
        $userid  = $info[0]->fk_users;

        if($phoneno == null) {
            return 0;
        }


        //check phone number

        $plain = preg_replace('/[^\p{N}]/u', '', str_replace('+', '', $phoneno));

        if(substr($plain,  0, 1) == 6) :

            $plain = substr($plain, 1);

        endif;

        $countDigit = 10;
        $countryCode = 11;

        if(strlen(substr($plain, 3)) == 8) :

            $countDigit = 11;
            $countryCode = 12;

        endif;

        if(strlen(substr($plain, 3)) == 1) :

            $countDigit = 11;
            $countryCode = 12;

        endif;


        if(!in_array(strlen($plain), [10,11])) :

            $Phone =  $phoneno;

            else:

                $Phone =  str_pad(str_pad($plain, $countDigit, "0", STR_PAD_LEFT), $countryCode, "60", STR_PAD_LEFT);

            endif;


            if($type == 1) {
                //SMS MAKLUMAN BAYARAN MENGIKUT JADUAL KADAR
                $harga = $info[0]->amount_camp;
                $sms = "PPJ: ".$compaun.". Kadar Selepas Diskaun.RM".$harga.".Sila bayar melalui M2U,CIMB Clicks, BIMB,portal PPj/Wang pos/kaunter PPj.Tel:0388877371";
            }

            // if($type == 1) {
            //     //SMS MAKLUMAN BAYARAN MENGIKUT JADUAL KADAR
            //     $harga = $info[0]->amount_camp;
            //     $sms = "PPJ: ".$compaun.". Rayuan Mengikut Jadual.Sila bayar RM".$harga." melalui M2U,CIMB Clicks, BIMB,portal PPj/Wang pos/kaunter PPj.Tel:0388877371";
            // }
            if($type == 2) {
                //SMS MAKLUMAN PERMOHONAN RAYUAN KE-2
                $sms = "PPJ: ".$compaun.". Status Rayuan: Diterima. Proses kelulusan akan mengambil masa 7 hari bekerja. PPj.Tel:0388877371";
            }
            if($type == 3) {
                //SMS MOHON DOKUMEN TAMBAHAN
                // $sms = "PPJ: ".$compaun.". Status Rayuan: Perlu dokumen tambahan. Sila log masuk ke https://erayuan.ppj.gov.my untuk hantar dokumen. Terima kasih.";

                $sms = "PPJ: ".$compaun.". Status Rayuan: Perlu dokumen tambahan. Sila log masuk ke https://erayuan.ppj.gov.my dalam tempoh 3 hari. Terima kasih.";
            }
            if($type == 4) {
                //SMS Lulus Pengurangan
                $harga = $info[0]->amount_after;
                $sms = "PPJ: ".$compaun.". Status Rayuan:LULUS.Sila bayar RM".$harga." melalui M2U,CIMB Clicks, BIMB,portal PPj/Wang pos/kaunter PPj.Tel:0388877371";
            }
            if($type == 5) {
                //SMS TIDAK LULUS/DITOLAK
                $harga = $info[0]->amount_default;
                $sms = "PPJ: ".$compaun.". Status Rayuan:Rayuan ditolak.Sila bayar RM".$harga." melalui M2U,CIMB Clicks, BIMB,portal PPj/Wang pos/kaunter PPj.Tel:0388877371";
            }
            if($type == 6) {
                //SMS AMARAN
                $sms = "PPJ: ".$compaun.". Status Rayuan:Rayuan digugurkan dengan amaran .Terima kasih.";
            }

            $Message     = urlencode($sms);

            $insert = DB::table('ek_notification_sms')-> insertGetId(
                array(

                'nsms_mobileno' => $Phone,
                'nsms_message' => $sms,
                'fk_applid' => $appid,
                'fk_userid' => $userid,
                'nsms_status' => 1,
                'created_by' => $uid,
                'created_date' => $created,

                )
            );

        return 1;



    }



}
