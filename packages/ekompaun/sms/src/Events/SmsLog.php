<?php

namespace Ekompaun\Sms\Events;

use Illuminate\Queue\SerializesModels;

class SmsLog
{
    use SerializesModels;

    public $smslog;

    /**
     * Create a new event instance.
     *
     * @param  Bid $bid
     * @return void
     */
    public function __construct($smslog)
    {
        $this->smslog = $smslog;
    }
    
}
