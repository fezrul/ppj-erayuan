<?php

namespace Ekompaun\Sms\Listeners;


class SmsEventSubscriber
{
   
    /**
     * Register the listeners for the subscriber.
     *
     * @param Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Ekompaun\Sms\Events\SmsLog',
            'Ekompaun\Sms\Handlers\Events\SmsTransaction',
        );

       
    }
}
