<?php
Route::group(
    [
        'prefix'     => LaravelLocalization::setLocale().'/sms',
        'middleware' => ['web', 'auth', 'localeSessionRedirect', 'localizationRedirect'],
        'namespace'  => 'Ekompaun\Sms\Http\Controllers',
        'as'         => 'sms::',
    ],
    function () {
        Route::any('send/{type}/{id}', [ 'as' => 'smsgate', 'uses' => 'SmsController@getSend']);
              
    }
);
 