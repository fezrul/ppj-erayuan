<?php
Route::group(
    ['middleware' => ['web','guest'], 'namespace' => 'Ekompaun\Auth\Http\Controllers'], function () {
        Route::get('/', 'LoginController@show')->name('login');
        Route::post('/', 'LoginController@store')->name('login');
    }
);
