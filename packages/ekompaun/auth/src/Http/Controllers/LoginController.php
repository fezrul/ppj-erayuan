<?php

namespace Ekompaun\Auth\Http\Controllers;

use Ekompaun\Systemconfig\Enum\UserStatus;
use Illuminate\Routing\Controller;

class LoginController extends Controller
{
    public function show()
    {
        return view('auth::login.show');
    }

    public function store()
    {
        if (auth()->attempt(
            ['icno' => request('icno'),
                             'password' => request('password'),
            'status' => UserStatus::ACTIVE]
        )
        ) {
            return redirect()->intended(route('home'));
        }

        return redirect()->back()->withInput()->withError(__("ID pengguna / kata laluan salah."));
    }
}
