<?php 
namespace Ekompaun\Sap\Data\Model;

use Ekompaun\Platform\Model\BaseModel;

class Sap extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ek_payment';
    protected $primaryKey = 'pymt_id';

    /**
     * Has many and belongs to relationship with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
  
    /**
     * Has One relationship with Elesen\Model\Data\Model\Application.
     */
  
    public function transaction()
    {
        return $this->belongsTo('Ekompaun\Appeal\Model\Transaction', 'fk_tranid');
    }
    public function paymod()
    {
        return $this->belongsTo('Ekompaun\Payment\Data\Model\LkpPaymentMethod', 'fk_paymentmethod');
    }
    public function users()
    {
        return $this->belongsTo('Ekompaun\Payment\Data\Model\Users', 'fk_users');
    }
   
   
}
