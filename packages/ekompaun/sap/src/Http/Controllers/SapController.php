<?php

namespace Ekompaun\Sap\Http\Controllers;
use App\Http\Controllers\Controller;
use Ekompaun\Appeal\Model\Transaction;
use SAPNWRFC;
use DB;
use Auth;
use Config;

class SapController extends Controller
{
    public function getFunction($fid,$noid,$param1,$param2)
    {
        
        $ashost = Config::get('ppj.host'); 
        $client = Config::get('ppj.clent'); 
        $user = Config::get('ppj.username'); 
        $passwd = Config::get('ppj.password'); 
        
        $fnoid =  $noid;
        $param1 = $param1;
        $param2 = $param2;
        $created = (date('Y-m-d H:i:s'));
        
        if (Auth::guest()) { 

            $uid = 1;
            $search = 1;
            $personal = 0;

        }else{

            $users = Auth::user();
            $uid = $users->id;
            $personal = 0;
            $search = 0;
            $checkprofile = DB::SELECT("SELECT * FROM user_profiles 
                                        WHERE fk_users = $users->id
                                        AND ((`user_companyssmno` = '$param1') 
                                        OR (`user_companyssmno` = '$param2')
                                        OR  (`user_vehicleno1` = '$param1') 
                                        OR (`user_vehicleno1` = '$param2')
                                        OR  (`user_vehicleno2` = '$param1') 
                                        OR (`user_vehicleno2` = '$param2')
                                        OR  (`user_vehicleno3` = '$param1') 
                                        OR (`user_vehicleno3` = '$param2'))
                                       ");

            if($checkprofile){$personal = 1;}

        }

        $sapoption     = Config::get('ppj.sap'); 
        if($sapoption == 0) {

            return 2;

        }

        $parameters = ['ashost' => $ashost,'sysnr'  => '00','client' => $client,'user' => $user,'passwd' => $passwd];

        try 
        {

            $connection = new SAPNWRFC\Connection($parameters);
            $constatus = 1;
               
        } catch (SAPNWRFC\ConnectionException $e) {
                                
               $constatus = $e->getErrorInfo()['message'];
                   
        }

                
        if($fid == 1) {
            if($constatus == 1) 
            {
                $param1 = strtoupper($param1);
                $param2 = strtoupper($param2);

                $fname = 'YBAPI_ESEMAK_KOMPAUN';
                $function = $connection->getFunction($fname);

                if(($fnoid == 1) OR ($fnoid == 5) OR ($fnoid == 7)) {
                    $result = $function->invoke(['NO_ID' => $fnoid,'IDNUM' => $param1]);

                }elseif(($fnoid == 2) OR ($fnoid == 6)) {
                    
                    $result = $function->invoke(['NO_ID' => $fnoid,'IDNUM' => $param1,'VREGNO' => $param2]);

                }elseif($fnoid == 3) {
                    $result = $function->invoke(['NO_ID' => $fnoid,'VREGNO' => $param2]);
                        
                }else
                {
                    $result = $function->invoke(['NO_ID' => $fnoid,'COMPNO' => $param2]);
                        
                }  


                    
                $process = $result['ARRAY1'];

                if ($process == []) 
                {
                    $errmsg = $result['RETURN'];

                    foreach ($errmsg as $key => $value) 
                    {

                        $err = trim($value['MESSAGE']);
                        $insert = DB::table('semak_sap')-> insertGetId(
                            array(

                            'remarks' => 'Function SEMAK : '.$err.',using function : '.$fname,
                            'status' => 0,
                            'fk_lkp_taskid' => 19,
                            'fk_lkp_parameterid' => $fnoid,
                            'parameter_value' => $param1.'|'.$param2,
                            'created_date' => $created,
                            'created_by' => $uid,
                                   
                                )
                        );

                    }
                   
                    return 0;

                }
                else
                {

                            
                    foreach ($process as $key => $value) 
                    {

                        $tran_offendername = trim($value['CUSTNAME']);
                        $tran_place = trim($value['OFFPLACE']);
                        $status = trim($value['STATUS']);
                        $str = trim($value['OFFCODE']);
                        $vtype = strtolower(trim($value['VCLTYP']));
                        $fk_lkp_sapstatus = 22;
                        $lkpstatuslkp = DB::SELECT("SELECT status_id from lkp_status_sap where status_code = '$status' limit 1");
                        $lkpstatus = $lkpstatuslkp[0]->status_id;
                                
                        $ofcode = DB::SELECT("SELECT offence_id from lkp_offence where offence_code = '$str' limit 1");
                        if(($ofcode == null) OR ($ofcode == [])) {
                            $fk_lkp_offencecode = 1;

                        }else{

                            $fk_lkp_offencecode = $ofcode[0]->offence_id;

                        }

                        $vehiclelist = DB::SELECT("SELECT vehicle_id from lkp_vehicle where LOWER(vehicle_name) = '$vtype' limit 1");
                        if(($vehiclelist == null) OR ($vehiclelist == [])) {

                            $fk_lkp_vehicletype = 6;

                        }else{

                            $fk_lkp_vehicletype = $vehiclelist[0]->vehicle_id;

                        }
                                

                        $tran_offendericno = trim($value['IDNUMBER']);
                        $tran_compoundno = trim($value['COMPSNO']);
                        $tran_compounddate = date("Y-m-d", strtotime(trim($value['DOCDATE'])));
                        $tran_compoundamount = trim($value['TCOMPDUE']);
                        $tran_compoundamount_prev = trim($value['AMTCOMP']);
                        $tran_refno = trim($value['NORUJUKAN']);
                        $tran_vehicleno = trim($value['VCLREGNO']);
                        $tran_vehiclecolor = trim($value['VCLCOLR']);


                        $checkexist = DB::SELECT("SELECT * from ek_transaction where tran_compoundno = '$tran_compoundno'");

                        
                        if($checkexist) 
                        {
                            
                            $tids = $checkexist[0]->tran_id;
                            $checkappl = DB::SELECT("SELECT * from ek_application where appl_transid = '$tids'");
                                        
                            if($checkappl) 
                            {
                                $updatew = DB::INSERT("INSERT INTO test SET check_et = 'yes',check_ea = 'yes',`update` = 'NO',kompaun = '$tran_compoundno'");
                                // add update table ek_transaction 
                                // check appl_status = 8,9
                                // 8-8-2018
                                //start
                                $latestappl = DB::SELECT("SELECT * from ek_application where appl_transid = '$tids' ORDER BY appl_date DESC LIMIT 1");
                                $currentstatus = $latestappl[0]->appl_status;
                                if(($currentstatus == 8) OR ($currentstatus == 9) OR ($currentstatus == 4))
                                {
                                    // Update ek_transaction
                                    $data = Transaction::find($tids);

                                    $data->fk_lkp_sapstatus             = $lkpstatus;
                                    $data->fk_rawsapid                  = 4;
                                    $data->fk_lkp_offencecode           = $str;
                                    $data->tran_offendername            = $tran_offendername;
                                    $data->tran_offendericno            = $tran_offendericno;
                                    $data->tran_compoundno              = $tran_compoundno;
                                    $data->tran_compounddate            = $tran_compounddate;
                                    $data->tran_compoundamount          = $tran_compoundamount;
                                    $data->tran_compoundamount_prev     = $tran_compoundamount_prev;
                                    $data->tran_place                   = $tran_place;
                                    $data->tran_vehicleno               = $tran_vehicleno;
                                    $data->tran_refno                   = $tran_refno;
                                    $data->fk_lkp_vehicletype           = $fk_lkp_vehicletype;
                                    $data->tran_vehiclecolor            = $tran_vehiclecolor;
                                    $data->modify_date                  = $created;
                                    $data->save();  


                                }
                                //end



                            }else
                            {

                                $updatew = DB::INSERT("INSERT INTO test SET check_et = 'yes',check_ea = 'no',`update` = $uid,kompaun = '$tran_compoundno'");


                                // $update = DB::UPDATE("UPDATE ek_transaction SET fk_userid = $uid where tran_id = $tids");
                                // add update table ek_transaction 
                                // check ek_application !exists
                                // 8-8-2018
                                //start

                                if($search == 1)
                                {

                                    $data = Transaction::find($tids);

                                    $data->fk_lkp_sapstatus             = $lkpstatus;
                                    $data->fk_rawsapid                  = 4;
                                    $data->fk_lkp_offencecode           = $str;
                                    $data->tran_offendername            = $tran_offendername;
                                    $data->tran_offendericno            = $tran_offendericno;
                                    $data->tran_compoundno              = $tran_compoundno;
                                    $data->tran_compounddate            = $tran_compounddate;
                                    $data->tran_compoundamount          = $tran_compoundamount;
                                    $data->tran_compoundamount_prev     = $tran_compoundamount_prev;
                                    $data->tran_place                   = $tran_place;
                                    $data->tran_vehicleno               = $tran_vehicleno;
                                    $data->tran_refno                   = $tran_refno;
                                    $data->fk_lkp_vehicletype           = $fk_lkp_vehicletype;
                                    $data->tran_vehiclecolor            = $tran_vehiclecolor;
                                    $data->modify_date                  = $created;
                                   
                                    $data->save();  

                                }else
                                {

                                    if($personal == 1)
                                    {
                                        $data = Transaction::find($tids);

                                        $data->fk_lkp_sapstatus             = $lkpstatus;
                                        $data->fk_rawsapid                  = 4;
                                        $data->fk_userid                    = $uid;
                                        $data->fk_lkp_offencecode           = $str;
                                        $data->tran_offendername            = $tran_offendername;
                                        $data->tran_offendericno            = $tran_offendericno;
                                        $data->tran_compoundno              = $tran_compoundno;
                                        $data->tran_compounddate            = $tran_compounddate;
                                        $data->tran_compoundamount          = $tran_compoundamount;
                                        $data->tran_compoundamount_prev     = $tran_compoundamount_prev;
                                        $data->tran_place                   = $tran_place;
                                        $data->tran_vehicleno               = $tran_vehicleno;
                                        $data->tran_refno                   = $tran_refno;
                                        $data->fk_lkp_vehicletype           = $fk_lkp_vehicletype;
                                        $data->tran_vehiclecolor            = $tran_vehiclecolor;
                                        $data->modify_date                  = $created;
                                       
                                        $data->save(); 

                                    }
                                    else
                                    {
                                        $data = Transaction::find($tids);

                                        $data->fk_lkp_sapstatus             = $lkpstatus;
                                        $data->fk_rawsapid                  = 4;
                                        $data->fk_userid                    = $uid;
                                        $data->fk_lkp_offencecode           = $str;
                                        $data->tran_offendername            = $tran_offendername;
                                        $data->tran_offendericno            = $tran_offendericno;
                                        $data->tran_compoundno              = $tran_compoundno;
                                        $data->tran_compounddate            = $tran_compounddate;
                                        $data->tran_compoundamount          = $tran_compoundamount;
                                        $data->tran_compoundamount_prev     = $tran_compoundamount_prev;
                                        $data->tran_place                   = $tran_place;
                                        $data->tran_vehicleno               = $tran_vehicleno;
                                        $data->tran_refno                   = $tran_refno;
                                        $data->fk_lkp_vehicletype           = $fk_lkp_vehicletype;
                                        $data->tran_vehiclecolor            = $tran_vehiclecolor;
                                        $data->modify_date                  = $created;
                                       
                                        $data->save(); 

                                    }



                                }

                                //end


                            }
                           


                        }else
                        {

                            $updatew = DB::INSERT("INSERT INTO test SET check_et = 'no',check_ea = 'no',`update` = 'NO',kompaun = '$tran_compoundno'");
                            if($search == 1)
                            { 

                                $insert = DB::table('ek_transaction')-> insertGetId
                                (
                                    array
                                    (

                                            'fk_lkp_sapstatus' => $lkpstatus,
                                            'fk_rawsapid' => 4,
                                            'fk_lkp_offencecode' => $str,
                                            'tran_offendername' => $tran_offendername,
                                            'tran_offendericno' => $tran_offendericno,
                                            'tran_compoundno' => $tran_compoundno,
                                            'tran_compounddate' => $tran_compounddate,
                                            'tran_compoundamount' => $tran_compoundamount,
                                            'tran_compoundamount_prev' => $tran_compoundamount_prev,
                                            'tran_place' => $tran_place,
                                            'tran_vehicleno' => $tran_vehicleno,
                                            'tran_refno' => $tran_refno,
                                            'fk_lkp_vehicletype' => $fk_lkp_vehicletype,
                                            'tran_vehiclecolor' => $tran_vehiclecolor,
                                               
                                    )
                                );              

           

                            }else
                            {
                                if($personal == 1)
                                { 
                                    $insert = DB::table('ek_transaction')-> insertGetId
                                    (
                                        array
                                        (

                                                'fk_lkp_sapstatus' => $lkpstatus,
                                                'fk_rawsapid' => 4,
                                                'fk_userid' => $uid,
                                                'fk_lkp_offencecode' => $str,
                                                'tran_offendername' => $tran_offendername,
                                                'tran_offendericno' => $tran_offendericno,
                                                'tran_compoundno' => $tran_compoundno,
                                                'tran_compounddate' => $tran_compounddate,
                                                'tran_compoundamount' => $tran_compoundamount,
                                                'tran_compoundamount_prev' => $tran_compoundamount_prev,
                                                'tran_place' => $tran_place,
                                                'tran_vehicleno' => $tran_vehicleno,
                                                'tran_refno' => $tran_refno,
                                                'fk_lkp_vehicletype' => $fk_lkp_vehicletype,
                                                'tran_vehiclecolor' => $tran_vehiclecolor,
                                                   
                                        )
                                    );
                                }else
                                {
                                    $insert = DB::table('ek_transaction')-> insertGetId
                                    (
                                        array
                                        (

                                                'fk_lkp_sapstatus' => $lkpstatus,
                                                'fk_rawsapid' => 4,
                                                'fk_lkp_offencecode' => $str,
                                                'fk_userid' => $uid,
                                                'tran_offendername' => $tran_offendername,
                                                'tran_offendericno' => $tran_offendericno,
                                                'tran_compoundno' => $tran_compoundno,
                                                'tran_compounddate' => $tran_compounddate,
                                                'tran_compoundamount' => $tran_compoundamount,
                                                'tran_compoundamount_prev' => $tran_compoundamount_prev,
                                                'tran_place' => $tran_place,
                                                'tran_vehicleno' => $tran_vehicleno,
                                                'tran_refno' => $tran_refno,
                                                'fk_lkp_vehicletype' => $fk_lkp_vehicletype,
                                                'tran_vehiclecolor' => $tran_vehiclecolor,
                                                   
                                        )
                                    );


                                }              
                            }
                        }
                                
                    }

                    $insert = DB::table('semak_sap')-> insertGetId(
                        array(

                        'remarks' => 'Function SEMAK : Successfully Retrived Data SAP',
                        'status' => 1,
                        'fk_lkp_taskid' => 19,
                        'fk_lkp_parameterid' => $fnoid,
                        'parameter_value' => $param1.'|'.$param2,
                        'created_date' => $created,
                        'created_by' => $uid,
                               
                            )
                    );

                    return 1;
                }



            }else
            {


                $insert = DB::table('semak_sap')-> insertGetId(
                    array(

                    'remarks' => 'Function SEMAK : Error : '.$constatus,
                    'status' => 0,
                    'fk_lkp_taskid' => 19,
                    'fk_lkp_parameterid' => $fnoid,
                    'parameter_value' => $param1.'|'.$param2,
                    'created_date' => $created,
                    'created_by' => $uid,
                       
                    )
                );

                return 0;
            }

           
        }else{


            if($constatus == 1) {
                    
                $fname = 'YBAPI_ESEMAK_KOMPAUN_UPD';
                $afloat = floatval($param2);
                $function = $connection->getFunction($fname);
                $result = $function->invoke(['KEY' => $fnoid,'COMPNO' => $param1,'AMOUNT' => $afloat,'CHGBY' => 'ERK' ]);


                 $process = $result['ARRAY2'];


                if ($process == []) 
                {
                    $errmsg = $result['RETURN'];

                    foreach ($errmsg as $key => $value) 
                    {

                        $err = trim($value['MESSAGE']);
                        $insert = DB::table('semak_sap')-> insertGetId(
                            array(

                            'remarks' => 'Function update : '.$err.',using function : '.$fname,
                            'status' => 0,
                            'fk_lkp_taskid' => 19,
                            'fk_lkp_parameterid' => $fnoid,
                            'parameter_value' => $param1.'|'.$param2,
                            'created_date' => $created,
                            'created_by' => $uid,
                                   
                                )
                        );

                    }
                            
                    return 0;

                }else{

                            
                    foreach ($process as $key => $value) 
                    {

                        $insert = DB::table('semak_sap')-> insertGetId(
                        array(

                        'remarks' => 'Function update :Successfully Update Sap Data',
                        'status' => 1,
                        'fk_lkp_taskid' => 19,
                        'fk_lkp_parameterid' => $fnoid,
                        'parameter_value' => $param1.'|'.$param2,
                        'created_date' => $created,
                        'created_by' => $uid,
                               
                            )
                    );
                   
                        return 1;
                        
                    }
                }

            }else{

                $insert = DB::table('semak_sap')-> insertGetId(
                    array(

                    'remarks' => 'Function update :Error : '.$constatus,
                    'status' => 0,
                    'fk_lkp_taskid' => 19,
                    'fk_lkp_parameterid' => $fnoid,
                    'parameter_value' => $param1.'|'.$param2,
                    'created_date' => $created,
                    'created_by' => $uid,
                       
                    )
                );

                   return 0;
            }

                
        }
    }
    
    public function getJadual($fid,$key,$ioffcode,$zseksyen,$from1,$to1,$z1a,$z1b,$from2,$to2,$z2a,$z2b,$from3,$to3,$z3a,$z3b,$from4,$to4,$z4a,$zmah1,$zmah2)
    {

        $ashost = Config::get('ppj.host'); 
        $client = Config::get('ppj.clent'); 
        $user = Config::get('ppj.username'); 
        $passwd = Config::get('ppj.password'); 
        
        $created = (date('Y-m-d H:i:s'));
        
        if (Auth::guest()) { 

            $uid = (NULL);

        }else{

            $users = Auth::user();
            $uid = $users->id;

        }

        $sapoption     = Config::get('ppj.sap'); 
        if($sapoption == 0) {

            return 2;

        }

        $parameters = ['ashost' => $ashost,'sysnr'  => '00','client' => $client,'user' => $user,'passwd' => $passwd];

        try 
        {

            $connection = new SAPNWRFC\Connection($parameters);
            $constatus = 1;
               
        } catch (SAPNWRFC\ConnectionException $e) {
                                
               $constatus = $e->getErrorInfo()['message'];
                   
        }

        //function update / delete / New
        if($constatus == 1) {

            if($fid == 1) {
            }

            if($fid == 2) {


                $fname = 'YBAPI_JADUAL_AKTA_KOMPAUN_UPD';
                $function = $connection->getFunction($fname);


                $result = $function->invoke([
                                            'KEY'       => $key,
                                            'IOFFCODE'  => $ioffcode,
                                            'ZSEKSYEN'  => $zseksyen,
                                            'FROM1'     => $from1,
                                            'TO1'       => $to1,
                                            'Z1A'       => $z1a,
                                            'Z1B'       => $z1b,
                                            'FROM2'     => $from2,
                                            'TO2'       => $to2,
                                            'Z2A'       => $z2a,
                                            'Z2B'       => $z2b,
                                            'FROM3'     => $from3,
                                            'TO3'       => $to3,
                                            'Z3A'       => $z3a,
                                            'Z3B'       => $z3b,
                                            'FROM4'     => $from4,
                                            'TO4'       => $to4,
                                            'Z4A'       => $z4a,
                                            'ZMAH1'     => $zmah1,
                                            'ZMAH2'     => $zmah2
                                            ]);



                $process = $result['ARRAY1'];

                // if ($process == []) {

                //     $insert = DB::table('semak_sap')-> insertGetId(
                //         array(

                //         'remarks' => 'No data found in SAP,using function : '.$fname,
                //         'status' => 1,
                //         'fk_lkp_taskid' => 19,
                //         'fk_lkp_parameterid' => $fnoid,
                //         'parameter_value' => $param1.'|'.$param2,
                //         'created_date' => $created,
                //         'created_by' => $uid,
                               
                //             )
                //     );
                            
                //     return 0;

                // }
                // else{

                // }


            }

            if($fid == 3) {
            }

            return 1;

        }else{

                $insert = DB::table('semak_sap')-> insertGetId(
                    array(

                    'remarks' => 'Error : '.$constatus,
                    'status' => 0,
                    'fk_lkp_taskid' => 20,
                    'fk_lkp_parameterid' => $key,
                    'parameter_value' => $key.'|'.$ioffcode.'|'.$zseksyen.'|'.$from1.'|'.$to1.'|'.$z1a.'|'.$z1b.'|'.$from2.'|'.$to2.'|'.$z2a.'|'.$z2b.'|'.$from3.'|'.$to3.'|'.$z3a.'|'.$z3b.'|'.$from4.'|'.$to4.'|'.$z4a.'|'.$zmah1.'|'.$zmah2,
                    'created_date' => $created,
                    'created_by' => $uid,
                       
                    )
                );

                   return 0;
            }




    }

    public function getRevert($komno,$komprc,$sapprc,$sapstat)
    {
        $ashost = Config::get('ppj.host'); 
        $client = Config::get('ppj.clent'); 
        $user = Config::get('ppj.username'); 
        $passwd = Config::get('ppj.password'); 
        
        $fnoid = '6';
                
        $created = (date('Y-m-d H:i:s'));
        
        if (Auth::guest()) { 

            $uid = (NULL);
            $uids  = 1;

        }else{

            $users = Auth::user();
            $uid = $users->id;
            $uids = $users->id;

        }

        $sapoption     = Config::get('ppj.sap'); 
        if($sapoption == 0) {

            return 2;

        }

        $parameters = ['ashost' => $ashost,'sysnr'  => '00','client' => $client,'user' => $user,'passwd' => $passwd];

        try 
        {

            $connection = new SAPNWRFC\Connection($parameters);
            $constatus = 1;
               
        } catch (SAPNWRFC\ConnectionException $e) {
                                
               $constatus = $e->getErrorInfo()['message'];
                   
        }


        if($constatus == 1) {
                    
                $fname = 'YBAPI_ESEMAK_KOMPAUN_UPD';
                $amount1 = floatval($komprc);
                $amount2 = floatval($sapprc);
                $function = $connection->getFunction($fname);
                $result = $function->invoke(['KEY' => $fnoid,'COMPNO' => $komno,'AMOUNT' => $amount1,'STATUS' =>$sapstat,'CHGBY' => 'ERK','AMOUNT2' => $amount2 ]);
                
                $process = $result['ARRAY2'];

                if ($process == []) {


                    $errmsg = $result['RETURN'];

                    foreach ($errmsg as $key => $value) 
                    {

                        $err = trim($value['MESSAGE']);
                         $insert = DB::table('semak_sap')-> insertGetId(
                        array(

                        'remarks' => '(Revert)Error : '.$err.' : '.$fname,
                        'status' => 0,
                        'fk_lkp_taskid' => 19,
                        'fk_lkp_parameterid' => $fnoid,
                        'parameter_value' => $komno.'|'.$amount1.'|'.$sapstat.'|'.$amount2,
                        'created_date' => $created,
                        'created_by' => $uid,
                               
                            )
                    );
                       

                    }





      
                            
                    return 0;

                }else{

                            
                    foreach ($process as $key => $value) 
                    {

                        $insert = DB::table('semak_sap')-> insertGetId(
                        array(

                        'remarks' => '(Revert)Successfully Update Sap Data',
                        'status' => 1,
                        'fk_lkp_taskid' => 19,
                        'fk_lkp_parameterid' => $fnoid,
                        'parameter_value' => $komno.'|'.$amount1.'|'.$sapstat.'|'.$amount2,
                        'created_date' => $created,
                        'created_by' => $uid,
                               
                            )
                    );
                   
                        return 1;
                        
                    }
                }
                        
            }else{

                $insert = DB::table('semak_sap')-> insertGetId(
                    array(

                    'remarks' => '(Revert)Error : '.$constatus,
                    'status' => 0,
                    'fk_lkp_taskid' => 19,
                    'fk_lkp_parameterid' => $fnoid,
                    'parameter_value' => $komno.'|'.$amount1.'|'.$sapstat.'|'.$amount2,
                    'created_date' => $created,
                    'created_by' => $uid,
                       
                    )
                );

                   return 0;
            }


    }

    public function getSap1($fid,$noid,$param1,$param2)
    {
        // dd('found');
        
        $ashost = Config::get('ppj.host'); 
        $client = Config::get('ppj.clent'); 
        $user = Config::get('ppj.username'); 
        $passwd = Config::get('ppj.password'); 
        
        $fnoid =  $noid;
        $param1 = $param1;
        $param2 = $param2;
        $created = (date('Y-m-d H:i:s'));
        
        if (Auth::guest()) { 

            $uid = (NULL);

        }else{

            $users = Auth::user();
            $uid = $users->id;

        }

        $sapoption     = Config::get('ppj.sap'); 
        if($sapoption == 0) {

            return 2;

        }

        $parameters = ['ashost' => $ashost,'sysnr'  => '00','client' => $client,'user' => $user,'passwd' => $passwd];

        try 
        {

            $connection = new SAPNWRFC\Connection($parameters);
            $constatus = 1;
               
        } catch (SAPNWRFC\ConnectionException $e) {
                                
               $constatus = $e->getErrorInfo()['message'];
                   
        }

                
        if($fid == 1) {
            if($constatus == 1) {

                $fname = 'YBAPI_ESEMAK_KOMPAUN';
                $function = $connection->getFunction($fname);

                if(($fnoid == 1) OR ($fnoid == 5) OR ($fnoid == 7)) {
                    $result = $function->invoke(['NO_ID' => $fnoid,'IDNUM' => $param1]);

                }elseif(($fnoid == 2) OR ($fnoid == 6)) {
                    
                    $result = $function->invoke(['NO_ID' => $fnoid,'IDNUM' => $param1,'VREGNO' => $param2]);

                }elseif($fnoid == 3) {
                    $result = $function->invoke(['NO_ID' => $fnoid,'VREGNO' => $param2]);
                        
                }else
                {
                    $result = $function->invoke(['NO_ID' => $fnoid,'COMPNO' => $param2]);
                        
                }  


                    
                $process = $result['ARRAY1'];

                dd($result);

                if ($process == []) {

                    $insert = DB::table('semak_sap')-> insertGetId(
                        array(

                        'remarks' => 'No data found in SAP,using function : '.$fname,
                        'status' => 1,
                        'fk_lkp_taskid' => 19,
                        'fk_lkp_parameterid' => $fnoid,
                        'parameter_value' => $param1.'|'.$param2,
                        'created_date' => $created,
                        'created_by' => $uid,
                               
                            )
                    );
                            
                    return 0;

                }
                else{

                            
                    foreach ($process as $key => $value) {

                        $tran_offendername = trim($value['CUSTNAME']);
                        $tran_place = trim($value['OFFPLACE']);
                        $status = trim($value['STATUS']);
                        $str = trim($value['OFFCODE']);
                        $fk_lkp_sapstatus = 22;
                        $lkpstatuslkp = DB::SELECT("SELECT status_id from lkp_status_sap where status_code = '$status' limit 1");
                        $lkpstatus = $lkpstatuslkp[0]->status_id;
                                
                        $ofcode = DB::SELECT("SELECT offence_id from lkp_offence where offence_code = '$str' limit 1");
                        if(($ofcode == null) OR ($ofcode == [])) {
                            $fk_lkp_offencecode = 1;

                        }else{

                            $fk_lkp_offencecode = $ofcode[0]->offence_id;

                        }
                                

                        $tran_offendericno = trim($value['IDNUMBER']);
                        $tran_compoundno = trim($value['COMPSNO']);
                        $tran_compounddate = date("Y-m-d", strtotime(trim($value['DOCDATE'])));
                        $tran_compoundamount = trim($value['TCOMPDUE']);
                        $tran_refno = trim($value['NORUJUKAN']);
                        $tran_vehicleno = trim($value['VCLREGNO']);
                        $fk_lkp_vehicletype = 6;
                        $tran_vehiclecolor = trim($value['VCLCOLR']);


                        $checkexist = DB::SELECT("SELECT COUNT(*) as counts from ek_transaction where tran_compoundno = '$tran_compoundno'");

                        if($checkexist[0]->counts > 0) {
                            $tid = DB::SELECT("SELECT tran_id from ek_transaction where tran_compoundno = '$tran_compoundno'");
                            $tids = $tid[0]->tran_id;

                            $checkappl = DB::SELECT("SELECT COUNT(*) as counts from ek_application where appl_transid = '$tids'");
                                        
                            if($checkappl[0]->counts > 0) {

                                // $update = DB::UPDATE("UPDATE ek_transaction SET tran_compoundamount = $tran_compoundamount,fk_lkp_sapstatus = $lkpstatus where tran_id = $tids");

                            }else{

                                if (Auth::guest()) 
                                { 



                                }else{

                                    $users = Auth::user();
                                    $uid = $users->id;
                                    $update = DB::UPDATE("UPDATE ek_transaction SET fk_userid = $uid where tran_id = $tids");

                                }

                                // $update = DB::UPDATE("UPDATE ek_transaction SET tran_compoundamount = $tran_compoundamount,fk_userid = $uid,fk_lkp_sapstatus = $lkpstatus where tran_id = $tids");
                            }



                            return 0;



                        }else
                        {

                            $insert = DB::table('ek_transaction')-> insertGetId(
                                array(

                                'fk_lkp_sapstatus' => $lkpstatus,
                                'fk_rawsapid' => 4,
                                'fk_userid' => $uid,
                                // 'fk_lkp_offencecode' => $fk_lkp_offencecode,
                                'fk_lkp_offencecode' => $str,
                                'tran_offendername' => $tran_offendername,
                                'tran_offendericno' => $tran_offendericno,
                                'tran_compoundno' => $tran_compoundno,
                                'tran_compounddate' => $tran_compounddate,
                                'tran_compoundamount' => $tran_compoundamount,
                                'tran_place' => $tran_place,
                                'tran_vehicleno' => $tran_vehicleno,
                                'tran_refno' => $tran_refno,
                                'fk_lkp_vehicletype' => $fk_lkp_vehicletype,
                                'tran_vehiclecolor' => $tran_vehiclecolor,
                                           
                                        )
                            );

                                       

                        }
                                
                    }

                    $insert = DB::table('semak_sap')-> insertGetId(
                        array(

                        'remarks' => 'Successfully Retrived Data SAP',
                        'status' => 1,
                        'fk_lkp_taskid' => 19,
                        'fk_lkp_parameterid' => $fnoid,
                        'parameter_value' => $param1.'|'.$param2,
                        'created_date' => $created,
                        'created_by' => $uid,
                               
                            )
                    );

                     return 1;
                }



            }else{

                $insert = DB::table('semak_sap')-> insertGetId(
                    array(

                    'remarks' => 'Error : '.$constatus,
                    'status' => 0,
                    'fk_lkp_taskid' => 19,
                    'fk_lkp_parameterid' => $fnoid,
                    'parameter_value' => $param1.'|'.$param2,
                    'created_date' => $created,
                    'created_by' => $uid,
                       
                    )
                );

                return 0;
            }

           
        }else{


            if($constatus == 1) {
                    
                $fname = 'YBAPI_ESEMAK_KOMPAUN_UPD';
                $afloat = floatval($param2);
                $function = $connection->getFunction($fname);
                $result = $function->invoke(['KEY' => $fnoid,'COMPNO' => $param1,'AMOUNT' => $afloat,'CHGBY' => 'ERK' ]);

                $process = $result['ARRAY2'];

                dd($result);

                 $insert = DB::table('semak_sap')-> insertGetId(
                        array(

                        'remarks' => 'Successfully Update Sap Data',
                        'status' => 1,
                        'fk_lkp_taskid' => 19,
                        'fk_lkp_parameterid' => $fnoid,
                        'parameter_value' => $param1.'|'.$param2,
                        'created_date' => $created,
                        'created_by' => $uid,
                               
                            )
                    );
                   
                return 1;
                        
            }else{

                $insert = DB::table('semak_sap')-> insertGetId(
                    array(

                    'remarks' => 'Error : '.$constatus,
                    'status' => 0,
                    'fk_lkp_taskid' => 19,
                    'fk_lkp_parameterid' => $fnoid,
                    'parameter_value' => $param1.'|'.$param2,
                    'created_date' => $created,
                    'created_by' => $uid,
                       
                    )
                );

                   return 0;
            }

                
        }
    }


    
}
