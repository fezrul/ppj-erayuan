<?php

namespace Ekompaun\Sap\Events;

use Illuminate\Queue\SerializesModels;

class SapLog
{
    use SerializesModels;

    public $saplog;

    /**
     * Create a new event instance.
     *
     * @param  Bid $bid
     * @return void
     */
    public function __construct($saplog)
    {
        $this->saplog = $saplog;
    }
    
}
