<?php

namespace Ekompaun\Sap\Listeners;


class SapEventSubscriber
{
   
    /**
     * Register the listeners for the subscriber.
     *
     * @param Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Ekompaun\Sap\Events\SapLog',
            'Ekompaun\Sap\Handlers\Events\SapTransaction',
        );

       
    }
}
