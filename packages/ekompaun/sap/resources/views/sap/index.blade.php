@extends('layouts.app')
@section('content')
    <div class="container-fluid mt-5">
        <div class="card">
            <div class="card-header">
                <h5 class="mb-0">Maklumat Bayaran</h5>
                <div class="card-actions"></div>
            </div>
<div class="card-body">
     <div class="row">
                    <div class="col"><h4>Butiran Kompaun</h4></div>
                </div>
                <div class="row">
                    <div class="col-sm-2">{{ __("Nama Pelanggan") }} </div>
                    <div class="col-sm">: {{data_get($datapay,'transaction.user.name')}}</div>
                    <div class="col-sm-2">{{ __("Tarikh Kompaun") }} </div>
                    <div class="col-sm">: {{ format_date(data_get($datapay,'transaction.tran_compounddate'))}}</div>
                   <div class="w-100"></div>
                    <div class="col-sm-2">{{ __("No Kad Pengenalan") }} </div>
                    <div class="col-sm">: {{data_get($datapay,'transaction.user.icno')}}</div>
                    <div class="col-sm-2">{{ __("No Kompaun") }} </div>
                    <div class="col-sm">: {{data_get($datapay,'transaction.tran_compoundno')}}</div>
                    <div class="w-100"></div>
                    <div class="col-sm-2">{{ __("Kod Kesalahan") }} </div>
                    <div class="col-sm">: {{data_get($datapay,'transaction.offence.offence_code')}}</div>
                    <div class="col-sm-2">{{ __("No Rujukan") }} </div>
                    <div class="col-sm">: {{data_get($datapay,'transaction.tran_refno')}}</div>
                    <div class="w-100"></div>
                    <div class="col-sm-2">{{ __("Jenis Kesalahan") }} </div>
                    <div class="col-sm">: {{data_get($datapay,'transaction.offence.offence_name')}}</div>
                    <div class="col-sm-2">{{ __("Amaun Kompaun (RM)") }} </div>
                    <div class="col-sm">: {{number_format(data_get($datapay,'transaction.tran_compoundamount'),2)}}</div>

                    <div class="w-100"></div>
                </div>
  <br>
  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
   <div class="table-responsive"> 
         <table class="table table-bordered table-striped table-condensed">
         <thead> 
            <tr>
              <th align="center"><b>Bil.</td></b>
              <th style="text-align: center"><b>No Kompaun</td></b>
              <th style="text-align: center"><b>Amaun (RM)</b></td>   
              <th style="text-align: center"><b>Total (RM)</b></td>
             
           </tr> 
        </thead>
         <tbody> 
           <tr>
              <td>
                 1.
              </td>
               <td align="center">
                 {{data_get($datapay,'pymt_compoundno')}}
              </td>
              <td align="center">
                 {{number_format(data_get($datapay,'pymt_received'),2)}}
              </td>
               <td align="center">
                 {{number_format(data_get($datapay,'pymt_totalamount'),2)}}
              </td>
             
           </tr>
            <tr><td colspan='3' align="right"><b>Jumlah (RM)</b></td>
              <td colspan='4' align="center"><b>{{number_format(data_get($datapay,'pymt_totalamount'),2)}}</b></td>
            </tr>
   
            
        </tbody>                           
       
      </table>
 
    </div>
  </div>
  <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-xs-right"> 
       <div class="form-group">
         <div class="col-sm-offset-2 col-sm-12 text-right">
           <a href="{{route('payment::pay-resit.show', data_get($datapay,'pymt_id'))}}" class="btn btn-primary">Cetak Resit</a>
          
        </div>
      </div>     
    </div>
</div>
</div>
          </div>
           
        </div>
                   
    </div>

@endsection