<?php


 Route::group(
     [
        'prefix'     => LaravelLocalization::setLocale().'/sap',
        'middleware' => ['web'],
        'namespace'  => 'Ekompaun\Sap\Http\Controllers',
        'as'         => 'sap::',
     ],
     function () {
        Route::any('sap1/{fid}/{noid}/{param1}/{param2}', ['uses' => 'SapController@getSap1']);
        Route::any('revert/{komno}/{komprc}/{sapprc}/{sapstat}', ['uses' => 'SapController@getRevert']);
               
     }
 );