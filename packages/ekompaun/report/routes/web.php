<?php
Route::group(
    [
    'prefix'       => LaravelLocalization::setLocale(),
    'middleware'   => ['web','auth', 'localeSessionRedirect', 'localizationRedirect'],
    'namespace'    => 'Ekompaun\Report\Http\Controllers',
    'as'           => 'report::'
    ],
    function () {
        Route::group(
            ['prefix' => 'report'], function () {
                Route::resource('appeal', 'AppealController')
                ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_RAYUAN);

                Route::resource('offence/{id?}', 'OffenceController', ['only' => ['index']]);

                Route::resource('assignment', 'AssignmentController')
                ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_PRESTASI_PEGAWAI_SUMMARY);

                Route::resource('compound', 'CompoundController')
                ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_KOMPAUN);

                // Route::resource('staff-detail', 'StaffDetailController');
                Route::resource('payment-history', 'PaymentHistoryController')
                ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_PAYMENT_DETAIL);

                Route::resource('payment-summary', 'PaymentSummaryController')
                ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_PAYMENT_SUMMARY);

                Route::resource('receipt-daily', 'ReceiptDailyController')
                ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_TERIMAAN_HARIAN);

                Route::resource('audit-application', 'ApplicationAuditTrailController', ['only' => ['index', 'show']])
                ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_AUDIT_TRAIL_RAYUAN);

                Route::resource('audit-trail', 'AuditTrailController', ['only' => ['index', 'show']])
                ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_AUDIT_TRAIL);

                Route::resource('sms', 'SmsController', ['only' => ['index']])
                ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_RAYUAN_SMS);

                Route::group(
                    ['namespace' => 'Pdf'], function () {
                        Route::resource('appeal-pdf', 'AppealPdfController', ['only' => ['index']])
                        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_RAYUAN);

                        Route::resource('assignment-pdf', 'AssignmentPdfController', ['only' => ['index']])
                        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_PRESTASI_PEGAWAI_SUMMARY);

                        Route::resource('compound-pdf', 'CompoundPdfController', ['only' => ['index']])
                        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_KOMPAUN);

                        Route::resource('payment-history-pdf', 'PaymentHistoryPdfController', ['only' => ['index']])
                        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_PAYMENT_DETAIL);

                        Route::resource('payment-summary-pdf', 'PaymentSummaryPdfController', ['only' => ['index']])
                        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_PAYMENT_SUMMARY);

                        Route::resource('receipt-daily-pdf', 'ReceiptDailyPdfController', ['only' => ['index']])
                        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_TERIMAAN_HARIAN);

                        Route::resource('audit-application-pdf', 'ApplicationAuditTrailPdfController', ['only' => ['index', 'show']])
                        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_AUDIT_TRAIL_RAYUAN);

                        Route::resource('audit-trail-pdf', 'AuditTrailPdfController', ['only' => ['index', 'show']])
                        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_AUDIT_TRAIL);

                        Route::resource('sms-pdf', 'SmsPdfController', ['only' => ['index']])
                        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_RAYUAN_SMS);

                        Route::resource('payment-receipt-pdf', 'PaymentReceiptPdfController', ['only' => ['index']]);
                    }
                );
                Route::group(
                    ['namespace' => 'Csv'], function () {
                        Route::resource('appeal-csv', 'AppealCsvController', ['only' => ['index']])
                        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_RAYUAN);

                        Route::resource('assignment-csv', 'AssignmentCsvController', ['only' => ['index']])
                        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_PRESTASI_PEGAWAI_SUMMARY);

                        Route::resource('compound-csv', 'CompoundCsvController', ['only' => ['index']])
                        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_KOMPAUN);

                        Route::resource('payment-history-csv', 'PaymentHistoryCsvController', ['only' => ['index']])
                        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_PAYMENT_DETAIL);

                        Route::resource('payment-summary-csv', 'PaymentSummaryCsvController', ['only' => ['index']])
                        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_PAYMENT_SUMMARY);

                        Route::resource('receipt-daily-csv', 'ReceiptDailyCsvController', ['only' => ['index']])
                        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_TERIMAAN_HARIAN);

                        Route::resource('audit-application-csv', 'ApplicationAuditTrailCsvController', ['only' => ['index', 'show']])
                        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_AUDIT_TRAIL_RAYUAN);

                        Route::resource('audit-trail-csv', 'AuditTrailCsvController', ['only' => ['index', 'show']])
                        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_AUDIT_TRAIL);

                        Route::resource('sms-csv', 'SmsCsvController', ['only' => ['index']])
                        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_RAYUAN_SMS);
                    }
                );
            }
        );

    }
);
