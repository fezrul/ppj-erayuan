@extends('layouts.app')
@section('content')
    <div class="my-5">
       <div class="card" style="overflow-x: scroll;">
            <div class="card-header">
                
                <div class="card-actions">
                    <a href="{{ route('report::payment-summary-pdf.index', ['year' => request('year')]) }}" target="blank">
                        <i class="fa fa-file-pdf-o"></i> {{ __("Print PDF") }}
                    </a>
                    <a href="{{ route('report::payment-summary-csv.index', ['year' => request('year')]) }}" target="blank">
                        <i class="fa fa-file-excel-o"></i> {{ __("Eksport CSV") }}
                    </a>
                </div>
                 <p></p><br>
                <h5 class="mb-0">{{ __("Laporan Kutipan Kewangan :year", ['year' => $selectedYear]) }}</h5>
            </div>
            <div class="card-header">
                @include('report::payment-summary._filter')
            </div>
                             <table class="table table-striped mb-0 table-responsive">
                <thead class="thead-light">
                <tr>
                    <th>{{ __('Bil') }}</th>
                    <th>{{ __('Kaedah Pembayaran') }}</th>
                    @foreach($months as $month)
                        <th>{{ $month }}</th>
                    @endforeach
                    <th>{{ __('Jumlah') }}</th>
                </tr>
                </thead>
                @php($total = 0)
                <tbody>
                @foreach($data as $paymentMethod)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $paymentMethod->paymethod_name }}</td>
                        @foreach($paymentMethod->monthlyPayments as $amount)
                            <td class="text-right">{{ format_money($amount) }}</td>
                        @endforeach
                        @php($total += $paymentMethod->monthlyPayments->sum())
                        <td class="text-right">{{ format_money($paymentMethod->monthlyPayments->sum()) }}</td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot class="thead-light">
                <tr>
                    <th colspan="14" class="text-right">{{ __('Jumlah') }}</th>
                    <th class="text-right">{{ format_money($total) }}</th>
                </tr>
                </tfoot>

            </table>
        </div>
    </div>

@endsection
