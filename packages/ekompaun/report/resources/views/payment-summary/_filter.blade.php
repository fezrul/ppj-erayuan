{{ html()->form('GET', route('report::payment-summary.index') )->open() }}
<div class="row">
    <div class="col-md-6">
        {{ html()->formGroup(__('Tahun'), html()->select('year', $years, $selectedYear)) }}
    </div>
    <div class="col-md-6">
        {{ html()->submit(__("Jana Laporan"))->class(['btn btn-secondary']) }}
    </div>
</div>
{{ html()->form()->close() }}
