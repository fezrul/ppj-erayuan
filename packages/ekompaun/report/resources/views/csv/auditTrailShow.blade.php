<!DOCTYPE html>
<html>
<body>
    <table>
        <tr>
            <td>{{ __("Nama Pengguna") }}</td>
            <td>{{ $application->transaction->tran_offendername }}</td>
        </tr>
        <tr>
            <td>{{ __("Id Pengguna") }}</td>
            <td>{{ $application->transaction->tran_offendericno }}</td>
        </tr>
        <tr>
            <td>{{ __("Tindakan") }} </td>
            <td>{{ $application->status->status_name }}</td>
        </tr>
        <tr>
            <td>{{ __("Tarikh Kompaun") }} </td>
            <td>{{ format_date($application->transaction->tran_compounddate) }}</td>
        </tr>
        <tr>
            <td>{{ __("No Kompaun") }}</td>
            <td>{{ $application->transaction->tran_compoundno }}</td>
        </tr>
        <tr>
            <td>{{ __("No Rujukan") }}</td>
            <td>{{ $application->transaction->tran_refno }}</td>
        </tr>
    </table>

    <table>
        <tr>
            <th>{{ __("Bil") }}</th>
            <th>{{ __("Aktiviti") }}</th>
            <th>{{ __("Tarikh Tindakan") }}</th>
            <th>{{ __("Pegawai") }}</th>
        </tr>
        @foreach($audits as $audit)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $audit->task }}</td>
                <td>{{ format_date($audit->created_date) }}</td>
                <td>{{ $audit->user->name }}</td>
            </tr>
        @endforeach
    </table>
</body>
</html>