<!DOCTYPE html>
<html>
<body>
	<table>
        <thead>
        <tr>
            <th>{{ __('Bil') }}</th>
            <th>{{ __('Kaedah Pembayaran') }}</th>
            @foreach($months as $month)
                <th>{{ $month }}</th>
            @endforeach
            <th>{{ __('Jumlah') }}</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        @php($total = 0)
        <tbody>
        @foreach($data as $paymentMethod)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $paymentMethod->paymethod_name }}</td>
                @foreach($paymentMethod->monthlyPayments as $amount)
                    <td class="text-right">{{ format_money($amount) }}</td>
                @endforeach
                @php($total += $paymentMethod->monthlyPayments->sum())
                <td class="text-right">{{ format_money($paymentMethod->monthlyPayments->sum()) }}</td>
            </tr>
        @endforeach
        </tbody>
        <tfoot class="thead-light">
        <tr>
            <th colspan="14" class="text-right">{{ __('Jumlah') }}</th>
            <th class="text-right">{{ format_money($total) }}</th>
        </tr>
        </tfoot>
        </tbody>
    </table>
</body>
</html>
