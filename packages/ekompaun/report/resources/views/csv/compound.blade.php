<!DOCTYPE html>
<html>
<body>
	<table>
        <thead>
        <tr>
            <th>{{ __('Jenis Kesalahan') }}</th>
            @foreach($months as $month)
                <th>{{ $month }}</th>
            @endforeach
            <th>{{ __('Jumlah') }}</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        @php($total = 0)
        @foreach($report as $act => $data)
            <tr>
                <th class="text-center" colspan="13">{{ $act }}</th>
            </tr>
            <tbody class="text-center">
            @foreach($data as $section)
                <tr>
                    <td>{{ $section->section_name }}</td>
                    @foreach($section->compounds as $compound)
                        <td class="{{ ($compound > 0) ? 'bg-light':'' }}">{{ $compound }}</td>
                    @endforeach
                    <td>{{ $section->compounds->sum() }}</td>
                </tr>
                @php($total += $section->compounds->sum())
            @endforeach
            </tbody>
        @endforeach
        <tfoot class="thead-light">
        <tr>
            <th colspan="13" class="text-right">{{ __('Jumlah') }}</th>
            <th class="text-center">{{ $total }}</th>
        </tr>
        </tfoot>
        </tbody>
    </table>
</body>
</html>
