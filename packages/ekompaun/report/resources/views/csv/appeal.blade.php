<!DOCTYPE html>
<html>
<body>
	<table>
        <thead>
        <tr>
            <th>{{ __("Bil") }}</th>
            <th>{{ __("No Kompaun") }}</th>
            <th>{{ __("Pelanggan") }}</th>
            <th>{{ __("Kod Kesalahan") }}</th>
            <th>{{ __("Tarikh Rayuan") }}</th>
            <th>{{ __("Tarikh Bayaran") }}</th>
            <th>{{ __("Tempoh Maklumbalas") }}</th>
            <th>{{ __("Amaun (RM)") }}</th>
            <th>{{ __("Pegawai") }}</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        @foreach($applications as $application)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $application->present_no_kompaun }}</td>
                <td>{{ $application->present_pelanggan }}</td>
                <td>{{ $application->transaction->offence->offence_code }}</td>
                <td>{{ $application->present_tarikh_rayuan }}</td>
                <td>{{ $application->present_tarikh_bayaran }}</td>
                <td>{{ $application->present_tempoh_maklumbalas }}</td>
                <td>{{ $application->present_amaun }}</td>
                <td>{{ $application->present_pegawai }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</body>
</html>