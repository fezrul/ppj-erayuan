<!DOCTYPE html>
<html>
<body>
	<table>
        <thead>
        <tr>
            <th>{{ __("Bil") }}</th>
            <th>{{ __("Nama Pegawai") }}</th>
            @foreach($months as $month)
                <th>{{ $month }}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        <tr>
        @foreach($staffs as $staff)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $staff->name }}</td>
                @foreach($staff['assignments'] as $assigment)
                    <td> {{ $assigment }}</td>
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
</body>
</html>
