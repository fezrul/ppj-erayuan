<!DOCTYPE html>
<html>
<body>
	<table>
        <tr>
            <th>{{ __("Bil") }}</th>
            <th>{{ __("Id Pengguna") }}</th>
            <th>{{ __("No Kompaun") }}</th>
            <th>{{ __("Aktiviti") }}</th>
            <th>{{ __("Tindakan") }}</th>
            <th>{{ __("Tarikh Tindakan") }}</th>
        </tr>
        @foreach($applications as $application)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $application->transaction->tran_offendericno }}</td>
                <td>{{ $application->transaction->tran_compoundno }}</td>
                <td>{{ $application->lastAuditrail->task }}</td>
                <td>{{ $application->status->status_name }}</td>
                <td>{{ format_date($application->appl_date) }}</td>
            </tr>
        @endforeach
    </table>
</body>
</html>