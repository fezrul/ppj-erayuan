<!DOCTYPE html>
<html>
<body>
    <table>
        <thead class="thead-light">
        <tr>
            <th>{{ __("No") }}</th>
            <th>{{ __("Nama Pelanggan") }}</th>
            <th>{{ __("No. ")}}<br> {{ __("Pelanggan") }}</th>
            <th>{{ __("No. ") }}<br>{{ __("Resit") }}</th>
            <th>{{ __("No. ") }}<br>{{ __("Resit Manual") }}</th>
            <th>{{ __("No.Bil/") }} <br>{{ __("No.Dokumen") }}</th>
            <th>{{ __("Rujukan") }}</th>
            <th>{{ __("Keterangan Bayaran") }}</th>
            <th>{{ __("Kod Bank") }}</th>
            <th>{{ __("No.Cek/ ") }}<br>{{ __("Kad Kredit/Dokumen") }}</th>
            <th>{{ __("Amoun(RM)")}}</th>
            <th>{{ __("Kod Akaun ")}}<br>{{ __(" KT") }}</th>
            <th>{{ __("Status ")}}<br>{{ __(" Resit")}}</th>
        </tr>
        </thead>
        @php($total = 0)
        <tbody>
        @foreach($applications as $application)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $application->present_offender_name }}</td>
            <td>{{ $application->present_offender_icno }}</td>
            <td>{{ $application->present_payment_fpx_trans_id }}</td>
            <td></td>
            <td></td>
            <td>{{ $application->present_payment_fpx_serial_no }}</td>
            <td>{{ $application->present_no_kompaun }}</td>
            <td>{{ $application->present_payment_bank }}</td>
            <td></td>
            <td>{{ $application->present_payment_received }}</td>
            <td></td>
            <td></td>
        </tr>
        @php($total += $application->present_payment_received)
        @endforeach
        </tbody>
        <tfoot class="thead-light">
        <tr>
            <th class="text-right">{{ __('Jumlah') }}</th>
            <th class="text-center">{{ format_money($total) }}</th>
            <th colspan="11" class="text-left"></th>
        </tr>
        </tfoot>
    </table>
</body>
</html>
