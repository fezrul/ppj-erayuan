<!DOCTYPE html>
<html>
<body>
	<table>
        <thead>
        <tr>
            <th>{{ __('Bil') }}</th>
            <th>{{ __('No. Kompaun') }}</th>
            <th>{{ __('No. Pendaftaran Kenderaan') }}</th>
            <th>{{ __('Amaun Perlu Bayar (RM)') }}</th>
            <th>{{ __('Tarikh Bayaran') }}</th>
            <th>{{ __('Jumlah Bayaran (RM)') }}</th>
            <th>{{ __('Status Bayaran') }}</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        @foreach($payments as $payment)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $payment->pymt_compoundno }}</td>
                <td>{{ $payment->pymt_refno }}</td>
                <td>{{ $payment->pymt_totalamount }}</td>
                <td>{{ $payment->pymt_date }}</td>
                <td>{{ $payment->pymt_totalamount }}</td>
                <td>{{ $payment->paystatus }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</body>
</html>
