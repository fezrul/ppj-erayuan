<!DOCTYPE html>
<html>
<body>
	<table>
        <thead>
            <tr>
                <th>{{ __("Bil") }}</th>
                <th>{{ __("User Aktiviti") }}</th>
                <th>{{ __("Aktiviti") }}</th>
                <th>{{ __("Model") }}</th>
                <th>{{ __("Tarikh Aktiviti") }}</th>
            </tr>
        </thead>
        <tbody>
        @foreach($audits as $audit)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $audit->user->name ?? '-' }}</td>
                <td>{{ $audit->event }}</td>
                <td>{{ $audit->auditable_type }}</td>
                <td>{{ format_date($audit->created_at) }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</body>
</html>