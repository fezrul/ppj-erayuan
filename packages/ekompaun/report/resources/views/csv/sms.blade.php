<!DOCTYPE html>
<html>
<body>
	<table>
		<thead>
			<tr>
				<th>{{ __("Bil") }}</th>
                <th>{{ __("Nombor Telefon") }}</th>
                <th>{{ __("Alasan") }}</th>
                <th>{{ __("Ulasan") }}</th>
                <th>{{ __("Tarikh Permohonan") }}</th>
                <th>{{ __("Pegawai Bertugas") }}</th>
                <th>{{ __("Tarikh Kelulusan") }}</th>
                <th>{{ __("Diluluskan Oleh") }}</th>
			</tr>
		</thead>
		<tbody>
			@foreach($notifications as $notification)
			<tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $notification->appl_phonenumber }}</td>
                <td>{{ $notification->appl_reason }}</td>
                <td>{{ $notification->appl_remark }}</td>
                <td>{{ format_date($notification->appl_date) }}</td>
                <td>{{ $notification->personInCharge->name }}</td>
                <td>{{ format_date($notification->appl_decisiondate) }}</td>
                <td>{{ $notification->approver->name }}</td>
            </tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>
