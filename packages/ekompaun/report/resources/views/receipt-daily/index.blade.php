@extends('layouts.app')
@push('head')
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
@endpush

@section('content')
<div class="my-5">
        <div class="card" style="overflow-x: scroll;">
            <div class="card-header">
                
                <div class="card-actions">
                    <a href="{{ route('report::receipt-daily-pdf.index', ['date_from' => request('date_from'), 'date_to' => request('date_to')]) }}" target="blank">
                        <i class="fa fa-file-pdf-o"></i> {{ __("Print PDF") }}
                    </a>
                    <a href="{{ route('report::receipt-daily-csv.index', ['date_from' => request('date_from'), 'date_to' => request('date_to')]) }}" target="blank">
                        <i class="fa fa-file-excel-o"></i> {{ __("Eksport CSV") }}
                    </a>
                </div>
                <p></p><br>
                <h5 class="mb-0">{{ __("Laporan Harian") }}</h5>
            </div>
            <div class="card-header">
                {{ html()->form('GET', route('report::receipt-daily.index') )->open() }}
				<div class="row">
				    <div class="col-md-5">
				        {{ html()->formGroup(__("Tarikh Dari"), html()->input('text','date_from', request('date_from'))->class(['form-control'])) }}
				    </div>
				    <div class="col-md-5">
				        {{ html()->formGroup(__("Tarikh Hingga"), html()->input('text','date_to', request('date_to'))->class(['form-control'])) }}
				    </div>
				    <div class="col-md-2">
				        {{ html()->submit(__("Jana Laporan"))->class(['btn btn-secondary']) }}
				    </div>
				</div>
				{{ html()->form()->close() }}
            </div>
             @if ($applications->isNotEmpty() )
             <table class="table table-striped mb-0 table-responsive">
                <thead class="thead-light">
                <tr>
                    <th>{{ __("No") }}</th>
                    <th>{{ __("Nama Pelanggan") }}</th>
                    <th>{{ __("No. Kad")}}<br>{{ __("Pengenalan/")}}<br> {{ __("No. Passport") }}</th>
                    <th>{{ __("No.") }}<br>{{ __("Resit") }}</th>
                    <th>{{ __("No.") }}<br>{{ __("Resit SAP") }}</th>
                    <th>{{ __("No.") }} <br>{{ __("Kompaun") }}</th>
                    <th>{{ __("Keterangan") }}</th>
                    <th>{{ __("No.") }}<br>{{ __("FPX") }}</th>
                    <th>{{ __("Amaun(RM)")}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($applications as $application)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $application->present_offender_name }}</td>
                    <td>{{ $application->present_offender_icno }}</td>
                    <td>{{ $application->present_payment_refno }}</td>
                    <td>{{ $application->present_payment_receipt_number }}</td>
                    <td>{{ $application->present_no_kompaun }}</td>
                    <td>{{ $application->present_keterangan }}</td>
                    <td>{{ $application->present_payment_fpx_serial_no }}</td>
                    <td>{{ $application->present_payment_received }}</td>
                </tr>
                @endforeach
                </tbody>
                <tfoot class="thead-light">
                <tr>
                    <th class="text-right">{{ __('Jumlah') }}</th>
                    <th class="text-center">{{ format_money($applications->sum('present_payment_received')) }}</th>
                    <th colspan="11" class="text-left"></th>
                </tr>
                </tfoot>

            </table>
            @else
                @include('components.empty')
            @endif
        </div>
    </div>
@endsection

@push('end')
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

<script type="text/javascript">
$(function() {
    $('input[name="date_from"], input[name="date_to"]').daterangepicker({
        locale: {
          format: 'DD-MM-YYYY'
        },
        singleDatePicker: true,
        showDropdowns: true
    },
    function(start, end, label) {
    });
});
</script>
@endpush
