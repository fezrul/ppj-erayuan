@extends('layouts.app')

@push('head')
<link rel="stylesheet" type="text/css" href="{{ asset('lib/bootstrap-daterangepicker/daterangepicker.css') }}" />
@endpush

@section('content')
<div class="container-fluid mt-5">
    <div class="card">
        <div class="card-header">
            <h5 class="mb-0">{{ __("Senarai Rayuan") }}</h5>
            <div class="card-actions">
                <a href="{{ route('report::sms-pdf.index',
                ['status'=>request('status'), 'from'=>request('from'), 'to'=>request('to')]) }}" target="blank"><i class="fa fa-file-pdf-o"></i> {{ __("Print PDF") }}</a>
                <a href="{{ route('report::sms-csv.index',
                ['status'=>request('status'), 'from'=>request('from'), 'to'=>request('to')]) }}" target="blank"><i class="fa fa-file-excel-o"></i> {{ __("Eksport CSV") }}</a>
            </div>
        </div>
        <div class="card-body">
            {{ html()->form('GET', route('report::sms.index'))->open() }}
                <div class="container-fluid mb-3">
                    <div class="row">
                        <div class="col-3">
                            <label class="col-sm-label">{{ __("Status")}}</label>
                            {{ html()->select('status', $status, request('status'))
                            ->addClass('form-control') }}
                        </div>
                        <div class="col-3">
                            <label class="col-sm-label">{{ __("Tarikh Rayuan Dari")}}</label>
                            {{ html()->text('from', request('from', $dateNow->format('d-m-y')))
                            ->addClass('form-control') }}
                        </div>
                        <div class="col-3">
                            <label class="col-sm-label">{{ __("Tarikh Rayuan Hingga")}}</label>
                            {{ html()->text('to', request('to', $dateNow->addDays(30)->format('d-m-y')))
                            ->addClass('form-control') }}
                        </div>
                        <div class="col-3">
                            <label class="col-sm-label">&nbsp;</label>
                            <br>
                            {{ html()->submit(__('Hantar'))->addClass('btn btn-primary') }}
                        </div>
                    </div>
                </div>
            {{ html()->form()->close() }}
        </div>
        <table class="table table-striped mb-0">
            <thead class="thead-light">
                <tr>
                    <th>{{ __("Bil") }}</th>
                    <th>{{ __("Nombor Telefon") }}</th>
                    <th>{{ __("Alasan") }}</th>
                    <th>{{ __("Ulasan") }}</th>
                    <th>{{ __("Tarikh Permohonan") }}</th>
                    <th>{{ __("Pegawai Bertugas") }}</th>
                    <th>{{ __("Tarikh Kelulusan") }}</th>
                    <th>{{ __("Diluluskan Oleh") }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($notifications as $notification)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $notification->appl_phonenumber }}</td>
                    <td>{{ $notification->appl_reason }}</td>
                    <td>{{ $notification->appl_remark }}</td>
                    <td>{{ format_date($notification->appl_date) }}</td>
                    <td>{{ $notification->personInCharge->name }}</td>
                    <td>{{ format_date($notification->appl_decisiondate) }}</td>
                    <td>{{ $notification->approver->name }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="card-footer">
            {{ $notifications->links() }}
        </div>
    </div>
</div>

@endsection

@push('end')
<script type="text/javascript" src="{{ asset('lib/bootstrap-daterangepicker/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('lib/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

<script type="text/javascript">
    $(function() {
        $('input[name="from"]').daterangepicker({
            locale: {
              format: 'DD-MM-YYYY'
          },
          singleDatePicker: true,
          showDropdowns: true
      },
      function(start, end, label) {
      });
    });

    $(function() {
        $('input[name="to"]').daterangepicker({
            locale: {
              format: 'DD-MM-YYYY'
          },
          singleDatePicker: true,
          showDropdowns: true
      },
      function(start, end, label) {
      });
    });
</script>


@endpush
