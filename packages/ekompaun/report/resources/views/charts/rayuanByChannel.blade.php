@inject('chart', '\Ekompaun\Report\Charts\JumlahRayuanInMonthComparedWithPrevious')

<div class="cards">
    <div class="cards-header cards-chart" style="background-color:whitesmoke">
    	 <div class="card-body">
        {!! $chart->container() !!}
    </div>    	
    </div>
    <div class="cards-content">
        <h4 class="title">{{ __('Jumlah Rayuan :prev vs :current', ['prev' => $chart->previousMonth(), 'current' => $chart->currentMonth()]) }}</h4>
        <p class="category">Bagi Tahun {{date('Y')}}</p>
       
    </div>
    <div class="cards-footer">

       <!--  <div class="stats">
            <i class="material-icons">access_time</i> campaign sent 2 days ago
        </div> -->
    </div>
</div>


@pushonce('end:chartjs')
<script src=//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js charset=utf-8></script>

@endpushonce

@push('end')

    {!! $chart->script() !!}
@endpush
