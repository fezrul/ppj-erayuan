@extends('layouts.pdf.master')

@section('title', 'Laporan Audit Trail')
@section('content')

    <div style="font-family:Arial; font-size:12px;">
       <div class="col-md-3">
           <img src="img/logo-ppj.png" height="100px">
      </div>
      <div class="col-md-9">
            <h2 style="text-align: center">{{ __("Laporan Audit Trail") }}</h2>
      </div>
  </div>
  <br>
  <table class="tg">
    <tr>
      	<th class="tg-3wr7" width="1">{{ __("Bil") }}</th>
        <th class="tg-3wr7">{{ __("User Aktiviti") }}</th>
        <th class="tg-3wr7">{{ __("Aktiviti") }}</th>
        <th class="tg-3wr7">{{ __("Model") }}</th>
        <th class="tg-3wr7">{{ __("Tarikh Aktiviti") }}</th>
  	</tr>
    @if ($audits->isNotEmpty() )
    @foreach($audits as $audit)
        <tr>
            <td class="tg-rv4w" width="1">{{ $loop->iteration }}</td>
            <td class="tg-ti5e">{{ $audit->user->name ?? '-' }}</td>
            <td class="tg-ti5e">{{ $audit->event }}</td>
            <td class="tg-ti5e">{{ $audit->auditable_type }}</td>
            <td class="tg-ti5e">{{ format_date($audit->created_at) }}</td>
        </tr>
    @endforeach
    @else
    <tr>
      <td colspan='5' class="tg-rv4w">{{ __("Maaf, tiada data tersedia") }}</td>
    </tr>
    @endif
  </table>
@endsection
