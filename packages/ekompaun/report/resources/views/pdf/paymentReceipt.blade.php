@extends('layouts.pdf.master')

@push('head')
    <style type="text/css">
       body { background: white !important; } 
       .left{
            margin-right: 5px;
            padding: 5px;
            float: left;
            width: 50%;
       }
       .right{
            margin-left: 5px;
            padding: 5px;
            float: left;
            width: 45%;
       }
       .clearfix{
            clear: both;
       }
       #watermark {
            position: fixed;
            bottom: 6cm;
            left: 8cm;
            width: 11cm;
            height: 11cm;
            opacity: 0.1;
            filter: alpha(opacity=40);
        }
    </style>
@endpush

@section('title', 'Resit Resmi')
@section('content')
<div id="watermark">
    <img src="img/logo.png" height="100%" width="100%"/>
</div>
<h4 class="mb-0" style="text-align: center">{{ __("Resit Resmi") }}</h5><br>

<table class="tg"> 
    <tr>
        <th>{{ __("No. Resit")}}</th>
        <th>{{ __("Tarikh")}}</th>
        <th>{{ __("MOD")}}</th>
        <th>{{ __("No. Dokumen")}}</th>
        <th>{{ __("Jumlah")}}</th>
    </tr>
    <tr>
        <td>{{ $application->present_payment_receipt_number }}</td>
        <td>{{ $application->present_tarikh_bayaran }}</td>
        <td>{{ $application->present_payment_payment_method }}</td>
        <td></td>
        <td>{{ $application->present_payment_amount }}</td>
    </tr>
</table>
<br>
<div>
    <div class="left">
        <table class="tg">
                <tr>
                    <th>{{ __("No. ID Pelanggan :") }}</th>
                    <td>{{ $application->present_pelanggan_id }}</td>
                </tr>
                <tr>
                    <th>{{ __("FPX ID :") }}</th>
                    <td>{{ $application->present_payment_fpx_trans_id }}</td>
                </tr>
                <tr>
                    <th>{{ __("Approval Code :") }}</th>
                    <td></td>
                </tr>
                <tr>
                    <th>{{ __("Nama Bank :") }}</th>
                    <td>{{ $application->present_payment_fpx_bank }}</td>
                </tr>
        </table>
    </div>
    <div class="right">
        <table class="tg">
            <tr>
                <th>{{ __("Rujukan :") }}</th>
                <td>{{ $application->present_payment_refno }}</td>
            </tr>
            <tr>
                <th>{{ __("No. Kenderaan :") }}</th>
                <td>{{ $application->present_no_daftar_kenderaan }}</td>
            </tr>
        </table>
    </div>
</div>
<div class="clearfix">
</div>
<br>
<div>
    <table class="tg">
        <thead>
            <tr>
                <th>{{ __("Untuk Bayaran") }}</th>
                <th>{{ __("KOD GST") }}</th>
                <th>{{ __("Amaun(RM)") }}</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ __("BAYARAN LALULINTAS") }}</td>
                <td>{{ __("OS") }}</td>
                <td>{{ $application->present_payment_amount }}</td>
            </tr>
        </tbody>
    </table>
</div>
<br>
<div>
    <small>{{ __("Resit ini cetakan komputer, tiada tanda tangan diperlukan.") }}</small>
</div>
@endsection