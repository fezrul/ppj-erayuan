<!DOCTYPE html>
<html>
<head>
    <title>Laporan Terimaan</title>
    <style type="text/css">
        .page-break {
            page-break-after: always;
        }
        body {
          font-family: "Montserrat", sans-serif !important;
          margin: 0 !important;
          font-size: 10px;
        }

        table {
          font-size: 10px;
          margin: 10px auto;
          border-collapse: separate;
          border-spacing: 0 5px;
        }

        table th {
          font-weight: normal;
          text-align: left;
        }

        table thead tr th {
          border-collapse: separate;
          border-spacing: 5px 5px;
          padding-bottom: 10px;
        }

        table tfoot tr td {
          border-collapse: separate;
          border-spacing: 5px 5px;
          padding-top: 10px;
        }

        table.header {
          border: 1px solid #333;
          padding: 10px;
        }

        table.header tbody tr td:nth-child(2) {
          text-align: center;
        }

        table.header tbody tr td:last-child {
          text-align: right;
        }

        table.sub-header {
          margin: 0px;
        }

        table.data thead tr th {
          border-bottom: 1px solid #333;
        }

        table.data tfoot tr td {
          border-top: 1px solid #333;
        }

        table.data tfoot tr td:nth-child(11) {
          border-bottom: 1px dashed #333;
          border-collapse: separate;
          border-spacing: 5px 5px;
          padding-bottom: 10px;
          position: relative;
        }

        table.data tfoot tr td:nth-child(11):after {
          border-bottom: 1px dashed #333;
          content: "";
          border-collapse: separate;
          border-spacing: 5px 5px;
          position: absolute;
          width: 100%;
          left: 0;
          bottom: 5px;
          padding-bottom: 10px;
        }

        table.footer {
          margin: 0 100px;
        }

        table.footer thead tr th:first-child {
          border-bottom: 1px solid #333;
        }

        table.footer thead tr th:nth-child(5) {
          padding: 0 25px;
        }

        table.footer thead tr th:last-child {
          border-bottom: 3px solid #333;
        }

        table.footer tbody tr td:nth-child(2) {
          padding: 0 25px;
        }

        table.footer tbody tr td:last-child, table.footer tfoot tr td:last-child {
          text-align: right;
        }

        table.footer tfoot tr td:nth-child(2), table.footer tfoot tr td:last-child {
          text-align: center;
          padding: 5px 0;
        }

        table.footer tfoot tr td:nth-child(2), table.footer tfoot tr td:last-child {
          border-bottom: 1px dashed blue;
          position: relative;
          border-top: 1px solid #333;
        }

        table.footer tfoot tr td:nth-child(2):after, table.footer tfoot tr td:last-child:after {
          content: "";
          position: absolute;
          border-bottom: 1px dashed blue;
          width: 100%;
          left: 0;
          bottom: -5px;
        }

        table.copyright tbody tr td:last-child {
          border-bottom: 1px solid #333;
        }


        table.jumlah {
          font-size: 14px;
        }

        table.jumlah h6 {
          font-weight: normal;
          font-size: 14px;
          border-bottom: 1px solid #333;
          width: 185px;
        }

        table.jumlah thead tr th {
          text-align: left;
        }

        table.jumlah table tbody tr:first-child td:not(:first-child) {
          border-top: 1px solid #333;
          padding-top: 10px;
        }

        table.jumlah tbody tr td:not(:first-child), table.jumlah tfoot tr td:not(:first-child) {
          text-align: center;
        }

        table.jumlah table tfoot tr td {
          border-bottom: 1px solid #333;
          border-top: 1px solid #333;
          padding: 10px;
        }
    </style>
</head>
<body>

    <table class="header" style="width: 100%">
        <tbody>
            <tr>
                <td> {{ __("Pengguna : ". Auth::user()->name) }} </td>
                <td> {{ __("PERBADANAN PUTRAJAYA") }}  </td>
                <td> {{ __("Tarikh :date", ['date' => Carbon\Carbon::now()->format('d-m-Y')]) }} </td>
            </tr>

            <tr>
                <td></td>
                <td> {{ __("LAPORAN TERIMAAN DARI :from HINGGA :to ", ['from' => $dateFrom, 'to' => $dateTo]) }} </td>
                <td> {{ __("Masa: "). Carbon\Carbon::now()->toTimeString() }}  </td>
            </tr>

            <tr>
                <td></td>
                <td> {{ __("Cawangan : HQ") }} </td>
                <td>  </td>
            </tr>
        </tbody>
    </table>

    {{-- <table class="sub-header">
        <tbody>
            <tr>
                <td> {{ __("Kod Akaun DT : 1011307") }} </td>
                <td> {{ __("PSCD - KAUNTER TUNAI MBB ABT") }} </td>
            </tr>

            <tr>
                <td>{{ __("Cara Terimaan : TUNAI") }}</td>
            </tr>
        </tbody>
    </table> --}}

    <table class="data" style="width: 100%;">
        <thead>
            <tr>
                <th>{{ __("No") }}</th>
                <th>{{ __("Nama Pelanggan") }}</th>
                <th>{{ __("No. ")}}<br> {{ __("Pelanggan") }}</th>
                <th>{{ __("No. ") }}<br>{{ __("Resit") }}</th>
                <th>{{ __("No. ") }}<br>{{ __("Resit Manual") }}</th>
                <th>{{ __("No.Bil/") }} <br>{{ __("No.Dokumen") }}</th>
                <th>{{ __("Rujukan") }}</th>
                <th>{{ __("Keterangan Bayaran") }}</th>
                <th>{{ __("Kod Bank") }}</th>
                <th>{{ __("No.Cek/ ") }}<br>{{ __("Kad Kredit/Dokumen") }}</th>
                <th>{{ __("Amoun(RM)")}}</th>
                <th>{{ __("Kod Akaun ")}}<br>{{ __(" KT") }}</th>
                <th>{{ __("Status ")}}<br>{{ __(" Resit")}}</th>
            </tr>
        </thead>
        @if ($applications->isNotEmpty() )
        @php($total = 0)
        <tbody>
        @foreach($applications as $application)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $application->present_offender_name }}</td>
            <td>{{ $application->present_offender_icno }}</td>
            <td>{{ $application->present_payment_refno }}</td>
            <td></td>
            <td></td>
            <td>{{ $application->present_payment_fpx_serial_no }}</td>
            <td>{{ $application->present_no_kompaun }}</td>
            <td>{{ $application->present_payment_bank }}</td>
            <td></td>
            <td>{{ $application->present_payment_received }}</td>
            <td></td>
            <td></td>
        </tr>
        @php($total += $application->present_payment_received)
        @endforeach
        </tbody>

        <tfoot>
            <tr>
                <td></td>
                <td>{{ __("Jumlah TUNAI")}}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{ format_money($total) }}</td>
                <td></td>
                <td></td>
            </tr>
        </tfoot>
    </table>

    <table class="footer">
        <thead>
            <tr>
                <th> {{ __("Ringkasan Penerimaan (RM)") }} </th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th> {{ __("Ringkasan Pembatalan (RM)") }} </th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td colspan="3">{{ __("Tunai :") }}</td>
                <td>{{ $total }}</td>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td colspan="3">{{ __("Cek :") }}</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td colspan="3">{{ __("Maklumat Kredit :") }}</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td colspan="3">{{ __("Kiriman Wang :") }}</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td colspan="3">{{ __("Kad Kredit :")}}</td>
                <td></td>
                <td rowspan="3"></td>
                <td></td>
            </tr>

            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>

        <tfoot>
            <tr>
                <td colspan="3"> {{ __("Jumlah") }} </td>
                <td> {{ $total }} </td>
                <td></td>
                <td></td>
            </tr>
        </tfoot>
    </table>

    <table class="copyright" style="margin: 15px 100px;">
        <tbody>
            <tr>
                <td>{{ __("Disemak Oleh") }}</td>
                <td>{{ __(":") }}</td>
                <td></td>
            </tr>

            <tr>
                <td>{{ __("Tarikh") }}</td>
                <td>{{ __(":") }}</td>
                <td></td>
            </tr>

            <tr>
                <td>{{ __("Disahkan Oleh") }}</td>
                <td>{{ __(":") }}</td>
                <td></td>
            </tr>

            <tr>
                <td>{{ __("Tarikh") }}</td>
                <td>{{ __(":") }}</td>
                <td></td>
            </tr>
        </tbody>
    </table>

    @else
    <tr>
      <td colspan='15' class="tg-rv4w">{{ __("Maaf, tiada data tersedia") }}</td>
    </tr>
    @endif

</body>
</html>
