@extends('layouts.pdf.master')

@section('title', 'Kes Mengikut Pegawai J(U)')
@section('content')

  <div style="font-family:Arial; font-size:12px;">
       <div class="col-md-3">
           <img src="img/logo-ppj.png" height="100px">
      </div>
      <div class="col-md-9">
            <h2 style="text-align: center">{{ __('Kes Mengikut Pegawai J(U) :year', ['year' => $selectedYear]) }}</h2>
      </div>
  </div>
  <br>
  <table class="tg">
    <tr>
        <th class="tg-3wr7" width="1">{{ __("Bil") }}</th>
        <th class="tg-3wr7" width="20%">{{ __("Nama Pegawai") }}</th>
        @foreach($months as $month)
            <th class="tg-3wr7">{{ $month }}</th>
        @endforeach
    </tr>
    @if ($staffs->isNotEmpty() )
    @foreach($staffs as $staff)
        <tr>
            <td class="tg-rv4w" width="1">{{ $loop->iteration }}</td>
            <td class="tg-ti5e" width="20%">{{ $staff->name }}</td>
            @foreach($staff['assignments'] as $assigment)
                <td class="tg-rv4w"> {{ $assigment }}</td>
            @endforeach
        </tr>
    @endforeach
    @else
    <tr>
      <td colspan='14' class="tg-rv4w">{{ __("Maaf, tiada data tersedia") }}</td>
    </tr>
    @endif
  </table>
@endsection
