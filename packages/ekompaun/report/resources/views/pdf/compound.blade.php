@extends('layouts.pdf.master')

@section('title', 'Compound')
@section('content')
  <div style="font-family:Arial; font-size:12px;">
       <div class="col-md-3">
           <img src="img/logo-ppj.png" height="100px">
      </div>
      <div class="col-md-9">
             <h2 style="text-align: center">{{ __("Tahunan Kompaun") }}</h2>
      </div>
  </div>
  <br>
  
  <table class="tg">
    <tr>
        <th class="tg-3wr7" width="1">{{ __('Jenis Kesalahan') }}</th>
        @foreach($months as $month)
            <th class="tg-3wr7">{{ $month }}</th>
        @endforeach
        <th class="tg-3wr7">{{ __('Jumlah') }}</th>
    </tr>
    @if ($report->isNotEmpty() )
    @php($total = 0)
    @foreach($report as $act => $data)
        <tr>
            <th class="tg-3wr7" colspan="13">{{ $act }}</th>
            <th class="tg-rv4w"></th>
        </tr>
        @foreach($data as $section)
            <tr>
                <td class="tg-rv4w">{{ $section->section_name }}</td>
                @foreach($section->compounds as $compound)
                    <td class="{{ ($compound > 0) ? 'bg-light':'' }} tg-rv4w">{{ $compound }}</td>
                @endforeach
                <td class="tg-rv4w">{{ $section->compounds->sum() }}</td>
            </tr>
            @php($total += $section->compounds->sum())
        @endforeach
    @endforeach
    <tfoot class="thead-light">
    <tr>
        <th colspan="13" style="text-align: right">{{ __('Jumlah') }}</th>
        <th class="tg-rv4w">{{ $total }}</th>
    </tr>
    </tfoot>
    @else
    <tr>
      <td colspan='14' class="tg-rv4w">{{ __("Maaf, tiada data tersedia") }}</td>
    </tr>  
    @endif
  </table>
@endsection
