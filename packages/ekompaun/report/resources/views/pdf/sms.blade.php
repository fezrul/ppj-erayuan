@extends('layouts.pdf.master')

@section('title', 'Laporan Notification SMS')
@section('content')
  <div style="font-family:Arial; font-size:12px;">
      <h2 style="text-align: center">{{ __("Laporan Notification SMS") }}</h2>
  </div>
  <br>
  @if ($notifications->isNotEmpty() )
  <table class="tg">
    <tr>
      <th class="tg-3wr7" width="1">{{ __("Bil") }}</th>
      <th class="tg-3wr7">{{ __("Nombor Telefon") }}</th>
      <th class="tg-3wr7">{{ __("Alasan") }}</th>
      <th class="tg-3wr7">{{ __("Ulasan") }}</th>
      <th class="tg-3wr7">{{ __("Tarikh Permohonan") }}</th>
      <th class="tg-3wr7">{{ __("Pegawai Bertugas") }}</th>
      <th class="tg-3wr7">{{ __("Tarikh Kelulusan") }}</th>
      <th class="tg-3wr7">{{ __("Diluluskan Oleh") }}</th>
	</tr>
    @foreach($notifications as $notification)
        <tr>
          <td class="tg-rv4w" width="1">{{ $loop->iteration }}</td>
          <td class="tg-rv4w">{{ $notification->appl_phonenumber }}</td>
          <td class="tg-rv4w">{{ $notification->appl_reason }}</td>
          <td class="tg-rv4w">{{ $notification->appl_remark }}</td>
          <td class="tg-rv4w">{{ format_date($notification->appl_date) }}</td>
          <td class="tg-rv4w">{{ $notification->personInCharge->name }}</td>
          <td class="tg-rv4w">{{ format_date($notification->appl_decisiondate) }}</td>
          <td class="tg-rv4w">{{ $notification->approver->name }}</td>
        </tr>
    @endforeach
  </table>
  @else
  <table class="tg">
    @include('layouts.pdf._headPdfAppeal')
    <td colspan='9' style="text-align: center">{{ __("Maaf, tiada data tersedia") }}</td>
  </table>
  @endif
@endsection
