@extends('layouts.pdf.master')

@section('title', 'Payment History')
@section('content')
  <div style="font-family:Arial; font-size:12px;">
       <div class="col-md-3">
           <img src="img/logo-ppj.png" height="100px">
      </div>
      <div class="col-md-9">
             <h2 style="text-align: center">{{ __("Kutipan Kewangan") }}</h2>
      </div>
  </div>
  <br>
  <table class="tg">
    <tr>
        <th class="tg-3wr7" width="1">{{ __('Bil') }}</th>
        <th class="tg-3wr7">{{ __('No. Kompaun') }}</th>
        <th class="tg-3wr7">{{ __('No. Pendaftaran Kenderaan') }}</th>
        <th class="tg-3wr7">{{ __('Amaun Perlu Bayar (RM)') }}</th>
        <th class="tg-3wr7">{{ __('Tarikh Bayaran') }}</th>
        <th class="tg-3wr7">{{ __('Jumlah Bayaran (RM)') }}</th>
        <th class="tg-3wr7">{{ __('Status Bayaran') }}</th>
    </tr>
    @if ($payments->isNotEmpty() )
    @foreach($payments as $payment)
        <tr>
            <td class="tg-rv4w" width="1">{{ $loop->iteration }}</td>
            <td class="tg-ti5e">{{ $payment->pymt_compoundno }}</td>
            <td class="tg-ti5e">{{ $payment->pymt_refno }}</td>
            <td class="tg-ti5e">{{ $payment->pymt_totalamount }}</td>
            <td class="tg-ti5e">{{ $payment->pymt_date }}</td>
            <td class="tg-ti5e">{{ $payment->pymt_totalamount }}</td>
            <td class="tg-ti5e">{{ $payment->paystatus }}</td>
        </tr>
    @endforeach
    @else
    <tr>
      <td colspan='8' class="tg-rv4w">{{ __("Maaf, tiada data tersedia") }}</td>
    </tr>
    @endif
  </table>
@endsection
