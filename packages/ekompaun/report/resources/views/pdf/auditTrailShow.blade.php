@extends('layouts.pdf.master')

@section('title', 'Appeal')
@section('content')

    <div style="font-family:Arial; font-size:12px;">
       <div class="col-md-3">
           <img src="img/logo-ppj.png" height="100px">
      </div>
      <div class="col-md-9">
            <h2 style="text-align: center">{{ __("Audit Trail") }}</h2>
      </div>
  </div>
<br>
<table class="tg">
	<tr>
		<th class="tg-ti5e">{{ __("Nama Pengguna") }}</th>
		<td class="tg-ti5e">{{ $application->transaction->tran_offendername }}</td>
		<th class="tg-ti5e">{{ __("Tarikh Kompaun") }} </th>
		<td class="tg-ti5e">{{ format_date($application->transaction->tran_compounddate) }}</td>
	</tr>
	<tr>
		<th class="tg-ti5e">{{ __("Id Pengguna") }}</th>
		<td class="tg-ti5e">{{ $application->transaction->tran_offendericno }}</td>
		<th class="tg-ti5e">{{ __("No Kompaun") }}</th>
		<td class="tg-ti5e">{{ $application->transaction->tran_compoundno }}</td>
	</tr>
	<tr>
		<th class="tg-ti5e">{{ __("Tindakan") }} </th>
		<td class="tg-ti5e">{{ $application->status->status_name }}</td>
		<th class="tg-ti5e">{{ __("No Rujukan") }}</th>
		<td class="tg-ti5e">{{ $application->transaction->tran_refno }}</td>
	</tr>
</table>
<br>
<table class="tg">
	<tr>
		<th class="tg-3wr7" width="1">{{ __("Bil") }}</th>
		<th class="tg-3wr7">{{ __("Aktiviti") }}</th>
		<th class="tg-3wr7">{{ __("Tarikh Tindakan") }}</th>
		<th class="tg-3wr7">{{ __("Pegawai") }}</th>
	</tr>
	@foreach($audits as $audit)
	<tr>
		<td class="tg-rv4w" width="1">{{ $loop->iteration }}</td>
		<td class="tg-rv4w">{{ $audit->task }}</td>
		<td class="tg-rv4w">{{ format_date($audit->created_date) }}</td>
		<td class="tg-rv4w">{{ $audit->user->name }}</td>
	</tr>
	@endforeach
</table>
@endsection
