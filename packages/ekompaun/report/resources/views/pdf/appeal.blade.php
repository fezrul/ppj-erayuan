@extends('layouts.pdf.master')

@section('title', 'Appeal')
@section('content')

  <div style="font-family:Arial; font-size:12px;">
       <div class="col-md-3">
           <img src="img/logo-ppj.png" height="100px">
      </div>
      <div class="col-md-9">
            <h2 style="text-align: center">{{ __("Kes Rayuan") }}</h2>
      </div>
  </div>
  <br>
  
  <table class="tg">
    <tr>
        <th class="tg-3wr7" width="1">{{ __("Bil") }}</th>
        <th class="tg-3wr7">{{ __("No Kompaun") }}</th>
        <th class="tg-3wr7">{{ __("Pelanggan") }}</th>
        <th class="tg-3wr7">{{ __("Kod Kesalahan") }}</th>
        <th class="tg-3wr7">{{ __("Tarikh Rayuan") }}</th>
        <th class="tg-3wr7">{{ __("Tarikh Bayaran") }}</th>
        <th class="tg-3wr7">{{ __("Tempoh Maklumbalas") }}</th>
        <th class="tg-3wr7">{{ __("Amaun (RM)") }}</th>
        <th class="tg-3wr7">{{ __("Pegawai") }}</th>
    </tr>
    @if ($applications->isNotEmpty() )
    @foreach($applications as $application)
        <tr>
            <td class="tg-rv4w" width="1">{{ $loop->iteration }}</td>
            <td class="tg-rv4w">{{ $application->present_no_kompaun }}</td>
            <td class="tg-ti5e">{{ $application->present_pelanggan }}</td>
            <td class="tg-ti5e">{{ $application->transaction->offence->offence_code }}</td>
            <td class="tg-rv4w">{{ $application->present_tarikh_rayuan }}</td>
            <td class="tg-rv4w">{{ $application->present_tarikh_bayaran }}</td>
            <td class="tg-ti5e">{{ $application->present_tempoh_maklumbalas }}</td>
            <td class="tg-ti5e">{{ $application->present_amaun }}</td>
            <td class="tg-ti5e">{{ $application->present_pegawai }}</td>
        </tr>
    @endforeach
    @else
    <tr>
        <td colspan='9' class="tg-rv4w">{{ __("Maaf, tiada data tersedia") }}</td>
    </tr>
    @endif
  </table>
@endsection
