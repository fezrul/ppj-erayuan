@extends('layouts.pdf.master')

@section('title', 'Appeal')
@section('content')
    <div style="font-family:Arial; font-size:12px;">
       <div class="col-md-3">
           <img src="img/logo-ppj.png" height="100px">
      </div>
      <div class="col-md-9">
            <h2 style="text-align: center">{{ __("Senarai Audit Trail") }}</h2>
      </div>
  </div>
  <br>
  <table class="tg">
  	<tr>
        <th class="tg-3wr7" width="1">{{ __("Bil") }}</th>
        <th class="tg-3wr7">{{ __("Id Pengguna") }}</th>
        <th class="tg-3wr7">{{ __("No Kompaun") }}</th>
        <th class="tg-3wr7">{{ __("Aktiviti") }}</th>
        <th class="tg-3wr7">{{ __("Tindakan") }}</th>
        <th class="tg-3wr7">{{ __("Tarikh Tindakan") }}</th>
    </tr>
    @if ($applications->isNotEmpty() )
    @foreach($applications as $application)
        <tr>
            <td class="tg-rv4w" width="1">{{ $loop->iteration }}</td>
            <td class="tg-ti5e">{{ $application->transaction->tran_offendericno }}</td>
            <td class="tg-ti5e">{{ $application->transaction->tran_compoundno }}</td>
            <td class="tg-ti5e">{{ $application->lastAuditrail->task }}</td>
            <td class="tg-ti5e">{{ $application->status->status_name }}</td>
            <td class="tg-ti5e">{{ format_date($application->appl_date) }}</td>
        </tr>
    @endforeach
    @else
    <tr>
      <td colspan='6' class="tg-rv4w">{{ __("Maaf, tiada data tersedia") }}</td>
    </tr>
    @endif
  </table>
@endsection
