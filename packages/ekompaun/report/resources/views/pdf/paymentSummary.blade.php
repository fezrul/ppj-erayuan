@extends('layouts.pdf.master')

@section('title', 'Payment Summary')
@section('content')
  <div style="font-family:Arial; font-size:12px;">
       <div class="col-md-3">
           <img src="img/logo-ppj.png" height="100px">
      </div>
      <div class="col-md-9">
             <h2 style="text-align: center">{{ __("Kutipan Kewangan :year", ['year' => $selectedYear]) }}</h2>
      </div>
  </div>
  <br>
  <table class="tg">
    <tr>
        <th class="tg-3wr7" width="1">{{ __('Bil') }}</th>
        <th class="tg-3wr7">{{ __('Kaedah Pembayaran') }}</th>
        @foreach($months as $month)
            <th class="tg-3wr7">{{ $month }}</th>
        @endforeach
        <th class="tg-3wr7">{{ __('Jumlah') }}</th>
    </tr>
    @php($total = 0)
    @if ($data->isNotEmpty() )
    <tbody>
    @foreach($data as $paymentMethod)
        <tr>
            <td class="tg-rv4w" width="1">{{ $loop->iteration }}</td>
            <td class="tg-rv4w">{{ $paymentMethod->paymethod_name }}</td>
            @foreach($paymentMethod->monthlyPayments as $amount)
                <td class="tg-rv4w" style="text-align: right">{{ format_money($amount) }}</td>
            @endforeach
            @php($total += $paymentMethod->monthlyPayments->sum())
            <td class="tg-rv4w" style="text-align: right">{{ format_money($paymentMethod->monthlyPayments->sum()) }}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot class="thead-light">
    <tr>
        <th colspan="14" class="tg-rv4w" style="text-align: right">{{ __('Jumlah') }}</th>
        <th class="tg-rv4w" style="text-align: right">{{ format_money($total) }}</th>
    </tr>
    </tfoot>
    @else
    <tr>
      <td colspan='15' class="tg-rv4w">{{ __("Maaf, tiada data tersedia") }}</td>  
    </tr>
    @endif
  </table>
@endsection
