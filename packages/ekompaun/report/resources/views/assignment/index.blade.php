@extends('layouts.app')
@section('content')
    <div class="my-5">
        <div class="card" style="overflow-x: auto;">
            <div class="card-header">
               
                <div class="card-actions">
                    <a href="{{ route('report::assignment-pdf.index', ['year' => request('year')])}}" target="blank"><i class="fa fa-file-pdf-o"></i> {{ __("Print PDF") }}</a>
                    <a href="{{ route('report::assignment-csv.index', ['year' => request('year')])}}" target="blank"><i class="fa fa-file-excel-o"></i> {{ __("Eksport CSV") }}</a>

                </div>
                <p></p><br>
                <h5>{{ __('Laporan Jumlah Kes Mengikut Pegawai J(U) :year', ['year' => $selectedYear]) }}</h5>
            </div>

            <div class="card-header">
                @include('report::assignment._filter')
            </div>
            <table class="table table-bordered mb-0 table-responsive">
                <thead class="thead-light">
                <tr>
                    <th>{{ __("Bil") }}</th>
                    <th>{{ __("Nama Pegawai") }}</th>
                    @foreach($months as $month)
                        <th>{{ $month }}</th>
                    @endforeach
                </tr>
                </thead>
                @foreach($staffs as $staff)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $staff->name }}</td>
                        @foreach($staff['assignments'] as $assigment)
                            <td class="text-center"> {{ $assigment }}</td>
                        @endforeach
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
