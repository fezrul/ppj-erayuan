@extends('layouts.app')
@section('content')
    <div class="my-5">
        <div class="card" style="overflow-x: scroll;">
            <div class="card-header">
                
                <div class="card-actions">
                    <a href="{{ route('report::payment-history-pdf.index', ['status' => request('status'), 'keyword' => request('keyword')]) }}" target="blank">
                        <i class="fa fa-file-pdf-o"></i> {{ __("Print PDF") }}
                    </a>
                    <a href="{{ route('report::payment-history-csv.index', ['status' => request('status'), 'keyword' => request('keyword')]) }}" target="blank">
                        <i class="fa fa-file-excel-o"></i> {{ __("Eksport CSV") }}
                    </a>
                </div>
                <p></p><br>
                <h5 class="mb-0">{{ __("Laporan Kutipan Kewangan") }}</h5>
            </div>
            <div class="card-header">
                @include('report::payment-history._filter')
            </div>
                        <table class="table table-bordered table-responsive mb-0" style="display:inline-table">
                <thead class="thead-light">
                <tr>
                    <th>{{ __('Bil') }}</th>
                    <th>{{ __('No. Kompaun') }}</th>
                    <th>{{ __('No. Rujukan Pembayaran') }}</th>
                    <th>{{ __('Amaun Perlu Bayar (RM)') }}</th>
                    <th>{{ __('Tarikh Bayaran') }}</th>
                    <th>{{ __('Jumlah Bayaran (RM)') }}</th>
                    <th>{{ __('Status Bayaran') }}</th>
                    <th>{{ __('Resit') }}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($payments as $payment)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $payment->pymt_compoundno }}</td>
                            <td>{{ $payment->pymt_refno }}</td>
                            <td>{{ $payment->pymt_totalamount }}</td>
                            <td>{{ $payment->pymt_date }}</td>
                            <td>{{ $payment->pymt_totalamount }}</td>
                            <td>{{ $payment->paystatus }}</td>
                             @if( $payment->pymt_status==1)
                            <td>   
                                <div class="dropdown">
                                    <button class="btn btn-secondary btn-sm dropdown-toggle" type="button"
                                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                        {{ __("Tindakan") }}
                                    </button>
                            
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                   
                                        <a class="dropdown-item"
                                           href="{{route('payment::pay-resit.show', data_get($payment,'pymt_id'))}}">{{ __("Resit") }}</a>
                                    
                                        <div class="dropdown-divider"></div>
                                    </div>
                                </div>  
                            </td>
                             @else
                             <td>
                                &nbsp;
                             </td>
                             @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="card-footer">
                {{ $payments->links() }}
            </div>
        </div>
    </div>

@endsection
