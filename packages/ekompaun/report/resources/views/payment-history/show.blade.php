@extends('layouts.app')
@section('content')
    <div class="my-5">
        <div class="card">
            <div class="card-header">
                <h5 class="mb-0">{{ __("Resit Resmi") }}</h5>
                <div class="card-actions">
                    <a href="{{ route('report::payment-receipt-pdf.index', ['appl_id' => $application->appl_id]) }}" target="_blank">
                        <i class="fa fa-file-pdf-o"></i> {{ __("PDF") }}
                    </a>
                </div>
                <p></p><br>
                <h5 class="mb-0">{{ __("Resit Resmi") }}</h5>
            </div>
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered"> 
                            <tr>
                                <th>{{ __("No. Resit") }}</th>
                                <th>{{ __("Tarikh") }}</th>
                                <th>{{ __("MOD") }}</th>
                                <th>{{ __("No. Dokumen") }}</th>
                                <th>{{ __("Jumlah") }}</th>
                            </tr>
                            <tr>
                                <td>{{ $application->present_payment_receipt_number }}</td>
                                <td>{{ $application->present_tarikh_bayaran }}</td>
                                <td>{{ $application->present_payment_payment_method }}</td>
                                <td></td>
                                <td>{{ $application->present_payment_amount }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-bordered">
                                <th>{{ __("No. ID Pelanggan :") }}</th>
                                <td>{{ $application->present_pelanggan_id }}</td><tr>
                                <th>{{ __("FPX ID :") }}</th>
                                <td>{{ $application->present_payment_fpx_trans_id }}</td><tr>
                                <th>{{ __("Approval Code :") }}</th>
                                <td></td><tr>
                                <th>{{ __("Nama Bank :") }}</th>
                                <td>{{ $application->present_payment_fpx_bank }}</td><tr>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-bordered">
                                <th>{{ __("Rujukan :") }}</th>
                                <td>{{ $application->present_payment_refno }}</td><tr>
                                <th>{{ __("No. Kenderaan :") }}</th>
                                <td>{{ $application->present_no_daftar_kenderaan }}</td><tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>{{ __("Untuk Bayaran") }}</th>
                                        <th>{{ __("KOD GST") }}</th>
                                        <th>{{ __("Amaun(RM)") }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ __("BAYARAN LALULINTAS") }}</td>
                                        <td>{{ __("OS") }}</td>
                                        <td>{{ $application->present_payment_amount }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-4" style="text-align: center;">
                    <small>{{ __("Resit ini cetakan komputer, tiada tanda tangan diperlukan.") }}</small>
                </div>
                <div class="card-footer">
                </div>
            </div>
        </div>
    </div>
@endsection
