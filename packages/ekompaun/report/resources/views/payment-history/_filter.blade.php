{{ html()->form('GET', route('report::payment-history.index') )->open() }}
<div class="row">
    <div class="col-md-3">
        {{ html()->formGroup(__('Status'), html()->select('status', $paymentStatus, request('status'))) }}
    </div>
    <div class="col-md-7">
        {{ html()->formGroup(__('No. Kompaun / No. Rujukan Pembayaran'), html()->text('keyword', request('keyword'))) }}
    </div>
    <div class="col-md-2">
        {{ html()->submit(__("Jana Laporan"))->class(['btn btn-secondary']) }}
    </div>
</div>
{{ html()->form()->close() }}
