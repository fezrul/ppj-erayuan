@extends('layouts.app')
@section('content')
    <div class="my-5">
        <div class="card" style="overflow-x: auto">
            <div class="card-header">
                
                <div class="card-actions">
                    <a href="{{ route('report::audit-application-pdf.index', ['search' => request('search')]) }}" target="blank"><i class="fa fa-file-pdf-o"></i> {{ __("Print PDF") }}</a>
                    <a href="{{ route('report::audit-application-csv.index', ['search' => request('search')]) }}" target="blank"><i class="fa fa-file-excel-o"></i> {{ __("Eksport CSV") }}</a>
                </div>
                 <p></p><br>
                 <h5 class="mb-0">{{ __("Jejak Audit Rayuan") }}</h5>
            </div>
            <div class="card-header">
                @include('report::audit-application._search')
            </div>
            @if ($applications->isNotEmpty() )
                <table class="table table-striped  mb-0" >
                    <thead class="thead-light">
                    <tr>
                        <th>{{ __("Bil") }}</th>
                        <th>{{ __("Id Pengguna") }}</th>
                        <th>{{ __("No Kompaun") }}</th>
                        <th>{{ __("Aktiviti") }}</th>
                        <th>{{ __("Tindakan") }}</th>
                        <th>{{ __("Tarikh Tindakan") }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($applications as $application)
                        <tr>
                            <td>{{ numbering($application, $applications) }}</td>
                            <td>{{ $application->transaction->tran_offendericno }}</td>
                            <td>
                            	<a href="{{ route('report::audit-application.show', $application) }}">
                            		{{ $application->transaction->tran_compoundno }}
                            	</a>
                            </td>
                            <td>{{ $application->lastAuditrail->task }}</td>
                            <td>{{ $application->status->status_name }}</td>
                            <td>{{ format_date($application->appl_date) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                @include('components.empty')
            @endif
            <div class="card-footer">
                {{ $applications->links() }}
            </div>
        </div>
    </div>
@endsection
