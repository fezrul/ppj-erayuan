@extends('layouts.app')
@section('content')
    <div class="my-5">
        <div class="card">
            <div class="card-header">
                <div class="card-actions">
                    <a href="{{ route('report::audit-application-pdf.show', $application) }}" target="blank"><i class="fa fa-file-pdf-o"></i> {{ __("PDF") }}</a>
                    <a href="{{ route('report::audit-application-csv.show', $application) }}" target="blank"><i class="fa fa-file-excel-o"></i> {{ __("Excel") }}</a>
                </div>
                <p></p><br>
                 <h5 class="mb-0">{{ __("Jejak Audit Rayuan") }}</h5>
            </div>
            <div class="card-body">
                <div class="row">
                	<div class="col-sm">
                		<h4>Detail Kompaun</h4>
                	</div>
	            </div>
            	<div class="row">
	                <div class="col-sm-2">{{ __("Nama Pengguna") }} </div>
	                <div class="col-sm">: {{$application->transaction->tran_offendername}}</div>
	                <div class="col-sm-2">{{ __("Tarikh Kompaun") }} </div>
	                <div class="col-sm">: {{ format_date($application->transaction->tran_compounddate) }}</div>
	                <div class="w-100"></div>
	                <div class="col-sm-2">{{ __("Id Pengguna") }} </div>
	                <div class="col-sm">: {{$application->transaction->tran_offendericno}}</div>
	                <div class="col-sm-2">{{ __("No Kompaun") }} </div>
	                <div class="col-sm">: {{$application->transaction->tran_compoundno}}</div>
	                <div class="w-100"></div>
	                <div class="col-sm-2">{{ __("Tindakan") }} </div>
	                <div class="col-sm">: {{$application->status->status_name}}</div>
	                <div class="col-sm-2">{{ __("No Rujukan") }} </div>
	                <div class="col-sm">: {{$application->transaction->tran_refno}}</div>
	                <div class="w-100"></div>
            	</div>
            </div>
			@if ($audits->isNotEmpty() )
                <table class="table table-striped mb-0">
                    <thead class="thead-light">
                    <tr>
                        <th>{{ __("Bil") }}</th>
                        <th>{{ __("Aktiviti") }}</th>
                        <th>{{ __("Tarikh Tindakan") }}</th>
                        <th>{{ __("Pegawai") }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($audits as $audit)
                        <tr>
                            <td>{{ numbering($audit, $audits) }}</td>
                            <td>{{ $audit->task }}</td>
                            <td>{{ format_date($audit->created_date) }}</td>
                            <td>{{ $audit->user->name }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                @include('components.empty')
            @endif
            <div class="card-footer">
                {{ $audits->links() }}
            </div>
            <div class="card-footer">
                <a href="{{ route('report::audit-application.index') }}" class="btn btn-sm btn-primary">{{ __("Kembali") }}</a>
            </div>
        </div>
    </div>
@endsection
