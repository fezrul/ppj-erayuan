{{ html()->form('GET', route('report::audit-application.index') )->open() }}
    <div class="row">
        <div class="col-md-10">
            {{ html()->formGroup(__('No Kompaun / Id Pengguna'), html()->text('search', request('search'))) }}
        </div>
        <div class="col-md-2">
        	{{ html()->submit(__("Carian"))->class(['btn btn-secondary btn-block'])->style('background-color: darkmagenta') }}
        </div>

    </div>

{{ html()->form()->close() }}
