<div class="modal fade" id="modal-audit" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{ __("Semak Audit Trail") }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
        	<div class="col-sm-6">
        		<h4>Old Value</h4>
        		<table class="table table-striped table-responsive">
        			<thead>
        				<tr>
        					<th>{{ __("Key") }}</th>
        					<th>{{ __("Value") }}</th>
        				</tr>
        			</thead>
        			<tbody id="body-old">
        			</tbody>
        		</table>
        	</div>
        	<div class="col-sm-6">
        		<h4>New Value</h4>
        		<table class="table table-striped table-responsive">
        			<thead>
        				<tr>
        					<th>{{ __("Key") }}</th>
        					<th>{{ __("Value") }}</th>
        				</tr>
        			</thead>
        			<tbody id="body-new">
        			</tbody>
        		</table>
        	</div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __("Kembali") }}</button>
      </div>
    </div>
  </div>
</div>