@extends('layouts.app')
@section('content')
    <div class="my-5">
        <div class="card" style="overflow-x: auto;">
            <div class="card-header">
                
                <div class="card-actions">
                    <a href="{{ route('report::audit-trail-pdf.index', ['search' => request('search')]) }}" target="blank"><i class="fa fa-file-pdf-o"></i> {{ __("Print PDF") }}</a>
                    <a href="{{ route('report::audit-trail-csv.index', ['search' => request('search')]) }}" target="blank"><i class="fa fa-file-excel-o"></i> {{ __("Eksport CSV") }}</a>
                </div>
                <p></p><br>
                <h5 class="mb-0">{{ __("Senarai Audit Trail") }}</h5>
            </div>
            <div class="card-header">
                @include('report::audit-trail._search')
            </div>
            @if ($audits->isNotEmpty() )
                <table class="table table-striped mb-0">
                    <thead class="thead-light">
	                    <tr>
	                        <th>{{ __("Bil") }}</th>
	                        <th>{{ __("ID Pengguna") }}</th>
	                        <th>{{ __("Aktiviti") }}</th>
	                        <th>{{ __("Model") }}</th>
	                        <th>{{ __("Tarikh Aktiviti") }}</th>
	                    </tr>
                    </thead>
                    <tbody>
                    @foreach($audits as $audit)
                        <tr>
                            <td>{{ numbering($audit, $audits) }}</td>
                            <td>
                            	<a href="#" class="detailAudit" id="{{ $audit->id }}">
                            	{{ $audit->user->name ?? '-' }}
                            	</a>
                            </td>
                            <td>{{ $audit->event }}</td>
                            <td>{{ $audit->auditable_type }}</td>
                            <td>{{ format_date($audit->created_at) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                @include('components.empty')
            @endif
            <div class="card-footer">
                <!-- {{ $audits->links() }} -->
                <?php 

                    if(isset($_GET['search'])) {$par = $_GET['search'];}else{$par = '';}


                ?>
                {!! str_replace('/?', '?', 

                        $audits->appends(['search' => "{$par}"])->render()) 
                !!}
            </div>
        </div>
    </div>
    @include('report::audit-trail._modal')
@endsection
@push('end')
    <script>
        $(function(){
           $(".detailAudit").click(function() {
           		var id    = this.id;
           		var modal = $("#modal-audit");

           		$.get('audit-trail/'+id, function(data, status){
			        if (status == 'success') {
			        	var olds    = data.old_values;
			        	var news    = data.new_values;
			        	var bodyOld = '';
			        	var bodyNew = '';

			        	for (var x in olds) {
			        		bodyOld += '<tr>\
				        		<td>'+x+'</td>\
				        		<td>'+olds[x]+'</td>\
			        		</tr>';
			        	}

			        	for (var x in news) {
			        		bodyNew += '<tr>\
				        		<td>'+x+'</td>\
				        		<td>'+news[x]+'</td>\
			        		</tr>';
			        	}

			        	$("#body-old").html(bodyOld);
			        	$("#body-new").html(bodyNew);
			        	modal.modal('show');
			        }
			    }, 'json');
           })
        });
    </script>
@endpush
