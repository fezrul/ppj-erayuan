{{ html()->form('GET', route('report::audit-trail.index') )->open() }}
    <div class="row">
        <div class="col-md-12">
		    <div class="form-group row">
		    	<label class="col-sm-2 col-form-label">{{ __('Search') }}</label>
		    	<div class="col-sm-8">
		    		{{ html()->text('search', request('search'))->addClass('form-control') }}
		    		<span class="text-muted small">Ex: created, user, pentadbir</span>
		    	</div>
		    	<div class="col-sm-2">
		    		{{ html()->submit(__("Carian"))->class(['btn btn-secondary btn-block'])->style('background-color: darkmagenta') }}
		    	</div>
		    </div>
		</div>
    </div>

{{ html()->form()->close() }}
