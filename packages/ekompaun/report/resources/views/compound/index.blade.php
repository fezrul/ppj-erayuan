@extends('layouts.app')
@section('content')
    <div class="my-5">
        <div class="card">
            <div class="card-header">
                
                <div class="card-actions">
                    <a href="{{ route('report::compound-pdf.index', ['year' => request('year')]) }}" target="blank"><i
                                class="fa fa-file-pdf-o"></i> {{ __("Print PDF") }}</a>

                    <a href="{{ route('report::compound-csv.index', ['year' => request('year')]) }}" target="blank"><i
                                class="fa fa-file-excel-o"></i> {{ __("Eksport CSV") }}</a>
                </div>
                <p></p><br>
                <h5 class="mb-0">{{ __("Laporan Rayuan Mengikut Kesalahan Tahun :year", ['year' => $selectedYear]) }}</h5>
            </div>
            <div class="card-header">
                @include('report::compound._filter')
            </div>
            <table class="table table-bordered mb-0 table-responsive">
                <thead class="thead-light">
                <tr>
                    <th>{{ __('Jenis Kesalahan') }}</th>
                    @foreach($months as $month)
                        <th>{{ $month }}</th>
                    @endforeach
                    <th>{{ __('Jumlah') }}</th>
                </tr>
                </thead>
                @php($total = 0)
                @foreach($report as $act => $data)
                    <thead>
                    <tr class="text-center">
                        <th colspan="13">{{ $act }}</th>
                        <th>{{ $data->reduce(function ($carry, $item) { return $carry + $item->compounds->sum();}) }}</th>
                    </tr>
                    </thead>
                    <tbody class="text-center">
                    @foreach($data as $section)
                        <tr>
                            <td>{{ $section->section_name }}</td>
                            @foreach($section->compounds as $compound)
                                <td class="{{ ($compound > 0) ? 'bg-light':'' }}">{{ $compound }}</td>
                            @endforeach
                            <td>{{ $section->compounds->sum() }}</td>
                        </tr>
                        @php($total += $section->compounds->sum())
                    @endforeach
                    </tbody>
                @endforeach
                <tfoot class="thead-light">
                <tr>
                    <th colspan="13" class="text-right">{{ __('Jumlah') }}</th>
                    <th class="text-center">{{ $total }}</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>

@endsection
