@extends('layouts.app')
@section('content')
    <div class="my-5">
        <div class="card">
            <div class="card-header">
               
                <div class="card-actions">
                    <a href="{{ route('report::appeal-pdf.index', ['pegawai' => request('pegawai'), 'year' => request('year'), 'month' => request('month'), 'kesalahan' => request('kesalahan'), 'tempoh' => request('tempoh')]) }}" target="blank"><i class="fa fa-file-pdf-o"></i> {{ __("Print PDF") }}</a>
                    <a href="{{ route('report::appeal-csv.index', ['pegawai' => request('pegawai'), 'year' => request('year'), 'month' => request('month'), 'kesalahan' => request('kesalahan'), 'tempoh' => request('tempoh')]) }}" target="blank"><i class="fa fa-file-excel-o"></i> {{ __("Eksport CSV") }}</a>
                </div>
                 <p></p><br>
                  <h5 class="mb-0">{{ __("Kes Rayuan") }}</h5>
            </div>
            <div class="card-header">
                @include('report::appeal._search')
            </div>

            {{--@if($searching)--}}
                @include('report::appeal._data')
            {{--@endif--}}
        </div>
    </div>
@endsection

@push('end')
<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="akta"]').on('change', function() {
            var aktaID = $(this).val();
            $.ajax({
                url: '/report/offence/'+aktaID,
                type: "GET",
                dataType: "json",
                success:function(data) {
                    data = sortByKey(data);
                    $('select[name="kesalahan"]').empty();
                    $.each(data, function(key, value) {
                        $('select[name="kesalahan"]').append('<option value="'+ value[0] +'">'+ value[1] +'</option>');
                    });
                }
            });
        });
    });

    function sortByKey(jsObj){
        var sortedArray = [];

        // Push each JSON Object entry in array by [key, value]
        for(var i in jsObj)
        {
            sortedArray.push([i, jsObj[i]]);
        }

        // Run native sort function and returns sorted array.
        return sortedArray.sort();
    }
</script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script type="text/javascript">
$(function() {
    $('input[name="date_from"], input[name="date_to"]').daterangepicker({
        locale: {
          format: 'DD-MM-YYYY'
        },
        singleDatePicker: true,
        showDropdowns: true
    },
    function(start, end, label) {
    });
});
</script>
@endpush
