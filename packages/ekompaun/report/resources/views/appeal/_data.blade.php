@if ($applications->isNotEmpty() )
    <table class="table table-striped mb-0 table-responsive">
        <tbody>
        <thead class="thead-light">
        <tr>
            <th>{{ __("Bil") }}</th>
            <th>{{ __("No Kompaun") }}</th>
            <th>{{ __("Pelanggan") }}</th>
            <th>{{ __("Kod Kesalahan") }}</th>
            <th>{{ __("Tarikh Rayuan") }}</th>
            <th>{{ __("Tarikh Bayaran") }}</th>
            <th>{{ __("Tempoh Maklumbalas") }}</th>
            <th>{{ __("Jenis Rayuan") }}</th>
            <th>{{ __("Amaun (RM)") }}</th>
            <th>{{ __("Pegawai") }}</th>
        </tr>
        </thead>
        @foreach($applications as $application)
            <tr>
                <td>{{ numbering($application, $applications) }}</td>
                <td>{{ $application->present_no_kompaun }}</td>
                <td>{{ $application->present_pelanggan }}</td>
                <td>{{ $application->transaction->offence->offence_code }}</td>
                <td>{{ $application->present_tarikh_rayuan }}</td>
                <td>{{ $application->present_tarikh_bayaran }}</td>
                <td>{{ $application->present_tempoh_maklumbalas }}</td>
                <td>{{ $application->present_jenis_rayuan }}</td>
                <td>{{ $application->present_amaun }}</td>
                <td>{{ $application->present_pegawai }}</td>
            </tr>
            @endforeach
            </tbody>
    </table>
@else
    @include('components.empty')
@endif
<div class="card-footer">
    {{ $applications->appends(request()->input())->links() }}
</div>
