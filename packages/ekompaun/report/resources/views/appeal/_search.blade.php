{{ html()->form('GET', route('report::appeal.index') )->open() }}
    <div class="row">
        <div class="col-md-6">
            {{ html()->formGroup(__('Pegawai'), html()->select('pegawai', $pegawai, request('pegawai'))) }}
            {{ html()->formGroup(__('Tahun Rayuan'), html()->select('year', $years, request('year'))) }}
            {{ html()->formGroup(__('Bulan'), html()->select('month', $months, request('month'))) }}
            {{ html()->formGroup(__('Status'), html()->select('status', $status, request('status'))) }}
        </div>
        <div class="col-md-6">
            {{ html()->formGroup(__('Akta'), html()->select('akta', $akta, request('akta'))) }}
            {{ html()->formGroup(__('Kesalahan'), html()->select('kesalahan', $kesalahan, request('kesalahan'))) }}
            {{ html()->formGroup(__('Tempoh Maklumbalas'), html()->radioGroup('tempoh', $tempoh, request('tempoh'))) }}
        </div>
        <div class="col-md-6">
            {{ html()->formGroup(__("Tarikh Dari"), html()->input('text','date_from', request('date_from'))->class(['form-control'])) }}
        </div>
        <div class="col-md-6">
            {{ html()->formGroup(__("Tarikh Hingga"), html()->input('text','date_to', request('date_to'))->class(['form-control'])) }}
        </div>
                    
    </div>
    <div class="col-md-2" style="float:right">
        {{ html()->submit(__("Carian"))->class(['btn btn-secondary btn-block'])->style('background-color: darkmagenta') }}
    </div>

    

{{ html()->form()->close() }}
