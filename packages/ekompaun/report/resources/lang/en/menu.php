<?php

return [
    'appeal'               => 'Appeal',
    'appeal_yearly'        => 'Yearly Appeal',
    'audit_trail'          => 'Audit Trail',
    'compound_by_campaign' => 'Compound by Campaign',
    'income_daily'         => 'Daily Income',
    'performance'          => 'Officer Performance',
    'report'               => 'Report',
];
