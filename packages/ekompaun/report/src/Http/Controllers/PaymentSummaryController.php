<?php

namespace Ekompaun\Report\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Ekompaun\Report\Repositories\PaymentReportRepository;
use Ekompaun\Systemconfig\Enum\Month;
use Ekompaun\Systemconfig\Enum\Year;

class PaymentSummaryController extends Controller
{
    public function index(PaymentReportRepository $report)
    {
        $years = Year::dropdown();
        $months = Month::dropdown('M');
        $selectedYear = request('year', Carbon::now()->year);
        $data = $report->totalKutipanByPaymentMethod($selectedYear);
        
        return view('report::payment-summary.index', compact('years', 'months', 'selectedYear', 'data'));
    }
}
