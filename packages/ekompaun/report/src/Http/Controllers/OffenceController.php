<?php

namespace Ekompaun\Report\Http\Controllers;

use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Model\Lookup\Offence;

class OffenceController extends Controller
{
    public function index($id = null)
    {
        if ($id) {
            $kesalahan = Offence::where('fk_lkp_actid', $id)
                ->pluck('offence_name', 'offence_id')
                ->prepend(__('Semua'), '');

            return response()->json($kesalahan);
        }

        $kesalahan = Offence::dropdown(__('Semua'));

        return response()->json($kesalahan);
    }
}
