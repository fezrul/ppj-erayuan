<?php

namespace Ekompaun\Report\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Ekompaun\Report\Repositories\AppealReportRepository;
use Ekompaun\Systemconfig\Enum\Month;
use Ekompaun\Systemconfig\Enum\Year;

class CompoundController extends Controller
{
    public function index(AppealReportRepository $report)
    {
        $years = Year::dropdown();
        $months = Month::dropdown('M');
        $selectedYear = request('year', Carbon::now()->year);
        $report = $report->totalCompoundByOffence($selectedYear);

        return view('report::compound.index', compact('years', 'months', 'selectedYear', 'report'));
    }
}
