<?php

namespace Ekompaun\Report\Http\Controllers;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Ekompaun\Report\Repositories\PaymentReportRepository;

class ReceiptDailyController extends Controller
{
    public function index(PaymentReportRepository $report)
    {
    	$dateFrom = $dateTo = Carbon::now()->format('Y-m-d');
    	if (request('date_from')) {
            $dateFrom = format_date(request('date_from'), 'd-m-Y', 'Y-m-d');
    	}
    	if (request('date_to')) {
            $dateTo = format_date(request('date_to'), 'd-m-Y', 'Y-m-d');
    	}

        $applications = $report->applicationPaymentDateExport($dateFrom, $dateTo);

        return view('report::receipt-daily.index', compact('applications', 'dateFrom', 'dateTo'));
    }
}
