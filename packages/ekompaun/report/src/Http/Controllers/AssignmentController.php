<?php

namespace Ekompaun\Report\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Ekompaun\Report\Repositories\AppealReportRepository;
use Ekompaun\Systemconfig\Enum\Month;
use Ekompaun\Systemconfig\Enum\Year;

class AssignmentController extends Controller
{
    public function index(AppealReportRepository $report)
    {
        $selectedYear = request('year', Carbon::now()->year);
        $staffs = $report->totalAssignmentByStaff($selectedYear);
        $months = Month::dropdown('M');
        $years = Year::dropdown();

        return view('report::assignment.index', compact('staffs', 'selectedYear', 'months', 'years'));
    }
}
