<?php

namespace Ekompaun\Report\Http\Controllers\Pdf;

use PDF;
use App\Http\Controllers\Controller;
use Ekompaun\Report\Repositories\AuditTrailReportRepository;

class AuditTrailPdfController extends Controller
{
    public function index(AuditTrailReportRepository $report)
    {
        $audits = $report->senaraiAudit(request('search'))->get();

        $pdf = PDF::loadview('report::pdf.audit', compact('audits'))->setPaper('a4');
        return $pdf->download(__('Audit Trail.pdf'));
    }
}
