<?php

namespace Ekompaun\Report\Http\Controllers\Pdf;

use PDF;
use App\Http\Controllers\Controller;
use Ekompaun\Report\Repositories\AuditTrailReportRepository;

class ApplicationAuditTrailPdfController extends Controller
{
    public function index(AuditTrailReportRepository $report)
    {
        $applications = $report->senaraiAuditApplication(request('search'))->get();

        $pdf = PDF::loadview('report::pdf.auditTrailIndex', compact('applications'))->setPaper('a4');

        return $pdf->download(__('Senarai Audit Trail.pdf'));
    }

    public function show(AuditTrailReportRepository $report, $id)
    {
        list($application, $audits) = $report->detailAuditApplication($id);
        $audits = $audits->get();

        $pdf = PDF::loadview('report::pdf.auditTrailShow', compact('application', 'audits'))->setPaper('a4');

        return $pdf->download(__('Audit Trail.pdf'));
    }
}
