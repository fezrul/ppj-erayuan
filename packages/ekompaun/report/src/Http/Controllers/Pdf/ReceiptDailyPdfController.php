<?php

namespace Ekompaun\Report\Http\Controllers\Pdf;

use PDF;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Ekompaun\Report\Repositories\PaymentReportRepository;

class ReceiptDailyPdfController extends Controller
{
    public function index(PaymentReportRepository $report)
    {
        $dateFrom = $dateTo = Carbon::now()->format('Y-m-d');
        if (request('date_from')) {
            $dateFrom = format_date(request('date_from'), 'd-m-Y', 'Y-m-d');
        }
        if (request('date_to')) {
            $dateTo = format_date(request('date_to'), 'd-m-Y', 'Y-m-d');
        }

        $applications = $report->applicationPaymentDateExport($dateFrom, $dateTo);

        $pdf = PDF::loadview('report::pdf.receiptDaily', compact('applications', 'dateFrom', 'dateTo'))->setPaper('a4', 'landscape');
        return $pdf->stream(__("Laporan Terimaan Harian :from to :to.pdf", ['from' => $dateFrom, 'to' => $dateTo]));
    }
}
