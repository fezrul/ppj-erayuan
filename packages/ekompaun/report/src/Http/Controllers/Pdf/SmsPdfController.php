<?php

namespace Ekompaun\Report\Http\Controllers\Pdf;

use PDF;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Enum\StatusKompaun;
use Ekompaun\Report\Repositories\SmsReportRepository;

class SmsPdfController extends Controller
{
    public function index(SmsReportRepository $report)
    {
        $byStatus = request('status')?:StatusKompaun::MENUNGGU_KELULUSAN;
        $dateNow = Carbon::now();
        $dateTo = (request('to'))?format_date(request('to'), 'd-m-Y', 'Y-m-d'):$dateNow->format('Y-m-d');
        $dateFrom = (request('from'))?format_date(request('from'), 'd-m-Y', 'Y-m-d'):
        $dateNow->subDays(30)->format('Y-m-d');
        $notifications = $report->exportSms($byStatus, $dateFrom, $dateTo);

        $pdf = PDF::loadview('report::pdf.sms', compact('notifications'))->setPaper('a4');
        return $pdf->download(__('Notification SMS.pdf'));
    }
}
