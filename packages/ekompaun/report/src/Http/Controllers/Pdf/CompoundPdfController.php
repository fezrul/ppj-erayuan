<?php

namespace Ekompaun\Report\Http\Controllers\Pdf;

use PDF;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Enum\Year;
use Ekompaun\Systemconfig\Enum\Month;
use Ekompaun\Report\Repositories\AppealReportRepository;

class CompoundPdfController extends Controller
{
    public function index(AppealReportRepository $report)
    {
        $months = Month::dropdown('M');
        $selectedYear = request('year', Carbon::now()->year);
        $report = $report->totalCompoundByOffence($selectedYear);

        $pdf = PDF::loadview(
            'report::pdf.compound',
            compact('months', 'selectedYear', 'report')
        )->setPaper('a4');

        return $pdf->download(__('Tahunan Kompoun.pdf'));
    }
}
