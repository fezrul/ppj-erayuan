<?php

namespace Ekompaun\Report\Http\Controllers\Pdf;

use PDF;
use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Model\Lookup\Status;
use Ekompaun\Report\Repositories\PaymentReportRepository;

class PaymentHistoryPdfController extends Controller
{
    public function index(PaymentReportRepository $report)
    {
        $payments = $report->paymentStatusExport(request('status'), request('keyword'));

        $pdf = PDF::loadview('report::pdf.paymentHistory', compact('payments'))->setPaper('a4');

        return $pdf->download(__('Kutipan Kewangan.pdf'));
    }
}
