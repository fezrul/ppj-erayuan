<?php

namespace Ekompaun\Report\Http\Controllers\Pdf;

use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ekompaun\Appeal\Model\Application;

class PaymentReceiptPdfController extends Controller
{
    public function index(Request $request)
    {
        $application = Application::findOrFail($request->appl_id);
        $pdf = PDF::loadview('report::pdf.paymentReceipt', compact('application'))->setPaper('a4', 'landscape');
        return $pdf->download(__('Resit-'.$application->present_payment_receipt_number.'.pdf'));
    }
}
