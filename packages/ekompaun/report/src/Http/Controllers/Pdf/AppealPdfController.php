<?php

namespace Ekompaun\Report\Http\Controllers\Pdf;

use PDF;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Enum\Year;
use Ekompaun\Systemconfig\Enum\Month;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Systemconfig\Model\Lookup\Offence;

class AppealPdfController extends Controller
{
    public function index(Request $request)
    {
        $applications = Application::with('transaction.user', 'personInCharge', 'payment')
                                   ->typeOfRayuanKedua()
                                   ->byTempohMaklumBalas($request->tempoh)
                                   ->assignedTo($request->pegawai)
                                   ->byYear($request->year)
                                   ->byMonth($request->month)
                                   ->byAkta($request->akta)
                                   ->byOffence($request->kesalahan)
                                   ->byProgress($request->status)
                                   ->get();

        $pdf = PDF::loadview('report::pdf.appeal', compact('applications'))->setPaper('a4');

        return $pdf->stream(__('Kes Rayuan.pdf'));
    }
}
