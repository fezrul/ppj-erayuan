<?php

namespace Ekompaun\Report\Http\Controllers\Pdf;

use PDF;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Enum\Month;
use Ekompaun\Report\Repositories\AppealReportRepository;

class AssignmentPdfController extends Controller
{
    public function index(AppealReportRepository $report)
    {
        $selectedYear = request('year', Carbon::now()->year);
        $staffs = $report->totalAssignmentByStaff($selectedYear);
        $months = Month::dropdown('M');

        $pdf = PDF::loadview('report::pdf.assignment', compact('staffs', 'selectedYear', 'months'))->setPaper('a4');
        return $pdf->download(__("Kes Mengikut Pegawai Tahun :year.pdf", ['year' => $selectedYear]));
    }
}
