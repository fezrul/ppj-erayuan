<?php

namespace Ekompaun\Report\Http\Controllers\Pdf;

use PDF;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Enum\Year;
use Ekompaun\Systemconfig\Enum\Month;
use Ekompaun\Report\Repositories\PaymentReportRepository;

class PaymentSummaryPdfController extends Controller
{
    public function index(PaymentReportRepository $report)
    {
        $months = Month::dropdown('M');
        $selectedYear = request('year', Carbon::now()->year);
        $data = $report->totalKutipanByPaymentMethod($selectedYear);
        
        $pdf = PDF::loadview('report::pdf.paymentSummary', compact('months', 'selectedYear', 'data'))->setPaper('a4');

        return $pdf->download(__("Kutipan kewangan $selectedYear.pdf"));
    }
}
