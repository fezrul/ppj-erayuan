<?php

namespace Ekompaun\Report\Http\Controllers;

use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Model\Audit;
use Ekompaun\Report\Repositories\AuditTrailReportRepository;

class AuditTrailController extends Controller
{
    public function index(AuditTrailReportRepository $report)
    {
        $audits = $report->senaraiAudit(request('search'))->paginate();
        
        return view('report::audit-trail.index', compact('audits'));
    }

    public function show($id)
    {
        $audits = Audit::find($id)->toArray();

        return response()->json($audits);
    }
}
