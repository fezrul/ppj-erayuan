<?php

namespace Ekompaun\Report\Http\Controllers;

use App\User;
use Ekompaun\Systemconfig\Enum\AppealProgress;
use Ekompaun\Systemconfig\Model\Holiday;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Enum\Year;
use Ekompaun\Systemconfig\Enum\Month;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Systemconfig\Model\Lookup\Act;
use Ekompaun\Systemconfig\Model\Lookup\Offence;
use Carbon\Carbon;

class AppealController extends Controller
{
    public function index(Request $request)
    {
        $pegawai = User::pegawaiJu()->pluck('name', 'id')->prepend(__('Semua'), '');
        $years = Year::dropdown()->prepend(__('Semua'), '');
        $months = Month::dropdown()->prepend(__('Semua'), '');
        $kesalahan = Offence::pluck('offence_name', 'offence_id')
            ->prepend(__('Semua'), '')
            ->sortBy('offence_name');

        $akta = Act::pluck('act_name', 'act_id')->prepend(__('Semua'), '');
        $tempoh = [-7 => '<= 7 hari', 7 => '> 7 hari'];
        $status = collect(AppealProgress::dropdown())->prepend(__('Semua'), '');

        $dateFrom = Carbon::now()->format('Y-m-d');
        $dateTo = Carbon::now()->format('Y-m-d');

        if (request('date_from')) {

            $dateFrom = format_date(request('date_from'), 'd-m-Y', 'Y-m-d');
        }
        if (request('date_to')) {

            $dateTo = format_date(request('date_to'), 'd-m-Y', 'Y-m-d');
        }

        // $applications = $searching = false;
        // if ($request->hasAny('pegawai', 'year', 'month', 'kesalahan', 'tempoh')) {
            $searching = true;
            $applications = Application::with('transaction.user', 'personInCharge', 'payment')
                ->typeOfRayuanKedua()
                ->byTempohMaklumBalas($request->tempoh)
                ->assignedTo($request->pegawai)
                ->byYear($request->year)
                ->byMonth($request->month)
                ->byAkta($request->akta)
                ->byOffence($request->kesalahan)
                ->byProgress($request->status)
                ->where('created_date', '>=' ,$dateFrom)
                ->where('created_date', '<=' ,$dateTo)
                ->orderByDesc('created_date')
                ->paginate();
        // }

        return view(
            'report::appeal.index',
            compact('applications', 'pegawai', 'years', 'months', 'kesalahan', 'tempoh', 'akta', 'searching', 'status','dateFrom','dateTo')
        );
    }
}
