<?php

namespace Ekompaun\Report\Http\Controllers;

use App\Http\Controllers\Controller;

class StaffDetailController extends Controller
{
    public function index()
    {
        return view('report::staff-detail.index');
    }
}
