<?php

namespace Ekompaun\Report\Http\Controllers;

use App\Http\Controllers\Controller;
use Ekompaun\Report\Repositories\AuditTrailReportRepository;

class ApplicationAuditTrailController extends Controller
{
    public function index(AuditTrailReportRepository $report)
    {
        $applications = $report->senaraiAuditApplication(request('search'))
            ->paginate()
            ->appends(['search' => request('search')]);

        return view('report::audit-application.index', compact('applications'));
    }

    public function show(AuditTrailReportRepository $report, $id)
    {
        list($application, $audits) = $report->detailAuditApplication($id);
        $audits = $audits->paginate();

        return view('report::audit-application.show', compact('application', 'audits'));
    }
}
