<?php

namespace Ekompaun\Report\Http\Controllers;

use App\Http\Controllers\Controller;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Systemconfig\Enum\PaymentStatus;
use Ekompaun\Systemconfig\Model\Lookup\Status;
use Ekompaun\Report\Repositories\PaymentReportRepository;

class PaymentHistoryController extends Controller
{
    public function index(PaymentReportRepository $report)
    {
        $paymentStatus = collect(PaymentStatus::dropdown())->prepend('Semua', '');
        $payments= $report->paymentStatus(request('status'), request('keyword'));

        return view('report::payment-history.index', compact('paymentStatus', 'payments'));
    }

    public function show($id)
    {
        $application = Application::findOrFail($id);
        return view('report::payment-history.show', compact('application'));
    }
}
