<?php

namespace Ekompaun\Report\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Ekompaun\Systemconfig\Enum\StatusKompaun;
use Ekompaun\Report\Repositories\SmsReportRepository;

class SmsController extends Controller
{
    public function index(SmsReportRepository $report)
    {
        $byStatus = request('status')?:StatusKompaun::MENUNGGU_KELULUSAN;
        $dateNow = Carbon::now();

        $dateTo = format_date(request('to', $dateNow->format('d-m-Y')), 'd-m-Y', 'Y-m-d');
        $dateFrom = format_date(request('from', $dateNow->subDays(30)->format('d-m-Y')), 'd-m-Y', 'Y-m-d');
        list($notifications, $status) = $report->reportSms($byStatus, $dateFrom, $dateTo);
        $notifications = $notifications->appends(
            [
            'status'=>request('status'),
            'from'=>request('from'),
            'to'=>request('to')
            ]
        );
        
        return view('report::sms.index', compact('notifications', 'status', 'dateNow'));
    }
}
