<?php

namespace Ekompaun\Report\Http\Controllers\Csv;

use Excel;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Enum\Year;
use Ekompaun\Systemconfig\Enum\Month;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Systemconfig\Model\Lookup\Offence;

class AppealCsvController extends Controller
{
    public function index(Request $request)
    {
        $applications = Application::with('transaction.user', 'personInCharge', 'payment')
                                   ->typeOfRayuanKedua()
                                   ->byTempohMaklumBalas($request->tempoh)
                                   ->assignedTo($request->pegawai)
                                   ->byYear($request->year)
                                   ->byMonth($request->month)
                                   ->byAkta($request->akta)
                                   ->byOffence($request->kesalahan)
                                   ->byProgress($request->status)
                                   ->get();

        Excel::create(
            'Kes Rayuan', function ($excel) use ($applications) {
                $excel->sheet(
                    'New sheet', function ($sheet) use ($applications) {
                        $sheet->loadView('report::csv.appeal', compact('applications'));
                    }
                );
            }
        )->export('csv');
    }
}
