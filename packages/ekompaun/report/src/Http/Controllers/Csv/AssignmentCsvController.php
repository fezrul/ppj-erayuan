<?php

namespace Ekompaun\Report\Http\Controllers\Csv;

use Excel;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Enum\Month;
use Ekompaun\Report\Repositories\AppealReportRepository;

class AssignmentCsvController extends Controller
{
    public function index(AppealReportRepository $report)
    {
        $selectedYear = request('year', Carbon::now()->year);
        $staffs = $report->totalAssignmentByStaff($selectedYear);
        $months = Month::dropdown('M');
        $filename = __("Kes Mengikut Pegawai Tahun :year", ['year' => $selectedYear]);

        Excel::create(
            $filename, function ($excel) use ($staffs, $months) {
                $excel->sheet(
                    'New sheet', function ($sheet) use ($staffs, $months) {
                        $sheet->loadView('report::csv.assignment', compact('staffs', 'months'));
                    }
                );
            }
        )->export('csv');
    }
}
