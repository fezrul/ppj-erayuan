<?php

namespace Ekompaun\Report\Http\Controllers\Csv;

use Excel;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Enum\Year;
use Ekompaun\Systemconfig\Enum\Month;
use Ekompaun\Report\Repositories\PaymentReportRepository;

class PaymentSummaryCsvController extends Controller
{
    public function index(PaymentReportRepository $report)
    {
        $months = Month::dropdown('M');
        $selectedYear = request('year', Carbon::now()->year);
        $data = $report->totalKutipanByPaymentMethod($selectedYear);
        
        Excel::create(
            'Kutipan Kewangan '. $selectedYear, function ($excel) use ($data, $months, $selectedYear) {
                $excel->sheet(
                    'New sheet', function ($sheet) use ($data, $months, $selectedYear) {
                        $sheet->loadView('report::csv.paymentSummary', compact('months', 'selectedYear', 'data'));
                    }
                );
            }
        )->export('csv');
    }
}
