<?php

namespace Ekompaun\Report\Http\Controllers\Csv;

use Excel;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Enum\StatusKompaun;
use Ekompaun\Report\Repositories\SmsReportRepository;

class SmsCsvController extends Controller
{
    public function index(SmsReportRepository $report)
    {
        $byStatus = request('status')?:StatusKompaun::MENUNGGU_KELULUSAN;
        $dateNow = Carbon::now();
        $dateTo = (request('to'))?format_date(request('to'), 'd-m-Y', 'Y-m-d'):$dateNow->format('Y-m-d');
        $dateFrom = (request('from'))?format_date(request('from'), 'd-m-Y', 'Y-m-d'):
        $dateNow->subDays(30)->format('Y-m-d');
        $notifications = $report->exportSms($byStatus, $dateFrom, $dateTo);

        Excel::create(
            'Notification SMS', function ($excel) use ($notifications) {
                $excel->sheet(
                    'New sheet', function ($sheet) use ($notifications) {
                        $sheet->loadView('report::csv.sms', compact('notifications'));
                    }
                );
            }
        )->export('csv');
    }
}
