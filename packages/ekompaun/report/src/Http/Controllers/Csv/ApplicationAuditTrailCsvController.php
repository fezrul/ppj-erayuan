<?php

namespace Ekompaun\Report\Http\Controllers\Csv;

use Excel;
use App\Http\Controllers\Controller;
use Ekompaun\Report\Repositories\AuditTrailReportRepository;

class ApplicationAuditTrailCsvController extends Controller
{
    public function index(AuditTrailReportRepository $report)
    {
        $applications = $report->senaraiAuditApplication(request('search'))->get();

        Excel::create(
            'Senarai Audit Trail', function ($excel) use ($applications) {
                $excel->sheet(
                    'New sheet', function ($sheet) use ($applications) {
                        $sheet->loadView('report::csv.auditTrailIndex', compact('applications'));
                    }
                );
            }
        )->export('csv');
    }

    public function show(AuditTrailReportRepository $report, $id)
    {
        list($application, $audits) = $report->detailAuditApplication($id);
        $audits = $audits->get();
        
        Excel::create(
            'Audit Trail', function ($excel) use ($application, $audits) {
                $excel->sheet(
                    'New sheet', function ($sheet) use ($application, $audits) {
                        $sheet->loadView('report::csv.auditTrailShow', compact('application', 'audits'));
                    }
                );
            }
        )->export('csv');
    }
}
