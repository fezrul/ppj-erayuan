<?php

namespace Ekompaun\Report\Http\Controllers\Csv;

use Excel;
use App\Http\Controllers\Controller;
use Ekompaun\Report\Repositories\PaymentReportRepository;

class PaymentHistoryCsvController extends Controller
{
    public function index(PaymentReportRepository $report)
    {
        $payments = $report->paymentStatusExport(request('status'), request('keyword'));

        Excel::create('Kutipan Kewangan', function ($excel) use ($payments) {
            $excel->sheet('New sheet', function ($sheet) use ($payments) {
                $sheet->loadView('report::csv.paymentHistory', compact('payments'));
            });
        })->export('csv');
    }
}
