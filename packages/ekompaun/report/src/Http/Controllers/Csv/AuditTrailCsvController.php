<?php

namespace Ekompaun\Report\Http\Controllers\Csv;

use Excel;
use App\Http\Controllers\Controller;
use Ekompaun\Report\Repositories\AuditTrailReportRepository;

class AuditTrailCsvController extends Controller
{
    public function index(AuditTrailReportRepository $report)
    {
        $audits = $report->senaraiAudit(request('search'))->get();

        Excel::create(
            'Audit Trail', function ($excel) use ($audits) {
                $excel->sheet(
                    'New sheet', function ($sheet) use ($audits) {
                        $sheet->loadView('report::csv.audit', compact('audits'));
                    }
                );
            }
        )->export('csv');
    }
}
