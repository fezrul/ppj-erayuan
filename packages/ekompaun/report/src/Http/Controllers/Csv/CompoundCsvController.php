<?php

namespace Ekompaun\Report\Http\Controllers\Csv;

use Excel;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Enum\Year;
use Ekompaun\Systemconfig\Enum\Month;
use Ekompaun\Report\Repositories\AppealReportRepository;

class CompoundCsvController extends Controller
{
    public function index(AppealReportRepository $report)
    {
        $months = Month::dropdown('M');
        $selectedYear = request('year', Carbon::now()->year);
        $report = $report->totalCompoundByOffence($selectedYear);

        Excel::create(
            'Tahunan Kompaun Bagi tahun '. $selectedYear, function ($excel) use ($report, $months) {
                $excel->sheet(
                    'New sheet', function ($sheet) use ($report, $months) {
                        $sheet->loadView('report::csv.compound', compact('report', 'months'));
                    }
                );
            }
        )->export('csv');
    }
}
