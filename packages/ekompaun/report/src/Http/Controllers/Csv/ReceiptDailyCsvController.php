<?php

namespace Ekompaun\Report\Http\Controllers\Csv;

use Excel;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Ekompaun\Report\Repositories\PaymentReportRepository;

class ReceiptDailyCsvController extends Controller
{
    public function index(PaymentReportRepository $report)
    {
        $dateFrom = $dateTo = Carbon::now()->format('Y-m-d');
        if (request('date_from')) {
            $dateFrom = format_date(request('date_from'), 'd-m-Y', 'Y-m-d');
        }
        if (request('date_to')) {
            $dateTo = format_date(request('date_to'), 'd-m-Y', 'Y-m-d');
        }

        $applications = $report->applicationPaymentDateExport($dateFrom, $dateTo);

        $filename = __("Laporan Terimaan Harian :from to :to.pdf", ['from' => $dateFrom, 'to' => $dateTo]);

        Excel::create(
            $filename, function ($excel) use ($applications) {
                $excel->sheet(
                    'New sheet', function ($sheet) use ($applications) {
                        $sheet->loadView('report::csv.receipt-daily', compact('applications'));
                    }
                );
            }
        )->export('csv');
    }
}
