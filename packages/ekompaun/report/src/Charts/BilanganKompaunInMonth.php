<?php

namespace Ekompaun\Report\Charts;

use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Systemconfig\Enum\PaymentStatus;
use Illuminate\Support\Collection;
use Jenssegers\Date\Date;

class BilanganKompaunInMonth extends Chart
{
    protected $date;

    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->date = Date::now();

        $colors = array_slice(config('colors'), 2);
        $statistic = collect($this->queryData($this->date));

        $this->labels($statistic->keys());
        $this->dataset('', 'doughnut', $statistic->values())->options(['backgroundColor' => $colors]);
        $this->height(300);
    }

    public function date()
    {
        return $this->date;
    }

    protected function queryData(Date $now)
    {

        /**
 * @var Collection $applications 
*/
        $applications = Application::byYear($now->year)->byMonth($now->month)->get();

        $byPaymentStatus = $applications->groupBy(
            function ($application) {
                return $application->present_payment_status;
            }
        );

        $paymentStatus = collect(array_fill_keys(PaymentStatus::dropdown(), []));
        $byPaymentStatus = $byPaymentStatus->union($paymentStatus)->mapToAssoc(
            function ($item, $key) {
                return [$key, count($item)];
            }
        );

        return $byPaymentStatus;
    }
}
