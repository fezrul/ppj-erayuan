<?php

namespace Ekompaun\Report\Charts;

use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Systemconfig\Enum\AppealType;
use Jenssegers\Date\Date;

class JumlahRayuanInMonth extends Chart
{
    protected $date;

    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->date = Date::now();

        $statistic = collect($this->queryData($this->date));

        $this->labels($statistic->keys());
        $this->dataset('jumlahRayuanInMonth', 'pie', $statistic->values())
            ->options(['backgroundColor' => config('colors')]);
        $this->height(300);

    }

    public function date()
    {
        return $this->date;
    }

    protected function queryData(Date $now)
    {
        $applications = Application::byYear($now->year)->byMonth($now->month)->get();

        $byType = $applications->groupBy('appl_type')->transform(
            function ($item) {
                return count($item);
            }
        );

        $types = collect(array_fill_keys(AppealType::values(), 0));
        $byType = $byType->union($types)->mapToAssoc(
            function ($item, $key) {
                $channel = AppealType::dropdown()[$key];
                return [$channel, $item];
            }
        );

        return $byType;
    }
}
