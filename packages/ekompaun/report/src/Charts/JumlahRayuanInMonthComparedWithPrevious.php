<?php

namespace Ekompaun\Report\Charts;

use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Systemconfig\Enum\AppealChannel;
use Ekompaun\Systemconfig\Enum\AppealType;
use Jenssegers\Date\Date;

class JumlahRayuanInMonthComparedWithPrevious extends Chart
{
    protected $months = [];

    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $statistic = $this->queryData(Date::now());
        $statisticByChannel = collect($statistic['byChannel']);
        $this->months = array_keys($statisticByChannel->first());

        $this->labels($this->months);

        $colors = config('colors');
        $i = 0;
        foreach ($statisticByChannel as $channel => $months) {
            $this->dataset($channel, 'bar', array_values($months))
                ->backgroundColor($colors[$i++])
                ->options(['borderWidth' => 0]);
        }

        $this->height(300);
        $this->options(
            [
                'scales' => [
                    'yAxes' => [
                        ['display' => true, 'ticks' => ['min' => 0]],
                    ],
                ],
            ]
        );
    }

    public function previousMonth()
    {
        return $this->months[0];
    }

    public function currentMonth()
    {
        return $this->months[1];
    }

    protected function queryData(Date $now)
    {
        $lastMonth = $now->copy()->startOfMonth()->subMonth();

        $applications = Application::byYear($now->year)->byMonth($now->month)->where('appl_channel', '<>', AppealChannel::SMS)->get();
        $lastMonthApps = Application::byYear($lastMonth->year)->byMonth($lastMonth->month)->where('appl_channel', '<>', AppealChannel::SMS)->get();
        $byChannelThisMonth = $applications->groupBy('appl_channel')->transform(
            function ($item) {
                return count($item);
            }
        );

        $byChannelPrevMonth = $lastMonthApps->groupBy('appl_channel')->transform(
            function ($item) {
                return count($item);
            }
        );

        $byType = $applications->groupBy('appl_type')->transform(
            function ($item) {
                return count($item);
            }
        );

        $channels = collect(array_fill_keys(AppealChannel::values(), 0));
        $byChannelThisMonth = $byChannelThisMonth->union($channels)->mapToAssoc(
            function ($item, $key) {
                $channel = AppealChannel::dropdown()[$key];
                return [$channel, $item];
            }
        );

        $byChannelPrevMonth = $byChannelPrevMonth->union($channels)->mapToAssoc(
            function ($item, $key) {
                $channel = AppealChannel::dropdown()[$key];
                return [$channel, $item];
            }
        );

        $byChannel = [];
        $channels = AppealChannel::dropdown();
        unset($channels[AppealChannel::SMS]);
        
        foreach ($channels as $channel) {
            $byChannel[$channel] = [
                $lastMonth->format('F') => $byChannelPrevMonth[$channel],
                $now->format('F') => $byChannelThisMonth[$channel]
            ];
        }

        $types = collect(array_fill_keys(AppealType::values(), 0));
        $byType = $byType->union($types)->mapToAssoc(
            function ($item, $key) {
                $channel = AppealType::dropdown()[$key];
                return [$channel, $item];
            }
        );

        return [
            'byChannel' => $byChannel,
            'byType' => $byType,
        ];
    }
}
