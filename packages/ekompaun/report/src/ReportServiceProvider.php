<?php

namespace Ekompaun\Report;

use Ekompaun\Systemconfig\Enum\Permission;
use Illuminate\Support\ServiceProvider;

class ReportServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'report');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'report');
        $this->bootMenu();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/../routes/web.php';
    }

    protected function bootMenu()
    {
        $menu = app('menu')->add(__("Laporan"))
                           ->data('icon', 'fa fa-bar-chart');

        $menu->add(__("Kes Rayuan"), route('report::appeal.index'))
            ->data('icon', 'fa fa-sticky-note-o')
            ->data('permission', Permission::VIEW_LAPORAN_RAYUAN);
         $menu->add(__("Laporan Rayuan SMS"), route('report::sms.index'))
             ->data('icon', 'fa fa-sticky-note-o')
             ->data('permission', Permission::VIEW_LAPORAN_RAYUAN_SMS);

        // $menu->add(__("Prestasi Pegawai (Detail)"), route('report::staff-detail.index'))
        //      ->data('icon', 'fa fa-signal')
        //      ->data('permission', Permission::VIEW_LAPORAN_PRESTASI_PEGAWAI_DETAIL);

        $menu->add(__("Jumlah Kes Mengikut Pegawai"), route('report::assignment.index'))
            ->data('icon', 'fa fa-signal')
            ->data('permission', Permission::VIEW_LAPORAN_PRESTASI_PEGAWAI_SUMMARY);

        $menu->add(__("Rayuan Mengikut Kesalahan"), route('report::compound.index'))
            ->data('icon', 'fa fa-sticky-note-o')
            ->data('permission', Permission::VIEW_LAPORAN_KOMPAUN);

        $menu->add(__("Terimaan Harian"), route('report::receipt-daily.index'))
            ->data('icon', 'fa fa-money')
            ->data('permission', Permission::VIEW_LAPORAN_TERIMAAN_HARIAN);

        $menu->add(__("Kutipan Kewangan Harian"), route('report::payment-history.index'))
            ->data('icon', 'fa fa-money')
            ->data('permission', Permission::VIEW_LAPORAN_PAYMENT_DETAIL);

        $menu->add(__("Kutipan Kewangan Tahunan"), route('report::payment-summary.index'))
            ->data('icon', 'fa fa-money')
            ->data('permission', Permission::VIEW_LAPORAN_PAYMENT_SUMMARY);

        $menu->add(__("Jejak Audit Rayuan"), route('report::audit-application.index'))
            ->data('icon', 'fa fa-code-fork')
            ->data('permission', Permission::VIEW_LAPORAN_AUDIT_TRAIL_RAYUAN);

        $menu->add(__("Jejak Audit"), route('report::audit-trail.index'))
            ->data('icon', 'fa fa-code-fork')
            ->data('permission', Permission::VIEW_LAPORAN_AUDIT_TRAIL);
    }
}
