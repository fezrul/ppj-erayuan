<?php

namespace Ekompaun\Report\Repositories;

use Ekompaun\Appeal\Model\Application;
use Ekompaun\Systemconfig\Enum\AppealChannel;
use Ekompaun\Systemconfig\Model\Lookup\Status;
use Ekompaun\Systemconfig\Enum\StatusType;

class SmsReportRepository
{
    public function reportSms($byStatus, $dateFrom, $dateTo)
    {
        $status = $this->getStatus();
        $notification = $this->smsByDate($byStatus, $dateFrom, $dateTo)->paginate();

        return [$notification, $status];
    }

    public function exportSms($byStatus, $dateFrom, $dateTo)
    {
        return $this->smsByDate($byStatus, $dateFrom, $dateTo)->get();
    }

    private function smsByDate($byStatus, $dateFrom, $dateTo)
    {
        return Application::with('personInCharge', 'approver')
            ->byChannel(AppealChannel::SMS)
            ->byStatus($byStatus)
            ->ByBetweenDate($dateFrom, $dateTo);
    }

    private function getStatus()
    {
        return Status::byType(StatusType::DECISION)
            ->orWhere('status_type', StatusType::SUBMIT)
            ->get()
            ->pluck("status_name", "status_id");
    }
}
