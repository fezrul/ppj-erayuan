<?php

namespace Ekompaun\Report\Repositories;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Systemconfig\Model\Lookup\PaymentMethod;
use Ekompaun\Payment\Data\Model\Payment;

class PaymentReportRepository
{
    protected $application;

    /**
     * PaymentReportRepository constructor.
     *
     * @param $application
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    public function paymentStatus($status = null, $keyword = null)
    {
        // return $this->application->whereHas('payment')
        //     ->byPaymentStatus($status)
        //     ->byKeyword($keyword)
        //     ->paginate();
        if($status==null){

        return Payment::where('pymt_status','<>',0)
                       ->byKeyword($keyword)
                       ->with('transaction','transaction.user','transaction.offence','paymod','users')
                       ->paginate();

        }else{
        return Payment::where('pymt_status',$status)
                       ->where('pymt_status','<>',0)
                       ->byKeyword($keyword)
                       ->with('transaction','transaction.user','transaction.offence','paymod','users')
                       ->paginate();

        }


    }

    public function paymentStatusExport($status = null, $keyword = null)
    {
        if($status==null){

        return Payment::where('pymt_status','<>',0)
                       ->byKeyword($keyword)
                       ->with('transaction','transaction.user','transaction.offence','paymod','users')
                       ->get();

        }else{
        return Payment::where('pymt_status',$status)
                       ->where('pymt_status','<>',0)
                       ->byKeyword($keyword)
                       ->with('transaction','transaction.user','transaction.offence','paymod','users')
                       ->get();

        }
    }

    public function applicationPaymentDateExport($dateFrom, $dateTo)
    {
        return $this->application->whereHas('payment', function($payment) use ($dateFrom, $dateTo) {
            $payment->whereBetween(
                'pymt_date',
                [
                    format_date($dateFrom, 'Y-m-d', 'Y-m-d 00:00:00'),
                    format_date($dateTo, 'Y-m-d', 'Y-m-d 23:59:59'),
                ]
            );
        })->get();
    }

    public function totalKutipanByPaymentMethod($year)
    {
        $paymentGroupByMethod = DB::select(
            "
            SELECT sum(pymt_totalamount) AS amount, MONTH(pymt_date) AS month, fk_paymentmethod
            FROM ek_payment
            WHERE YEAR(pymt_date) = :year
            AND pymt_status = 1
            GROUP BY month, fk_paymentmethod
        ", ['year' => $year]
        );

        $paymentGroupByMethod = collect($paymentGroupByMethod)->groupBy('fk_paymentmethod');

        $paymentMethods = PaymentMethod::all()
            ->sortBy('paymethod_name')
            ->transform(
                function ($method) use ($paymentGroupByMethod) {
                    $monthlyPayment = $this->fillMonths();

                    foreach ($paymentGroupByMethod->get($method->getKey(), []) as $payment) {
                        $monthlyPayment[$payment->month] = $payment->amount;
                    }

                    $method->setAttribute('monthlyPayments', collect($monthlyPayment));

                    return $method;
                }
            );

        return $paymentMethods;
    }

    protected function fillMonths($fill = 0)
    {
        return array_combine(
            range(1, 12),
            array_fill(1, 12, 0)
        );
    }
}
