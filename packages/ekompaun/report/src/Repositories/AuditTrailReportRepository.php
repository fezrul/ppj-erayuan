<?php

namespace Ekompaun\Report\Repositories;

use Ekompaun\Appeal\Model\Application;
use Ekompaun\Systemconfig\Model\Audit;

class AuditTrailReportRepository
{
    public function senaraiAuditApplication($search)
    {
        return Application::with('transaction', 'status', 'lastAuditrail')
            ->byKeyword($search);
    }

    public function detailAuditApplication($id)
    {
        $application = Application::with('transaction', 'status')->findOrFail($id);
        $audits      = $application->auditrail()->with('user');

        return [$application, $audits];
    }

    public function senaraiAudit($search)
    {
        // return Audit::with('user')->byKeyword($search);
        return Audit::with('user')->byKeyword($search)->where('old_values','!=','[]')->where('new_values','!=','[]');
    }
}
