<?php

namespace Ekompaun\Report\Repositories;

use App\User;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Appeal\Model\Transaction;
use Ekompaun\Systemconfig\Model\Lookup\Act;
use Ekompaun\Systemconfig\Model\Lookup\Section;
use Illuminate\Support\Facades\DB;

class AppealReportRepository
{
    protected $transaction;

    protected $application;

    protected $user;

    protected $lookupSection;

    protected $lookupAct;

    /**
     * AppealReportRepository constructor.
     */
    public function __construct(
        Application $application,
        Transaction $transaction,
        User $user,
        Section $lookupSection,
        Act $lookupAct
    ) {
        $this->transaction = $transaction;
        $this->application = $application;
        $this->user = $user;
        $this->lookupSection = $lookupSection;
        $this->lookupAct = $lookupAct;
    }

    public function totalAssignmentByStaff($year)
    {
        // Ambil data assignment dari database
        $assignments = $this->application
            ->selectRaw('count(*) as count, MONTH(appl_date) as month, appl_personincharge')
            // ->completed()
            ->byYear($year)
            ->groupBy(DB::raw('appl_personincharge, MONTH(appl_date)'))
            ->get()
            ->groupBy('appl_personincharge');

        // Ambil data semua pegawai
        $pegawai = $this->user
            ->pegawaiJu()
            ->orderBy('name')
            ->get()
            ->transform(
                function ($user) use ($assignments) {
                    // inisiasi assignment selama 12 bulan
                    $monthlyAssigment = $this->fillMonths();

                    // isi bulan-bulan dimana user memiliki assigment
                    foreach ($assignments->get($user->getKey(), []) as $assignment) {
                        $monthlyAssigment[$assignment['month']] = $assignment['count'];
                    }

                    $user->setAttribute('assignments', $monthlyAssigment);

                    return $user;
                }
            );

        return $pegawai;
    }

    public function totalCompoundByOffence($year)
    {
        // Ambil data transaction (compound) dari database group by offencecode and month
        $transGroupBySection = DB::select(
            "
            SELECT count(*) AS count, MONTH(appl_date) AS month, fk_lkp_sectionid
            FROM ek_transaction
            JOIN ek_application ON (appl_transid = ek_transaction.tran_id)
            JOIN lkp_offence ON (ek_transaction.fk_lkp_offencecode = lkp_offence.offence_code)
            WHERE YEAR(appl_date) = :year
            AND fk_lkp_offencecode <> ''
            GROUP BY MONTH(appl_date), fk_lkp_sectionid
        ", ['year' => $year]
        );

        $transGroupBySection = collect($transGroupBySection)->groupBy('fk_lkp_sectionid');

        $acts = $this->lookupAct->all()->pluck('act_name', 'act_id');

        $sections = $this->lookupSection
            ->with('act')
            ->get()
            ->transform(
                function ($section) use ($transGroupBySection) {

                    // inisiasi assignment selama 12 bulan
                    $monthlyCompound = $this->fillMonths();

                    // isi bulan-bulan dimana ada pelanggaran terhadap section tertentu
                    foreach ($transGroupBySection->get($section->getKey(), []) as $transactions) {
                        $monthlyCompound[$transactions->month] = $transactions->count;
                    }

                    $section->setAttribute('compounds', collect($monthlyCompound));

                    return $section;
                }
            )
            ->groupBy(
                function ($item) use ($acts) {
                    return array_get($acts, $item->fk_lkp_actid, '');
                }
            );

        return $sections;
    }

    protected function fillMonths($fill = 0)
    {
        return array_combine(
            range(1, 12),
            array_fill(1, 12, 0)
        );
    }
}
