<?php

return [
    'appeal'        => 'Appeal',
    'action'        => 'Action',
    'normal_appeal' => 'Appeal',
    'second_appeal' => 'Second Appeal',
    'payment'       => 'Payment',
    'decision'      => 'Decision',
];
