@extends('layouts.pdf.master')

@section('title', 'Senarai Rayuan')
@section('content')

    <div style="font-family:Arial; font-size:12px;">
       <div class="col-md-3">
           <img src="img/logo-ppj.png" height="100px">
      </div>
      <div class="col-md-9">
            <h2 style="text-align: center">{{ __("Senarai Rayuan Kompaun") }}</h2>
      </div>
  </div>
  <br>
  <table class="tg">
    <tr>
	    <th class="tg-3wr7" width="1">{{ __("Bil") }}</th>
	    <th class="tg-3wr7">{{ __("No Kompaun") }}</th>
	    <th class="tg-3wr7">{{ __("Pelanggan") }}</th>
	    <th class="tg-3wr7">{{ __("Kod Kesalahan") }}</th>
	    <th class="tg-3wr7">{{ __("Tarikh Kompaun") }}</th>
	    <th class="tg-3wr7">{{ __("Tarikh Rayuan") }}</th>
	    <th class="tg-3wr7">{{ __("Status Kompaun") }}</th>
	</tr>
    @foreach($applications as $application)
    <tr>
        <td class="tg-rv4w" width="1">{{ $loop->iteration }}</td>
        <td class="tg-rv4w">{{ $application->transaction->tran_compoundno }}</td>
        <td class="tg-ti5e">{{ $application->user->name }}</td>
        <td class="tg-ti5e">{{ $application->transaction->offence->offence_code }}</td>
        <td class="tg-ti5e">{{ format_date($application->transaction->tran_compounddate) }}</td>
        <td class="tg-ti5e">{{ format_date($application->appl_date) }}</td>
        <td class="tg-rv4w">{{ $application->status->status_name }}</td>
    </tr>
    @endforeach
  </table>
@endsection
