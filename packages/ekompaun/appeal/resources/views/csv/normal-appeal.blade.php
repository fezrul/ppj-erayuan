<!DOCTYPE html>
<html>
<body>
	<table class="table table-striped mb-0">
        <thead class="thead-light">
            <tr>
                <th>{{ __("Bil") }}</th>
                <th>{{ __("No Kompaun") }}</th>
                <th>{{ __("Pelanggan") }}</th>
                <th>{{ __("Kod Kesalahan") }}</th>
                <th>{{ __("Tarikh Kompaun") }}</th>
                <th>{{ __("Tarikh Rayuan") }}</th>
                <th>{{ __("Status Kompaun") }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($applications as $application)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $application->transaction->tran_compoundno }}</td>
                <td>{{ $application->user->name }}</td>
                <td>{{ $application->transaction->offence->offence_code }}</td>
                <td>{{ format_date($application->transaction->tran_compounddate) }}</td>
                <td>{{ format_date($application->appl_date) }}</td>
                <td>{{ $application->status->status_name }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>