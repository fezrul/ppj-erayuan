@extends('layouts.app')
@push('head')
<style>
    div {
        padding: 3px 3px 3px;
    }
</style>
@endpush
@section('content')
<div class="container-fluid mt-5">
    <div class="card">
        <div class="card-header">
            <h5 class="mb-0">{{ __("Maklumat Kompaun") }}</h5>
            <div class="card-actions">

            </div>
        </div>
        {{ html()->form('PUT', route('appeal::action.update', $application))->open() }}
        <div class="card-body">
            <div class="row">
                <div class="col-sm-2">{{ __("Nama Pelanggan") }} </div>
                <div class="col-sm">: {{$application->transaction->tran_offendername}}</div>
                <div class="col-sm-2">{{ __("Tarikh Kompaun") }} </div>
                <div class="col-sm">: {{ format_date($application->transaction->tran_compounddate) }}</div>
                <div class="w-100"></div>
                <div class="col-sm-2">{{ __("No. Kad Pengenalan") }} </div>
                <div class="col-sm">: {{$application->transaction->tran_offendericno}}</div>
                <div class="col-sm-2">{{ __("No Kompaun") }} </div>
                <div class="col-sm">: {{$application->transaction->tran_compoundno}}</div>
                <div class="w-100"></div>
               <!--  <div class="col-sm-2">{{ __("Alamat") }} </div>
                <div class="col-sm">: {{$application->transaction->user->profile->user_address1 ?? '-'}}</div> -->
                
                <div class="col-sm-2">{{ __("No Rujukan") }} </div>
                <div class="col-sm">: {{$application->transaction->tran_refno}}</div>
                <div class="w-100"></div>
				<div class="col-sm-2">{{ __("Tarikh Memohon") }} </div>
                <div class="col-sm">:
                	{{format_date($application->appl_date)}} {{format_time($application->appl_date)}}
                </div>
                <div class="col-sm-2">{{ __("Kod Kesalahan") }} </div>
                <div class="col-sm">: {{$application->transaction->offence->offence_code}}</div>
                <div class="w-100"></div>
                <div class="col-sm-2">{{ __("Tempat Kesalahan") }} </div>
                <div class="col-sm-4">: {{$application->transaction->tran_place}}</div>
                <div class="col-sm-2">{{ __("Jenis Kesalahan") }} </div>
                <div class="col-sm-4">: {{$application->transaction->offence->offence_name}}</div>
                <div class="w-100"></div>
                <div class="col-sm-2">{{ __("Amaun Asal (RM)") }} </div>
                <div class="col-sm">: {{$application->amaun_asal}}</div>
                <div class="col-sm-2">{{ __("No. Pendaftaran Kenderaan") }} </div>
                <div class="col-sm">: {{$application->transaction->tran_vehicleno}}</div>
                <div class="w-100"></div>
                <div class="col-sm-2">{{ __("Alasan") }} </div>
                <div class="col-sm">: {{ $application->appl_reason }}</div>
                <div class="col-sm-2">{{ __("Jenis") }} </div>
                <div class="col-sm">: {{$application->transaction->vehicle->vehicle_name}}</div>
                <div class="w-100"></div>
                <div class="col-sm-2">{{ __("Dokumen Sokongan") }} </div>
                <div class="col-sm">:
                	@if($application->attachments->count() > 0)
                		@foreach($application->attachments as $attachment)
                			<a href="{{asset('storage/attachments/'.$attachment->attc_filename)}}" target="_blank">{{ __("Dokumen ".$loop->iteration) }}&nbsp;&nbsp;</a>
                		@endforeach
            		@else
            			{{ __("Tiada") }}
	            	@endif
        		</div>
                <div class="col-sm-2">{{ __("Warna") }} </div>
                <div class="col-sm">: {{$application->transaction->tran_vehiclecolor}}</div>
                <div class="w-100"></div>
                <div class="col-sm-2">{{ __("Keputusan") }} </div>
                <div class="col-sm">
                	{{ html()->select('appl_status', $status, $application->appl_status)->addClass('form-control') }}
                </div>
                <div class="col-sm-2">{{ __("Ulasan") }} </div>
                <div class="col-sm">
                	{{ html()->textarea('appl_reason')->addClass('form-control') }}
                </div>
                <div class="w-100"></div>
                <br>
            	<div class="col-sm-2 bayaran"
            	@if($application->appl_status != '2' && old('appl_status') != '2')
            	style="display:none"
            	@endif
            	>{{ __("Bayaran (RM)") }} </div>
                <div class="col-sm bayaran"
                @if($application->appl_status != '2' && old('appl_status') != '2')
            	style="display:none"
            	@endif
                >
                	{{ html()->input('number', 'appl_approveamount')->addClass('form-control') }}
                </div>
                <div class="w-100"></div>

            </div>
        </div>
        <div class="card-footer">

            @can('keputusan', $application)
        	{{ html()->submit( __("Hantar") )->class(['btn btn-primary']) }}
            @endcan

            {{ html()->a(route('appeal::action.index'),__("Kembali"))->class(['btn btn-link']) }}
        </div>
        {{ html()->form()->close() }}
    </div>
</div>

@endsection

@push('end')
    <script type="text/javascript">
        $(function() {
            $('select[name="appl_status"]').change(function() {
            	$('input[name="appl_approveamount"]').val('');
            	if ($(this).val() == '2') {
            		$(".bayaran").show();
            	}
            	else {
            		$(".bayaran").hide();
            	}
            });
        });
    </script>
@endpush
