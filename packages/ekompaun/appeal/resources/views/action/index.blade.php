@extends('layouts.app')
@section('content')
<div class="container-fluid mt-5">
    <div class="card" style="overflow-x: auto;">
        <div class="card-header">
            <h5 class="mb-0">{{ __("Senarai Tugasan") }}</h5>
            <div class="card-actions">

            </div>
        </div>
        <div class="tab-content">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                   <a class="nav-link active" data-toggle="tab" href="#notyet"> Belum diambil Tindakan </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#already"> Telah diambil Tindakan </a>
                </li>
            </ul>


              <div id="notyet" class="tab-pane active">
                   <table class="table table-striped mb-0">
                        <thead class="thead-light">
                            <tr>
                                <th>{{ __("Bil") }}</th>
                                <th>{{ __("No Kompaun") }}</th>
                                <th>{{ __("Kod Kesalahan") }}</th>
                                <th>{{ __("Tarikh Kompaun") }}</th>
                                <th>{{ __("Tarikh Mohon") }}</th>
                                <th>{{ __("Status Kompaun") }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($applications->isNotEmpty())
                            @foreach($applications as $application)
                            @if(($application->status->status_id == '1') OR ($application->status->status_id =='10'))
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td><a href="{{route('appeal::action.edit', $application)}}">
                                        {{ $application->transaction->tran_compoundno }}
                                        @if($application->transaction->rayuanCount()->count() > 1) ({{ $application->transaction->rayuanCount()->count()-1 }}) @endif
                                        @if($application->latestdetail->detl_reassign == 1) (R) @endif
                                </a></td>
                                <td>{{ $application->transaction->offence->offence_code }}</td>
                                <td>{{ format_date($application->transaction->tran_compounddate) }}</td>
                                <td>{{ format_date($application->appl_date) }}</td>
                                <td>{{ $application->status->status_name }}</td>
                            </tr>

                            @endif
                            @endforeach
                            @else
                            <tr>
                                <td colspan="6">
                                    @include('components.empty')
                                </td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
              </div>
              <div id="already" class="tab-pane fade">
                     <table class="table table-striped mb-0">
                        <thead class="thead-light">
                            <tr>
                                <th>{{ __("Bil") }}</th>
                                <th>{{ __("No Kompaun") }}</th>
                                <th>{{ __("Kod Kesalahan") }}</th>
                                <th>{{ __("Tarikh Kompaun") }}</th>
                                <th>{{ __("Tarikh Mohon") }}</th>
                                <th>{{ __("Status Kompaun") }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($applications->isNotEmpty())
                            @foreach($applications as $application)
                            @if(($application->status->status_id != '1') AND ($application->status->status_id != '10'))
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td><a href="{{route('appeal::action.edit', $application)}}">
                                    {{ $application->transaction->tran_compoundno }} @if($application->latestdetail->detl_reassign == 1) (R) @endif
                                </a></td>
                                <td>{{ $application->transaction->offence->offence_code }}</td>
                                <td>{{ format_date($application->transaction->tran_compounddate) }}</td>
                                <td>{{ format_date($application->appl_date) }}</td>
                                <td>{{ $application->status->status_name }}</td>
                            </tr>

                            @endif
                            @endforeach
                            @else
                            <tr>
                                <td colspan="6">
                                    @include('components.empty')
                                </td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
              </div>


      </div>
        <div class="card-footer">
            {{ $applications->links() }}
        </div>
    </div>
</div>

@endsection
