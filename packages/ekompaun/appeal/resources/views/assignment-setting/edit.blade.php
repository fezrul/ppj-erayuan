@extends('layouts.app')
@section('content')
    <div class="container-fluid mt-5">
        <div class="card">
            <div class="card-header mb-4">
                <h5 class="mb-0">{{ __("Tetapan Pegawai Auto Assign") }}</h5>
                <div class="card-actions">

                </div>
            </div>
            <div class="card-body">
                {{ html()->form('PUT', route('appeal::assignment-setting.update'))->open() }}
                @for($i=1;$i<=$autoAssignCount;$i++)
                {{ html()->formGroup(__("Pegawai :priority", ['priority' => $i]), html()->select("assignee[$i]", $pegawai, $assignees[$i - 1])->class(['form-control'])) }}
                @endfor
                <div {{ html()->class(['form-group row mb-0']) }}>
                    <div class="col-sm-4"></div>
                    <div {{ html()->class(['col-sm-8']) }}>
                        {{ html()->submit(__("Hantar"))->class(['btn btn-primary']) }}
                        {{ html()->a("#",__("Batal"))->class(['btn btn-link']) }}
                    </div>
                </div>
                {{ html()->form()->close() }}
            </div>
        </div>
    </div>
@endsection
