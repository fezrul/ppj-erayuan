@extends('layouts.app')
@section('content')
<div class="my-5">
    <div class="card" style="overflow-x: auto;">
        <div class="card-header mb-4">
            <h5 class="mb-0">{{ __("Senarai Agihan") }}</h5>
            <div class="card-actions">

            </div>
        </div>
        <ul class="nav nav-tabs">
            <li class="nav-item">
                {{ html()->a(route('appeal::assignment.index'), __("Menunggu Agihan"))
                ->class(['nav-link active']) }}
            </li>
            <li class="nav-item">
                {{ html()->a(route('appeal::assignee.index'), __("Agihan Selesai"))
                ->class(['nav-link']) }}
            </li>
        </ul>
        @if ($transactions->isNotEmpty() )
        <table class="table table-striped mb-0 table-responsive">
            @include('appeal::assignment._table-head')
            <tbody>
                @foreach($transactions as $transaction)
                <tr>
                    <td>{{ numbering($transaction, $transactions) }}</td>
                    <td>{{ $transaction->tran_compoundno }}@if($transaction->rayuanCount()->count() > 1) ({{ $transaction->rayuanCount()->count()-1 }}) @endif</td>
                    <td>{{ $transaction->offence->offence_code }}</td>
                    <td>{{ format_date($transaction->tran_compounddate) }}</td>
                    <td>{{ format_date($transaction->lastApplication->appl_date) }}</td>
                    <td>{!! $transaction->lastApplication->present_status !!}</td>
                    {{ html()->form('PUT', route('appeal::assignment.update', $transaction->lastApplication))->open() }}
                    <td>
                        {{ html()->select('appl_personincharge['.$loop->index.']', $user, $transaction->lastApplication->appl_personincharge)
                        ->addClass('form-control')->style('width:220px !important') }}
                    </td>
                    <td>
                        {{ html()->submit( __("Assign") )->class(['btn btn-primary']) }}
                    </td>
                    {{ html()->form()->close() }}
                </tr>
                @endforeach
            </tbody>
        </table>
        @else
            @include('components.empty')
        @endif
        <div class="card-footer">
            {{ $transactions->links() }}
        </div>
    </div>
</div>

@endsection
