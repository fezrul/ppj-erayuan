@extends('layouts.app')

@push('head')
<link rel="stylesheet" type="text/css" href="{{ asset('lib/bootstrap-daterangepicker/daterangepicker.css') }}" />
@endpush

@section('content')
<div class="my-5">
    <div class="card" style="overflow-x: auto;">
        <div class="card-header">
           
            <div class="card-actions">
                <a href="{{ route('appeal::pdf.normal-appeal', 
                ['status'=>request('status'), 'from'=>request('from'), 'to'=>request('to')]) }}" target="blank"><i class="fa fa-file-pdf-o"></i> {{ __("Print PDF") }}</a>
                <a href="{{ route('appeal::csv.normal-appeal', 
                ['status'=>request('status'), 'from'=>request('from'), 'to'=>request('to')]) }}" target="blank"><i class="fa fa-file-excel-o"></i> {{ __("Eksport CSV") }}</a>
            </div>
            <p></p><br>
             <h5 class="mb-0">{{ __("Senarai Rayuan") }}</h5>
        </div>
        <div class="card-body">
            {{ html()->form('GET', route('appeal::normal-appeal.index'))->open() }}
                <div class="container-fluid mb-3">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row form-group">
                                <div class="col-sm-5">
                                    <label class="col-sm-label">{{ __("Status Kelulusan")}}</label>
                                </div>
                                <div class="col-sm-7">
                                    {{ html()->select('status', $status, request('status'))->addClass('form-control') }}
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-5">
                                    <label class="col-sm-label">{{ __("Tarikh Rayuan Dari")}}</label>
                                </div>
                                <div class="col-sm-7">
                                    {{ html()->text('from', request('from'))->addClass('form-control') }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row form-group">
                                <div class="col-sm-5">
                                    <label class="col-sm-label">&nbsp;</label>
                                </div>
                                <div class="col-sm-7"></div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-5">
                                    <label class="col-sm-label">{{ __("Tarikh Rayuan Hingga")}}</label>
                                </div>
                                <div class="col-sm-7">
                                    {{ html()->text('to', request('to'))->addClass('form-control') }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid mb-3">
                    <div class="row">
                        <div class="col-6">
                            {{ html()->submit(__('Hantar'))->addClass('btn btn-primary') }}
                        </div>
                    </div>
                </div>
            {{ html()->form()->close() }}
        </div>
        <table class="table table-striped mb-0 table-responsive">
            <thead class="thead-light">
                <tr>
                    <th>{{ __("Bil") }}</th>
                    <th>{{ __("No Kompaun") }}</th>
                    <th>{{ __("Pelanggan") }}</th>
                    <th>{{ __("Kod Kompaun") }}</th>
                    <th>{{ __("Tarikh Kompaun") }}</th>
                    <th>{{ __("Tarikh Rayuan") }}</th>
                    <th>{{ __("Status Kompaun") }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($applications as $application)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $application->transaction->tran_compoundno }}</td>
                    <td>{{ $application->user->name }}</td>
                    <td>{{ $application->transaction->offence->offence_code }}</td>
                    <td>{{ format_date($application->transaction->tran_compounddate) }}</td>
                    <td>{{ format_date($application->appl_date) }}</td>
                    <td>{{ $application->status->status_name }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="card-footer">
            {{ $applications->links() }}
        </div>
    </div>
</div>

@endsection

@push('end')
<script type="text/javascript" src="{{ asset('lib/bootstrap-daterangepicker/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('lib/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

<script type="text/javascript">
    $(function() {
        $('input[name="from"]').daterangepicker({
            locale: {
              format: 'DD-MM-YYYY'
          },
          singleDatePicker: true,
          showDropdowns: true
      },
      function(start, end, label) {
      });
    });

    $(function() {
        $('input[name="to"]').daterangepicker({
            locale: {
              format: 'DD-MM-YYYY'
          },
          singleDatePicker: true,
          showDropdowns: true
      },
      function(start, end, label) {
      });
    });
</script>


@endpush
