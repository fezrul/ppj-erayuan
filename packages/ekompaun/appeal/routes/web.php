<?php
Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['web', 'auth', 'localeSessionRedirect', 'localizationRedirect'],
        'namespace' => 'Ekompaun\Appeal\Http\Controllers',
        'as' => 'appeal::'
    ],
    function () {
        Route::resource('action', 'ActionController')
        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::PROCESS_RAYUAN);

        Route::resource('assignment', 'AssignmentController', ['only'=>['index', 'update']])
        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::ASSIGN_RAYUAN);

        Route::resource('assignee', 'AssigneeController', ['only'=>['index']])
        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::ASSIGN_RAYUAN);

        Route::resource('normal-appeal', 'NormalAppealController')
        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_RAYUAN_ALL);

        Route::group(
            ['prefix'=>'csv', 'namespace' => 'Csv'], function () {
                Route::get('normal-appeal', 'CsvNormalAppealController@index')->name('csv.normal-appeal')
                ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_RAYUAN_ALL);
            }
        );

        Route::group(
            ['prefix'=>'pdf', 'namespace' => 'Pdf'], function () {
                Route::get('normal-appeal', 'PdfNormalAppealController@index')->name('pdf.normal-appeal')
                ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::VIEW_RAYUAN_ALL);
            }
        );

        Route::group(['prefix' => 'assignment-setting', 'middleware' => 'can:'.\Ekompaun\Systemconfig\Enum\Permission::ASSIGN_RAYUAN], function() {
            Route::get('', ['uses' => 'AssignmentSettingController@edit', 'as' => 'assignment-setting.edit']);
            Route::put('', ['uses' => 'AssignmentSettingController@update', 'as' => 'assignment-setting.update']);
        });
    }
);
