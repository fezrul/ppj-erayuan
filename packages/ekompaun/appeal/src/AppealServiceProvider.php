<?php

namespace Ekompaun\Appeal;

use Ekompaun\Systemconfig\Enum\Permission;
use Illuminate\Support\ServiceProvider;

class AppealServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'appeal');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'appeal');
        $this->bootMenu();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/../routes/web.php';
    }

    protected function bootMenu()
    {
        $menu = app('menu')->add(__("Rayuan"))->data('icon', 'fa fa-file-o');

        $menu->add(__("Tindakan"), route('appeal::action.index'))
            ->data('icon', 'fa fa-table')
            ->data('permission', Permission::PROCESS_RAYUAN);

        $menu->add(__("Tugasan"), route('appeal::assignment.index'))
            ->data('icon', 'fa fa-table')
            ->data('permission', Permission::ASSIGN_RAYUAN);

        $menu->add(__("Rayuan"), route('appeal::normal-appeal.index'))
            ->data('icon', 'fa fa-file-o')
            ->data('permission', Permission::VIEW_RAYUAN_ALL);

        $menu->add(__("Tetapan Pegawai Auto Assign"), route('appeal::assignment-setting.edit'))
            ->data('icon', 'fa fa-id-badge')
            ->data('permission', Permission::ASSIGN_RAYUAN);
    }
}
