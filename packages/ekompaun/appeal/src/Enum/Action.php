<?php

namespace Ekompaun\Appeal\Enum;

use MyCLabs\Enum\Enum;

class Action extends Enum
{
    const SUBMIT_RAYUAN_IKUT_JADWAL = 'action.ikut-jadwal';
    const SUBMIT_RAYUAN_KEDUA = 'action.kedua';
    const RUNDINGCARA = 'action.rundingcara';
    const BATAL_RAYUAN_IKUT_JADWAL = 'action.batal-rayuan-ikut-jadwal';
    const BATAL_RAYUAN_KEDUA = 'action.batal-rayuan-kedua';
    const BAYAR_FPX = 'action.bayar';
}
