<?php

namespace Ekompaun\Appeal\Model;

use Illuminate\Http\UploadedFile;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Platform\Model\BaseModel;

class Attachment extends BaseModel
{
    protected $table = 'ek_attachment';

    protected $primaryKey = 'attc_id';

    protected $guarded = [];

    public function application()
    {
        return $this->belongsTo(Application::class);
    }
}
