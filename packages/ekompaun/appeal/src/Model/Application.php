<?php

namespace Ekompaun\Appeal\Model;

use Ekompaun\Platform\Model\BaseModel;
use Ekompaun\Appeal\Model\Traits\Application\ApplicationAttribute;
use Ekompaun\Appeal\Model\Traits\Application\ApplicationRelation;
use Ekompaun\Appeal\Model\Traits\Application\ApplicationScope;
use Ekompaun\Systemconfig\Enum\StatusKompaun;
use Illuminate\Database\Eloquent\SoftDeletes;

class Application extends BaseModel
{
    use ApplicationAttribute,
        ApplicationRelation,
        ApplicationScope,
        SoftDeletes;

    protected $table = 'ek_application';

    protected $primaryKey = 'appl_id';

    protected $guarded = [];

    public function batalCampaign()
    {
        $this->appl_approveamount = $this->transaction->tran_compoundamount;
        $this->appl_campamount = 0;
        $this->appl_status = StatusKompaun::BATAL_AUTOMATIK;

        return $this->save();
    }
}
