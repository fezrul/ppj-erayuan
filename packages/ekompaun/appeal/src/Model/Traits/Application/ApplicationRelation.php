<?php

namespace Ekompaun\Appeal\Model\Traits\Application;

use App\User;
use Ekompaun\Appeal\Model\ApplicationDetail;
use Ekompaun\Appeal\Model\Payment;
use Ekompaun\Appeal\Model\Attachment;
use Ekompaun\Appeal\Model\PaymentFpx;
use Ekompaun\Appeal\Model\Transaction;
use Ekompaun\Systemconfig\Enum\AssignmentIndicator;
use Ekompaun\Systemconfig\Enum\StatusKompaun;
use Ekompaun\Systemconfig\Model\Lookup\Status;
use Ekompaun\Systemconfig\Model\CampaignMaster;
use Ekompaun\Systemconfig\Model\Lookup\StatusSap;

trait ApplicationRelation
{
    public function user()
    {
        return $this->belongsTo(User::class, 'created_by')->withDefault();
    }

    public function personInCharge()
    {
        return $this->belongsTo(User::class, 'appl_personincharge')->withDefault();
    }

    public function approver()
    {
        return $this->belongsTo(User::class, 'appl_approveby')->withDefault();
    }

    public function attachments()
    {
        return $this->hasMany(Attachment::class, 'fk_applid');
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'appl_transid')->with('offence');
    }

    public function statusSap()
    {
        return $this->belongsTo(StatusSap::class, 'appl_status_sap')->withDefault();
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'appl_status')->withDefault(
            [
                'status_id'   => StatusKompaun::BELUM_BAYAR,
                'status_name' => 'Belum Bayar',
            ]
        );
    }

    public function campaign()
    {
        return $this->belongsTo(CampaignMaster::class, 'fk_campid');
    }

    public function payment()
    {
        return $this->hasOne(Payment::class, 'fk_applid')->where('pymt_status','=',1)->withDefault(
            [
            'pymt_date' => '',
            'pymt_status' => '',
            ]
        );
    }

    public function paymentstatus()
    {
        return $this->hasOne(Payment::class, 'fk_applid')->withDefault(
            [
            'pymt_date' => '',
            'pymt_status' => '',
            ]
        )->where('pymt_status','=',1);
    }



    public function paymentFpx()
    {
        return $this->hasOne(PaymentFpx::class, 'fk_applid')->withDefault();
    }

    public function auditrail()
    {
        return $this->morphMany(
            'Ekompaun\Systemconfig\Model\AuditTrail',
            'tableRef',
            'table_ref_type',
            'table_ref_id'
        );
    }

    public function lastAuditrail()
    {
        return $this->morphOne(
            'Ekompaun\Systemconfig\Model\AuditTrail',
            'tableRef',
            'table_ref_type',
            'table_ref_id'
        )->orderByDesc('audt_id')->withDefault(
            [
            'task' => __('Tidak ada Aktifiti')
            ]
        );
    }

    public function details()
    {
        return $this->hasMany(ApplicationDetail::class, 'fk_appl_id');
    }

    public function autoAssignedDetails()
    {
        return $this->details()->where('detl_indicator', '=', AssignmentIndicator::AUTO_ASSIGN);
    }

     public function latestdetail()
    {
        return $this->hasOne(ApplicationDetail::class, 'fk_appl_id')->orderByDesc('modify_date');
    }
}
