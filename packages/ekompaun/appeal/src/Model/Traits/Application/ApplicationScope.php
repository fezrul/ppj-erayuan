<?php

namespace Ekompaun\Appeal\Model\Traits\Application;

use Ekompaun\Systemconfig\Enum\AppealProgress;
use Ekompaun\Systemconfig\Enum\AppealType;
use Illuminate\Database\Eloquent\Builder;
use Ekompaun\Systemconfig\Enum\PaymentStatus;
use Ekompaun\Systemconfig\Enum\StatusKompaun;
use Illuminate\Support\Facades\DB;

trait ApplicationScope
{
    public function scopeAssignedTo($query, $user)
    {
        if ($user) {
            return $query->where('appl_personincharge', $user);
        }
    }

    public function scopeByBetweenDate($query, $dateFrom, $dateTo)
    {
        if ($dateFrom && $dateTo) {
            return $query->whereBetween(
                'appl_date',
                [
                    format_date($dateFrom, 'Y-m-d', 'Y-m-d 00:00:00'),
                    format_date($dateTo, 'Y-m-d', 'Y-m-d 23:59:59'),
                ]
            );
        }
    }

    public function scopeByYear($query, $year)
    {
        if ($year) {
            return $query->whereRaw("YEAR(appl_date) = $year");
        }
    }

    public function scopeByMonth($query, $month)
    {
        if ($month) {
            return $query->whereRaw("MONTH(appl_date) = $month");
        }
    }

    public function scopeByDateDiff($query, $date, $diff)
    {
        if ($date && $diff) {
            return $query->whereRaw("datediff('$date', appl_date) >= $diff");
        }
    }

    public function scopeByDate(Builder $query, $date)
    {
        return $query->whereBetween(
            'appl_date',
            [
                format_date($date, 'Y-m-d', 'Y-m-d 00:00:00'),
                format_date($date, 'Y-m-d', 'Y-m-d 23:59:59'),
            ]
        );
    }

    public function scopeByOffence($query, $offence)
    {
        if ($offence) {
            return $query->whereHas(
                'transaction', function ($query2) use ($offence) {
                return $query2->where("fk_lkp_offencecode", $offence);
            }
            );
        }
    }

    public function scopeByAkta($query, $akta)
    {
        if ($akta) {
            return $query->whereHas(
                'transaction.offence', function ($query2) use ($akta) {
                return $query2->where("fk_lkp_actid", $akta);
            }
            );
        }
    }

    public function scopeByTempohMaklumBalas($query, $time)
    {
        $tempohMaklumBalasQuery = "
                (SELECT 5 * (DATEDIFF(appl_decisiondate, appl_date) DIV 7) + MID('0123444401233334012222340111123400012345001234550', 7 * WEEKDAY(appl_date) + WEEKDAY(appl_decisiondate) + 1, 1))                 
                -              
                (SELECT COUNT(DISTINCT hday_date_from) 
                FROM ek_holiday 
                WHERE hday_status = 2 
                AND (DAYOFWEEK(hday_date_from) < 6) 
                AND (hday_date_from between date(appl_date) and date(appl_decisiondate))
                )              
                tempoh_maklum_balas
        ";
        $query->select("*", DB::raw($tempohMaklumBalasQuery));

        if ($time) {
            $time = (int)$time;
            $operator = ">";
            if($time < 0) {
                $operator = "<=";
                $time = abs($time);
            }

            $query->whereRaw("
                (SELECT 5 * (DATEDIFF(appl_decisiondate, appl_date) DIV 7) + MID('0123444401233334012222340111123400012345001234550', 7 * WEEKDAY(appl_date) + WEEKDAY(appl_decisiondate) + 1, 1))                 
                -              
                (SELECT COUNT(DISTINCT hday_date_from) 
                FROM ek_holiday 
                WHERE hday_status = 2 
                AND (DAYOFWEEK(hday_date_from) < 6) 
                AND (hday_date_from between date(appl_date) and date(appl_decisiondate))
                ) $operator $time            
            ");
        }
    }

    public function scopeByStatus(Builder $query, $status)
    {
        $status = (array)$status;
        if (!empty($status)) {
            return $query->whereIn('appl_status', $status);
        }
    }

    public function scopeByPaymentStatus(Builder $query, $status)
    {
        if ($status == PaymentStatus::SELESAI) {
            return $query->with(
                [
                    'payment' => function ($query2) {
                        $query2->whereRaw('pymt_totalamount = pymt_received');
                    },
                ]
            );
        }

        return $query->with(
            [
                'payment' => function ($query2) {
                    $query2->whereRaw('pymt_totalamount > pymt_received');
                },
            ]
        );
    }

    public function scopeByChannel(Builder $query, $channel)
    {
        if ($channel) {
            return $query->where('appl_channel', $channel);
        }
    }

    public function scopeRevertable(Builder $query)
    {
        return $query->whereIn(
            'appl_status', [
                             StatusKompaun::LULUS,
                             StatusKompaun::MENGIKUT_JADWAL,
                         ]
        );
    }

    public function scopeByKeyword(Builder $query, $keyword = null)
    {
        if ($keyword) {
            $query->with('transaction')->where(
                function (Builder $query2) use ($keyword) {
                    $query2->whereHas(
                        'transaction', function ($query3) use ($keyword) {
                        $query3->where('tran_offendericno', "$keyword")
                               ->orWhere('tran_compoundno', "$keyword")
                               ->orWhere('tran_vehicleno', "$keyword");
                    }
                    );
                }
            );
        }
    }

    public function scopeOngoing(Builder $query)
    {
        return $query->whereIn(
            'appl_status',
            [
                StatusKompaun::MENUNGGU_KELULUSAN,
                StatusKompaun::DOKUMEN,
                StatusKompaun::ASSIGNMENT,
                StatusKompaun::SUBMIT,
                StatusKompaun::DOKUMEN_LENGKAP,
                StatusKompaun::TIDAK_SELESAI,
                StatusKompaun::PENDING_PAYMENT,
            ]
        );
    }

    public function scopeCompleted(Builder $query)
    {
        return $query->whereIn(
            'appl_status',
            [
                StatusKompaun::LULUS,
                StatusKompaun::AMARAN,
                StatusKompaun::DITOLAK,
                StatusKompaun::PEMBATALAN,
                StatusKompaun::PAID,
            ]
        );
    }

    public function scopeByProgress(Builder $query, $progress)
    {
        switch ($progress) {
            case AppealProgress::BELUM_SELESAI:
                $query->ongoing();
                break;
            case AppealProgress::SELESAI:
                $query->completed();
                break;
            default:
                break;
        }

        return $query;
    }

    public function scopeTypeOfRayuanKedua(Builder $query)
    {
        $query->where('appl_type', '=', AppealType::RAYUAN_KEDUA);
    }

    public function scopeTypeOfRayuan(Builder $query)
    {
        $query->whereIn('appl_type', [AppealType::MENGIKUTI_JADUAL, AppealType::RAYUAN_KEDUA, AppealType::KES_MAHKAMAH]);
    }

    public function scopeExpiredCampaign(Builder $query)
    {
        $query->whereNotNull('fk_campid');
    }
}
