<?php

namespace Ekompaun\Appeal\Model\Traits\Application;

use Carbon\Carbon;
use Ekompaun\Systemconfig\Enum\AppealStatusType;
use Ekompaun\Systemconfig\Enum\AppealType;
use Ekompaun\Systemconfig\Enum\PaymentStatus;
use Ekompaun\Systemconfig\Enum\StatusKompaun;
use Ekompaun\Systemconfig\Model\CampaignDetail;
use Ekompaun\Systemconfig\Model\CampaignMaster;
use Ekompaun\Systemconfig\Model\Holiday;
use Ekompaun\Systemconfig\Model\Lookup\Offence;
use Ekompaun\Systemconfig\Model\Lookup\PaymentMethod;
use Ekompaun\Systemconfig\Services\CampaignService;
use Illuminate\Support\Collection;

trait ApplicationAttribute
{
    public function getPresentNoKompaunAttribute()
    {
        return $this->transaction->tran_compoundno;
    }

    public function getPresentPelangganAttribute()
    {
        return $this->transaction->user->name;
    }

    public function getPresentPelangganIdAttribute()
    {
        return $this->transaction->user->icno;
    }

    public function getPresentNoDaftarKenderaanAttribute()
    {
        return $this->transaction->tran_vehicleno;
    }

    public function getPresentTarikhRayuanAttribute()
    {
        return format_date($this->appl_date);
    }

    public function getPresentTarikhBayaranAttribute()
    {
        return format_date($this->payment->pymt_date);
    }

    public function getPresentTempohMaklumBalasAttribute()
    {
        return $this->tempoh_maklum_balas;
        // try {
        //     $start = Carbon::createFromFormat('Y-m-d H:i:s', $this->appl_date);
        //     $finish = Carbon::createFromFormat('Y-m-d H:i:s', $this->appl_decisiondate);
        //
        //     $holidays = Holiday::allDays();
        //     $diff =  $start->diffInDaysWithoutWeekendAndHolidays($finish, $holidays);
        //     // $diff =  $start->diffInDays($finish);
        //
        //     return __(':count hari', ['count' => $diff]);
        // } catch (\Exception $e) {
        //     return __('-');
        // }
    }

    public function getPresentTempohSetelahKelulusanAttribute()
    {
        try {
            $start = Carbon::createFromFormat('Y-m-d H:i:s', $this->appl_decisiondate);
            $finish = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now());

            $diff =  $finish->diffInDays($start);
            if($diff == 0){

                return __('Rayuan Diluluskan');

            }else{

                return __(' Rayuan Diluluskan<br>(:count hari)', ['count' => $diff]);
            }


        } catch (\Exception $e) {
            return __('-');
        }
    }

    public function getPresentAmaunAttribute()
    {
        return $this->appl_approveamount;
    }

    public function getPresentPaymentAmountAttribute()
    {
        return $this->payment->pymt_totalamount;
    }

    public function getPresentPaymentReceivedAttribute()
    {
        return $this->payment->pymt_received;
    }

    public function getPresentPaymentReceiptNumberAttribute()
    {
        return $this->payment->pymt_receiptnumber;
    }

    public function getPresentPaymentRefnoAttribute()
    {
        return $this->payment->pymt_refno;
    }

    public function getPresentPaymentPaymentMethodAttribute()
    {
        $paymentMethod = PaymentMethod::find($this->payment->fk_paymentmethod);
        return $paymentMethod->paymethod_name;
    }

    public function getPresentPaymentStatusAttribute()
    {
        return ($this->payment->pymt_totalamount == $this->payment->pymt_received) ?
            __("enum.".PaymentStatus::class.".".PaymentStatus::BERJAYA) :
            __("enum.".PaymentStatus::class.".".PaymentStatus::TIDAK_BERJAYA);
    }

    public function getPresentPaymentFpxTransIdAttribute()
    {
        return $this->paymentFpx->pfpx_transid;
    }

    public function getPresentPaymentFpxBankAttribute()
    {
        return $this->paymentFpx->bank;
    }

    public function getPresentPegawaiAttribute()
    {
        return $this->personInCharge->name;
    }


    public function getPresentOffenderNameAttribute()
    {
        return $this->transaction->tran_offendername;
    }

    public function getPresentOffenderIcnoAttribute()
    {
        return $this->transaction->tran_offendericno;
    }

    public function getPresentPaymentBankAttribute()
    {
        return $this->payment->pymt_bank;
    }

    public function getPresentPaymentFpxSerialNoAttribute()
    {
        return $this->paymentFpx->pfpx_serialno;
    }

    public function getPresentPaymentFpxAmountAttribute()
    {
        return $this->paymentFpx->pfpx_amountpaid;
    }

    public function getPresentActiveCampAmounAttribute()
    {
        $service = app(CampaignService::class);
        $campaign = $service->getCampaignFor($this->transaction);

        // return $campaign->cdet_amount;
        return $service->calculateDiscount($this->transaction->tran_compoundamount, $campaign->cdet_amount, $campaign->type);
    }

    public function getPresentStatusAttribute()
    {
        if ($this->transaction->is_kes_mahkamah) {
           return __('Kes Mahkamah');
        }

        if ($this->transaction->is_kes_selesai) {
            return __('Telah Dibayar');
        }

        // sekiranya pelanggan membuat rayuan ke-2, ketik butang "Tidak Setuju"  dan seterusnya hantar permohonan rayuan, status = Menunggu Kelulusan
        if ($this->is_menunggu_kelulusan) {
            return __('Menunggu Kelulusan');
        }

        if ($this->appl_status == StatusKompaun::MENGIKUT_JADWAL) {

            $start = Carbon::createFromFormat('Y-m-d H:i:s', $this->appl_date);
            $now = Carbon::now();

            $diff =  $now->diffInDays($start);

            return __('Belum Bayar (:n hari)', ['n' => $diff]);
        }

        if ($this->appl_status == StatusKompaun::LULUS) {
            return $this->present_tempoh_setelah_kelulusan ;
        }

        if ($this->is_bayaran_selesai) {
            return __('Bayaran Selesai') ;
        }

        return $this->status->status_name;
    }

    public function getPresentJenisRayuanAttribute()
    {
        $class = AppealType::class;

        return __("enum.$class.$this->appl_type");
    }

    public function getPresentAmauanAsalAttribute()
    {
        return $this->transaction->tran_compoundamount;
    }

    public function getIsMenungguKelulusanAttribute()
    {
        return $this->appl_status == StatusKompaun::MENUNGGU_KELULUSAN && $this->appl_type == AppealType::RAYUAN_KEDUA;
    }

    public function getIsBayaranSelesai()
    {
        return $this->appl_status == StatusKompaun::PAID;
    }
}
