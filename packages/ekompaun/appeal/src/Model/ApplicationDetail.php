<?php

namespace Ekompaun\Appeal\Model;

use Ekompaun\Platform\Model\BaseModel;

class ApplicationDetail extends BaseModel
{
    protected $table = 'ek_application_details';

    protected $primaryKey = 'detl_id';

    protected $guarded = [];
}
