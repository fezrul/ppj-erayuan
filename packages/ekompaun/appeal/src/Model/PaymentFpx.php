<?php

namespace Ekompaun\Appeal\Model;

use Ekompaun\Platform\Model\BaseModel;

class PaymentFpx extends BaseModel
{
    protected $table = 'ek_payment_fpx';

    protected $primaryKey = 'pfpx_id';

    protected $guarded = [];
}
