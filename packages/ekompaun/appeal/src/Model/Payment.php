<?php

namespace Ekompaun\Appeal\Model;

use Ekompaun\Platform\Model\BaseModel;

class Payment extends BaseModel
{
    protected $table = 'ek_payment';

    protected $primaryKey = 'pymt_id';

    protected $guarded = [];
}
