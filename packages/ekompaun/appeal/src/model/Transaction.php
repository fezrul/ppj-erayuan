<?php

namespace Ekompaun\Appeal\Model;

use App\User;
use Carbon\Carbon;
use Ekompaun\Systemconfig\Enum\AppealStatusType;
use Ekompaun\Systemconfig\Enum\AppealType;
use Ekompaun\Systemconfig\Enum\KompaunType;
use Ekompaun\Systemconfig\Enum\StatusKompaun;
use Illuminate\Database\Eloquent\Builder;
use Ekompaun\Platform\Model\BaseModel;
use Ekompaun\Systemconfig\Model\Lookup\Offence;
use Ekompaun\Systemconfig\Model\Lookup\Vehicle;
use Ekompaun\Systemconfig\Model\Lookup\StatusSap;

class Transaction extends BaseModel
{
    protected $table = 'ek_transaction';

    protected $appends = ['ek_transaction'];

    protected $primaryKey = 'tran_id';

    protected $guarded = [];

    protected $casts = [
        'tran_compoundamount' => 'int',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'fk_userid');
    }

    public function application()
    {
        return $this->hasMany(Application::class, 'appl_transid');
    }

    public function rayuanKedua()
    {
        return $this->hasMany(Application::class, 'appl_transid')->where('appl_type', '=', AppealType::RAYUAN_KEDUA);
    }

    public function rayuanCount()
    {
        return $this->hasMany(Application::class, 'appl_transid')->whereIn('appl_type', array(1,2));

    }

    public function lastApplication()
    {
        return $this->hasOne(Application::class, 'appl_transid')->orderByDesc('created_date')->withDefault(
            [
                'appl_campamount'    => '0',
                'appl_approveamount' => '0',
                'appl_gst'           => '0',
                'appl_status'        => StatusKompaun::BELUM_BAYAR,
            ]
        );
    }

    public function offence()
    {
        return $this
            ->belongsTo(Offence::class, 'fk_lkp_offencecode', 'offence_code')
            ->withDefault(
                [
                    'offence_name' => __('-'),
                ]
            );
    }



    public function statusSap()
    {
        return $this->belongsTo(StatusSap::class, 'fk_lkp_sapstatus')->withDefault();
    }

    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class, 'fk_lkp_vehicletype');
    }

    public function payment()
    {
        return $this->hasOne(Payment::class, 'fk_applid');
    }

    public function scopeByUser(Builder $query, $user)
    {
        return $query->where('fk_userid', $user);
    }

    public function scopeByYear(Builder $query, $year)
    {
        if ($year) {
            return $query->whereRaw("YEAR(tran_compounddate) = $year");
        }
    }

    public function scopeByKeyword(Builder $query, $keyword = null)
    {
        if ($keyword) {
            $query->with('user')->where(
                function (Builder $query2) use ($keyword) {
                    $query2
                        ->where('tran_vehicleno', "$keyword")
                        ->orWhere('tran_compoundno', "$keyword")
                        ->orWhere('tran_offendericno', "$keyword")
                        ->orWhereHas(
                            'user', function ($query3) use ($keyword) {
                            $query3->where('icno', "$keyword");
                        }
                        );
                }
            );
        }
    }


    public function scopeByKeywordnoUser(Builder $query, $keyword = null)
    {
        if ($keyword) {
            $query->with('user')->where(
                function (Builder $query2) use ($keyword) {
                    $query2
                        ->where('tran_vehicleno', "$keyword")
                        ->orWhere('tran_compoundno', "$keyword")
                        ->orWhere('tran_offendericno', "$keyword");
                        // ->orWhereHas(
                        //     'user', function ($query3) use ($keyword) {
                        //     $query3->where('icno', "$keyword");
                        // }
                        // );
                }
            );
        }
    }
    public function getDaysSinceOffenceAttribute()
    {
        $now = Carbon::now();
        $date = Carbon::createFromFormat('Y-m-d H:i:s', $this->tran_compounddate);

        return $date->diffInDays($now);
    }

    public function getAmountAttribute()
    {
        $lastApplication = $this->lastApplication;
        if (in_array($lastApplication->appl_status, [StatusKompaun::BATAL_AUTOMATIK, StatusKompaun::DITOLAK, StatusKompaun::BATAL_PERMOHONAN, StatusKompaun::RESET])) {
            return $this->tran_compoundamount;
        }

        return ($this->lastApplication->exists) ? $this->lastApplication->appl_approveamount : $this->tran_compoundamount;
    }

    public function getTotalAmountAttribute()
    {
        return $this->amount + $this->lastApplication->appl_gst;
    }

    public function getIsKesMahkamahAttribute()
    {
        return $this->statusSap->appeal_type == KompaunType::KES_MAHKAMAH;
    }

    public function getIsKesSelesaiAttribute()
    {
        return $this->statusSap->appeal_type == KompaunType::KES_SELESAI;
    }

    public function getAllowedToAppealAttribute()
    {
        return in_array(
            $this->statusSap->status_code,
            [
                'AP', 'AV',
                'CC', 'CM', 'CML', 'CN', 'CS', 'CSV', 'CSX',
                'DX',
                'KSS',
                'SC', 'SI', 'SR'
            ]
        );
    }

    public function getNotAllowedToAppealAttribute()
    {
        return in_array(
            $this->statusSap->status_code,
            [
                'AF', 'CT', 'DB', 'HD', 'NS', 'SBS', 'SN', 'SV', 'WT',
            ]
        );
    }

    public function getPresentCompoundamountAttribute()
    {
        if ($this->lastApplication->appl_status == StatusKompaun::BELUM_BAYAR &&
            $this->is_kes_mahkamah == false &&
            $this->is_kes_selesai == false
            // $this->lastApplication->appl_status != StatusKompaun::LULUS &&
            // $this->lastApplication->appl_status != StatusKompaun::PAID
            ) {
            //default belum bayar:
            return config('site.max_saman');
        }
        return $this->tran_compoundamount;
    }

    public function getPresentBelumBayarAttribute()
    {
        try {
            $start = Carbon::createFromFormat('Y-m-d H:i:s', $this->tran_compounddate);
            $finish = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now());

            $diff =  $finish->diffInDays($start);

            return __(' Belum Bayar (:count hari)', ['count' => $diff]);
        } catch (\Exception $e) {
            return __('-');
        }
    }
}
