<?php

namespace Ekompaun\Appeal\Repositories;

use Carbon\Carbon;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Systemconfig\Enum\StatusKompaun;
use App\Events\Mail\SendMailTindakanEvent;

class ApplicationRepository
{
    public function normalAppeal($status, $dateFrom, $dateTo)
    {
        return $this->appealByKeyword($status, $dateFrom, $dateTo)->paginate();
    }

    public function exportNormalAppeal($status, $dateFrom, $dateTo)
    {
        return $this->appealByKeyword($status, $dateFrom, $dateTo)->get();
    }

    private function appealByKeyword($status, $dateFrom, $dateTo)
    {
        return Application::with('transaction', 'status', 'user')
        ->byStatus($status)
        ->byBetweenDate($dateFrom, $dateTo);
    }

    public function assignedButIdle($days)
    {
        $date = Carbon::now()->format('Y-m-d');
        Application::byStatus(StatusKompaun::ASSIGNMENT)
        ->byDateDiff($date, $days)
        ->get()
        ->each(
            function ($appeal) {
                // sementara tanpa memperhatikan hari kerja dan holiday
                event(new SendMailTindakanEvent($appeal->appl_id));
            }
        );
    }

    public function appealCustomerIdle($date, $diff)
    {
        // $appeal = Application::with('transaction')
        //         ->revertable()
        //         ->byDateDiff($date, $diff)
        //         ->get();

        $appeal = Application::with('transaction')
                ->byDateDiff($date, $diff)
                ->whereRaw('appl_status in ("2","7")')
                ->get();

        return $appeal;
    }

    public function appealCustomerIdleByStatus6($date, $diff)
    {
        $appeal = Application::byDateDiff($date, $diff)
                ->where('appl_status','=',6)
                ->get();

        return $appeal;

        // $appeal = Application::byDateDiff($date, $diff)
        //         ->where('appl_status','=',6)
        //         ->get();
    }
}
