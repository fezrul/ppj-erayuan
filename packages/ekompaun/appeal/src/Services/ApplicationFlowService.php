<?php

namespace Ekompaun\Appeal\Services;

use App\Events\Application\DokumenLengkap;
use Carbon\Carbon;
use Ekompaun\Appeal\Model\Transaction;
use Ekompaun\Systemconfig\Enum\AssignmentIndicator;
use Ekompaun\Systemconfig\Enum\AssignmentType;
use Ekompaun\Systemconfig\Services\CampaignService;
use Illuminate\Support\Facades\DB;
use Ekompaun\Appeal\Model\Attachment;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Systemconfig\Enum\AppealType;
use Ekompaun\Systemconfig\Enum\StatusKompaun;
use Ekompaun\Appeal\Repositories\ApplicationRepository;
use Ekompaun\Systemconfig\Model\ScheduleRate;
use Ekompaun\Sap\Http\Controllers\SapController;
use App\Events\Application\ApplicationAssignEvent;
use App\Events\Mail\SendMailAssignmentEvent;

class ApplicationFlowService
{
    protected $repo;

    protected $campaignService;

    public function __construct(ApplicationRepository $repo, CampaignService $campaignService,ScheduleRate $scheduleRate,SapController $sap)
    {
        $this->repo = $repo;
        $this->campaignService = $campaignService;
        $this->scheduleRate = $scheduleRate;
        $this->sap = $sap;

    }

    public function rayuanIkutJadwal($transaction, $user)
    {
        $campaign = $this->campaignService->getCampaignFor($transaction);

        $amaunKadarRayuan = $this->scheduleRate->getAmaunKadarRayuan($transaction);

        $date = Carbon::now()->format('Y-m-d H:i:s');
        $data = [
            'appl_reason'        => __('Rayuan Mengikut jadual'),
            'appl_status_sap'    => $transaction->fk_lkp_sapstatus,
            'appl_status'        => StatusKompaun::MENGIKUT_JADWAL,
            'appl_type'          => AppealType::MENGIKUTI_JADUAL,
            'appl_date'          => $date,
            'fk_campid'          => $campaign->fk_campid,
            'appl_campamount'    => $amaunKadarRayuan,
            'appl_approveamount' => $this->campaignService->calculateAmountAfterDiscount($amaunKadarRayuan, $campaign->cdet_amount, $campaign->type),
        ];

        $application = Application::make($data);
        $application->transaction()->associate($transaction);
        $application->user()->associate($user);
        $application->save();

        return $application;
    }

    public function decision($application, $data, $user)
    {
        $data['appl_approveamount'] = $data['appl_approveamount']??0;
        $data['modify_by'] = $user->id;
        $data['appl_approveby'] = $user->id;
        $data['appl_status_sap_prev'] = $application->appl_status_sap;
        $data['appl_decisiondate'] = new Carbon();

        $application->update($data);

        return $application;
    }

    public function assign($application, $assignee, $user)
    {
        $now = Carbon::now();
        $data['appl_status'] = ($assignee !== null) ? StatusKompaun::ASSIGNMENT : StatusKompaun::MENUNGGU_KELULUSAN;
        $data['appl_personincharge'] = $assignee->getKey();
        $data['appl_assigndate'] = $now;

        $assignmentType = AssignmentType::NORMAL;
        if ($application->appl_personincharge !== null && ($application->appl_personincharge !== $assignee->getKey())) {
            $assignmentType = AssignmentType::REASSIGN;
        }

        $application->update($data);

        $application->details()->create(
            [
                'detl_assignpic'  => $assignee->getKey(),
                'detl_reassign'   => $assignmentType,
                'detl_assigndate' => $now,
                'detl_indicator'  => AssignmentIndicator::MANUAL_ASSIGN,
                'created_by'      => $user->getKey(),
                'modify_by'       => $user->getKey(),
            ]
        );

        event(new ApplicationAssignEvent($user, $application->getKey()));

        if ($assignee) {
            event(new SendMailAssignmentEvent($application->getKey()));
        }

        return $application;
    }

    public function autoAssign($application, $assignee)
    {
        $now = Carbon::now();

        $application->appl_personincharge = $assignee;
        $application->appl_status = StatusKompaun::ASSIGNMENT;
        $application->appl_assigndate = $now;
        $application->save();

        $application->details()->create(
            [
                'detl_assignpic'  => $application->appl_personincharge,
                'detl_reassign'   => AssignmentType::REASSIGN,
                'detl_assigndate' => $now,
                'detl_indicator'  => AssignmentIndicator::AUTO_ASSIGN,
                'created_by'      => null,
                'modify_by'       => null,
            ]
        );

        event(new SendMailAssignmentEvent($application->getKey()));

        return $application;
    }

    public function revertAmount()
    {
        $date = Carbon::now()->format('Y-m-d');
        $diff = 14;
        $applications = $this->repo->appealCustomerIdle($date, $diff);
        $indi = 0;



        foreach ($applications as $application)
        {
            $name = "id = ".$application->appl_id." | appl_status = ".$application->appl_status."\r\n ";
            echo $name;
            $return = $this->sap->getFunction('2', '1', $application->transaction->tran_compoundno, $application->transaction->tran_compoundamount);

              if ($return == 1){$indi = 1;}

            $application->appl_approveamount = $application->transaction->tran_compoundamount;
            $application->fk_campid = null;
            $application->appl_revert_indi = $indi;
            $application->appl_status = 14;
            $application->appl_campamount = 0;
            $application->save();

        }
    }

    public function revertStatus6()
    {
        $date = Carbon::now()->format('Y-m-d');
        $diff = config('site.auto_cancel_status_6');
        $applications = $this->repo->appealCustomerIdleByStatus6($date, $diff);
  
        foreach ($applications as $application)
        {
            $name = "id = ".$application->appl_id." | appl_status = ".$application->appl_status."\r\n ";
            echo $name;
            $application->appl_status = 8;
            $application->save();
        }
    }

    public function cancel($application)
    {

        $indi = 0;
        $appl_type = $application->appl_type;

        if($appl_type == 4)
        {
            $return = $this->sap->getFunction('2', '3', $application->transaction->tran_compoundno, $application->transaction->tran_compoundamount_prev);
        }else{

            $return = $this->sap->getFunction('2', '1', $application->transaction->tran_compoundno, $application->transaction->tran_compoundamount);
        }

        if ($return == 1){$indi = 1;}

        $application->appl_status = StatusKompaun::BATAL_PERMOHONAN;
        $application->appl_revert_indi = $indi;
        $application->save();



    }

    public function rayuanKedua($transaction, $request, $application = null)
    {
        return DB::transaction(
            function () use ($transaction, $request, $application) {
                $files = collect($request->file('file'))->filter()->all();

                if ($application == null) {

                    $amaunKadarRayuan = $this->scheduleRate->getAmaunKadarRayuan($transaction);

                    $dataApplication = [
                        'appl_reason'        => $request->get('appl_reason'),
                        'appl_transid'       => $transaction->getKey(),
                        'created_by'         => $request->user()->id,
                        'appl_date'          => Carbon::now(),
                        'appl_type'          => AppealType::RAYUAN_KEDUA,
                        'appl_status'        => StatusKompaun::MENUNGGU_KELULUSAN,
                        'appl_email'         => $request->get('appl_email'),
                        'appl_phonenumber'   => $request->get('appl_phonenumber'),
                        'appl_approveamount' => $amaunKadarRayuan,
                    ];

                    $application = Application::create($dataApplication);

                } else {
                    $data = $request->only(['appl_phonenumber', 'appl_email', 'appl_reason']);

                    $data['appl_status'] = StatusKompaun::MENUNGGU_KELULUSAN;

                    // jika previous status PERLU DOKUMEN TAMBAHAN and user upload document,
                    // maka set current status menjadi DOKUMEN LENGKAP
                    if ($application->appl_status == StatusKompaun::DOKUMEN && (count($files) > 0)) {
                        $data['appl_status'] = StatusKompaun::DOKUMEN_LENGKAP;
                        event(new DokumenLengkap($application));
                    }

                    $application->update($data);
                }

                $listFile = [];
                foreach ($files as $file) {
                    $file->store('attachments', 'public');
                    $listFile[] = [
                        'attc_filename' => $file->hashName(),
                        'fk_applid'     => $application->appl_id,
                        'created_by'    => $request->user()->id
                    ];
                }

                foreach ($listFile as $file) {
                    Attachment::create($file);
                }

                return $application;
            }
        );
    }

    public function bayarKempen(Transaction $transaction, $campaign, $amount, $user)
    {
        $application = $transaction->lastApplication;
        if ($application->exists) {
            $application->update([
                 'appl_type'          => AppealType::KEMPEN,
                 'appl_status'        => StatusKompaun::PENDING_PAYMENT,
                 'appl_approveamount' => $amount,
                 'fk_campid'          => $campaign->campaignMaster->getKey(),
                 'appl_campamount'    => $amount,
             ]);
        } else {
            $application = $transaction
                ->lastApplication()
                ->create([
                     'appl_reason'        => __('Bayaran Kempen'),
                     'created_by'         => $user->getKey(),
                     'appl_date'          => Carbon::now(),
                     'appl_type'          => AppealType::KEMPEN,
                     'appl_status'        => StatusKompaun::PENDING_PAYMENT,
                     'appl_approveamount' => $amount,
                     'fk_campid'          => $campaign->campaignMaster->getKey(),
                     'appl_campamount'    => $amount,
                 ]);
        }

        return $application;
    }
}
