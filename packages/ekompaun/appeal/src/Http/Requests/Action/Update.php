<?php

namespace Ekompaun\Appeal\Http\Requests\Action;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Ekompaun\Systemconfig\Model\UserProfile;
use Ekompaun\Systemconfig\Enum\StatusKompaun;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'appl_status'           => 'required|exists:lkp_status,status_id',
            'appl_approveamount'    => 'nullable|numeric|required_if:appl_status,2'
        ];

        // If status kelulusan = Perlu Dokumen Tambahan, reason tidak wajib diisi
        if (request()->appl_status == StatusKompaun::DOKUMEN) {
            $rules['appl_reason'] = 'required|min:5|max:100';
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'appl_approveamount' => __("Bayaran (RM)"),
            'appl_reason'        => __("Ulasan"),
        ];
    }

    public function messages()
    {
        return [
            'appl_approveamount.required_if' => __("Bayaran (RM) diperlukan bila Keputusan sama dengan Pengurangan.")
        ];
    }
}
