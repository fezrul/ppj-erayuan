<?php

namespace Ekompaun\Appeal\Http\Requests\Assignment;

use App\User;
use Ekompaun\Systemconfig\Model\UserProfile;
use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'appl_personincharge' => 'nullable|exists:users,id',
        ];
    }

    public function attributes()
    {
        return [
            'appl_personincharge' => __("Pegawai JU"),
        ];
    }
}
