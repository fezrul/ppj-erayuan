<?php

namespace Ekompaun\Appeal\Http\Controllers\Pdf;

use PDF;
use App\Http\Controllers\Controller;
use Ekompaun\Appeal\Repositories\ApplicationRepository;

class PdfNormalAppealController extends Controller
{
    public function index(ApplicationRepository $service)
    {
        $customPaper = array(0,0,1440,3000);
        $dateFrom = format_date(request('from'), 'd-m-Y', 'Y-m-d');
        $dateTo = format_date(request('to'), 'd-m-Y', 'Y-m-d');
        $applications = $service->exportNormalAppeal(request('status'), $dateFrom, $dateTo);

        $pdf = PDF::loadview(
            'appeal::pdf.normal-appeal',
            compact('applications')
        )->setPaper($customPaper, 'portrait');

        return $pdf->download(__('Normal-appeal.pdf'));
    }
}
