<?php

namespace Ekompaun\Appeal\Http\Controllers\Csv;

use Excel;
use App\Http\Controllers\Controller;
use Ekompaun\Appeal\Repositories\ApplicationRepository;

class CsvNormalAppealController extends Controller
{
    public function index(ApplicationRepository $service)
    {
        $dateFrom = format_date(request('from'), 'd-m-Y', 'Y-m-d');
        $dateTo = format_date(request('to'), 'd-m-Y', 'Y-m-d');
        $applications = $service->exportNormalAppeal(request('status'), $dateFrom, $dateTo);
        Excel::create(
            'Normal-appeal', function ($excel) use ($applications) {
                $excel->sheet(
                    'New sheet', function ($sheet) use ($applications) {
                        $sheet->loadView('appeal::csv.normal-appeal', compact('applications'));
                    }
                );
            }
        )->export('csv');
    }
}
