<?php

namespace Ekompaun\Appeal\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Appeal\Model\Transaction;
use Ekompaun\Systemconfig\Enum\StatusKompaun;

class AssigneeController extends Controller
{

    public function index()
    {
        $user = User::pegawaiJuCount()->get()->pluck('name', 'id')->prepend('-No Assignee-', '')->sort();
        $applications = Application::byStatus([2,3,4,5,6,10])->paginate();

        $transactions = Transaction::whereHas('lastApplication', function ($query) {
            $query->where('appl_status', '<>', 1);
            $query->whereNotNull('appl_personincharge');
        })->paginate();

        return view('appeal::assignment.assignee', compact('applications', 'transactions', 'user'));
    }
}
