<?php

namespace Ekompaun\Appeal\Http\Controllers;

use App\Http\Controllers\Controller;
use Ekompaun\Systemconfig\Model\Lookup\Status;
use Ekompaun\Appeal\Repositories\ApplicationRepository;

class NormalAppealController extends Controller
{
    public function index(ApplicationRepository $service)
    {
        $status = Status::dropdown();
        $dateFrom = format_date(request('from'), 'd-m-Y', 'Y-m-d');
        $dateTo = format_date(request('to'), 'd-m-Y', 'Y-m-d');
        $applications = $service->normalAppeal(request('status'), $dateFrom, $dateTo)
            ->appends(['status'=>request('status'), 'from'=>request('from'), 'to'=>request('to')]);
        return view('appeal::normal-appeal.index', compact('applications', 'status'));
    }
}
