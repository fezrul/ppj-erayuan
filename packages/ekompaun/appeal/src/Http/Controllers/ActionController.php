<?php

namespace Ekompaun\Appeal\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Systemconfig\Enum\StatusType;
use Ekompaun\Systemconfig\Enum\StatusKompaun;
use Ekompaun\Systemconfig\Model\Lookup\Status;
use App\Events\Application\ApplicationViewEvent;
use Ekompaun\Appeal\Http\Requests\Action\Update;
use Ekompaun\Appeal\Services\ApplicationFlowService;
use App\Events\Application\ApplicationStatusChangeEvent;
use App\Events\Mail\SendMailApplicationStatusChangeEvent;
use Ekompaun\Sms\Http\Controllers\SmsController;
use Ekompaun\Sap\Http\Controllers\SapController;

class ActionController extends Controller
{

    public function __construct(SmsController $sms,SapController $sap)
    {

        $this->sms = $sms;
        $this->sap = $sap;

    }

    public function index(Request $request)
    {
        $user = $request->user();
        $applications = Application::with('transaction', 'status','latestdetail')->assignedTo($user->id)->paginate(100);

        return view('appeal::action.index', compact('applications'));
    }

    public function edit(Request $request, $id)
    {
        $application = Application::with(
            'transaction.user',
            'transaction.offence',
            'status',
            'attachments'
        )
        ->findOrFail($id);
        if ($application->appl_status == StatusKompaun::LULUS) {
            return redirect()->back()->withError(__("Rayuan Telah Diberi Keputusan"));
        }
        $status = Status::whereStatusType(StatusType::DECISION)
            ->get()
            ->pluck('status_name', 'status_id')
            ->prepend(__("--Choose One--"), '');
        event(new ApplicationViewEvent($request->user(), $id));

        return view('appeal::action.edit', compact('application', 'status'));
    }

    public function update(ApplicationFlowService $service, Update $request, $id)
    {
        $status = $request->appl_status;
        $application = Application::findOrFail($id);
        $this->authorize('keputusan', $application);

        try {
            $application = $service->decision($application, $request->only(['appl_status', 'appl_reason', 'appl_approveamount']), $request->user());
            event(new ApplicationStatusChangeEvent($request->user(), $id));
            $applicationdata = Application::with(
            'transaction.user',
            'transaction.offence'
             )->findOrFail($application->appl_id);

             $compno = $applicationdata->transaction->tran_compoundno;
             $price = $applicationdata->appl_approveamount;


            if($status == 2) {
                $return = $this->sms->getSend('4', $id);
                $return = $this->sap->getFunction('2', '1', $compno, $price);
            }
            if($status == 3) {
                $return = $this->sms->getSend('6', $id);
                $return = $this->sap->getFunction('2', '5', $compno, $price);
            }
            if($status == 4) {
                $return = $this->sms->getSend('5', $id);
                // $return = $this->sap->getFunction('2', '5', $compno, $price);

            }if($status == 5) {
                $return = $this->sap->getFunction('2', '5', $compno, $price);

            }if($status == 6) {
                $return = $this->sms->getSend('3', $id);
            }
            event(new SendMailApplicationStatusChangeEvent($id));


            
            return redirect()->route('appeal::action.index')->withSuccess(__("Berjaya Mengubah Tugasan"));
        } catch (\Exception $e) {
            return redirect()->back()->withError($e->getMessage())->withInput();
        }
    }
}
