<?php

namespace Ekompaun\Appeal\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Ekompaun\Systemconfig\Model\AssignmentSetting;

class AssignmentSettingController extends Controller
{
    public function edit()
    {
        $pegawai = User::pegawaiJu()->pluck("name", "id")->prepend(__('--Sila Pilih--'), '');
        $savedAssignees = collect(AssignmentSetting::all())->pluck('asgn_pic_userid', 'id');
        $autoAssignCount = setting_fallback('site.auto_assign_staff_number');
        if(!$autoAssignCount) {
            $autoAssignCount = 2;
        }

        $assignees = collect()->times($autoAssignCount, function ($key) use ($savedAssignees){
            return $savedAssignees->get($key);
        })->toArray();

        return view('appeal::assignment-setting.edit', compact('pegawai', 'assignees', 'autoAssignCount'));
    }

    public function update()
    {
        $data = collect(request()->get('assignee'))
            ->each(
                function ($id, $key) {
                    $pegawai = User::pegawai()->find($id);
                    if ($pegawai) {
                        AssignmentSetting::updateOrCreate(
                            ['id' => $key],
                            ['asgn_pic_userid' => $id]
                        );
                    }
                }
            );

        $keys = $data->filter(function($item){return $item === null;})->keys();
        AssignmentSetting::whereIn('id', $keys)->delete();

        $autoAssignCount = setting_fallback('site.auto_assign_staff_number');
        AssignmentSetting::where('id', '>', $autoAssignCount)->delete();

        return redirect()->back()->withSuccess(__('Berjaya Mengubah Tetapan Pegawai Auto Assign'));
    }
}
