<?php

namespace Ekompaun\Appeal\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Appeal\Model\Transaction;
use Ekompaun\Systemconfig\Enum\StatusKompaun;
use Ekompaun\Appeal\Http\Requests\Assignment\Update;
use Ekompaun\Appeal\Services\ApplicationFlowService;

class AssignmentController extends Controller
{

    public function index()
    {
        $user = User::pegawaiJuCount()->get()->pluck('name', 'id')->prepend('-No Assignee-', '')->sort();
        $applications = Application::byStatus(StatusKompaun::MENUNGGU_KELULUSAN)->paginate();
        $transactions = Transaction::whereHas('lastApplication', function ($query) {
            $query->where('appl_status', '=', StatusKompaun::MENUNGGU_KELULUSAN);
        })->paginate();

        return view('appeal::assignment.index', compact('applications', 'transactions', 'user'));
    }

    public function update(ApplicationFlowService $service, Update $request, $id)
    {
        $application = Application::findOrFail($id);
        $assigneeId = current($request->get('appl_personincharge', []));
        $staff = User::pegawai()->find($assigneeId);

        try {
            $service->assign($application, $staff, $request->user());

            return redirect()->back()->withSuccess(__("Berjaya Mengubah Tindakan"));
        } catch (\Exception $e) {
            return redirect()->back()->withError($e->getMessage())->withInput();
        }
    }
}
