<?php

namespace Ekompaun\Appeal\Policies;

use App\User;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Appeal\Model\Transaction;
use Ekompaun\Systemconfig\Enum\AppealType;
use Ekompaun\Systemconfig\Enum\Status;
use Ekompaun\Systemconfig\Enum\StatusKompaun;
use Ekompaun\Systemconfig\Services\CampaignService;
use Illuminate\Auth\Access\HandlesAuthorization;

class AppealPolicy
{
    use HandlesAuthorization;

    public function submitRayuanIkutJadwal(User $user, Transaction $transaction)
    {
        if ($transaction->is_kes_mahkamah || $transaction->is_kes_selesai) {
            return false;
        }

        if ($transaction->lastApplication->is_menunggu_kelulusan) {
            return false;
        }

        // Kalau ada active campaign, tidak boleh buat rayuan (Diskaun)
        $activeCampaign = app(CampaignService::class)->getCampaignFor($transaction);
        if ($activeCampaign->exists) {
            return false;
        }

        return in_array($transaction->lastApplication->appl_status, [
            StatusKompaun::BELUM_BAYAR,
            StatusKompaun::DITOLAK,
            StatusKompaun::PEMBATALAN,
            StatusKompaun::BATAL_AUTOMATIK,
            StatusKompaun::BATAL_PERMOHONAN,
            StatusKompaun::RESET,
        ]);

    }

    public function submitRayuanKedua(User $user, Transaction $transaction)
    {
        if ($transaction->is_kes_mahkamah || $transaction->is_kes_selesai) {
            return false;
        }

        // Kalau ada active campaign, tidak boleh buat rayuan
        $activeCampaign = app(CampaignService::class)->getCampaignFor($transaction);
        if ($activeCampaign->exists) {
            return false;
        }

        //@todo: add more checking
    }

    public function updateRayuanKedua(User $user, Application $application)
    {
        return in_array($application->appl_status, [
            StatusKompaun::DOKUMEN
        ]);
    }

    public function keputusan(User $user, Application $application)
    {
        return in_array($application->appl_status, [
            StatusKompaun::MENUNGGU_KELULUSAN,
            StatusKompaun::ASSIGNMENT,
            StatusKompaun::DOKUMEN_LENGKAP,
        ]);
    }

    public function rundingcara(User $user, Transaction $transaction)
    {
        $activeCampaign = app(CampaignService::class)->getCampaignFor($transaction);

        if ($activeCampaign->exists) {
            return false;
        } else {
            if (in_array($transaction->statusSap->status_code, ['AF', 'CT', 'DB', 'HD', 'NS', 'SBS', 'SN', 'SV', 'WT', 'DNAA'])) {
                return true;
            }
        }

        return false;
    }

    public function batalRayuanIkutJadwal(User $user, Transaction $transaction)
    {
        if ($transaction->lastApplication->appl_type == AppealType::KEMPEN && $transaction->lastApplication->appl_status == StatusKompaun::MENGIKUT_JADWAL) {
            return false;
        }

        return $transaction->lastApplication->appl_status == StatusKompaun::MENGIKUT_JADWAL;
    }

    public function batalRayuanKedua(User $user, Transaction $transaction)
    {
        return true;
    }

    public function bayarFpx(User $user, Transaction $transaction)
    {
        // Kalau ada active campaign, harus langusng bayar
        $activeCampaign = app(CampaignService::class)->getCampaignFor($transaction);
        $belumBayar = in_array($transaction->lastApplication->appl_status, [
            StatusKompaun::BELUM_BAYAR,
            StatusKompaun::PENDING_PAYMENT,
        ]);

        if ($activeCampaign->exists && $belumBayar) {
            return true;
        }

        if ($activeCampaign->exists && (in_array($transaction->statusSap->status_code, ['DB', 'WT', 'AF', 'DNAA']))) {
            return true;
        }

        if ($transaction->is_kes_mahkamah || $transaction->is_kes_selesai) {
            return false;
        }

        return in_array($transaction->lastApplication->appl_status, [
            StatusKompaun::MENGIKUT_JADWAL,
            StatusKompaun::LULUS,
        ]);
    }
}
