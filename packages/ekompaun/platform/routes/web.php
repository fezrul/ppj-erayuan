<?php
Route::group(
    ['middleware' => ['web','auth'], 'namespace' => 'Ekompaun\Platform\Http\Controllers', 'as' => 'platform::'], function () {

        Route::resource('package', 'PackageController', ['only' => 'index'])
        ->middleware('can:'.\Ekompaun\Systemconfig\Enum\Permission::MANAGE_SYSTEM);
        Route::get('package/enable', 'PackageController@enable')->name('package.enable');
        Route::get('package/disable', 'PackageController@disable')->name('package.disable');

    }
);
