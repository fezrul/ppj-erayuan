<?php

namespace Ekompaun\Platform\Model;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'modify_date';

    protected $displayField;

    public static function dropdown($placeholder = null)
    {
        $model = new static;

        $list = $model->pluck($model->displayField, $model->getKeyName());

        if (!$placeholder) {
            $placeholder = __("--Choose One--");
        }

        $list->prepend($placeholder, '');

        return $list;
    }
}
