<?php

namespace Ekompaun\Platform\Enum;

use MyCLabs\Enum\Enum;

class PackageStatus extends Enum
{
    const ENABLED = 'ENABLED';
    const DISABLED = 'DISABLED';
}
