<?php

namespace Ekompaun\Platform\Enum;

use MyCLabs\Enum\Enum;

class BaseEnum extends Enum
{
    public static function dropdown()
    {
        $list = [];
        $class = static::class;

        foreach (static::toArray() as $key) {
            // $list[$key] = trans("enum.$class.$key");
            $list[$key] = __("enum.$class.$key");
        }

        return $list;
    }
}
