<?php

namespace Ekompaun\Platform;

use Ekompaun\Platform\Enum\PackageStatus;

class PackageManager
{
    const STORAGE_KEY = 'platform.packages';

    /**
     * List of available packages
     *
     * @var array
     */
    protected $packages = [];

    public function __construct()
    {
        $this->load();
    }

    public function all()
    {
        return $this->packages;
    }

    public function save()
    {
        setting()->set(static::STORAGE_KEY, $this->packages);

        return setting()->save();
    }

    /**
     * Load available packages from filesystem
     * and then store it to persistent storage
     */
    public function sync()
    {
        $this->packages = [];

        $packages = glob(base_path('packages') . '/ekompaun/*');

        foreach ($packages as $package) {
            $composerFile = $package . '/composer.json';

            if (file_exists($composerFile)) {
                $contents = file_get_contents($composerFile);
                $contents = utf8_encode($contents);
                $results = json_decode($contents, true);

                if ($results !== null && isset($results['name'])) {
                    $results['status'] = PackageStatus::ENABLED;
                    $this->packages[$results['name']] = $results;
                }
            }
        }

        $this->save();
    }

    public function load()
    {
        $packages = setting(self::STORAGE_KEY);

        if ($packages == null) {
            $this->sync();
            $packages = setting(self::STORAGE_KEY);
        }

        $this->packages = $packages;
    }

    public function getServiceProviders()
    {
        $providers = [];
        foreach ($this->getEnabledPackages() as $package) {
            $providers[] = $package['providers'];
        }

        $providers = collect($providers)->flatten()->unique()->toArray();

        return $providers;
    }

    public function enable($packageName)
    {
        return $this->setPackageStatus($packageName, PackageStatus::ENABLED);
    }

    public function disable($packageName)
    {
        return $this->setPackageStatus($packageName, PackageStatus::DISABLED);
    }

    public function getEnabledPackages()
    {
        return collect($this->packages)->filter(
            function ($package) {
                return $package['status'] == PackageStatus::ENABLED;
            }
        )->toArray();
    }

    protected function setPackageStatus($packageName, $status)
    {
        // Find package
        $package = collect($this->packages)->first(
            function ($item) use ($packageName) {
                return $item['name'] == $packageName;
            }
        );

        // enabled if it exists
        if ($package !== null) {
            $package['status'] = $status;

            $this->packages[$packageName] = $package;
        }

        $this->save();

        return $package;
    }
}
