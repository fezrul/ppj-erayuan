<?php

namespace Ekompaun\Platform;

use Ekompaun\Systemconfig\Enum\Permission;
use Illuminate\Support\ServiceProvider;
use Lavary\Menu\Builder;

class PlatformServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'platform');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'platform');

        if (!$this->app->runningInConsole()) {
            $this->bootServiceProviders();
        }

        // $this->bootMenu();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            'menu', function () {
                return \Menu::make(
                    'app', function (Builder $menu) {
                        return $menu;
                    }
                );
            }
        );

        $this->app->singleton(
            'platform.package', function () {
                return new PackageManager();
            }
        );

        include __DIR__ . '/../routes/web.php';
    }

    protected function bootServiceProviders()
    {
        $providers = with(new PackageManager())->getServiceProviders();

        foreach ($providers as $provider) {
            $this->app->register($provider);
        }
    }

    protected function bootMenu()
    {
        $menu  = app('menu')->add('Platform')
                            ->data('icon', 'fa fa-cube')
                            ->data('permission', Permission::MANAGE_SYSTEM);

        $menu->add('Packages', route('platform::package.index'))->data('icon', 'fa fa-puzzle-piece');
    }
}
