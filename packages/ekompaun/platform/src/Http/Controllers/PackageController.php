<?php

namespace Ekompaun\Platform\Http\Controllers;

use Illuminate\Routing\Controller;

class PackageController extends Controller
{
    public function index()
    {
        $packages = app('platform.package')->all();

        return view('platform::package.index', compact('packages'));
    }

    public function enable()
    {
        app('platform.package')->enable(request('package'));

        return redirect()->route('platform::package.index');
    }

    public function disable()
    {
        app('platform.package')->disable(request('package'));

        return redirect()->route('platform::package.index');
    }
}
