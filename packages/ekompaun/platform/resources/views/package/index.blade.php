@extends('layouts.app')
@section('content')
    <div class="my-5">
        @component("components.panel")
            @slot('title', 'Package Manager')

            <table class="table">
                <thead>
                <tr>
                    <th>Package</th>
                    <th>Status</th>
                </tr>
                </thead>
                @foreach($packages as $package)
                    <tr>
                        <td>
                            <h3>{{ $package['name'] }}</h3>
                            <p>{{ $package['description'] }}</p>
                        </td>
                        <td>
                            @if($package['status'] == \Ekompaun\Platform\Enum\PackageStatus::ENABLED)
                                <span class="badge badge-success">{{ $package['status'] }}</span>
                                <a class="btn btn-link" href="{{ route('platform::package.disable', ['package' => $package['name']]) }}">Click to disable</a>
                            @else
                                <span class="badge badge-secondary">{{ $package['status'] }}</span>
                                <a href="{{ route('platform::package.enable', ['package' => $package['name']]) }}">Click to enable</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </table>

        @endcomponent
    </div>
@endsection
