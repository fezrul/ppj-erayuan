<?php

return [
    'action'             => 'Action',
    'add'                => 'Add',
    'all'                => 'All',
    'back'               => 'Back',
    'cancel'             => 'Cancel',
    'choose_one'         => '--Choose One--',
    'delete'             => 'Delete',
    'edit'               => 'Edit',
    'export'             => 'Export',
    'print'              => 'Print',
    'reset'              => 'Reset',
    'save'               => 'Save',
    'search_button'      => 'Search',
    'search_placeholder' => 'Search for...',
    'submit'             => 'Submit',
    'show'               => 'Show Detail',
];
