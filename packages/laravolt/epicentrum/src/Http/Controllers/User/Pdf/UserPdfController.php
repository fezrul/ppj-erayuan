<?php

namespace Laravolt\Epicentrum\Http\Controllers\User\Pdf;

use PDF;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Laravolt\Epicentrum\Repositories\RepositoryInterface;

class UserPdfController extends Controller
{
    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        // $users = $this->repository->scopeQuery(function ($query) {
        //     return $query->orderBy('name');
        // })->skipPresenter()->get();

        $users = $this->repository->scopeQuery(function ($query) {
            return $query->pegawai()->orderBy('name');
             })->skipPresenter()->get();





        $pdf = PDF::loadview('epicentrum::pdf.user', compact('users'))
        ->setPaper('a4');
        return $pdf->download('Pengguna Dalaman.pdf');
    }
}
