<?php

namespace Laravolt\Epicentrum\Http\Controllers\Csv;

use Illuminate\Routing\Controller;
use Laravolt\Epicentrum\Repositories\RepositoryInterface;
use Laravolt\Epicentrum\Repositories\TimezoneRepository;
use Excel;

class CsvUserController extends Controller
{

    /**
     * @var UserRepositoryEloquent
     */
    protected $repository;

    /**
     * @var TimezoneRepositoryArray
     */
    protected $timezone;

    /**
     * UserController constructor.
     * @param UserRepositoryEloquent $repository
     * @param TimezoneRepositoryArray $timezone
     */
    public function __construct(RepositoryInterface $repository, TimezoneRepository $timezone)
    {
        $this->repository = $repository;
        $this->timezone = $timezone;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // $users = $this->repository->scopeQuery(function ($query) {
        //     return $query->orderBy('name');
        // })->skipPresenter()->get();

         $users = $this->repository->scopeQuery(function ($query) {
            return $query->pegawai()->orderBy('name');
             })->skipPresenter()->get();

        Excel::create('Users', function ($excel) use ($users) {
            $excel->sheet('New sheet', function ($sheet) use ($users) {
                $sheet->loadView('epicentrum::csv.csv-users', compact('users'));
            });
        })->export('csv');
    }
}
