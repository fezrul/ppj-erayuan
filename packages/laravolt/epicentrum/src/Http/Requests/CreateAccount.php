<?php

namespace Laravolt\Epicentrum\Http\Requests;

class CreateAccount extends BaseAccount
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'name'     => 'required|max:255',
            'email'    => 'required|email|unique:users',
            // 'status'   => 'required',
        ];

        if (!$this->ldapuser) {
            $rules['password'] = 'required|min:6|max:255';
        }

        return $rules;
    }
}
