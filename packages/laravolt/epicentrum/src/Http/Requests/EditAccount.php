<?php

namespace Laravolt\Epicentrum\Http\Requests;

use Illuminate\Validation\Rule;

class EditAccount extends BaseAccount
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request()->route('account');

        return [
            'name'     => 'required|max:255',
            'email'    => ['required', 'email', Rule::unique('users', 'email')->ignore($id)],
            // 'status'   => 'required',
            // 'timezone' => 'required',
        ];
    }
}
