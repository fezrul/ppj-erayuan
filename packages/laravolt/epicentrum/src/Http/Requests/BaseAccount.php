<?php

namespace Laravolt\Epicentrum\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BaseAccount extends FormRequest
{

    protected $ldapuser = false;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $suffix = '@ppj.gov.my';
        $suffixLdap = config('adldap.connections.default.connection_settings.admin_account_suffix');

        if (ends_with($this->email, $suffix)) {
            $ldapUsername = str_replace($suffix, $suffixLdap, $this->email);
            $this->merge(['ldap_username' => $ldapUsername, 'password' => null]);
            $this->ldapuser = true;
        }
    }


}
