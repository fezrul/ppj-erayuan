<?php

return [
    'name'                               => 'Nama',
    'email'                              => 'E-mel',
    'password'                           => 'Kata Laluan',
    'password_new'                       => 'Kata Laluan Baru',
    'password_confirmation'              => 'Pengesahan Kata Laluan',
    'status'                             => 'Status',
    'registered_at'                      => 'Didaftarkan',
    'bio'                                => 'Bio',
    'timezone'                           => 'Zon Waktu',
    'roles'                              => 'Peranan',
    'pages'                              => [
        'index' => [
            'header' => 'Urus Pengguna',
        ]
    ],
    'send_account_information_via_email' => 'Hantar maklumat akaun dan kata laluan melalui e-mel',
    'change_password_on_first_login'     => 'Tukar kata laluan ketika log masuk kali pertama',
    'delete_account'                     => 'Padam Akaun',
];
