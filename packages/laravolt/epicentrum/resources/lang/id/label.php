<?php
return [
    'roles'                    => 'Peranan',
    'permissions'              => 'Hak Akses',
    'users'                    => 'Pengguna',
    'add_role'                 => 'Tambah Peranan Baru',
    'edit_role'                => 'Kemas kini Peranan',
    'back_to_roles_index'      => 'Kembali ke daftar peranan',
    'delete_role'              => 'Hapus Peranan',
    'reset_password_manual'    => 'Manual',
    'reset_password_automatic' => 'Automatik',
];
