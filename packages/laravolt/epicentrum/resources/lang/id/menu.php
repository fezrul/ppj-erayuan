<?php
return [
    'epicentrum' => 'Pengurusan Pengguna',
    'account'    => 'Akaun',
    'password'   => 'Tukar Kata Laluan',
    'email'      => 'E-mel',
    'role'       => 'Peranan',
    'add_user'   => 'Tambah Pengguna',
];
