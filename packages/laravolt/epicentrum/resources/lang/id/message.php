<?php

return [
    'user_created'                       => 'Pengguna baru berjaya ditambah',
    'user_deleted'                       => 'Pengguna berjaya dipadam',
    'account_updated'                    => 'Data pengguna berjaya dikemaskini',
    'password_reset_sent'                => 'Link untuk reset kata laluan telah dihantar ke email pengguna',
    'password_changed_and_sent_to_email' => 'Kata laluan berjaya diubah dan telah dihantar ke email pengguna',
    'role_created'                       => 'Peranan telah berjaya ditambah',
    'role_updated'                       => 'Peranan telah berjaya dikemas kini',
    'role_deleted'                       => 'Peranan telah berjaya dipadam',
    'cannot_delete_yourself'             => 'Anda tidak boleh memadam akaun sendiri. Sila minta bantuan admin lain untuk memadam akaun anda.',
    'account_deletion_confirmation'      => 'Adakah anda pasti untuk memadamkan akaun ini?',
    'role_deletion_confirmation'         => 'Adakah anda pasti untuk memadamkan peranan ini?',
    'delete_role_intro'                  => 'Ada :count pengguna yang akan terlibat.',
    'reset_password_manual_intro'        => 'Pengguna akan mendapat email yang berisi link untuk reset kata laluan. Pengguna perlu mengisi sendiri kata laluan baru.',
    'reset_password_automatic_intro'     => 'Jana kata laluan baru, dan hantar kata laluan tersebut ke emel. Pengguna boleh terus log masuk menggunakan kata laluan baru tersebut.',
];
