<?php

return [
    'enabled' => env('MAILKEEPER_ENABLED', false),
    'take'    => 100,
];
