<?php

namespace Laravolt\Auth\Http\Controllers;

use Ekompaun\Auth\Services\LdapService;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide this functionality to your appliations.
    |
    */

    use ValidatesRequests;
    use AuthenticatesUsers {
        login as defaultLogin;
    }

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);

        $this->redirectTo = config('laravolt.auth.redirect.after_login');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('auth::login');
    }

    protected function validateLogin(Request $request)
    {
        $rules = [
            $this->username() => 'required',
            'password'        => 'required',
        ];

        if (config('laravolt.auth.captcha')) {
            $rules['g-recaptcha-response'] = 'required|captcha';
        }

        $this->validate($request, $rules);
    }

    protected function login(Request $request)
    {
        if (env('ADLDAP_ENABLED')) {
            try {
                return $this->ldapLogin($request);
            } catch (\Exception $e) {
                return $this->defaultLogin($request);
            }
        }

        return $this->defaultLogin($request);
    }

    protected function ldapLogin(Request $request)
    {
        $ldapService = new LdapService();

        $username = $request->get($this->username());
        $urn = $username.'@putrajaya.local';
        $password = $request->password;
        $remember = $request->filled('remember');

        $user = $ldapService->getUserViaLdap($urn, $password);
        auth()->login($user, $remember);
        return redirect()->to($this->redirectPath());
    }
}
