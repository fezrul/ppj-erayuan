<?php

if (!function_exists('numbering')) {
    /**
     * Generate number sequence (1,2,3..) for pagination
     */
    function numbering($item, $pagination)
    {
        $collections = collect($pagination->items());
        $index = $collections->search($item) + 1;
        $start = (request('page', 1) - 1) * $pagination->perPage();

        return $start + $index;
    }
}

if (!function_exists('attributes')) {
    /**
     * Generate attr=value pair for HTML tag
     */
    function attributes($attributes)
    {
        $result = '';
        foreach ($attributes as $attribute => $value) {
            $result .= " {$attribute}=\"{$value}\"";
        }

        return $result;
    }
}

if (!function_exists('setting_fallback')) {
    /**
     * Generate attr=value pair for HTML tag
     */
    function setting_fallback($key, $default = null)
    {
        return setting($key, config($key, $default));
    }
}

if (!function_exists('format_date')) {
    function format_date($date, $fromFormat = 'Y-m-d H:i:s', $toFormat = 'j M Y')
    {
        try {
            $date = Carbon\Carbon::createFromFormat($fromFormat, $date);

            return $date->format($toFormat);
        } catch (Exception $e) {
            return $date;
        }
    }
}

if (!function_exists('format_time')) {
    function format_time($date)
    {
        try {
            return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('H:i:s A');
        } catch (Exception $e) {
            return $date;
        }
    }
}

if (!function_exists('format_money')) {
    function format_money($amount)
    {
        return number_format($amount, 2);
    }
}
