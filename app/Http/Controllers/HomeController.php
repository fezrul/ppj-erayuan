<?php

namespace App\Http\Controllers;

use Ekompaun\Systemconfig\Model\CampaignMaster as Master;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Appeal\Model\Transaction;
use Ekompaun\Systemconfig\Enum\StatusKompaun;
use DB;
use App\User;
use Ekompaun\Systemconfig\Model\Holiday;
use Auth;

class HomeController extends Controller
{
    public function index()
    {
        $campaigns = Master::active()->get();

        $user = Auth::user()->roles;
        $uid = $user[0]->pivot->user_id;
        $roles = $user[0]->pivot->role_id;
        $payment=DB::SELECT("SELECT COUNT(*) AS AGGREGATE FROM `ek_application` WHERE `ek_application`.`created_by` = '$uid' AND `ek_application`.`created_by` IS NOT NULL AND `appl_status` = 12");
        $compoundlist = DB::SELECT("SELECT COUNT(*) AS AGGREGATE FROM `ek_transaction` WHERE `fk_userid` = '$uid'");
        $noofcompound = $compoundlist[0]->AGGREGATE;
        $noofresult = Auth::user()->UserApp()->typeOfRayuanKedua()->with('transaction')->where('appl_status','!=',12)->count();
        $noofpayment = $payment[0]->AGGREGATE;
                       
        // $applications = Application::with('transaction', 'status')->assignedTo($uid)->count();
        // $assignment = Application::byStatus(StatusKompaun::MENUNGGU_KELULUSAN)->count();
         $assignment = Transaction::whereHas('lastApplication', function ($query) {
            $query->where('appl_status', '=', StatusKompaun::MENUNGGU_KELULUSAN);
        })->count();
        $applications = Application::whereIn('appl_status',[1,10])->with('transaction', 'status','latestdetail')->assignedTo($uid)->count();

        $assignee = Transaction::whereHas('lastApplication', function ($query) {
            $query->whereIn('appl_status', [StatusKompaun::ASSIGNMENT, StatusKompaun::DOKUMEN, StatusKompaun::DOKUMEN_LENGKAP]);
        })->count();

        $campaing = DB::SELECT("SELECT COUNT(*) as count FROM ek_campaign_master where camp_status = 1");
        $camcount = $campaing[0]->count;
        $customer = User::customer()->count();
        $holidays = Holiday::byYear(date("Y"))->count();
        $schedule = DB::SELECT("SELECT COUNT(*) as count FROM ek_schedule_rate where schd_status = 1");
        $schdcount = $schedule[0]->count;

        $internal = User::pegawai()->count();

         // dd($noofresult);
   
        return view('home.index', compact('campaigns', 'noofresult', 'noofcompound', 'noofpayment','applications','assignment','assignee','roles','camcount','customer','holidays','schdcount','internal'));
    }

    public function show($campId)
    {
        $campaign = Master::find($campId);
        return view('home.show', compact('campaign'));
    }
}
