<?php

namespace App\Http\Controllers;

use Ekompaun\Appeal\Model\Transaction;
use Illuminate\Database\Query\paginate;
use Ekompaun\Sap\Http\Controllers\SapController;
use Config;
use DB;

class SemakanKompaunController extends Controller
{

    public function __construct(SapController $sap)
    {
        
        $this->sap = $sap;
        
    }

    

    public function index()
    {
        //fezrul recommit
        $types = request('type');
        $inputs = request('search');
        $return = '';

        $sap = Config::get('ppj.sap');
        
        if($sap == 1) {

            if(($types == 1) OR ($types == 3) OR ($types == 4) OR ($types == 6) OR ($types == 7)) {
                  $return = $this->sap->getFunction('1', '2', $inputs, $inputs);

            }elseif($types == 5) {

                  $return = $this->sap->getFunction('1', '3', $inputs, $inputs);

            }else{

                 $return = $this->sap->getFunction('1', '4', $inputs, $inputs);
            }
        }


        $transactionsQuery = Transaction::byKeywordnoUser(request('search'))
        ->with('offence', 'statusSap', 'lastApplication.status');


        // $transactions = Transaction::with('lastApplication.status')->with('offence')
        
       // ->where(DB::raw("tran_id not in (select appl_transid from ek_application) and deleted_date"))
        // ->byKeyword(request('search'))
        // ->paginate();

          $transactions = $transactionsQuery->paginate()->appends(['type'=>request('type'),'search'=>request('search')]);
        // $transactions->appends(['type'=>request('type'), 'search'=>request('search')]);

        return view('semakan-kompaun.index', compact('transactions'));
    }
}
