<?php

namespace App\Mail;

use Ekompaun\Systemconfig\Model\ScheduleRate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailRayuanIkutJadwal extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $application;

    public function __construct($application)
    {
        $this->application = $application;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $amaunKadarRayuan = app(ScheduleRate::class)->getAmaunKadarRayuan($this->application->transaction);

        return $this->markdown('mail.rayuan-ikut-jadwal')
            ->with(
                [
                'data' => $this->application,
                'amaunKadarRayuan' => $amaunKadarRayuan
                ]
            )
            ->subject(
                'Makluman Amaun Rayuan Mengikut Jadual Untuk No Kompaun '
                .$this->application->transaction->tran_compoundno
            );
    }
}
