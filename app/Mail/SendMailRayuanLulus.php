<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailRayuanLulus extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $application;

    public function __construct($application)
    {
        $this->application = $application;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.rayuan-lulus')
            ->with(
                [
                'data' => $this->application,
                ]
            )
            ->subject(
                'Makluman Status Permohonan Rayuan Untuk No Kompaun '
                .$this->application->transaction->tran_compoundno
            );
    }
}
