<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailDokumenTambahan extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $application;

    public function __construct($application)
    {
        $this->application = $application;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.rayuan-dokumen-tambahan')
            ->with(
                [
                'data' => $this->application,
                ]
            )
            ->subject(
                'Makluman Dokumen Tambahan Permohonan Rayuan Untuk No Kompaun '
                .$this->application->transaction->tran_compoundno
            );
    }
}
