<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailDokumenLengkap extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $application;

    public function __construct($application)
    {
        $this->application = $application;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.rayuan-dokumen-lengkap')
            ->with(
                [
                'application' => $this->application,
                ]
            )
            ->subject(
                'Makluman Dokumen Tambahan Lengkap No. Kompaun '
                .$this->application->transaction->tran_compoundno
            );
    }
}
