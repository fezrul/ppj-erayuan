<?php

namespace App\Listeners;

use App\Events\Application\DokumenLengkap;
use Ekompaun\Systemconfig\Services\MailService;

class ApplicationEmailEventSubscriber
{
    protected $service;

    public function __construct(MailService $service)
    {
        $this->service = $service;
    }

    public function onSubmitRayuanIkutJadwal($event)
    {
        $this->service->mailSubmitRayuanIkutJadwal($event->applId);
    }

    public function onSubmitRayuanKedua($event)
    {
        $this->service->mailSubmitRayuanKedua($event->applId);
    }

    public function onApplicationStatusChanged($event)
    {
        $this->service->mailApplicationStatusChange($event->applId);
    }

    public function onRayuanAssigned($event)
    {
        $this->service->mailApplicationAssigned($event->applId);
    }

    public function onApplicationIdle($event)
    {
        $this->service->mailApplicationIdle($event->applId);
    }

    public function onSendMailApplicationReverted($event)
    {
        $this->service->mailApplicationReverted($event->applId);
    }

    public function onDokumenLengkap($event)
    {
        $this->service->mailDokumenLengkap($event->application);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {

        $events->listen(
            'App\Events\Mail\SendMailMengikutRayuanEvent',
            'App\Listeners\ApplicationEmailEventSubscriber@onSubmitRayuanIkutJadwal'
        );

        $events->listen(
            'App\Events\Mail\SendMailRayuanKeduaEvent',
            'App\Listeners\ApplicationEmailEventSubscriber@onSubmitRayuanKedua'
        );

        $events->listen(
            'App\Events\Mail\SendMailApplicationStatusChangeEvent',
            'App\Listeners\ApplicationEmailEventSubscriber@onApplicationStatusChanged'
        );

        $events->listen(
            'App\Events\Mail\SendMailApplicationRevertEvent',
            'App\Listeners\ApplicationEmailEventSubscriber@onSendMailApplicationReverted'
        );

        $events->listen(
            'App\Events\Mail\SendMailTindakanEvent',
            'App\Listeners\ApplicationEmailEventSubscriber@onApplicationIdle'
        );

        $events->listen(
            'App\Events\Mail\SendMailAssignmentEvent',
            'App\Listeners\ApplicationEmailEventSubscriber@onRayuanAssigned'
        );

        $events->listen(
            DokumenLengkap::class,
            'App\Listeners\ApplicationEmailEventSubscriber@onDokumenLengkap'
        );
    }
}
