<?php

namespace App\Listeners;

use Ekompaun\Systemconfig\Services\PaymentEventService;

class PaymentEventSubscriber
{
    protected $service;

    public function __construct(PaymentEventService $service)
    {
        $this->service = $service;
    }

    public function onPayLog($event)
    {
        $this->service->addpaylog($event);
    }

   

    /**
     * Register the listeners for the subscriber.
     *
     * @param Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\Payment\PaymentLogEvent',
            'App\Listeners\PaymentEventSubscriber@onPayLog'
        );

       
    }
}
