<?php

namespace App\Listeners;

use Ekompaun\Systemconfig\Services\ApplicationEventService;

class ApplicationEventSubscriber
{
    protected $service;

    public function __construct(ApplicationEventService $service)
    {
        $this->service = $service;
    }

    public function onApplicationViewed($event)
    {
        $this->service->applicationViewed($event->user, $event->applId);
    }

    public function onApplicationSubmitted($event)
    {
        $this->service->applicationSubmit($event->user, $event->applId, $event->task);
    }

    public function onApplicationAssigned($event)
    {
        $this->service->applicationAssign($event->user, $event->applId);
    }

    public function onApplicationStatusChanged($event)
    {
        $this->service->applicationStatusChange($event->user, $event->applId);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\Application\ApplicationViewEvent',
            'App\Listeners\ApplicationEventSubscriber@onApplicationViewed'
        );

        $events->listen(
            'App\Events\Application\ApplicationSubmitEvent',
            'App\Listeners\ApplicationEventSubscriber@onApplicationSubmitted'
        );

        $events->listen(
            'App\Events\Application\ApplicationAssignEvent',
            'App\Listeners\ApplicationEventSubscriber@onApplicationAssigned'
        );

        $events->listen(
            'App\Events\Application\ApplicationStatusChangeEvent',
            'App\Listeners\ApplicationEventSubscriber@onApplicationStatusChanged'
        );
    }
}
