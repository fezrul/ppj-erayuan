<?php

namespace App\Events\Mail;

use Illuminate\Queue\SerializesModels;

class SendMailMengikutRayuanEvent
{
    use SerializesModels;

    //application_id
    public $applId;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    public function __construct($applId)
    {
        $this->applId   = $applId;
    }
}
