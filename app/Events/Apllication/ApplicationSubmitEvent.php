<?php

namespace App\Events\Application;

use App\User;
use Illuminate\Queue\SerializesModels;

class ApplicationSubmitEvent
{
    use SerializesModels;

    //user
    public $user;

    //application_id
    public $applId;

    //task
    public $task;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    public function __construct(User $user, $applId, $task)
    {
        $this->task     = $task;
        $this->user     = $user;
        $this->applId   = $applId;
    }
}
