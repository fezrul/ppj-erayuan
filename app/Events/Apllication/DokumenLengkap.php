<?php

namespace App\Events\Application;

use App\User;
use Ekompaun\Appeal\Model\Application;
use Illuminate\Queue\SerializesModels;

class DokumenLengkap
{
    use SerializesModels;

    public $application;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    public function __construct(Application $application)
    {
        $this->application   = $application;
    }
}
