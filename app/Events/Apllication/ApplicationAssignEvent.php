<?php

namespace App\Events\Application;

use App\User;
use Illuminate\Queue\SerializesModels;

class ApplicationAssignEvent
{
    use SerializesModels;

    //user
    public $user;

    //application_id
    public $applId;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    public function __construct(User $user, $applId)
    {
        $this->user     = $user;
        $this->applId   = $applId;
    }
}
