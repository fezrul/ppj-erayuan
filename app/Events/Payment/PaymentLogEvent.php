<?php

namespace App\Events\Payment;

use App\User;
use Illuminate\Queue\SerializesModels;

class PaymentLogEvent
{
    use SerializesModels;

    public $paylog;

    /**
     * Create a new event instance.
     *
     * @param  Bid $bid
     * @return void
     */
    public function __construct($paylog)
    {
        $this->paylog = $paylog;
    }
}
