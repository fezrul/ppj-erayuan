<?php

namespace App\Classifiers;

use Wnx\LaravelStats\ReflectionClass;
use Wnx\LaravelStats\Contracts\Classifier;

class RepositoryClassifier implements Classifier
{
    public function getName()
    {
        return 'Repository';
    }

    public function satisfies(ReflectionClass $class)
    {
        return ends_with($class->getShortName(), 'Repository');
    }
}
