<?php

namespace App\Providers;

use Ekompaun\Appeal\Model\Application;
use Ekompaun\Appeal\Model\Transaction;
use Ekompaun\Appeal\Policies\AppealPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Application::class => AppealPolicy::class,
        Transaction::class => AppealPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
