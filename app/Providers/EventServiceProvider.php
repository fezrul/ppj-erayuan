<?php

namespace App\Providers;

use Ekompaun\Systemconfig\Events\UserRegistered;
use Ekompaun\Systemconfig\Listeners\SendEmailAfterUserRegistered;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        // UserRegistered::class => [
        //     SendEmailAfterUserRegistered::class,
        // ]
    ];

    protected $subscribe = [
        'App\Listeners\ApplicationEventSubscriber',
        'App\Listeners\ApplicationEmailEventSubscriber',
        'App\Listeners\PaymentEventSubscriber',
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
