<?php

namespace App\Providers;

use Carbon\Carbon;
use Ekompaun\Systemconfig\Enum\Permission;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;
use Spatie\Html\Elements\Option;
use Spatie\Html\Html;
use Illuminate\Database\Eloquent\Relations\Relation;
use Themsaid\LangmanGUI\Manager;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Schema::defaultStringLength(191);

        Relation::morphMap(
            [
            'user'                  => 'App\User',
            'userProfile'           => 'Ekompaun\Systemconfig\Model\UserProfile',
            'application'           => 'Ekompaun\Appeal\Model\Application',
            'campaignMaster'        => 'Ekompaun\Systemconfig\Model\CampaignMaster',
            'holiday'               => 'Ekompaun\Systemconfig\Model\Holiday',
            ]
        );

        app('menu')
            ->add('Home', url('home'))
            ->data('icon', 'fa fa-home');

        if ($this->app->bound('laravolt.acl')) {
            $this->app['laravolt.acl']->registerPermission(Permission::values());
        }

        Carbon::macro('diffInDaysWithoutWeekendAndHolidays', function($finish, $holidays){
            $day = $this->addDay();
            $days = 0;
            while ($day < $finish) {
                $day = $day->addDay();

                if (in_array($day->format('d-m-Y'), $holidays)) {
                    continue;
                }

                if ($day->isWeekend()) {
                    continue;
                }

                $days++;
            }

            return $days;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Html::macro(
            'formGroup', function ($label, $control) {
                $label = html()->label($label)->addClass('col-sm-4 col-form-label');
                $divControl = html()->div($control->addClass('form-control'))->addClass('col-sm-8');

                return html()
                ->div()
                ->addClass('form-group row')
                ->addChildren([$label, $divControl]);
            }
        );

        Html::macro(
            'radioGroup', function ($name, $options, $value = null) {
                $div = html()->div()->class('form-check');
                $html = '';
                foreach ($options as $key => $text) {
                    $checked = old($name, $value) == $key;

                    $radio = html()->radio($name, false, $key)
                    ->class('form-check-input')
                    ->forgetAttribute('checked')
                    ->forgetAttribute('id')
                    ->forgetAttribute('value')
                    ->value($key)
                    ->attributeIf($checked, 'checked');

                    $label = html()
                    ->label($text)
                    ->class('form-check-label')
                    ->prependChildren($radio);
                    $html .= $label->toHtml() . str_repeat('&nbsp;', 5);
                }

                return $div->addChildren($html);
            }
        );

        Html::macro(
            'selectKesalahan', function ($name, $kesalahan, $selectedValue) {
                $select = html()
                    ->select($name, [], $selectedValue)
                    ->addChildren($kesalahan, function ($item) use ($selectedValue) {
                    return Option::create()
                                 ->value($item->offence_id)
                                 ->text($item->offence_name)
                                 ->data('act', $item->fk_lkp_actid)
                                 ->selectedIf($item->offence_id === $selectedValue);
                });

                return $select;
            }
        );

        Collection::macro(
            'toAssoc', function () {
                return $this->reduce(
                    function ($assoc, $keyValuePair) {
                        list($key, $value) = $keyValuePair;
                        $assoc[$key] = $value;
                        return $assoc;
                    }, new static
                );
            }
        );

        Collection::macro(
            'mapToAssoc', function ($callback) {
                return $this->map($callback)->toAssoc();
            }
        );

        $this->app->singleton(
            Manager::class, function () {
                return new Manager(
                    new Filesystem(),
                    $this->app['path.lang'],
                    [$this->app['path.resources'], $this->app['path'], base_path('packages/ekompaun/*/resources')]
                );
            }
        );
    }
}
