<?php

namespace App;

use Carbon\Carbon;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Appeal\Model\Transaction;
use Ekompaun\Systemconfig\Enum\UserType;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;
use Ekompaun\Systemconfig\Enum\Citizenship;
use Ekompaun\Systemconfig\Enum\UserCategory;
use Ekompaun\Systemconfig\Model\UserProfile;
use Laravolt\Epicentrum\Models\User as LaravoltUser;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use DB;

class User extends LaravoltUser implements AuditableContract
{
    use Notifiable, Auditable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'status', 'timezone', 'type', 'icno', 'ldap_username'];

    protected $casts = [
        'ldap_information'  => 'array',
    ];

    /**
     * Auditable attributes excluded from the Audit.
     *
     * @var array
     */
    protected $auditExclude = ['remember_token'];

    public function profile()
    {
        return $this
            ->hasOne(UserProfile::class, 'fk_users')
            ->withDefault(
                function ($profile) {
                    $profile->user_category = UserCategory::INDIVIDUAL;
                    $profile->user_citizenship = Citizenship::RESIDENT;
                }
            );
    }

    public function scopeByRolesName(Builder $query, $name)
    {
        if ($name) {
            return $query->whereHas(
                'roles', function ($q) use ($name) {
                    return $q->whereName($name);
                }
            );
        }
    }

    public function scopeCustomer(Builder $query)
    {
        return $query->whereHas(
            'roles', function ($q) {
                return $q->whereName('Pelanggan');
            }
        );
    }

    public function scopePegawaiJu(Builder $query)
    {
        return $query->whereHas(
            'roles', function ($q) {
                return $q->whereIn('name', ['Ketua Pegawai J(U)', 'Pegawai J(U)']);
            }
        );
    }

    public function scopePegawaiJuCount(Builder $query)
    {
        return $this
         ->select(DB::raw("CONCAT(name,' - (',(SELECT COUNT(*) FROM ek_application WHERE `appl_personincharge` = id), ')') AS 'name'"),'id')
         ->whereHas(
            'roles', function ($q) {
                return $q->whereIn('name', ['Ketua Pegawai J(U)', 'Pegawai J(U)']);
            }
        );
    }

    public function scopePegawai(Builder $query)
    {
        return $query->whereType(UserType::STAF);
    }

    public function scopeSearch(Builder $query, $keyword = null)
    {
        if ($keyword) {
            $query
                ->where('name', 'like', "%$keyword%")
                ->orWhere('email', 'like', "%$keyword%");
        }
    }

    public function transaction()
    {
        return $this->hasMany(Transaction::class, 'fk_userid');
    }

    public function application()
    {
        return $this->hasManyThrough(Application::class, Transaction::class, 'fk_userid', 'appl_transid');
    }

    public function userApplication()
    {
        return $this->hasMany(Application::class, 'created_by')->orderByDesc('appl_id');
    }

        public function scopeUserApp()
    {
        return $this->hasMany(Application::class, 'created_by')->orderByDesc('appl_id');
    }
}
