<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Laravolt\Acl\Models\Role;

class SyncRoleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:sync-role';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync roles';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $roles = config('roles');
        $result = collect();

        foreach ($roles as $roleName => $permissions) {
            $role = Role::firstOrNew(['name' => $roleName], ['name' => $roleName]);
            $status = 'Exists';

            if (!$role->exists) {
                $role->save();
                $status = 'New';
            }
            $result->push(['id' => $role->getKey(), 'name' => $roleName, 'status' => $status]);
            $role->syncPermission($permissions);
        }

        $this->table(['ID', 'Name', 'Status'], $result);
    }
}
