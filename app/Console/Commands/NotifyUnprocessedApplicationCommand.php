<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Ekompaun\Appeal\Repositories\ApplicationRepository;

class NotifyUnprocessedApplicationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:notify-application {--days=3}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notifikasi tindakan rayuan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(ApplicationRepository $application)
    {
        if (is_numeric($this->option('days'))) {
            $application->assignedButIdle($this->option('days'));
        }
        $this->error('--days must be numeric');
    }
}
