<?php

namespace App\Console\Commands;

use Ekompaun\Appeal\Model\Application;
use Ekompaun\Systemconfig\Enum\StatusKompaun;
use Illuminate\Console\Command;

class DeleteExpiredApplicationWithDiscountedPrice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:delete-expired-application';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete application with discountend price (coz of campaign) but have not paid when the campaign ends';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $applications = Application::expiredCampaign()->byStatus(StatusKompaun::PENDING_PAYMENT)->get();
        $counter = 0;
        foreach ($applications as $app) {
            if ($app->campaign->is_active) {
                $app->batalCampaign();
                $counter++;
            }
        }

        $this->info('Application deleted: '.$counter);
    }
}
