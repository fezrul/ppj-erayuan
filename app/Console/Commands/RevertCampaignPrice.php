<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use DB;
use Auth;
use Config;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Ekompaun\Sap\Http\Controllers\SapController;



class RevertCampaignPrice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:revert-camp-price';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Campaign Expired Application reverted to default';

    /**
     * Create a new command instance.
     *
     * @return void
     */

     public function __construct(SapController $sap)
    {

        $this->sap = $sap;  
        parent::__construct();

    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $current = (date('Y-m-d H:i:s'));
        $list = DB::SELECT("SELECT * from vw_app_kempen where kempen_end < '$current'");

        if ($list) {

            foreach ($list as $key => $value) 
            {

                $id = $value->id;
                $compno = $value->nokompaun;
                $price = $value->harga;

                $return = $this->sap->getFunction('2', '3', $compno, $price);
                if ($return == 1)
                {
                    $update = DB::INSERT("UPDATE ek_application SET appl_status = 8,appl_revert_indi = 1,modify_date = '$current' where appl_id = $id ");
                }


                
            }


        }

        
    }
}
