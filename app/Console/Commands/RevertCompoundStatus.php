<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use DB;
use Auth;
use Config;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Ekompaun\Sap\Http\Controllers\SapController;



class RevertCompoundStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:revert-comp-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Compound Expired Application reverted to default status';

    /**
     * Create a new command instance.
     *
     * @return void
     */

     public function __construct(SapController $sap)
    {

        $this->sap = $sap;  
        parent::__construct();

    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $current = (date('Y-m-d H:i:s'));
        $list = DB::SELECT("SELECT * from vw_app_revert where updated < '$current'");

        if ($list) {

            foreach ($list as $key => $value) 
            {
                $sapprice = 0;
                $applid = $value->id;

                if($value->type == 4){$sapprice  = $value->harga_asal;}
                else{$sapprice  = $value->amount;}

                $id        = $value->id;
                $compno    = $value->nokompaun;
                $camprice  = $value->harga;
                $sapstatus = $value->sap_status_code;


                $return = $this->sap->getRevert($compno, $camprice,$sapprice,$sapstatus);
                if ($return == 1)
                {
                    $update = DB::UPDATE("UPDATE ek_application SET appl_revert_indi = 2 where appl_id = $applid");
                }

            }


        }

        
    }
}
