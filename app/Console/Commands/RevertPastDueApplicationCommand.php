<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Ekompaun\Appeal\Services\ApplicationFlowService;

class RevertPastDueApplicationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:revert-application';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tempo pembayaran rayuan telah melewati jatuh tempo (lebih dari 14 hari)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(ApplicationFlowService $application)
    {
        $application->revertAmount();
    }
}
