<?php

namespace App\Console\Commands;

use Ekompaun\Auth\Services\LdapService;
use Illuminate\Console\Command;

class LdapLogin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ldap:login {username} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test login via LDAP';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(LdapService $ldapService)
    {
        // LDAP variables
        $ldaphost = env('ADLDAP_CONTROLLERS');
        $ldapport = env('ADLDAP_PORT');

        // Connecting to LDAP
        $ldapconn = ldap_connect($ldaphost, $ldapport) or die("Could not connect to $ldaphost");
        // dump('LDAP connect to host successful');

        // binding to ldap server
        $ldapbind = ldap_bind($ldapconn, env('ADLDAP_ADMIN_USERNAME').env('ADLDAP_ADMIN_ACCOUNT_SUFFIX'), env('ADLDAP_ADMIN_PASSWORD'));

        // verify binding
        if ($ldapbind) {
            // dump("LDAP bind successful");
        } else {
            // dump("LDAP bind failed");
        }

        $username = $this->argument('username');
        $password = $this->argument('password');

        try {
            $user = $ldapService->getUserViaLdap($username, $password);
            // dd($user);
        } catch (\Exception $e) {
            $this->error(get_class($e).":".$e->getMessage());
        }
    }
}
