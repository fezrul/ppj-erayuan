<?php

namespace App\Console\Commands;

use Ekompaun\Appeal\Services\ApplicationFlowService;
use Ekompaun\Systemconfig\Model\AssignmentSetting;
use Illuminate\Console\Command;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Systemconfig\Enum\StatusKompaun;
use Laravolt\Acl\Models\Role;

class AutoAssignRayuanCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:auto-assign';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $service;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ApplicationFlowService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $this->autoAssignLevel1();
        $this->autoAssignLevel2();
        $this->autoAssignLevel3();
    }

    protected function autoAssignLevel1()
    {
        $duration = config('site.auto_assign.duration_1');
        $applications = Application::query()
                                   ->where('appl_status', StatusKompaun::MENUNGGU_KELULUSAN)
                                   ->whereRaw("DATEDIFF(CURDATE(), appl_date) >=  $duration")
                                   ->get();

        $assignee = optional(AssignmentSetting::find(1))->user;
        if(!$assignee) {
            throw new \Exception('Petugas auto assign pertama tidak ditemukan');
        }

        $this->info(sprintf("Auto assign pertama: %d applications to %s", $applications->count(), $assignee->name));

        foreach ($applications as $application) {
            $this->service->autoAssign($application, $assignee->getKey());
        }
    }

    protected function autoAssignLevel2()
    {
        $duration1 = config('site.auto_assign.duration_1');
        $duration2 = config('site.auto_assign.duration_2');
        $duration = $duration2 - $duration1;

        if ($duration <= 0) {
            throw new \Exception('Duration untuk auto assign kedua harus lebih besar dari duration auto assign pertama');
        }

        $applications = Application::query()
                                   ->has('autoAssignedDetails', '=', 1)
                                   ->where('appl_status', StatusKompaun::ASSIGNMENT)
                                   ->whereRaw("DATEDIFF(CURDATE(), appl_assigndate) >=  $duration")
                                   ->get();

        $assignee = optional(AssignmentSetting::find(2))->user;
        if(!$assignee) {
            throw new \Exception('Petugas auto assign kedua tidak ditemukan');
        }

        $this->info(sprintf("Auto assign kedua: %d applications to %s", $applications->count(), $assignee->name));

        foreach ($applications as $application) {
            $this->service->autoAssign($application, $assignee->getKey());
        }
    }

    protected function autoAssignLevel3()
    {
        $duration2 = config('site.auto_assign.duration_2');
        $duration3 = config('site.auto_assign.duration_3');
        $duration = $duration3 - $duration2;

        if ($duration <= 0) {
            throw new \Exception('Duration untuk auto assign ketiga harus lebih besar dari duration auto assign kedua');
        }

        $applications = Application::query()
                                   ->has('autoAssignedDetails', '=', 2)
                                   ->where('appl_status', StatusKompaun::ASSIGNMENT)
                                   ->whereRaw("DATEDIFF(CURDATE(), appl_assigndate) >=  $duration")
                                   ->get();

        $assignee = $this->getDefaultAssignee();
        if(!$assignee) {
            throw new \Exception('Petugas auto assign ketiga [Ketua Pegawai J(U)] tidak ditemukan');
        }

        $this->info(sprintf("Auto assign ketiga: %d applications to %s", $applications->count(), $assignee->name));

        foreach ($applications as $application) {
            $this->service->autoAssign($application, $assignee->getKey());
        }
    }

    protected function getDefaultAssignee()
    {
        $role = Role::find(config('site.fallback_assignee_role_id'));
        if ($role) {
            return $role->users->first();
        }

        return null;
    }

}
