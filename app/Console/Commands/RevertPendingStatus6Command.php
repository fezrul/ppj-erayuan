<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Ekompaun\Appeal\Services\ApplicationFlowService;

class RevertPendingStatus6Command extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:revert-status6';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tempoh 3 hari status mohon dokomen tambahan tak berubah (Pembatalan Automatik)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(ApplicationFlowService $application)
    {
        $application->revertStatus6();
    }
}
