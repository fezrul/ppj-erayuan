<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use DB;
use Auth;
use Config;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Ekompaun\Sap\Http\Controllers\SapController;



class UpdateAmountManual extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule:update-amount-manual';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update SAP status after payment reciept is trigger';

    /**
     * Create a new command instance.
     *
     * @return void
     */

     public function __construct(SapController $sap)
    {

        $this->sap = $sap;  
        parent::__construct();

    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $current = (date('Y-m-d H:i:s'));
        $list = DB::SELECT("SELECT * from vw_app_amount_manual");

        if ($list) {

            foreach ($list as $key => $value) 
            {
                $sapprice = 0;
                $applid = $value->id;
                $type = $value->type;

                $id        = $value->id;
                $compno    = $value->nokompaun;
                $camprice  = $value->amount;
                $comprice  = $value->comprice;
                $sapstatus = $value->sap_status_code;


                if($type == 1){$return = $this->sap->getFunction('2', '1', $compno, $camprice);}
                if($type == 2){$return = $this->sap->getFunction('2', '1', $compno, $camprice);}
                if($type == 4){$return = $this->sap->getFunction('2', '3', $compno, $comprice);}
                if ($return == 1)
                {
                    $update = DB::UPDATE("UPDATE ek_application SET appl_reciept_indi = 2,appl_revert_indi = 99 where appl_id = $applid");
                }

            }
        }

        
    }
}
