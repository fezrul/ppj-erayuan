<?php

namespace App\Console;

// use App\Console\Commands\DeleteExpiredApplicationWithDiscountedPrice;
use App\Console\Commands\NotifyUnprocessedApplicationCommand;
use App\Console\Commands\RevertPastDueApplicationCommand;
use App\Console\Commands\SmsSend;
use App\Console\Commands\RevertCampaignPrice;
use App\Console\Commands\RevertCompoundStatus;
use App\Console\Commands\UpdateSapAfterPayment;
use Illuminate\Console\Scheduling\Schedule;
use App\Console\Commands\AutoAssignRayuanCommand;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Laravolt\Mailkeeper\SendMailCommand;


class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [

        'App\Console\Commands\RevertPastDueApplicationCommand',
        'App\Console\Commands\RevertPendingStatus6Command',
        'App\Console\Commands\NotifyUnprocessedApplicationCommand',
        'App\Console\Commands\AutoAssignRayuanCommand',
        'App\Console\Commands\SmsSend',
        'App\Console\Commands\RevertCampaignPrice',
        'App\Console\Commands\RevertCompoundStatus',
        'App\Console\Commands\UpdateSapAfterPayment',
        // 'App\Console\Commands\DeleteExpiredApplicationWithDiscountedPrice',

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command(SendMailCommand::class)->everyMinute();
        $schedule->command(SmsSend::class)->cron('*/2 * * * *');
        $schedule->command(AutoAssignRayuanCommand::class)->everyMinute();
        $schedule->command(NotifyUnprocessedApplicationCommand::class)->daily();
        $schedule->command(RevertPastDueApplicationCommand::class)->daily();
        $schedule->command(RevertPendingStatus6Command::class)->daily();
        $schedule->command(RevertCampaignPrice::class)->everyMinute();
        $schedule->command(RevertCompoundStatus::class)->everyMinute();
        $schedule->command(UpdateSapAfterPayment::class)->everyMinute();
        // $schedule->command(DeleteExpiredApplicationWithDiscountedPrice::class)->everyThirtyMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        include base_path('routes/console.php');
    }
}
