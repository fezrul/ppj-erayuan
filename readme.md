# E-KOMPAUN
Sistem e-Rayuan Kompaun untuk Perbadanan Putrajaya

## Technical Requirements
* Having a strong knowledge of Laravel
* Having a strong knowledge of package development (https://laravel.com/docs/5.5/packages) 
* Having a strong knowledge of OOP
* Having a good knowledge of popular design patterns
* Having a good knowledge of PHP PSR-2 (http://www.php-fig.org/psr/psr-2/) and PHP code sniffer (https://github.com/squizlabs/PHP_CodeSniffer)

## Technology Stack
* Laravel 5.5
* MariaDB 10.2
* Bootstrap 4

## Server Requirements
* PHP >= 7.0.0
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension

## Installation
* Clone this repository
* Copy `.env.example` to `.env`
* Run `php artisan key:generate`
* Run `php composer.phar install`
* Run `php artisan migrate --seed` or `php artisan migrate:fresh --seed` to cleanup database and repopulate sample data
* Run `php artisan laravolt:acl:sync-permission`
* Run `php artisan storage:link` (just once)

## Playing with Docker
### Development
* Run `docker-compose -f docker/docker-compose.yml up`
* Run `docker-compose -f docker/docker-compose.yml run --rm php-fpm php artisan migrate:fresh --seed`
* Run `docker-compose -f docker/docker-compose.yml run --rm php-fpm php artisan laravolt:acl:sync-permission`
* Browse `http://localhost:8000`
* If done, destroy with `docker-compose -f docker/docker-compose.yml down --rmi local`

### Testing/Staging/Production

#### Build Images
* Run `docker build -t docker.javan.co.id/ekompaun/php-fpm:latest -f docker/php-fpm/Dockerfile.production .`
* Run `docker build -t docker.javan.co.id/ekompaun/nginx:latest -f docker/nginx/Dockerfile.production .`
* Run `docker push docker.javan.co.id/ekompaun/php-fpm`
* Run `docker push docker.javan.co.id/ekompaun/nginx`

#### Rolling Update to Server
* Run `docker-compose -f docker/docker-compose.production.yml down -v`
* Run `docker-compose -f docker/docker-compose.production.yml pull`
* Run `docker-compose -f docker/docker-compose.production.yml up`
* Run `docker-compose -f docker/docker-compose.production.yml run --rm php-fpm php artisan migrate:fresh --seed`
* Run `docker-compose -f docker/docker-compose.production.yml run --rm php-fpm php artisan laravolt:acl:sync-permission`
* Browse `https://localhost:8000`
* If done, destroy with `docker-compose -f docker/docker-compose.production.yml down -v`

## Sample Users
* pentadbir-sistem@erayuan.dev
* ketua-pegawai-ju@erayuan.dev
* pegawai-ju@erayuan.dev
* pegawai-kewangan@erayuan.dev
* kerani@erayuan.dev
* pelanggan@erayuan.dev

Password for all users: `asdf1234`

## Development

### Backend Development
* Run `npm run watch` to livereload browser while editing PHP code

### Frontend Development
* Make sure you already had https://yarnpkg.com/en/ installed
* Run `yarn` to install all necessary libraries
* Run `npm run watch` to livereload browser while editing assets
* Run `npm run prod` to compile assets for production

## Coding Guidelines

### ACL
### Translation
### Menu Registration

## Credits

## Known Issues
