/*****
* CONFIGURATION
*/

//Main navigation
$.navigation = $('nav > ul.nav');

$.panelIconOpened = 'icon-arrow-up';
$.panelIconClosed = 'icon-arrow-down';

//Default colours
$.brandPrimary =  '#20a8d8';
$.brandSuccess =  '#4dbd74';
$.brandInfo =     '#63c2de';
$.brandWarning =  '#f8cb00';
$.brandDanger =   '#f86c6b';

$.grayDark =      '#2a2c36';
$.gray =          '#55595c';
$.grayLight =     '#818a91';
$.grayLighter =   '#d1d4d7';
$.grayLightest =  '#f8f9fa';

'use strict';

/****
* MAIN NAVIGATION
*/

$(document).ready(function($){

  // Add class .active to current link
  // $.navigation.find('.nav-item-level-2 a').each(function(){
  //
  //   var cUrl = String(window.location).split('?')[0];
  //
  //   if (cUrl.substr(cUrl.length - 1) == '#') {
  //     cUrl = cUrl.slice(0,-1);
  //   }
  //
  //   if ($($(this))[0].href==cUrl) {
  //     $(this).addClass('active');
  //     $(this).parents('ul').add(this).each(function(){
  //       $(this).parent().addClass('open');
  //     });
  //   }
  // });

    // Initiate dropdown height
    $.navigation.find('.nav-dropdown:not(.open) .nav-dropdown-items').css({height: '0px'});
    $.navigation.find('.nav-dropdown.open .nav-dropdown-items').each(function(idx, elm){
        $(elm).css({height: $(elm).get(0).scrollHeight});
    });

  // Dropdown Menu
  $.navigation.on('click', 'a', function(e){

    if ($.ajaxLoad) {
      e.preventDefault();
    }

    if ($(this).hasClass('nav-dropdown-toggle') && (!$('body').hasClass('sidebar-minimized') || $('body').hasClass('sidebar-mobile-show')) ) {

      var opened = $(this).parent().hasClass('open');
      $(this).parent().toggleClass('open');

      // collapse all other submenu
      $(this).parent().siblings().find('.nav-dropdown-items').animate({height: 0});
      $(this).parent().siblings().removeClass('open');

      var targetHeight = $(this).next().get(0).scrollHeight + 'px';
      if (opened) {
        targetHeight = '0px';
      }

      $(this).next().stop().animate({height: targetHeight});
      // $(this).next().stop().slideDown('slow');

      resizeBroadcast();
    }

  });

  function resizeBroadcast() {

    var timesRun = 0;
    var interval = setInterval(function(){
      timesRun += 1;
      if(timesRun === 5){
        clearInterval(interval);
      }
      window.dispatchEvent(new Event('resize'));
    }, 62.5);
  }

  /* ---------- Main Menu Open/Close, Min/Full ---------- */
  $('.sidebar-toggler').click(function(){
    $('body').toggleClass('sidebar-minimized');
    $('body').toggleClass('brand-minimized');

    if ($('body').hasClass('sidebar-minimized')) {
        $.navigation.find('.nav-dropdown-items').css('height', 'auto');
    } else {
        $.navigation.find('.nav-dropdown:not(".open") .nav-dropdown-items').css('height', '0');
    }

    resizeBroadcast();
  });

  $('.mobile-sidebar-toggler').click(function(){
    $('body').toggleClass('sidebar-mobile-show');

      if ($('body').hasClass('sidebar-mobile-show')) {
          $.navigation.find('.nav-dropdown:not(".open") .nav-dropdown-items').css('height', '0');
      }

    resizeBroadcast();
  });

});
