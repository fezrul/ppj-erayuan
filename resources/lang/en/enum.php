<?php

use Ekompaun\Systemconfig\Enum\AppealType;
use Ekompaun\Systemconfig\Enum\UserStatus;
use Ekompaun\Systemconfig\Enum\Citizenship;
use Ekompaun\Systemconfig\Enum\SectionType;
use Ekompaun\Systemconfig\Enum\UserCategory;
use Ekompaun\Systemconfig\Enum\AppealChannel;
use Ekompaun\Systemconfig\Enum\PaymentStatus;
use Ekompaun\Systemconfig\Enum\AppealProgress;
use Ekompaun\Systemconfig\Enum\AppealStatusType;
use Ekompaun\Systemconfig\Enum\CampaignRateType;

return [
    'user_category' => [
        UserCategory::INDIVIDUAL => 'Individu',
        UserCategory::COMPANY    => 'Company',
    ],
    Citizenship::class => [
        Citizenship::RESIDENT => 'Warganegara',
        Citizenship::NON_RESIDENT => 'Bukan Warganegara',
    ],
    UserCategory::class => [
        UserCategory::INDIVIDUAL => 'Individu',
        UserCategory::COMPANY => 'Company',
    ],
    UserStatus::class => [
        UserStatus::ACTIVE=> 'Active',
        UserStatus::INACTIVE=> 'Inactive',
        UserStatus::PENDING=> 'Pending',
    ],
    AppealType::class => [
        AppealType::MENGIKUTI_JADUAL => 'Mengikut Jadual',
        AppealType::RAYUAN_KEDUA => 'Rayuan Kedua',
        AppealType::KES_MAHKAMAH => 'Kes Mahkamah',
        AppealType::KEMPEN=> 'Kempen',
    ],
    AppealStatusType::class => [
        AppealStatusType::NOTIS_KOMPAUN => 'Notis Kompaun',
        AppealStatusType::KES_MAHKAMAH => 'Kes Mahkamah',
    ],
    AppealChannel::class => [
        AppealChannel::ONLINE => 'Online',
        AppealChannel::SMS => 'SMS',
    ],
    AppealProgress::class => [
        AppealProgress::BELUM_SELESAI => 'Belum Selesai',
        AppealProgress::SELESAI => 'Selesai',
    ],
    CampaignRateType::class => [
        CampaignRateType::KADAR_TETAP => 'Kadar Tetap',
        CampaignRateType::KADAR_DISKAUN => 'Kadar Diskaun',
        CampaignRateType::DISKAUN => 'Diskaun(%)',
    ],
    PaymentStatus::class => [
        PaymentStatus::BERJAYA => 'Berjaya',
        PaymentStatus::TIDAK_BERJAYA => 'Tidak Berjaya',
    ],
    SectionType::class => [
        SectionType::SECTION => 'Section',
        SectionType::KAEDAH => 'Kaedah',
        SectionType::PERINTAH => 'Perintah',
    ],
];
