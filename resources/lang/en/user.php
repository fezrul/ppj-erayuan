<?php

return [
    'attributes' => [
        'name'          => "Name",
        'email'         => "Email",
        'icno'          => "ID Card",
        'registered_at' => "Registered",
        'category'      => "Category",
        'citizenship'   => "Citizenship",
        'status'        => "Status",
    ],
];
