<?php

return [
    'attributes' => [
        'name'          => "Nama",
        'email'         => "Emel",
        'icno'          => "No Kad Pengenalan/No Passport",
        'registered_at' => "Terdaftar",
        'category'      => "Kategori",
        'status'        => "Status",
    ],
];
