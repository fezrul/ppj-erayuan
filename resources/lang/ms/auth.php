<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'          => 'ID/Kata Laluan tidak wujud.',
    'throttle'        => 'Terlalu banyak percubaan log masuk. Sila cuba lagi dalam :seconds saat.',
    'unauthenticated' => 'You must be logged in to access this page.',

];
