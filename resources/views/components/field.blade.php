{{--Initiate default value to prevent undefined index error--}}
@php($field = $field + ['label' => '', 'name' => '', 'value' => '', 'options' => '', 'help' => '', 'id' => '', 'attributes' => [], 'required' => false])

<div class="form-group {{ $field['required'] ? 'required':'' }} row {{ $field['attributes']['class'] ?? '' }}" {!! attributes($field['attributes']) !!}>
    <label class="col-sm-5 col-form-label">{{ $field['label'] }}</label>
    <div class="col-sm-7">
        @include('components.form.'.($field['type'] ?? 'text'))
        @if($field['help'] && !$errors->has($field['name']))
            <div class="form-text text-muted">{{ $field['help'] }}</div>
        @endif
        <div class="invalid-feedback">{{ $errors->first($field['name']) }}</div>
    </div>
</div>
