<div class="p-5" style="text-align: center">
    <div class="mb-4">
        <img src="{{ asset('img/logo-ppj.png') }}" width="200px" alt="">
    </div>
    <h5 class="text-muted">{{ $message or __("Maaf, tiada data tersedia")  }}</h5 class="text-muted">

    <p class="mt-4">
        @include('components.contact')
    </p>
</div>
