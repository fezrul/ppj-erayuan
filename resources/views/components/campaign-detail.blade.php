<div>
    <a href="{{ asset($item->picture) }}" target="_blank">
        <img class="card-img-top" src="{{ asset($item->picture) }}" alt="Card image cap">
    </a>
    <div class="card-body">
        <h5 class="card-title">{{$item->camp_name}}</h5>
        <p class="card-text">{{$item->camp_content}}</p>
    </div>
</div>
