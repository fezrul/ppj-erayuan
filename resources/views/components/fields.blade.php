@foreach($fields as $field)

    {{--Initiate default value to prevent undefined index error--}}
    @php($field = $field + ['label' => '', 'name' => '', 'value' => '', 'options' => '', 'help' => '', 'id' => '', 'attributes' => [], 'required' => false])

    <div class="form-group row {{ $field['required'] ? 'required':'' }}">
        <label class="col-sm-2 col-form-label">{{ $field['label'] }}</label>
        <div class="col-sm-10">
            @include('components.form.'.($field['type'] ?? 'text'))
            @if($field['help'] && !$errors->has($field['name']))
                <div class="form-text text-muted">{{ $field['help'] }}</div>
            @endif
            <div class="invalid-feedback">{{ $errors->first($field['name']) }}</div>
        </div>
    </div>
@endforeach
