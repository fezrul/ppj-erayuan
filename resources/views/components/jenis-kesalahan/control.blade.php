<?php
    // filter empty value
    $value = array_filter($value);

    $count = count($value ?? []);
?>

<button type="button" class="btn btn-link" role="jeniskesalahan" data-target="input[name='{{ $name }}']">
    Jenis Kesalahan
</button>
<input type="hidden" name="{{ $name }}" value="{{ implode(',', $value ?? [])}}">
<span class="badge badge-light">{{ $count }}</span>

@pushonce('end:jeniskesalahan')

@include('components.jenis-kesalahan.modal')

<script>
    $('[role="jeniskesalahan"]').on('click', function(e){
        e.preventDefault();
        var input = $(this).next();
        var ids = input.val().split(",");

        // update checkbox tree state
        $('#modalJenisKesalahan').find('input:checkbox[name="offences[]"]').each(function(idx, elm){
            var elm = $(elm);
            if (ids.indexOf(elm.attr('value')) >= 0) {
                elm.prop('checked', true);
            } else {
                elm.prop('checked', false);
            }
        });

        // show modal
        $('#modalJenisKesalahan').modal('show');
        $('#modalJenisKesalahan').data('target', $(this).data('target'));
    });

    $('#modalJenisKesalahan').on('click', '[role="submit"]', function(e){
        e.preventDefault();

        var modal = $(e.delegateTarget);
        var ids = modal.find('input:checkbox[name="offences[]"]').map(function() {
            return this.checked ? this.value : undefined;
        }).get();

        // update control values
        $(modal.data('target')).val(ids.join());

        // update counter text
        $(modal.data('target')).next().html(ids.length);
        $('#modalJenisKesalahan').modal('hide');
    });

    // CHECKBOX TREE

    // Act
    $('#modalJenisKesalahan').on('change', 'input:checkbox[data-role="act"]', function(e){
        var checked = $(e.currentTarget).is(':checked');
        var container = $(e.delegateTarget);
        var id = $(e.currentTarget).data('id');

        container.find('input:checkbox[data-role="section"][data-parent-id="' + id + '"]').prop('checked', checked).trigger('change');
    });

    // Section
    $('#modalJenisKesalahan').on('change', 'input:checkbox[data-role="section"]', function(e){
        var checked = $(e.currentTarget).is(':checked');
        var container = $(e.delegateTarget);
        var id = $(e.currentTarget).data('id');
        var parentId = $(e.currentTarget).data('parent-id');
        var parent = $('input:checkbox[data-role="act"][data-id="' + parentId + '"]');

        container.find('input:checkbox[data-role="offence"][data-parent-id="' + id + '"]').prop('checked', checked).trigger('change');

        setCheckboxState(parent, 'section');
    });

    // Offence
    $('#modalJenisKesalahan').on('change', 'input:checkbox[data-role="offence"]', function(e){
        var parentId = $(e.currentTarget).data('parent-id');
        var parent = $('input:checkbox[data-role="section"][data-id="' + parentId + '"]');
        var grandParentId = parent.data('parent-id');
        var grandParent = $('input:checkbox[data-role="act"][data-id="' + grandParentId + '"]');

        setCheckboxState(parent, 'offence');
        setCheckboxState(grandParent, 'section');
    });


    function setCheckboxState(parent, role) {
        var id = parent.data('id');
        var checkedCount = $('input:checkbox:checked[data-role="'+role+'"][data-parent-id="'+id+'"]').length;
        var childCount = $('input:checkbox[data-role="'+role+'"][data-parent-id="'+id+'"]').length;

        var checked = checkedCount == childCount;
        parent.prop('checked', checked);
    }

</script>
@endpushonce
