@inject('campaignService', '\Ekompaun\Systemconfig\Services\CampaignService')

<div class="modal fade" tabindex="-1" role="dialog" id="modalJenisKesalahan">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Jenis Kesalahan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-0">
                <table class="table table-sm mb-0">
                    @foreach($campaignService->getTypeOfMistakes() as $act)
                        <tr>
                            <th>
                                <input type="checkbox" id="act{{ $act->getKey() }}" data-role="act" data-id="{{ $act->getKey() }}">
                            </th>
                            <th colspan="3"><label for="act{{ $act->getKey() }}">{{ $act->act_name }}</label></th>
                        </tr>
                        @foreach($act->sections as $section)
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <input type="checkbox" id="section{{ $section->getKey() }}" data-role="section" data-id="{{ $section->getKey() }}" data-parent-id="{{ $act->getKey() }}">
                                </td>
                                <td colspan="2"><label for="section{{ $section->getKey() }}">{{ $section->section_name }}</label></td>
                            </tr>
                            @foreach($section->offences as $offence)
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td><input type="checkbox" name="offences[]" value="{{ $offence->getKey() }}" id="offence{{ $offence->getKey() }}"  data-role="offence" data-id="{{ $offence->getKey() }}" data-parent-id="{{ $section->getKey() }}"></td>
                                    <td><label for="offence{{ $offence->getKey() }}">{{ $offence->offence_name }}</label></td>
                                </tr>
                            @endforeach
                        @endforeach
                    @endforeach
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" role="submit">{{ __('Simpan') }}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Tutup') }}</button>
            </div>
        </div>
    </div>
</div>
