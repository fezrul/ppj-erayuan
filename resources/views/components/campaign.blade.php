<div class="col-sm-4">
    <div class="card b-a-0">
        <img class="card-img-top" src="{{ asset($item->picture) }}" alt="Card image cap">
        <div class="card-body">
            <h5 class="card-title">{{$item->camp_name}}</h5>
            <a class="btn btn-primary" href="{{route('home.show',$item->camp_id)}}">{{  __("Butiran Lanjut")  }}</a>
        </div>
    </div>
</div>
