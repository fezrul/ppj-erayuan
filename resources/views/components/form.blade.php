<form action="{{ $action }}" method="{{ $method ?? 'POST' }}">
    {!! csrf_field() !!}
    {{ $slot }}
</form>
