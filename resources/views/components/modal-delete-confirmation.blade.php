<div class="modal fade" id="modal-delete-confirmation" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">{{ $label }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
        	<div class="col-sm-12">
        		<p id="msg">{{ __('Adakah anda pasti ingin memadam rekod ini?') }}</p>
        	</div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" id="submit" >{{ __("Padam") }}</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __("Kembali") }}</button>
      </div>
    </div>
  </div>
</div>