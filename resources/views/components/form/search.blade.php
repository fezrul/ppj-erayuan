<form action="{{ $action or ''  }}" method="get" role="search">
    <div class="input-group">
        <input type="text" name="search" class="form-control" placeholder="{{ __("Carian Untuk...") }}"
               aria-label="{{ __("Carian Untuk...") }}"
               value="{{ $search or '' }}">
        <div class="input-group-btn">
            <button class="btn btn-secondary"
                    type="button">{{ __("Carian") }}</button>
        </div>
    </div>
</form>
