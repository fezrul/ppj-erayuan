<input name="{{ $field['name'] }}"
       type="text"
       class="form-control {{ $errors->has($field['name']) ?'is-invalid':'' }}"
       value="{{ old($field['name'], $field['value']) }}"
>
