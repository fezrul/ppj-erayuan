<div {{ $field['id'] ? "id={$field['id']}":'' }} class="form-control {{ $errors->has($field['name']) ?'is-invalid':'' }}">
    @foreach($field['options'] as $value => $label)
    <div class="form-check">
        <label class="form-check-label">
            <input class="form-check-input" type="radio" name="{{ $field['name'] }}" value="{{ $value }}" {{ $value == old($field['name'], $field['value']) ? 'checked':'' }}>
            {{ $label }}
        </label>
    </div>
    @endforeach
</div>
