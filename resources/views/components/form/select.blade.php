<select name="{{ $field['name'] }}" class="form-control {{ $errors->has($field['name']) ?'is-invalid':'' }}">
    @foreach($field['options'] as $value => $label)
        <option value="{{ $value }}" {{ $value == old($field['name'], $field['value']) ? 'selected':'' }}>{{ $label }}</option>
    @endforeach
</select>
