<div class="card">

    <div class="card-header">
        <h5 class="mb-0">{{ $title }}</h5>
        @if(isset($actions) && is_array($actions))
        <div class="card-actions">
            @foreach($actions as $action)
            <a href="{{ $action['url'] }}"><i class="{{ $action['icon'] }}"></i> {{ $action['text'] ?? 'Add' }}</a>
            @endforeach
        </div>
        @endif
    </div>

    <div class="card-body">
        {{ $slot }}
    </div>

    @if(isset($footer))
    <div class="card-footer">
        {{ $footer }}
    </div>
    @endif

</div>
