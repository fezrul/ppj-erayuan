@extends(config('laravolt.auth.layout'))

@section('content')

    @php($suggestedUser =  \App\User::find(request('id')))

    <div class="card-group">
        <div class="card p-2">
            <div class="card-body m-0">
            	{{ html()->form('GET', route('semakan-kompaun.index'))->class(['container-fluid'])->open() }}
                {{-- <form action="" class="container-fluid"> --}}

                    <div class="mb-3">
                        <h2>{{ __("Semakan Kompaun") }}</h2>
                        @include('components.panduan-semakan')
                    </div>
                    <div class="form-group">
                        <label>{{ __("Jenis Carian") }}</label>
                        <select name="type" class="form-control" >
                            @foreach(\Ekompaun\Systemconfig\Model\Lookup\SearchParameter::whereParamStatus(1)->get() as $item)
                                <option value="{{ $item->getKey() }}">{{ $item->param_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __("Maklumat Carian") }}</label>
                        {{ html()->text('search', request('search'))->class(['form-control'])->required('true')->style('text-transform:uppercase')  }}
                    </div>
                    <div class="form-group">
                    	{{ html()->a(route('login'), __("Log Masuk untuk Rayuan/Bayar"))->class(['btn btn-primary']) }}
                        {{ html()->submit(__("Hantar"))->class(['btn btn-primary']) }}
                    </div>
                {{ html()->form()->close() }}
				<table class="table table-striped mb-0 table-responsive">
		            <thead class="thead-light">
		                <tr>
		                    <th>{{ __("Bil") }}</th>
		                    <th>{{ __("No Kompaun") }}</th>
                            <th>{{ __("Kesalahan") }}</th>
		                    <th>{{ __("Jumlah (RM)") }}</th>
		                    <th>{{ __("Tarikh Kompaun") }}</th>
		                    <th>{{ __("Status") }}</th>
		                </tr>
		            </thead>
		            <tbody>
		            	@if($transactions->count() > 0)
		                @foreach($transactions as $transaction)
		                <tr>
		                    <td>{{ $loop->iteration }}</td>
		                    <td>{{ $transaction->tran_compoundno }}</td>
                             <td>{{ $transaction->offence->offence_name }}</td>
		                    <td>{{ number_format($transaction->tran_compoundamount, 2) }}</td>
		                    <td>{{ format_date($transaction->tran_compounddate) }}</td>
		                    <!-- <td>{{ $transaction->lastApplication->status->status_name }}</td> -->
                            <td>{!! $transaction->lastApplication->present_status !!}</td>
		                </tr>
		                @endforeach
		                @else
		                <tr>
		                    <td colspan="6">@include('components.empty')</td>
		                </tr>
		                @endif
		            </tbody>
		        </table>
            </div>

            <div class="card-footer">
            	{{ $transactions->links() }}
            </div>
        </div>
    </div>
@endsection
