<header class="app-header navbar">
    <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">
        <span style="color:white"><i class="fa fa-bars" aria-hidden="true"></i></span>

    </button>
    <a class="navbar-brand" href="#">{{ config('app.name') }}</a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button">
        <span style="color:white"><i class="fa fa-bars" aria-hidden="true"></i></span>

    </button>

    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <span class="d-md-down-none"><i class="fa fa-cog" aria-hidden="true"></i>
</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                @can(\Ekompaun\Systemconfig\Enum\Permission::EDIT_MY_PROFILE)
                    @if(\Route::has('my.profile.edit'))
                    <a class="dropdown-item" href="{{ route('my.profile.edit') }}"><i class="fa fa-user"></i> {{ __('Kemas Kini Profil') }}</a>
                    @endif
                @endcan

                @if(\Route::has('my.password.edit'))
                <a class="dropdown-item" href="{{ route('my.password.edit') }}"><i class="fa fa-lock"></i> {{ __('Tukar Kata Laluan') }}</a>
                @endif

                @can(\Ekompaun\Systemconfig\Enum\Permission::VIEW_MY_PAYMENT_HISTORY)
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('my::payment.index') }}"><i class="fa fa-credit-card custom"></i> {{ __('Rekod Pembayaran') }}</a>
                @endcan

                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('auth::logout') }}"><i class="fa fa-sign-out"></i> {{ __('Log Keluar') }}</a>
            </div>
        </li>
    </ul>
</header>
