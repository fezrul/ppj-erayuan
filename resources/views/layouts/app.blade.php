@extends('layouts.base')

@push('body.class', 'sidebar-fixed header-fixed')

@section('body')
    @include('layouts._header')

    <div class="app-body">

        @include('layouts._sidebar')

        <main class="main mb-5">

            {{--@include('layouts._breadcrumb')--}}
            @include('layouts._alert')

            <div class="container-fluid">
                @yield('content')
            </div>

        </main>

    </div>

    <!-- @include('layouts._footer') -->

@endsection
