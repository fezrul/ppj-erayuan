<?php $flash = [
    'error'   => ['icon' => 'error'],
    'danger'  => ['icon' => 'error'],
    'warning' => ['icon' => 'warning'],
    'info'    => ['icon' => 'info'],
    'message' => ['icon' => 'info'],
    'success' => ['icon' => 'success'],
]; ?>

@push('end')
    @foreach($flash as $type => $context)
        @if(session()->has($type))
            <script>
                swal({
                    text: "{{ session($type) }}",
                    buttons: false,
                    timer: 3000,
                    type: "{{ $context['icon'] }}"
                });
            </script>
        @endif
    @endforeach

    @if(!$errors->isEmpty())
        <script>
            swal({
                buttons: false,
                timer: 3000,
                type: "error",
                html: "{!! collect($errors->all())->implode('<br>') !!}"
            });
        </script>
    @endif

@endpush
