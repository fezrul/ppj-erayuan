<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>@yield('title')</title>
    @stack('head')
    <style type="text/css">
        * {
            font-family: "Arial", Helvetica, sans-serif !important
        }

        header { 
            position: fixed; 
            top: -30px; 
            left: 0px; 
            right: 0px; 
            background-color: lightblue; 
            height: 50px; 
        }

        footer { 
            position: fixed; 
            bottom: -50px; 
            left: 0px; 
            right: 0px;
            font-size:9px;
            text-align: center; 
            line-height: 5px;
            
            height: 50px; }

        html {
            margin-left: 1.5em;
        }

        .tg td h4 {
            margin: 0 0 5px 0;
        }

        .tg {
            border-spacing: 0;
            border-color: #ccc;
            width: 100%;
        }

        .tg td {
            font-family: Arial;
            font-size: 12px;
            padding: 2px;
            border: 1px solid #999;
            overflow: hidden;
            word-break: normal;
            border-color: #ccc;
            color: #333;
            background-color: #fff;
            text-align: center;
        }

        .tg th {
            font-family: Arial;
            font-size: 14px;
            font-weight: normal;
            padding: 5px;
            border-style: solid;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            border-color: #ccc;
            color: #333;
            background-color: #f0f0f0;
            text-align: center;
            font-weight: bold;
        }

        .tg .tg-3wr7 {
            font-weight: bold;
            font-size: 12px;
            text-align: center
        }

        .tg .tg-ti5e {
            font-size: 10px;
            font-family: "Arial", Helvetica, sans-serif !important;;
            text-align: left
        }

        .tg .tg-rv4w {
            font-size: 10px;
            font-family: "Arial", Helvetica, sans-serif !important;;
            text-align: center
        }
        .footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 12px;
            line-height: 12px;
            font-size:9px;
            text-align: center;
        }
    </style>
</head>
<body>
<footer><p>E-Rayuan Kompaun</p>{{ __('Dicetak oleh :name, pada :time', ['name' => auth()->user()->name, 'time' => \Carbon\Carbon::now()->format('j F Y H:i:s')]) }}</footer>

@yield('content')
</body>
</html>

