<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>{{ config('app.name') }}</title>

    <!-- Main styles for this application -->
    <link href="{{ mix('css/all.css') }}" rel="stylesheet">
    <style>
        .form-group.required .col-form-label:after {
            content:" *";
            color:red;
        }
    </style>

    @stack('head')
</head>

<body class="app @stack('body.class')" style="background-image: url(/img/manstop.png);background-size: cover;">

@stack('begin')

@yield('body')

<!-- Main scripts -->
<script src="{{ mix('js/all.js') }}"></script>
<script src="{{ asset('lib/sweetalert2.all.min.js') }}"></script>
@stack('end')

</body>
</html>
