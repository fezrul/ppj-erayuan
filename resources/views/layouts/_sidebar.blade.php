<?php
// @todo: move filter somewhere else
$menus = app('menu')->roots()->filter(function($item){
    return auth()->user()->can($item->data('permission'));
});
?>

<div class="sidebar">
<div style="overflow: hidden;border-bottom-style: ridge;margin-left: 5px;margin-right: 5px;margin-bottom: 10px;margin-top: 15px;" >
 <div style="margin-left: 5px;margin-top:20px;margin-bottom:10px;font-weight: 600;width:150px">
    <i class="fa fa-user-circle-o fa-2x" aria-hidden="true"></i>
    <div style="margin-left: 44px;margin-top: -33px;">Welcome Back</div>
    <br>
     <div style="margin-left:44px;margin-top: -25px;">{{ auth()->user()->name }}</div>
     <br><br>
</div>
</div>
    <nav class="sidebar-nav">

        @if(!$menus->isEmpty())
            <ul class="nav">
                @foreach($menus as $menu)

                    {{--check if current menu opened--}}
                    @php
                        $opened = false;
                        $validChildren = 0;
                    @endphp
                    @foreach($menu->children() as $submenu)
                        @if($submenu->isActive)
                            @php
                                $opened = true;
                            @endphp
                        @endif
                        @if(auth()->user()->can($submenu->data('permission')))
                            @php
                                $validChildren++;
                            @endphp
                        @endif
                    @endforeach

                    <li class="nav-item nav-item-level-1 nav-dropdown {{ ($opened)?'open':'' }}">
                        @if($menu->hasChildren() && $validChildren > 0)
                            <a class="nav-link nav-dropdown-toggle" href="#"><i class="{{ $menu->icon }}"></i> {{ $menu->title }}</a>
                            <ul class="nav-dropdown-items">
                                @foreach($menu->children() as $submenu)
                                    @if(auth()->user()->can($submenu->data('permission')))
                                    <li class="nav-item nav-item-level-2">
                                        <a class="nav-link {{ ($submenu->url() == request()->url())?'active':'' }}" href="{{ $submenu->url() }}"><i class="{{ $submenu->icon }}"></i> {{ $submenu->title }}</a>
                                    </li>
                                    @endif
                                @endforeach
                            </ul>
                        @elseif(!$menu->hasChildren() && auth()->user()->can($menu->data('permission')))
                            <a class="nav-link {{ ($menu->url() == request()->url())?'active':'' }}" href="{{ $menu->url() }}"><i class="{{ $menu->icon }}"></i> {{ $menu->title }}</a>
                        @endif
                    </li>

                @endforeach
            </ul>
        @endif
    </nav>
</div>
