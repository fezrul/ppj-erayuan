<footer class="app-footer">
	<div class="col-sm-4">
            <ul>
                <li>
                   @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        <?php $active = '' ?>
                        <?php if ($localeCode == LaravelLocalization::getCurrentLocale()) $active = 'active' ?>
                        <a class=" {{ $active }}" rel="alternate" hreflang="{{ $localeCode }}"
                           href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                            {{ strtoupper($localeCode) }}
                            @if(!$loop->last) | @endif
                        </a>
                    @endforeach
                </li>
            </ul>
        </div>
</footer>
