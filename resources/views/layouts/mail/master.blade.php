<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta name="viewport" content="width=device-width" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Ekompaun</title>
	<style type="text/css">
	* {
		margin: 0;
		padding: 0;
	}

	* {
		font-family: "Open Sans", Arial, sans-serif;
	}

	img {
		max-width: 100%;
	}

	.collapse {
		margin: 0;
		padding: 0;
	}


	body {
		-webkit-font-smoothing: antialiased;
		-webkit-text-size-adjust: none;
		background: #f5f8fa;
		color: #000;
	}

	.header {
		max-width: 800px;
		margin-left: auto;
		margin-right: auto;
		top: 10px;
		left: 0;
		right: 0;
		width: 85%;
		height: 200px;
		background: linear-gradient(180deg, rgba(0, 51, 150, 0.6) 100%, #1474AE 100%), url('https://burst.shopifycdn.com/photos/new-york-city-buildings-and-water-towers_925x@2x.jpg');
		background-repeat: no-repeat;
		background-size: cover;
		z-index: -999;
	}

	.divider {
		border: 0.5px solid #eee;
		margin: 15px auto;
	}

	.divider.hidden {
		border: 0.5px solid transparent;
		margin: 15px auto;
	}

	h3 {
		color: #003483;
	}

	a {
		color: #2BA6CB;
	}

	p.callout {
		padding: 15px;
		background-color: #ECF8FF;
		margin-bottom: 15px;
	}

	.callout a {
		font-weight: bold;
		color: #2BA6CB;
	}

	table.body-wrap {
		max-width: 800px;
		width: 800px;
		margin: 0 auto;
		display: block;
		border-radius: 2px;
		background-color: #fff;
		-webkit-box-shadow: 0 0 10px #aaa;
		-moz-box-shadow: 0 0 10px #aaa;
		box-shadow: 0 0 10px #aaa;
	}

	.collapse {
		margin: 0!important;
	}

	p,
	ul,
	.content-table {
		margin-bottom: 10px;
		font-weight: normal;
		font-size: 14px;
		line-height: 1.6;
		margin-left: 20px;
	}

	.container {
		display: block!important;
		max-width: 800px!important;
		margin: 0 auto!important;
		clear: both!important;
	}

	.content {
		padding: 35px;
	}

	.content table, .content table tr td {
		width: 730px;
	}

	.detail {
		border-left: 10px solid #477ac1;
		padding-left: 15px;
		padding-right: 15px;
	}

	.column {
		width: 300px;
		float: left;
	}

	.column tr td {
		padding: 15px;
	}

	.column-wrap {
		padding: 0!important;
		margin: 0 auto;
		max-width: 600px!important;
	}

	.column table {
		width: 100%;
	}

	.social .column {
		width: 280px;
		min-width: 279px;
		float: left;
	}

	.disclaimer {
		max-width: 800px;
		margin-left: auto;
		margin-right: auto;
		top: 10px;
		left: 0;
		right: 0;
		width: 85%;
		position: relative;
		overflow-wrap: break-word;
		text-align: justify;
		text-justify: inter-word;
		letter-spacing: 0.5px;
	}

	.disclaimer img {
		float: left;
	}

	.disclaimer .title {
		height: 70px;
		display: table-cell;
		vertical-align: middle;
		padding: 10px;
		font-weight: bold;
	}

	.footer {
		color: #aaa;
		width: 100%;
		text-align: center;
	}

	.clear {
		display: block;
		clear: both;
	}

	@media only screen and (max-width: 600px) {
		table.body-wrap, .disclaimer, .header {
			max-width: 500;
			width: auto;
			margin: 0 20px;
		}
		.content table, .content table tr td {
			max-width: 500;
			width: auto;
		}
	}
</style>
</head>
<body>
	<!-- <div class="header"></div> -->
	<table class="body-wrap">
		<tbody>
			<tr>
				<td></td>
				<td class="container">
					<div class="content">
						@yield('content')
					</div>
				</td>
				<td></td>
			</tr>
		<tr>
			<td></td>
			<td style="text-align: center">
				<img src="{{ asset('img/logo-ppj.png') }}" width="200px" alt="">
				<h3 style="text-align: center">PERBADANAN PUTRAJAYA</h3>
				<div class="divider hidden"></div>
				<div class="divider hidden"></div>
			</td>
			<td></td>
		</tr>
		</tbody>
	</table>
	<div class="divider hidden"></div>
	<div class="footer">&copy; {{ config('app.name') }} {{ date('Y') }}</div>
	<div class="divider hidden"></div>
	<div class="divider hidden"></div>
</body>
</html>
