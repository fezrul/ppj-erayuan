<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>{{ config('app.name') }}</title>

    <!-- Main styles for this application -->
    <link href="{{ mix('css/all.css') }}" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
    @stack('head')
</head>

<body class="app @stack('body.class')" style="background-image: url({{url('/img/bg.png')}});background-size: repeat;background-color: ghostwhite;background-attachment: fixed;">

@stack('begin')

@yield('body')

<!-- Main scripts -->
<script src="{{ mix('js/all.js') }}"></script>
<script src="{{ asset('lib/sweetalert2.all.min.js') }}"></script>

@stack('end')

</body>
</html>
