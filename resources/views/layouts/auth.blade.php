@extends('layouts.public-base')

@section('body')
    <div class="auth-body">
        <main class="main">
            @include('layouts._alert')
            <div class="container">
                <div class="row justify-content-center mt-5">
                    <div class="col">
                        <div class="text-center py-4">
                            <h1 class="display-4 text-white"><img alt="" src="/img/logo.png" height="80px" style="margin-right: 20px;">{{ config('site.name') }}</h1>
                            <p class="lead">{{ config('site.description') }}</p>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-center">
                    <div class="col" style="background-color: transparent;">
                        @yield('content')
                    </div>
                </div>

                <div class="row justify-content-center">
                    <div class="col-6 mt-4">
                        <p class="text-center">{{ config('site.copyright') }}</p>
                    </div>
                </div>

            </div>
        </main>
    </div>
@endsection
