@extends('layouts.base')

@section('body')
    <div class="app-body">
        <main class="main">
            @include('layouts._alert')
            <div class="container-fluid">
                @yield('content')
            </div>
        </main>
    </div>
@endsection
