@extends('layouts.app')
@section('content')

    <div class="mt-5">
        <div class="row">
        <?php 

            if($roles == 1){ ?>

                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="cards cards-stats">
                        <div class="cards-header" data-background-color="orange">
                            <i class="fa fa-percent"></i>
                        </div>
                        <div class="cards-content">
                            <p class="category">Kempen Aktif</p>
                            <h3 class="title">{{$camcount}}</h3>
                        </div>
                        <div class="cards-footer">
                            <div class="stats">
                                <i class="material-icons text-danger">donut_small</i>
                                <a href="campaign">Lihat Tetapan Kempen</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="cards cards-stats">
                        <div class="cards-header" data-background-color="blue">
                            <i class="fa fa-users"></i>
                        </div>
                        <div class="cards-content">
                            <p class="category">Pelanggan</p>
                            <h3 class="title">{{$customer}}</h3>
                        </div>
                        <div class="cards-footer">
                           <div class="stats">
                                <i class="material-icons text-info">group</i>
                                <a href="customer">Pengurusan Pelanggan</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="cards cards-stats">
                        <div class="cards-header" data-background-color="orange">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <div class="cards-content">
                            <p class="category">Cuti</p>
                            <h3 class="title">{{$holidays}}</h3>
                        </div>
                        <div class="cards-footer">
                          <div class="stats">
                                <i class="material-icons text-info">event</i>
                                <a href="holiday">Lihat Tetapan Cuti</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="cards cards-stats">
                        <div class="cards-header" data-background-color="green">
                            <i class="fa fa-calendar-plus-o "></i>
                        </div>
                        <div class="cards-content">
                            <p class="category">Jadual Kadar</p>
                            <h3 class="title">{{$schdcount}}</h3>
                        </div>
                        <div class="cards-footer">
                          <div class="stats">
                                <i class="material-icons text-info">event</i>
                                <a href="schedule-rate">Jadual Kadar Rayuan</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="cards cards-stats">
                        <div class="cards-header" data-background-color="purple">
                            <i class="fa fa-users"></i>
                        </div>
                        <div class="cards-content">
                            <p class="category">Pengguna Dalaman</p>
                            <h3 class="title">{{$internal}}</h3>
                        </div>
                        <div class="cards-footer">
                           <div class="stats">
                                <i class="material-icons text-info">group</i>
                                <a href="epicentrum/users">Pengurusan Pengguna</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="cards cards-stats">
                        <div class="cards-header" data-background-color="grey">
                            <i class="fa fa-search-plus"></i>
                        </div>
                        <div class="cards-content">
                            <p class="category">Audit</p>
                            <h3 class="title">~</h3>
                        </div>
                        <div class="cards-footer">
                          <div class="stats">
                                <i class="material-icons text-info">face</i>
                                <a href="report/audit-trail">Lihat Jejak Audit</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


         <?php }

            if($roles == 2){ ?>

             
                <div class="col-md-4 ">
                    <a href="action" style="text-decoration: none !important;">
                        <div class="card-body card" style="background-color: cornflowerblue;color: white;margin-right:5px;border-radius: 10px;margin-bottom: 40px;height: 115px;">

                            <i class="fa fa-id-badge fa-4x" aria-hidden="true" style="width:100%;margin-top: -40px;"><span style="margin-left:5px;float:right;margin-top:40px">{{$applications}}</span></i>
                            <br>
                            <span style="margin-top: -25px"><h6><strong>Senarai Tugasan Anda</strong></h6></span>
                           
                        </div>
                    </a>
                    </div>
              
                
                    <div class="col-md-4 " >
                    <a href="assignment" style="text-decoration: none !important;">
                        <div class="card-body card" style="background-color: cadetblue;color: white;margin-right:5px;border-radius: 10px;margin-bottom: 40px;height: 115px;" >

                            <i class="fa fa-folder-open fa-4x" aria-hidden="true" style="width:100%;margin-top: -40px;"><span style="margin-left:5px;float:right;margin-top:40px">{{$assignment}}</span></i>
                            <br>
                            <span style="margin-top: -25px"><h6><strong>Menunggu Agihan</strong></h6></span>
                            
                        </div>
                    </a>
                    </div>

                    <div class="col-md-4 " >
                    <a href="assignee" style="text-decoration: none !important;">
                        <div class="card-body card" style="background-color: initial;color: white;margin-right:5px;border-radius: 10px;margin-bottom: 40px;height: 115px;" >

                            <i class="fa fa-folder fa-4x" aria-hidden="true" style="width:100%;margin-top: -40px;"><span style="margin-left:5px;float:right;margin-top:40px">{{$assignee}}</span></i>
                            <br>
                            <span style="margin-top: -25px"><h6><strong>Agihan Selesai</strong></h6></span>
                            
                        </div>
                    </a>
                </div>
          <?php } 

             if($roles == 3){ ?>

                    <div class="col-md-12 ">
                        <a href="action" style="text-decoration: none !important;">
                            <div class="card-body card" style="background-color: cornflowerblue;color: white;margin-right:5px;border-radius: 10px;margin-bottom: 40px;height: 115px;">

                                <i class="fa fa-id-badge fa-4x" aria-hidden="true" style="width:100%;margin-top: -40px;"><span style="margin-left:5px;float:right;margin-top:40px">{{$applications}}</span></i>
                                <br>
                                <span style="margin-top: -25px"><h6><strong>Senarai Tugasan Anda</strong></h6></span>
                               
                            </div>
                        </a>
                    </div>

           <?php } ?>


            @can(\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_RAYUAN)

    
       
    

                <div class="col-md-6">
                    @include('report::charts.rayuanByChannel')
                </div>
                <div class="col-md-6">
                    @include('report::charts.rayuanByType')
                </div>
            @endcan

            @can(\Ekompaun\Systemconfig\Enum\Permission::VIEW_LAPORAN_KOMPAUN)
                <div class="col-md-6">
                    @include('report::charts.kompaunByProgress')
                </div>
            @endcan
        </div>

    </div>

    <div class="container-fluid mt-5">
        @includeWhen(auth()->user()->can(\Ekompaun\Systemconfig\Enum\Permission::VIEW_MY_CAMPAIGN), 'home._customer')
    </div>

@endsection
