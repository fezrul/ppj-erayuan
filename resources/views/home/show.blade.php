@extends('layouts.app')
@section('content')
    <div class="container-fluid mt-5">
        <div class="card">
            <div class="card-header">{{ __('Butiran Kempen') }}</div>
            <div class="card-body">
                <div class="row">
                    @include('components.campaign-detail', ['item' => $campaign])
                </div>
            </div>
            <div class="card-footer">
                <a href="{{ route('home') }}" class="btn btn-primary">{{ __('Kembali') }}</a>
            </div>
        </div>
@endsection
