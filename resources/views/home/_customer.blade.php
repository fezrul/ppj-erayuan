<div class="container-fluid mt-5">
    <div class="row">


        <div class="col-md-3 ">
            <a href="my/profile" style="text-decoration: none !important;">
                <div class="card-body card"
                     style="background-color: orange;color: white;margin-right:5px;border-radius: 10px;margin-bottom: 40px;height: 115px;">
                    <i class="fa fa-address-card fa-4x" aria-hidden="true" style="width:100%;margin-top: -40px;"><span
                                style="margin-left:5px;float:right;margin-top:40px"><i class="fa fa-user"
                                                                                       aria-hidden="true"></i>
</span></i>
                    <br>
                    <span style="margin-top: -25px"><h6><strong>Kemas Kini Profil</strong></h6></span>
                </div>
        </div>


        <div class="col-md-3 ">
            <a href="my/appeal" style="text-decoration: none !important;">
                <div class="card-body card"
                     style="background-color: teal;color: white;margin-right:5px;border-radius: 10px;margin-bottom: 40px;height: 115px;">

                    <i class="fa fa-newspaper-o fa-4x" aria-hidden="true" style="width:100%;margin-top: -40px;"><span
                                style="margin-left:5px;float:right;margin-top:40px">{{$noofresult}}</span></i>
                    <br>
                    <span style="margin-top: -25px"><h6><strong>Keputusan Rayuan</strong></h6></span>

                </div>
        </div>

        <div class="col-md-3 ">
            <a href="my/kompaun" style="text-decoration: none !important;">
                <div class="card-body card"
                     style="background-color: cornflowerblue;color: white;margin-right:5px;border-radius: 10px;margin-bottom: 40px;height: 115px;">

                    <i class="fa fa-id-badge fa-4x" aria-hidden="true" style="width:100%;margin-top: -40px;"><span
                                style="margin-left:5px;float:right;margin-top:40px">{{$noofcompound}}</span></i>
                    <br>
                    <span style="margin-top: -25px"><h6><strong>Senarai Kompaun</strong></h6></span>

                </div>
            </a>
        </div>
        </a>

        <div class="col-md-3 ">
            <a href="my/payment" style="text-decoration: none !important;">
                <div class="card-body card"
                     style="background-color: midnightblue;color: white;margin-right:5px;border-radius: 10px;margin-bottom: 40px;height: 115px;">

                    <i class="fa fa-money fa-4x" aria-hidden="true" style="width:100%;margin-top: -40px;"><span
                                style="margin-left:5px;float:right;margin-top:40px">{{$noofpayment}}</span></i>
                    <br>
                    <span style="margin-top: -25px"><h6><strong>Rekod Pembayaran</strong></h6></span>

                </div>
            </a>
        </div>
    </div>


    <br>
    <div class="card">
        <div class="card-header">Kempen E-Rayuan</div>
        <div class="card-body">
            @if($campaigns->count() > 0)
                <div class="row">
                    @foreach($campaigns as $campaign)
                        @include('components.campaign', ['item' => $campaign])
                    @endforeach
                </div>
            @else
                @include('components.empty', ['message' => __('Harap maaf. Tiada kempen buat masa ini')])
            @endif
        </div>
    </div>
