@extends('layouts.mail.master')
@section('content')

<table>
	<tbody>
		<tr>
			<td><center>
				<h3>MAKLUMAN DISKAUN MENGIKUT JADUAL KADAR KOMPAUN</h3>
			</center>
			<div class="divider hidden"></div>
			<div class="divider"></div>
			<div class="divider hidden"></div>
			<div class="detail"><b>Tuan/Puan,</b>
				<div class="divider hidden"></div>
				<p>Adalah dengan ini dimaklumkan bahawa anda telah membuat permohonan diskaun mengikut jadual kadar untuk no.kompaun seperti di atas, berikut adalah butiran :<br /></p>
				<div class="divider hidden"></div>
				<p>
					<table class="content-table">
						<tbody>
							<tr>
								<td>No. Kompaun</td>
								<td>: {{ $data->transaction->tran_compoundno }}</td>
							</tr>
							<tr>
								<td>Kadar Asal</td>
								<td>: RM{{ format_money($data->transaction->tran_compoundamount) }}</td>
							</tr>
						</tbody>
					</table>
				</p>

				<div class="divider hidden"></div>
				<p>SILA AMBIL PERHATIAN, pihak tuan adalah diberi tempoh 14 hari dari tarikh surat ini diterima untuk membayar kompaun tersebut. Kompaun tersebut bolehlah dibayar sama ada:
					melalui perkhidmatan atas talian dengan menggunakan FPX melalui laman sesawang Perbadanan iaitu https://erayuan.ppj.gov.my; atau
					melalui kiriman wang pos  atas nama Perbadanan Putrajaya kepada alamat seperti di dalam surat ini; atau
					datang sendiri ke Kaunter Bayaran, Pusat Khidmat Pelanggan Perbadanan Putrajaya; atau
					menerusi perkhidmatan e-perbankan MAYBANK, BANK ISLAM atau CIMB CLICKS.<br /></p>

					<br /> <br />
					<p><b>Sekian terima kasih.</b></p>
					<p>
					Untuk maklumat lanjut , sila log masuk ke sistem https://erayuan.ppj.gov.my untuk tindakan selanjutnya.<br />
				</p>

					<br /> <br />
					<p><b>{{ config("app.name") }}</b></p>
				</div>
			</td>
		</tr>
	</tbody>
</table>

@endsection
