@extends('layouts.mail.master')
@section('content')

<table>
	<tbody>
		<tr>
			<td>
				<center>
					<h3>MAKLUMAN PERMOHONAN RAYUAN</h3>
				</center>
				<div class="divider hidden"></div>
				<div class="divider"></div>
				<div class="divider hidden"></div>
				<div class="detail"><b>Tuan/Puan,</b>
					<div class="divider hidden"></div>
					<p>
						Adalah dengan ini dimaklumkan bahawa permohonan rayuan telah dibuat untuk no.kompaun seperti di atas, berikut adalah butiran :<br />
					</p>
					<div class="divider hidden"></div>
					<p>
						<table class="content-table">
							<tbody>
								<tr>
									<td>No. Kompaun</td>
									<td>: {{ $data->transaction->tran_compoundno }}</td>
								</tr>
								<tr>
									<td>Kadar Asal</td>
									<td>: RM{{ $data->transaction->tran_compoundamount }}</td>
								</tr>
								<tr>
									<td>Kadar Mengikut Jadual Kadar</td>
									<td>: RM{{ $data->appl_amount }}</td>
								</tr>
							</tbody>
						</table>
					</p>

					<div class="divider hidden"></div>
					<p>
						Klik https://erayuan.ppj.gov.my untuk log masuk dan membuat kelulusan.<br />
					</p>
					<br /> <br />
					<p><b>Sekian terima kasih.</b></p>

					<br /> <br />
					<p><b>{{ config("app.name") }}</b></p>
				</div>
			</td>
		</tr>
	</tbody>
</table>

@endsection
