@extends('layouts.mail.master')
@section('content')

<table>
	<tbody>
		<tr>
			<td><center>
				<h3>MAKLUMAN STATUS PERMOHONAN RAYUAN</h3>
			</center>
			<div class="divider hidden"></div>
			<div class="divider"></div>
			<div class="divider hidden"></div>
			<div class="detail"><b>Tuan/Puan,</b>
				<div class="divider hidden"></div>
				<p>
					Adalah dengan ini dimaklumkan bahawa permohonan rayuan anda telah diluluskan RM{{ format_money($data->present_amaun) }}.
					SILA AMBIL PERHATIAN, pihak tuan adalah diberi tempoh 14 hari dari tarikh surat ini diterima untuk membayar kompaun tersebut. Kompaun tersebut bolehlah dibayar sama ada: <br />
				</p>
				<div class="divider hidden"></div>
				<ul>
					<li>
						melalui perkhidmatan atas talian dengan menggunakan FPX melalui laman sesawang Perbadanan iaitu https://erayuan.ppj.gov.my; atau
					</li>
					<li>
						melalui kiriman wang pos  atas nama Perbadanan Putrajaya kepada alamat seperti di dalam surat ini; atau
					</li>
					<li>
						datang sendiri ke Kaunter Bayaran, Pusat Khidmat Pelanggan Perbadanan Putrajaya; atau
					</li>
					<li>
						menerusi perkhidmatan e-perbankan MAYBANK, BANK ISLAM atau CIMB CLICKS.
					</li>
				</ul>
				<div class="divider hidden"></div>
				<br /> <br />
				<p><b>Sekian terima kasih.</b></p>
				<p>
					Untuk maklumat lanjut , sila log masuk ke sistem https://erayuan.ppj.gov.my untuk tindakan selanjutnya.<br />
				</p>

				<br /> <br />
				<p><b>{{ config("app.name") }}</b></p>
			</div>
		</td>
	</tr>
</tbody>
</table>

@endsection
