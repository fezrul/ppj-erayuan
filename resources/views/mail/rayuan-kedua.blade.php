@extends('layouts.mail.master')
@section('content')

<table>
	<tbody>
		<tr>
			<td>
				<center>
					<h3>MAKLUMAN PERMOHONAN RAYUAN</h3>
				</center>
				<div class="divider hidden"></div>
				<div class="divider"></div>
				<div class="divider hidden"></div>
				<div class="detail"><b>Tuan/Puan,</b>
					<div class="divider hidden"></div>
					<p>Adalah dengan ini dimaklumkan bahawa anda telah membuat permohonan rayuan untuk no.kompaun seperti di atas, berikut adalah butiran :<br /></p>
					<div class="divider hidden"></div>
					<p>
						<table class="content-table">
							<tbody>
								<tr>
									<td>No. Kompaun</td>
									<td>: {{ $data->transaction->tran_compoundno }}</td>
								</tr>
								<tr>
									<td>Kadar Asal</td>
									<td>: RM{{ $data->transaction->tran_compoundamount }}</td>
								</tr>
							</tbody>
						</table>
					</p>

					<div class="divider hidden"></div>
					<p>Proses kelulusan dari pihak Jabatan Undang-undang akan mengambil masa 7 hari bekerja.<br /></p>
					<br /> <br />
					<p><b>Sekian terima kasih.</b></p>
					<p>
					Untuk maklumat lanjut , sila log masuk ke sistem https://erayuan.ppj.gov.my untuk tindakan selanjutnya.<br />
				</p>

					<br /> <br />
					<p><b>{{ config("app.name") }}</b></p>
				</div>
			</td>
		</tr>
	</tbody>
</table>

@endsection
