@extends('layouts.mail.master')
@section('content')

<table>
	<tbody>
		<tr>
			<td><center>
				<h3>MAKLUMAN STATUS PERMOHONAN RAYUAN</h3>
			</center>
			<div class="divider hidden"></div>
			<div class="divider"></div>
			<div class="divider hidden"></div>
			<div class="detail"><b>Tuan/Puan,</b>
				<div class="divider hidden"></div>
				<p>Adalah dengan ini dimaklumkan bahawa permohonan rayuan anda telah ditolak.<br /></p>
				<div class="divider hidden"></div>
				<br /> <br />
				<p><b>Sekian terima kasih.</b></p>
				<p>
					Untuk maklumat lanjut , sila log masuk ke sistem https://erayuan.ppj.gov.my untuk tindakan selanjutnya.<br />
				</p>

				<br /> <br />
				<p><b>{{ config("app.name") }}</b></p>
			</div>
		</td>
	</tr>
</tbody>
</table>

@endsection
