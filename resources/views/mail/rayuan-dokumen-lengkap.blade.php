@extends('layouts.mail.master')
@section('content')

<table>
	<tbody>
		<tr>
			<td><center>
				<h3>MAKLUMAN DOKUMEN TAMBAHAN LENGKAP NO. KOMPAUN: {{ $application->transaction->tran_compoundno }}</h3>
			</center>
			<div class="divider hidden"></div>
			<div class="divider"></div>
			<div class="divider hidden"></div>
			<div class="detail"><b>Tuan/Puan,</b>
				<div class="divider hidden"></div>
				<p>Mohon tuan/puan untuk menyemak dan mengambil tindakan ke atas permohonan rayuan ini dengan segera.</p>
				<br /> <br />
				<p><b>Sekian terima kasih.</b></p>
				<p>
					Untuk maklumat lanjut , sila log masuk ke sistem https://erayuan.ppj.gov.my untuk tindakan selanjutnya.<br />
				</p>

				<br /> <br />
				<p><b>{{ config("app.name") }}</b></p>
			</div>
		</td>
	</tr>
</tbody>
</table>

@endsection
