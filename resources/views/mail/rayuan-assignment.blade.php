@extends('layouts.mail.master')
@section('content')

<table>
	<tbody>
		<tr>
			<td><center>
				<h3>MAKLUMAN TINDAKAN KELULUSAN RAYUAN </h3>
			</center>
			<div class="divider hidden"></div>
			<div class="divider"></div>
			<div class="divider hidden"></div>
			<div class="detail"><b>Tuan/Puan,</b>
				<div class="divider hidden"></div>
				<p>Mohon tuan/puan untuk menyemak dan mengambil tindakan ke atas permohonan rayuan ini dengan segera.<br /></p>
				<div class="divider hidden"></div>
				<br /> <br />
				<p><b>Sekian terima kasih.</b></p>

				<br /> <br />
				<div class="divider hidden"></div>
					<p>
						Klik https://erayuan.ppj.gov.my untuk log masuk dan membuat kelulusan.<br />
					</p>
				<p><b>{{ config("app.name") }}</b></p>
			</div>
		</td>
	</tr>
</tbody>
</table>

@endsection
