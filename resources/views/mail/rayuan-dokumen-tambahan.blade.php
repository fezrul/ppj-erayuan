@extends('layouts.mail.master')
@section('content')

<table>
	<tbody>
		<tr>
			<td><center>
				<h3>MAKLUMAN DOKUMEN TAMBAHAN PERMOHONAN RAYUAN</h3>
			</center>
			<div class="divider hidden"></div>
			<div class="divider"></div>
			<div class="divider hidden"></div>
			<div class="detail"><b>Tuan/Puan,</b>
				<div class="divider hidden"></div>
				<p>Adalah dengan ini dimaklumkan bahawa permohonan rayuan anda sedang diproses, namun dukacita dimaklumkan pihak kami masih memerlukan dokumen sokongan tambahan yang berkaitan sekiranya ada untuk mempercepatkan proses permohonan tuan/puan.<br /></p>
				<div class="divider hidden"></div>
				<p>
					Untuk maklumat lanjut , sila log masuk ke sistem https://erayuan.ppj.gov.my untuk tindakan selanjutnya.<br />
				</p>

				<p>Sekiranya tidak ada maklumbalas selepas 3 hari, permohonan rayuan automatik akan <strong>BATAL</strong>.</p>

				<br /> <br />
				<p><b>Sekian terima kasih.</b></p>

				<br /> <br />
				<p><b>{{ config("app.name") }}</b></p>
			</div>
		</td>
	</tr>
</tbody>
</table>

@endsection
