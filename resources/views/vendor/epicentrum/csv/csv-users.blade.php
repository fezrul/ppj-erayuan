<!DOCTYPE html>
<html>
<body>
	<table>
        <thead>
        <tr>
            <th>{{ __("Nama") }}</th>
            <th>{{ __("Emel") }}</th>
            <th>{{ __("Peranan") }}</th>
            <th>{{ __("Status") }}</th>
            <th>{{ __("Tarikh Daftar") }}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->roles->implode('name', ', ') }}</td>
                <td>{{ $user->getStatusLabel() }}</td>
                <td>{{ $user->created_at->format('j F Y') }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</body>
</html>