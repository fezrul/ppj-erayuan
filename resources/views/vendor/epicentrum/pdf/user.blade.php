@extends('layouts.pdf.master')

@section('title', 'User')
@section('content')
  <div style="font-family:Arial; font-size:12px;">
      <h2 style="text-align: center">{{ __("Pengguna Dalaman") }}</h2>  
  </div>
  <br>
  @if ($users->count() > 0 )
  <table class="tg">
    <tr>
        <th class="tg-3wr7" width="1">{{ __("No") }}<br></th>
        <th class="tg-3wr7">{{ __("Nama") }}<br></th>
        <th class="tg-3wr7">{{ __("Emel") }}<br></th>
        <th class="tg-3wr7">{{ __("Peranan") }}<br></th>
        <th class="tg-3wr7">{{ __("Status") }}<br></th>
        <th class="tg-3wr7">{{ __("Tarikh Daftar") }}<br></th>
    </tr>
    @foreach($users as $u)
    <tr>
        <td class="tg-rv4w" width="1">{{ $loop->iteration }}</td>
        <td class="tg-ti5e">{{ $u->name }}</td>
        <td class="tg-ti5e">{{ $u->email }}</td>
        <td class="tg-ti5e">{{ $u->roles->implode('name', ', ') }}</td>
        <td class="tg-rv4w">{{ $u->getStatusLabel() }}</td>
        <td class="tg-rv4w">{{ $u->created_at->format('j F Y') }}</td>
    </tr>
    @endforeach
  </table>
  @else
  <table class="tg">
    @include('layouts.pdf._headPdfUser')
    <td colspan='6' style="text-align: center">{{ __("Maaf, tiada data tersedia") }}</td>
  </table>
  @endif
@endsection