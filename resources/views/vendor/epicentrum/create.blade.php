@extends(config('laravolt.epicentrum.view.layout'))
@section('content')

    <div class="my-5">

        <div class="mb-3">
            <a href="{{ route('epicentrum::users.index') }}" class="btn btn-sm btn-secondary">
                <i class="fa fa-arrow-left"></i>
                {{ __("Back") }}
            </a>
        </div>

        @component('components.panel')
            @slot('title', __("Tambah Penggguna Dalaman Baharu"))

            @component('components.form', ['action' => route('epicentrum::users.store')])
                @component('components.fields', ['fields' => [
                    ['name' => 'name', 'label' => __("Nama")],
                    ['name' => 'email', 'label' => __("Emel")],
                ]])
                @endcomponent

                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">{{ __("Password") }}</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="password" id="password" value="{{ old('password') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <legend class="col-sm-2 col-form-legend">{{ __("Peranan") }}</legend>
                    <div class="col-sm-10">
                        @if($multipleRole)
                            {!! SemanticForm::checkboxGroup('roles', $roles) !!}
                        @else
                            {!! SemanticForm::radioGroup('roles', $roles) !!}
                        @endif
                    </div>
                </div>


                <div class="form-group row" style="display:none">
                    <label class="col-sm-2">{{ __("Status") }}</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="status" id="status" value="ACTIVE">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2"></label>
                    <div class="col-sm-10">
                        <div class="ui checkbox">
                            <input type="checkbox" name="send_account_information" {{ request()->old('send_account_information')?'checked':'' }}>
                            <label>{{ __("Hantar Maklumat Akaun Melalui Emel") }}</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2"></label>
                    <div class="col-sm-10">
                        <div class="ui checkbox">
                            <input type="checkbox" name="must_change_password" {{ request()->old('must_change_password')?'checked':'' }}>
                            <label>{{ __("Tekan Untuk Menukar Kata Laluan Pada Login Pertama") }}</label>
                        </div>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">{{ __("Save") }}</button>
                    </div>
                </div>
            @endcomponent

        @endcomponent

    </div>

@endsection

@push('end')
<script>
    $(function(){
        $('[data-role="randomize-password"]').on('click', function(){
            document.getElementById('password').setAttribute('value', Math.random().toString(36).substr(2,8));
        });
    });
</script>
@endpush
