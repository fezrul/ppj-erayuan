@extends('epicentrum::edit', ['tab' => 'password'])

@section('content-user-edit')

    <div class="mb-5">
        <h4>{{ __("Manual") }}</h4>
        <p>{{ __("Pengguna akan mendapat e-mel yang mengandungi pautan untuk menetapkan semula kata laluan. Pengguna mesti mengisi kata laluan baru mereka sendiri.") }}</p>
        <form action="{{ route('epicentrum::password.reset', [$user['id']]) }}" method="POST">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-default" href="">{{ __("Hantar Pautan Untuk Menetap Kata Laluan") }}</button>
        </form>
    </div>

    <h4>{{ __("Manual") }}</h4>
    <p>{{ __("Pengguna akan mendapat e-mel yang mengandungi pautan untuk menetapkan semula kata laluan. Pengguna mesti mengisi kata laluan baru mereka sendiri.") }}</p>
    {!! SemanticForm::open()->post()->action(route('epicentrum::password.generate', $user['id'])) !!}
    {{ csrf_field() }}
    <div class="form-group">
        <label class="form-check-label">
            <input class="form-check-input" type="checkbox" name="must_change_password" {{ request()->old('must_change_password')?'checked':'' }}>
            {{ __("Tekan Untuk Menukar Kata Laluan Pada Login Pertama") }}
        </label>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-default" href="">{{ __("Hantar Kata Laluan Baru") }}</button>
    </div>
    {!! SemanticForm::close() !!}
@endsection
