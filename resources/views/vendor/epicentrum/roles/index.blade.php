@extends(config('laravolt.epicentrum.view.layout'))

@section('content')

    <div class="my-5">
        <div class="card" style="overflow-x: auto;">
            <div class="card-header">
                <h5 class="mb-0">{{ __("Peranan") }}</h5>
            </div>
            <div class="card-body">
                @foreach($roles->chunk(4) as $rows)
                    <div class="row mb-3">
                        @foreach($rows as $role)
                            <div class="col-3">
                                <div class="b-a-1 p-3 text-center">
                                    <h3><a href={{ route('epicentrum::roles.edit',$role['id']) }}>{{ $role['name'] }}</a></h3>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
