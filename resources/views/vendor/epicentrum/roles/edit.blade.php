@extends(config('laravolt.epicentrum.view.layout'))

@section('content')
<div class="my-5">
    <div class="card">
        <div class="card-header">
            <h2 class="ui header">{{ __("Kemas Kini Peranan dan Hak Akses") }}</h2>
            <a href="{{ route('epicentrum::roles.index') }}"><i class="fa fa-arrow-left"></i>{{ __(" Kembali Ke Peranan") }}</a>
        </div>

        <div class="card-body">

            {{ html()->form('PUT', route('epicentrum::roles.update', $role['id']))->open() }}
            {{ html()->formGroup(__("Nama"), html()->input('text', 'name', $role['name'])->placeholder(__("Nama"))->class(['form-control'])) }}

            <table class="table">
                <thead>
                <tr>
                    <th width="300px">
                        <div class="form-check" data-toggle="checkall" data-selector=".checkbox[data-type='check-all-child']">
                            {{ html()->checkbox('permissions[]')->class(['form-check-input'])->value('0') }}
                            <label class="form-check-label"><strong>{{ __("Hak Akses") }}</strong></label>
                            {{ html()->hidden('permissions[]')->value('0') }}
                        </div>
                    </th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                @foreach($permissions as $permission)
                    <tr>
                        <td>
                            <div class="form-check" data-type="check-all-child">
                                {{ html()->checkbox('permissions[]', (in_array($permission->id, $assignedPermissions))?'checked=checked':'')->class(['form-check-input'])->value( $permission->id ) }}
                                <label class="form-check-label">{{ $permission->name }}</label>
                            </div>
                        </td>
                        <td>
                            {{ $permission->description }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="card-footer">
            {{ html()->submit(__("Hantar"))->class(['btn btn-primary'])->value('1') }}
            {{ html()->a(route('epicentrum::roles.index'),__("Batalkan"))->class(['btn btn-link']) }}
        </div>
            {{ html()->form()->close() }}
    </div>
</div>
@endsection
