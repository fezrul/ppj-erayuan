@extends(config('laravolt.epicentrum.view.layout'))

@section('content')

    <a href="{{ route('epicentrum::roles.index') }}" class="ui button mini"><i class="icon angle left"></i> {{ __("Kembali Kepada Peranan") }}</a>

    <div class="ui segment very padded">

        <h2 class="ui header">{{ __("Buat Peranan Baru") }}</h2>

        {!! SemanticForm::open()->post()->action(route('epicentrum::roles.store')) !!}
        {!! SemanticForm::text('name', old('name'))->label({{ __("Name") }})->required() !!}

        <table class="ui table">
            <thead>
            <tr>
                <th>
                    <div class="ui checkbox" data-toggle="checkall" data-selector=".checkbox[data-type='check-all-child']">
                        <input type="checkbox">
                        <label><strong>{{ __("Kebenaran") }}</strong></label>
                    </div>
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($permissions as $permission)
                <tr>
                    <td>
                        <div class="ui checkbox" data-type="check-all-child">
                            <input type="checkbox" name="permissions[]" value="{{ $permission->id }}" {{ (false)?'checked=checked':'' }}>
                            <label>{{ $permission->name }}</label>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="ui divider hidden"></div>

        <button class="ui button primary" type="submit" name="submit" value="1">{{ __("Save") }}</button>
        <a href="{{ route('epicentrum::roles.index') }}" class="ui button">{{ __("Cancel") }}</a>
        {!! SemanticForm::close() !!}

    </div>
@endsection
