@extends(config('laravolt.epicentrum.view.layout'))
@section('content')

    <div class="my-5">
        <div class="card">
            <div class="card-header">
                <a href="{{ route('epicentrum::users.index') }}" class="btn btn-link"><i class="fa fa-long-arrow-left"></i> {{ __("Back") }}</a>
            </div>
            <div class="card-body bg-light">
                <img class="avatar avatar-status" src="{{ $user->present('avatar') }}" alt=""> {{ $user->present('name') }}
            </div>
            <div class="card-body my-3">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link {{ ($tab == 'account')?'active':'' }}" href="{{ route('epicentrum::account.edit', $user['id']) }}">{{ __("Akaun") }}</a>
                    </li>
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link {{ ($tab == 'password')?'active':'' }}" href="{{ route('epicentrum::password.edit', $user['id']) }}">{{ __("Kata Laluan") }}</a>--}}
                    {{--</li>--}}
                </ul>
                <div class="tab-content">
                    <div class="p-5">
                        @yield('content-user-edit')
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
