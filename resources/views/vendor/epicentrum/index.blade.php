@extends(config('laravolt.epicentrum.view.layout'))

@section('content')

    <div class="my-5">
        <div class="card" style="overflow-x: auto;">
            <div class="card-header">
                
                <div class="card-actions">
                    <a style="margin-left: -15px !important;" href="{{ route('epicentrum::users.create') }}"><i style="margin-left: -15px !important;" class="fa fa-plus"></i> {{ __("Tambah") }}</a>
                    <a style="margin-left: -15px !important;" href="{{ route('epicentrum::users-pdf.index', ['search'=>request(config('suitable.query_string.search'))]) }}" target="blank"><i style="margin-left: -15px !important;" class="fa fa-file-pdf-o"></i> {{ __("Print PDF") }}</a>
                    <a style="margin-left: -15px !important;" href="{{ route('epicentrum::csv.user', ['search'=>request('search')]) }}" target="blank"><i style="margin-left: -15px !important;" class="fa fa-file-excel-o"></i> {{ __("Eksport CSV") }}</a>
                </div>
                <p></p><br>
                <h5 class="mb-0">{{ __("Pengguna") }}</h5>
            </div>
            <div class="card-header">
                <form method="GET" action="{{ request()->fullUrl() }}">
                    <div class="input-group">
                        <input class="form-control" name="{{ config('suitable.query_string.search') }}" value="{{ request(config('suitable.query_string.search')) }}" type="text" placeholder="{{ __("Carian...") }}">
                        <span class="input-group-btn"><button class="btn btn-secondary" type="submit">{{ __("Carian") }}</button></span>
                    </div>
                </form>
            </div>
            {{--<div class="card-header">--}}
                {{--@if($trashed)--}}
                    {{--<a class="item" href="{{ route('epicentrum::users.index') }}">Hide Trashed</a>--}}
                {{--@else--}}
                    {{--<a class="item" href="{{ route('epicentrum::users.index', ['trashed' => 1]) }}">Show Trashed</a>--}}
                {{--@endif--}}
            {{--</div>--}}
            <div class="card-body">
            @if ($users->count() > 0 )
                <table class="table">
                    <thead>
                    <tr>
                        <th>{{ __("Nama") }}</th>
                        <th>{{ __("Emel") }}</th>
                        <th>{{ __("Peranan") }}</th>
                        <th>{{ __("Status") }}</th>
                        <th>{{ __("Tarikh Daftar") }}</th>
                        <th width="15%"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>
                                <img width="30px" src="{{ Laravolt\Avatar\Facade::create($user->name)->toBase64() }}" alt="">
                                {{ $user->name }}
                            </td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->roles->implode('name', ', ') }}</td>
                            <td>{{ $user->getStatusLabel() }}</td>
                            <td>{{ $user->created_at->format('j F Y') }}</td>
                            <td>
                                {{ html()->form('DELETE', route('epicentrum::users.destroy',$user))->open() }}
                                    <a class='btn btn-secondary btn-sm' href='{{ route('epicentrum::users.edit', $user->id) }}'>{{ __("Kemas Kini") }}</a>
                                    {{ html()->input('submit', '')->value(__("Padam"))->addClass('btn btn-secondary btn-sm padam') }}
                                {{ html()->form()->close() }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                @include('components.empty')
            @endif
            </div>
            <div class="card-footer">
                {{ $users->links() }}
            </div>
        </div>
    </div>
    @include('components.modal-delete-confirmation', ['label' => __('Padam data Pengguna')])
@endsection

@push('end')
<script>
    $(function(){
       var modal = $("#modal-delete-confirmation");
       var submit = $("#submit");
       var padam = $(".padam");
       padam.click(function (e) {
            var form = $(this).closest('form');
            e.preventDefault();
            modal.modal('show');

            submit.click(function () {
                form.submit();
            });
       })
    });
</script>
@endpush
