@extends('epicentrum::edit', ['tab' => 'account'])

@section('content-user-edit')
    {!! SemanticForm::open()->put()->action(route('epicentrum::account.update', $user['id'])) !!}

    @component('components.fields', ['fields' => [
        ['label' => __("Nama"), 'name' => 'name', 'value' => $user['name']],
        ['label' => __("Emel"), 'name' => 'email', 'value' => $user['email']],
    ]])
    @endcomponent

    <div class="form-group row">
        <label class="col-sm-2">{{ __('Peranan') }}</label>
        <div class="col-sm-10">
            @foreach($roles as $role)
                <div class=form-check>
                    <label class="form-check-label">
                        <input class="form-check-input" type="{{ $multipleRole?'checkbox':'radio' }}" name="roles[]"
                               value="{{ $role->id }}" {{ ($user->hasRole($role))?'checked=checked':'' }}>
                        {{ $role->name }}
                    </label>
                </div>
            @endforeach
        </div>
    </div>


    <div class="form-group row">
        <div class="col-sm-2"></div>
        <div class="col-sm-10">
            <button class="btn btn-primary" type="submit" name="submit" value="1">{{ __("Save") }}</button>
            <a href="{{ route('epicentrum::users.index') }}" class="btn btn-link">{{ __("Cancel") }}</a>
        </div>
    </div>
    {!! SemanticForm::close() !!}

    {{--<div class="b-t-1 p-5 mt-5">--}}
        {{--<h3>{{ __("Padam Akaun") }}</h3>--}}

        {{--@if($user['id'] == auth()->id())--}}
            {{--<div class="alert alert-warning">{{ __("Anda Tidak Dibenarkan Memadamkan Diri Anda. Sila Minta Pentadbir Lain Untuk Melakukannya.") }}</div>--}}
        {{--@else--}}
            {{--{!! SemanticForm::open()->delete()->action(route('epicentrum::users.destroy', $user['id'])) !!}--}}
            {{--<button class="btn btn-danger" type="submit" name="submit" value="1"--}}
                    {{--onclick="return confirm('__("Adakah anda pasti memadamkan akaun ini?")')')">{{ __("Delete") }}</button>--}}
            {{--{!! SemanticForm::close() !!}--}}
        {{--@endif--}}
    {{--</div>--}}

@endsection
