@extends(config('laravolt.auth.layout'))

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="m-0">{{ __("Lupa Kata Laluan") }}</h4>
                </div>
                <div class="card-body p-4">
                    @if (session('status'))
                        <?php flash()->success(session('status')); ?>
                    @endif

                    <form class="ui form" method="POST" action="{{ route('auth::forgot') }}">
                        {{ csrf_field() }}

                        <div class="input-group mb-3">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <input class="form-control" type="text" name="email" placeholder="{{ __("Emel") }}" value="{{ old('email') }}">
                        </div>

                        <div class="row">
                            <div class="col">
                                <button type="submit"
                                        class="btn btn-primary">{{ __("Hantar Semula Pautan Kata Laluan") }}</button>
                            </div>
                            <div class="col text-right">
                                @if(config('laravolt.auth.registration.enable'))
                                    <div class="ui divider hidden section"></div>
                                    {{ __("Belum Didaftarkan Lagi?") }} <a href="{{ route('auth::register') }}">{{ __("Daftar Disini") }}</a>
                                @endif
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
