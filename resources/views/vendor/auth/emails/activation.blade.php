{{ __("Please click <a href=":link">this activation link</a> to activate your account.",['link' => route('auth::activate', $token)]) }}
