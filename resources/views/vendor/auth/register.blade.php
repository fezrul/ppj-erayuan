@extends(config('laravolt.auth.layout'))

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="m-0">{{ __("Pendaftaran") }}</h4>
                </div>
                <div class="card-body p-4">
                    <form class="" method="POST" action="{{ route('auth::register') }}">
                        {{ csrf_field() }}

                        <div class="input-group mb-3">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input class="form-control" type="text" name="name" placeholder="{{ __("Nama") }}" value="{{ old('name') }}">
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                            <input class="form-control" type="email" name="email" placeholder="{{ __("Emel") }}"
                                   value="{{ old('email') }}">
                        </div>
                        <div class="input-group mb-3">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input class="form-control" type="password" name="password" placeholder="{{ __("Kata Laluan") }}">
                        </div>
                        <div class="row">
                            <div class="col">
                                <button type="submit" class="btn btn-primary">{{ __("Pendaftaran") }})</button>
                            </div>
                            <div class="col text-right">
                                {{ __("Mempunyai Akaun?") }} <a
                                        href="{{ route('auth::login') }}">{{ __("Daftar Masuk Sini") }}</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
