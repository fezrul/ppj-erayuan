@extends(config('laravolt.auth.layout'))

@section('content')

    @php($suggestedUser =  \App\User::find(request('id')))

    <div class="card-group" style="background-color: white;">
        <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="card-body m-0">

                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __("Log Masuk Awam") }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link bg-light active" href="{{ route('auth::login') }}">{{ __("Login Kakitangan") }}</a>
                    </li>
                </ul>
                <div class="tab-content p-5l bg-light">
                    {{ html()->form('POST', route('auth::login'))->class(['ui form'])->open() }}
                        <h2 class="mb-4">{{ __("Login Kakitangan") }}</h2>


                            <div class="cards">
                                <div class="cards-header cards-chart" data-background-color="orange" style="padding:20px">
                                        <div class="input-group mb-3">
                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                            {{ html()->text('email')->placeholder(__("Nama Pengguna"))->class(['form-control']) }}
                                        </div>
                                        <div class="input-group mb-3">
                                            <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                            {{ html()->password('password')->placeholder(__("Kata Laluan"))->class(['form-control']) }}
                                        </div>
                                        @if(config('laravolt.auth.captcha'))
                                            <div class="form-group mb-3">
                                                {!! app('captcha')->display() !!}
                                            </div>
                                        @endif

                                </div>
                                <div class="cards-content">
                                    <h4 class="title"></h4>
                                    <p class="category"><div class="form-group">
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    {{ html()->checkbox('remember')->class(['form-check-input']) }}
                                    {{ __("Ingat Saya") }}
                                </label>
                            </div>
                        </div></p>
                                </div>
                                <div class="cards-footer">
                                    <div class="stats" style="display: block;">

                            {{ html()->submit(__("Log Masuk"))->class(['btn btn-primary px-4 btn-block']) }}

                                    </div>
                                </div>
                            </div>

                        <div class="form-group row">
                            <div class="col-6">
                                {{ html()->a(route('auth::forgot'), __("Lupa Kata Laluan"))->class(['btn btn-link px-0']) }}
                            </div>
                            <div class="col-6 text-right">
                                {{ html()->a(route('citizenship.index'), __("Daftar"))->class(['btn btn-link px-0']) }}
                            </div>
                        </div>



                    {{ html()->form()->close() }}

























                </div>
            </div>
        </div>
             <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="card-body">
                {{ html()->form('GET', route('semakan-kompaun.index'))->class(['container-fluid'])->open() }}
                {{-- <form action="" class="container-fluid"> --}}

                    <div class="mb-3">
                        <h2>{{ __("Semakan Kompaun") }}</h2>
                        @include('components.panduan-semakan')
                    </div>
                    <div class="form-group">
                        <label>{{ __("Jenis Carian") }}</label>
                        <select name="type" class="form-control" >
                            @foreach(\Ekompaun\Systemconfig\Model\Lookup\SearchParameter::whereParamStatus(1)->get() as $item)
                                <option value="{{ $item->getKey() }}">{{ $item->param_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ __("Maklumat Carian") }}</label>
                        {{ html()->text('search')->class(['form-control'])->required('true')->style('text-transform:uppercase') }}
                    </div>
                    <div class="form-group">
                        {{ html()->submit(__("Hantar"))->class(['btn btn-primary']) }}
                    </div>
                {{ html()->form()->close() }}
            </div>
        </div>
    </div>
@endsection
