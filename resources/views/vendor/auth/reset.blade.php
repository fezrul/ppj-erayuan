@extends(config('laravolt.auth.layout'))

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="m-0">{{ __("Menetapkan Semula Kata Laluan") }}</h4>
                </div>
                <div class="card-body p-4">
                    @if (session('status'))
                        <?php flash()->success(session('status')); ?>
                    @endif

                    <form class="ui form" method="POST" action="{{ route('auth::reset', $token) }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="input-group mb-3">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <input class="form-control" type="text" name="email" placeholder="{{ __("Emel") }}" value="{{ old('email', urldecode(request('email'))) }}">
                        </div>

                        <div class="input-group mb-3">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <input class="form-control" type="password" name="password" placeholder="{{ __("Kata Laluan Baharu") }}">
                        </div>

                        <div class="input-group mb-3">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <input class="form-control" type="password" name="password_confirmation" placeholder="{{ __("Sahkan Kata Laluan Baru") }}">
                        </div>

                        <div class="row">
                            <div class="col">
                                <button type="submit"
                                        class="btn btn-primary">{{ __("Menetapkan Semula Kata Laluan") }}</button>
                            </div>
                            <div class="col text-right">
                                {{ __("Mempunyai Akaun") }} <a href="{{ route('auth::login') }}">{{ __("Daftar Masuk Sini") }}</a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
