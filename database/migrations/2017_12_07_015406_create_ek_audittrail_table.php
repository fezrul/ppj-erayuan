<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEkAudittrailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ek_audittrail', function(Blueprint $table)
		{
			$table->increments('audt_id');
			$table->unsignedInteger('fk_users')->unsigned()->index('ek_audittrail_ibfk_1');
			$table->unsignedInteger('fk_lkp_task')->index('ek_audittrail_ibfk_2');
			$table->integer('table_ref_id')->nullable();
			$table->string('table_ref_type')->nullable();
			$table->string('task')->nullable();
			$table->integer('created_by');
			$table->dateTime('created_date')->nullable();
			$table->integer('modify_by')->nullable();
			$table->dateTime('modify_date')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ek_audittrail');
	}

}
