<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLkpCustomPeriodTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lkp_custom_period', function(Blueprint $table)
		{
			$table->increments('custom_id');
			$table->string('custom_code', 10);
			$table->string('custom_name');
			$table->integer('custom_period');
			$table->integer('custom_haribekerja')->comment('0-hari bekerja tak dikira 1-hari bekerja dikira');
			$table->integer('custom_status')->comment('0-inactive, 1-active');
			$table->integer('created_by')->nullable();
			$table->dateTime('created_date')->nullable();
			$table->integer('modify_by')->nullable();
			$table->dateTime('modify_date')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->dateTime('deleted_date')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lkp_custom_period');
	}

}
