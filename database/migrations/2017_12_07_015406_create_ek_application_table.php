<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEkApplicationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ek_application', function(Blueprint $table)
		{
			$table->increments('appl_id');
			$table->unsignedInteger('appl_transid')->index('ek_application_ibfk_1')->comment('ek_transaction');
			$table->string('appl_reason', 5000)->nullable();
			$table->integer('appl_result')->nullable();
			$table->integer('appl_status_sap_prev')->nullable();
			$table->unsignedInteger('appl_status_sap')->nullable()->index('appl_status_sap')->comment('lkp_status_sap');
			$table->unsignedInteger('appl_status')->nullable()->index('appl_status')->comment('lkp_status');
			$table->unsignedSmallInteger('appl_type')->default(\Ekompaun\Systemconfig\Enum\AppealType::MENGIKUTI_JADUAL)->index('appl_type')->comment('see '.\Ekompaun\Systemconfig\Enum\AppealType::class);
			$table->unsignedSmallInteger('appl_channel')->default(\Ekompaun\Systemconfig\Enum\AppealChannel::ONLINE)->comment('see '.\Ekompaun\Systemconfig\Enum\AppealChannel::class);
            $table->string('appl_email')->nullable();
			$table->string('appl_phonenumber', 20)->nullable();
			$table->dateTime('appl_date');
			$table->integer('appl_personincharge')->nullable()->comment('users');
			$table->decimal('appl_approveamount', 15)->nullable();
			$table->integer('appl_approveby')->nullable()->comment('users');
            $table->string('appl_remark', 1000)->nullable();
			$table->dateTime('appl_decisiondate')->nullable();
			$table->unsignedInteger('fk_campid')->nullable()->index('fk_campid')->comment('ek_campaign_master');
			$table->decimal('appl_campamount', 15)->nullable();
			$table->decimal('appl_gst', 15)->default(0)->nullable();
			$table->integer('created_by')->nullable()->comment('users');
			$table->dateTime('created_date')->nullable();
			$table->integer('modify_by')->nullable()->comment('users');
			$table->dateTime('modify_date')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ek_application');
	}

}
