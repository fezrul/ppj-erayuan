<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUserProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_profiles', function(Blueprint $table)
		{
			$table->foreign('fk_users', 'user_profiles_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('fk_lkp_country', 'user_profiles_ibfk_2')->references('country_id')->on('lkp_country')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('fk_lkp_state', 'user_profiles_ibfk_3')->references('state_id')->on('lkp_state')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_profiles', function(Blueprint $table)
		{
			$table->dropForeign('user_profiles_ibfk_1');
			$table->dropForeign('user_profiles_ibfk_2');
			$table->dropForeign('user_profiles_ibfk_3');
		});
	}

}
