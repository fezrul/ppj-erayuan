<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEkTransactionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ek_transaction', function(Blueprint $table)
		{
			$table->foreign('fk_userid', 'ek_transaction_ibfk_4')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ek_transaction', function(Blueprint $table)
		{
			$table->dropForeign('ek_transaction_ibfk_4');
		});
	}

}
