<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEkNotificationSmsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ek_notification_sms', function(Blueprint $table)
		{
			$table->foreign('fk_applid', 'ek_notification_sms_ibfk_1')->references('appl_id')->on('ek_application')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('fk_userid', 'ek_notification_sms_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ek_notification_sms', function(Blueprint $table)
		{
			$table->dropForeign('ek_notification_sms_ibfk_1');
			$table->dropForeign('ek_notification_sms_ibfk_2');
		});
	}

}
