<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEkPaymentFpxTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ek_payment_fpx', function(Blueprint $table)
		{
			$table->increments('pfpx_id');
			$table->unsignedInteger('fk_pymtid')->unsigned()->index('ek_payment_fpx_ibfk_1');
			$table->unsignedInteger('fk_applid')->index('ek_payment_fpx_ibfk_2');
			$table->unsignedInteger('fk_users')->unsigned()->index('ek_payment_fpx_ibfk_3');
			$table->unsignedInteger('fk_tranid')->index('fk_tranid');
			$table->string('bank', 100);
			$table->string('pfpx_serialno', 30)->nullable();
			$table->string('pfpx_transid', 30)->nullable();
			$table->dateTime('pfpx_date')->nullable();
			$table->decimal('pfpx_totalamount', 15)->nullable();
			$table->decimal('pfpx_amountpaid', 15)->nullable();
			$table->integer('pfpx_status')->nullable();
			$table->dateTime('pfpx_transdate')->nullable();
			$table->integer('created_by')->nullable();
			$table->dateTime('created_date');
			$table->integer('updated_by')->nullable();
			// $table->dateTime('updated_date');
			$table->integer('deleted_by')->nullable();
			$table->dateTime('modify_date')->nullable();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ek_payment_fpx');
	}

}
