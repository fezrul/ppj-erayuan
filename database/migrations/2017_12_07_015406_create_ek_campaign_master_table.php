<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEkCampaignMasterTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ek_campaign_master', function(Blueprint $table)
		{
			$table->increments('camp_id');
			$table->string('camp_name', 100);
			$table->integer('camp_ratetype')->comment('1-kadar tetap,2-kadar diskaun');
			$table->dateTime('camp_startdate');
			$table->dateTime('camp_enddate');
			$table->string('camp_filename', 155)->nullable();
			$table->text('camp_content', 65535)->nullable();
			$table->integer('camp_status')->default(1)->comment('1-active,2-inactive');
			$table->integer('created_by')->nullable();
			$table->dateTime('created_date')->nullable();
			$table->integer('modify_by')->nullable();
			$table->dateTime('modify_date')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->dateTime('deleted_date')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ek_campaign_master');
	}

}
