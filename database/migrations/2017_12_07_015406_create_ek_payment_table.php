<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEkPaymentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ek_payment', function(Blueprint $table)
		{
			$table->increments('pymt_id');
			$table->unsignedInteger('fk_paymentmethod')->index('ek_payment_ibfk_1');
			$table->unsignedInteger('fk_applid')->index('ek_payment_ibfk_3');
			$table->unsignedInteger('fk_users')->unsigned()->index('ek_payment_ibfk_2');
			$table->unsignedInteger('fk_tranid')->index('ek_payment_ibfk_4');
			$table->decimal('pymt_totalamount', 15)->default(0);
            $table->decimal('pymt_received', 15)->default(0);
            $table->string('pymt_receiptnumber', 50)->nullable();
			$table->string('pymt_refno', 50)->nullable();
			$table->dateTime('pymt_receiptdate')->nullable();
			$table->string('pymy_cek', 30)->nullable();
			$table->string('pymt_bank', 55)->nullable();
			$table->integer('pymt_status')->nullable();
			$table->integer('created_by')->nullable();
			$table->dateTime('created_date')->nullable();
			$table->integer('updated_by')->nullable();
			$table->dateTime('updated_date')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->dateTime('deleted_date')->nullable();
			$table->dateTime('modify_date')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ek_payment');
	}

}
