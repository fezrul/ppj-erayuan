<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEkTransactionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ek_transaction', function(Blueprint $table)
		{
			$table->increments('tran_id');
			$table->unsignedInteger('fk_rawsapid')->unsigned();
			$table->unsignedInteger('fk_userid')->unsigned()->nullable()->index('fk_userid');
            $table->string('fk_lkp_sapstatus', 10)->nullable()->index('fk_lkp_sapstatus')->comment('lkp_status_sap');
            $table->string('fk_lkp_offencecode', 15)->nullable()->index('fk_lkp_offencecode')->comment('lkp_offence');
			$table->string('tran_offendername', 100)->nullable();
			$table->string('tran_offendericno', 20)->nullable();
			$table->string('tran_compoundno', 10);
			$table->dateTime('tran_compounddate')->nullable();
			$table->decimal('tran_compoundamount', 15)->nullable();
			$table->string('tran_place', 500)->nullable();
			$table->string('tran_refno', 50)->nullable();
			$table->string('tran_vehicleno', 10)->nullable();
			$table->unsignedInteger('fk_lkp_vehicletype')->nullable()->index('fk_lkp_vehicletype')->comment('lkp_vehicle');
			$table->string('tran_vehiclecolor', 30)->nullable();
			$table->integer('created_by')->nullable();
			$table->dateTime('created_date')->nullable();
			$table->integer('modify_by')->nullable();
			$table->dateTime('modify_date')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->dateTime('deleted_date')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ek_transaction');
	}

}
