<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEkAudittrailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ek_audittrail', function(Blueprint $table)
		{
			$table->foreign('fk_users', 'ek_audittrail_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('fk_lkp_task', 'ek_audittrail_ibfk_2')->references('task_id')->on('lkp_task')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ek_audittrail', function(Blueprint $table)
		{
			$table->dropForeign('ek_audittrail_ibfk_1');
			$table->dropForeign('ek_audittrail_ibfk_2');
		});
	}

}
