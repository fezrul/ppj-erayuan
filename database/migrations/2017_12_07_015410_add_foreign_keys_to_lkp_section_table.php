<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToLkpSectionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('lkp_section', function(Blueprint $table)
		{
			$table->foreign('fk_lkp_actid', 'lkp_section_ibfk_1')->references('act_id')->on('lkp_act')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('lkp_section', function(Blueprint $table)
		{
			$table->dropForeign('lkp_section_ibfk_1');
		});
	}

}
