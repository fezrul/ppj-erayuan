<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToLkpOffenceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('lkp_offence', function(Blueprint $table)
		{
			$table->foreign('fk_lkp_actid', 'lkp_offence_ibfk_1')->references('act_id')->on('lkp_act')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('fk_lkp_sectionid', 'lkp_offence_ibfk_2')->references('section_id')->on('lkp_section')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('lkp_offence', function(Blueprint $table)
		{
			$table->dropForeign('lkp_offence_ibfk_1');
			$table->dropForeign('lkp_offence_ibfk_2');
		});
	}

}
