<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLkpStatusSapTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lkp_status_sap', function(Blueprint $table)
		{
			$table->increments('status_id');
			$table->integer('status_mandt')->nullable();
			$table->string('status_code', 10);
			$table->string('status_name', 100);
			$table->integer('appeal_type');
			$table->string('status_desc', 100)->nullable();
			$table->integer('created_by')->nullable();
			$table->dateTime('created_date')->nullable();
			$table->integer('modify_by')->nullable();
			$table->dateTime('modify_date')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->dateTime('deleted_date')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lkp_status_sap');
	}

}
