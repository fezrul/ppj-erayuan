<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEkCampaignDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ek_campaign_detail', function(Blueprint $table)
		{
			$table->foreign('fk_campid', 'ek_campaign_detail_ibfk_1')->references('camp_id')->on('ek_campaign_master')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ek_campaign_detail', function(Blueprint $table)
		{
			$table->dropForeign('ek_campaign_detail_ibfk_1');
		});
	}

}
