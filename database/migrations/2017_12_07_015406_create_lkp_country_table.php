<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLkpCountryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lkp_country', function(Blueprint $table)
		{
			$table->increments('country_id');
			$table->string('country_name', 50);
			$table->string('country_code', 11);
			$table->integer('country_status')->default(1)->comment('0-inactive, 1-active');
			$table->integer('created_by')->nullable();
			$table->dateTime('created_date')->nullable();
			$table->integer('modify_by')->nullable();
			$table->dateTime('modify_date')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->dateTime('deleted_date')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lkp_country');
	}

}
