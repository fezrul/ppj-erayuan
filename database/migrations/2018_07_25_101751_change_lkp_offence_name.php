<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeLkpOffenceName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lkp_offence', function (Blueprint $table) {
            $table->dropIndex('legaltype');
        });
        Schema::table('lkp_offence', function (Blueprint $table) {
            $table->text('offence_name')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lkp_offence', function (Blueprint $table) {
            $table->string('offence_name', 500)->change();
        });
        Schema::table('lkp_offence', function (Blueprint $table) {
            $table->index('offence_name', 'legaltype');
        });
    }
}
