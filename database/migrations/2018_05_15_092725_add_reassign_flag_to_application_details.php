<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReassignFlagToApplicationDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ek_application_details', function (Blueprint $table) {
            $table->unsignedTinyInteger('detl_reassign')->after('detl_assignpic')->default(\Ekompaun\Systemconfig\Enum\AssignmentType::NORMAL)->comment('0=Normal assignment, 1=Reassign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ek_application_details', function (Blueprint $table) {
            $table->dropColumn('detl_reassign');
        });
    }
}
