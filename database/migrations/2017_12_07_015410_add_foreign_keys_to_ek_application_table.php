<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEkApplicationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ek_application', function(Blueprint $table)
		{
			$table->foreign('appl_transid', 'ek_application_ibfk_1')->references('tran_id')->on('ek_transaction')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('appl_status', 'ek_application_ibfk_2')->references('status_id')->on('lkp_status')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('appl_status_sap', 'ek_application_ibfk_3')->references('status_id')->on('lkp_status_sap')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('fk_campid', 'ek_application_ibfk_4')->references('camp_id')->on('ek_campaign_master')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ek_application', function(Blueprint $table)
		{
			$table->dropForeign('ek_application_ibfk_1');
			$table->dropForeign('ek_application_ibfk_2');
			$table->dropForeign('ek_application_ibfk_3');
			$table->dropForeign('ek_application_ibfk_4');
		});
	}

}
