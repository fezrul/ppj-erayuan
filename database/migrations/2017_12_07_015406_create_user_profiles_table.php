<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_profiles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('fk_users')->unsigned()->nullable()->index('fk_users');
			$table->unsignedInteger('fk_lkp_country')->nullable()->index('fk_lkp_country');
			$table->unsignedInteger('fk_lkp_state')->nullable()->index('fk_lkp_state');
			$table->integer('user_category')->default(\Ekompaun\Systemconfig\Enum\UserCategory::INDIVIDUAL)->comment('1-individual, 2-company');
			$table->integer('user_citizenship')->default(\Ekompaun\Systemconfig\Enum\Citizenship::RESIDENT)->comment('1-no.ic, 2-no.pasport');
			$table->string('user_companyssmno', 20)->nullable();
			$table->string('user_premisno', 20)->nullable();
			$table->string('user_referenceid')->nullable();
			$table->string('user_address1', 100)->nullable();
			$table->string('user_address2', 100)->nullable();
			$table->string('user_address3', 100)->nullable();
			$table->string('user_postcode', 10)->nullable();
			$table->string('user_mobileno', 100)->nullable();
			$table->string('user_phoneno', 20)->nullable();
			$table->string('user_officeno', 20)->nullable();
			$table->string('user_vehicleno1', 11)->nullable();
			$table->string('user_vehicleno2', 11)->nullable();
			$table->string('user_vehicleno3', 11)->nullable();
			$table->string('user_staffid', 10)->nullable();
			$table->string('user_department', 100)->nullable();
			$table->timestamp('created_date')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('created_by')->nullable()->comment('user id');
			$table->dateTime('modify_date')->nullable();
			$table->integer('modify_by')->nullable()->comment('user id');
			$table->timestamp('deleted_date')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('deleted_by')->nullable()->comment('user id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_profiles');
	}

}
