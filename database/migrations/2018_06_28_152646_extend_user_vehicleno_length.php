<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendUserVehiclenoLength extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_profiles', function (Blueprint $table) {
            $table->string('user_vehicleno1', 30)->nullable()->change();
            $table->string('user_vehicleno2', 30)->nullable()->change();
            $table->string('user_vehicleno3', 30)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_profiles', function (Blueprint $table) {
            $table->string('user_vehicleno1', 11)->nullable()->change();
            $table->string('user_vehicleno2', 11)->nullable()->change();
            $table->string('user_vehicleno3', 11)->nullable()->change();
        });
    }
}
