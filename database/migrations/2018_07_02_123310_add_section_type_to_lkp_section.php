<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSectionTypeToLkpSection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lkp_section', function (Blueprint $table) {
            if (!Schema::hasColumn('lkp_section', 'section_type')) {
                $table->integer('section_type')->default(1)->after('section_status')->comment('1-section 2-kaedah 3-perintah');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lkp_section', function (Blueprint $table) {
            $table->dropColumn('section_type');
        });
    }
}
