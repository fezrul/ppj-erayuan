<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEkAttachmentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ek_attachment', function(Blueprint $table)
		{
			$table->foreign('fk_applid', 'ek_attachment_ibfk_1')->references('appl_id')->on('ek_application')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ek_attachment', function(Blueprint $table)
		{
			$table->dropForeign('ek_attachment_ibfk_1');
		});
	}

}
