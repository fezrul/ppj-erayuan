<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEkCampaignDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ek_campaign_detail', function(Blueprint $table)
		{
			$table->increments('cdet_id');
			$table->unsignedInteger('fk_campid')->index('fk_campid');
			$table->string('cdet_offencedate_from', 4);
			$table->string('cdet_offencedate_to', 4);
			$table->string('cdet_offenceid', 500)->comment('lkp_offence')->nullable();
			$table->integer('cdet_vehicletype')->nullable()->comment('lkp_vehicle');
			$table->decimal('cdet_amount', 15);
            $table->integer('cdet_compoundstatus')->default(\Ekompaun\Systemconfig\Enum\AppealStatusType::NOTIS_KOMPAUN);
			$table->integer('cdet_status')->comment('1-Active 0-Inactive')->nullable();
			$table->integer('created_by')->nullable();
			$table->dateTime('created_date')->nullable();
			$table->integer('modify_by')->nullable();
			$table->dateTime('modify_date')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->dateTime('deleted_date')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ek_campaign_detail');
	}

}
