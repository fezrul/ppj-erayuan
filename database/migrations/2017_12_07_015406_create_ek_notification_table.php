<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEkNotificationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ek_notification', function(Blueprint $table)
		{
			$table->increments('ntfy_id');
			$table->integer('ntfy_userid')->unsigned()->index('ek_notification_ibfk_1');
			$table->string('ntfy_name', 100);
			$table->string('ntfy_subject')->nullable();
			$table->text('ntfy_content', 65535)->nullable();
			$table->unsignedInteger('fk_applid')->index('ek_notification_ibfk_2');
			$table->integer('created_by')->nullable();
			$table->dateTime('created_date')->nullable();
			$table->integer('modify_by')->nullable();
			$table->dateTime('modify_date')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ek_notification');
	}

}
