<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEkAttachmentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ek_attachment', function(Blueprint $table)
		{
			$table->increments('attc_id');
			$table->unsignedInteger('fk_applid')->index('ek_attachment_ibfk_1');
			$table->dateTime('attc_date')->nullable();
			$table->string('attc_filename', 155)->nullable();
			$table->integer('attc_status')->nullable();
			$table->integer('created_by')->nullable();
			$table->dateTime('created_date')->nullable();
			$table->integer('modify_by')->nullable();
			$table->dateTime('modify_date')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->dateTime('deleted_date')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ek_attachment');
	}

}
