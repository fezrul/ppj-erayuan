<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLkpZtranstypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lkp_ztranstype', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('fk_maintrans_id')->index('lkp_ztranstype_ibfk_1');
            $table->string('appltype')->nullable();
            $table->string('transtype')->nullable();
            $table->integer('subtrans')->nullable();
            $table->string('description')->nullable();
            $table->integer('created_by')->nullable();
            $table->dateTime('created_date')->nullable();
            $table->integer('modify_by')->nullable();
            $table->dateTime('modify_date')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->dateTime('deleted_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lkp_ztranstype');
    }
}
