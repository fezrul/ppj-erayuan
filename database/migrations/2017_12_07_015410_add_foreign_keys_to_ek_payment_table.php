<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEkPaymentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ek_payment', function(Blueprint $table)
		{
			$table->foreign('fk_paymentmethod', 'ek_payment_ibfk_1')->references('paymethod_id')->on('lkp_payment_method')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('fk_users', 'ek_payment_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('fk_applid', 'ek_payment_ibfk_3')->references('appl_id')->on('ek_application')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('fk_tranid', 'ek_payment_ibfk_4')->references('tran_id')->on('ek_transaction')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ek_payment', function(Blueprint $table)
		{
			$table->dropForeign('ek_payment_ibfk_1');
			$table->dropForeign('ek_payment_ibfk_2');
			$table->dropForeign('ek_payment_ibfk_3');
			$table->dropForeign('ek_payment_ibfk_4');
		});
	}

}
