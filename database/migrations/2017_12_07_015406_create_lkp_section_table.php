<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLkpSectionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lkp_section', function(Blueprint $table)
		{
			$table->increments('section_id');
			$table->unsignedInteger('fk_lkp_actid')->index('fk_actid')->comment('lkp_act');
			$table->string('section_code', 20);
			$table->string('section_name', 100);
			$table->integer('section_status')->default(1)->comment('0-tak aktif 1-aktif');
			$table->integer('section_type')->default(1)->comment('1-section 2-kaedah 3-perintah');
			$table->integer('created_by')->nullable();
			$table->dateTime('created_date')->nullable();
			$table->integer('modify_by')->nullable();
			$table->dateTime('modify_date')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->dateTime('deleted_date')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lkp_section');
	}

}
