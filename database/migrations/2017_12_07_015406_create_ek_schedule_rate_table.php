<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEkScheduleRateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ek_schedule_rate', function(Blueprint $table)
		{
			$table->increments('schd_id');
			$table->unsignedInteger('fk_lkp_period')->unsigned()->index('ek_schedule_rate_ibfk_1')->comment('lkp_period');
			$table->unsignedInteger('fk_lkp_offence')->index('ek_schedule_rate_ibfk_2')->comment('lkp_offence');
			$table->unsignedInteger('fk_lkp_vehicle')->nullable()->index('fk_lkp_vehicle')->comment('lkp_vehicle');
			$table->unsignedInteger('fk_lkp_act')->index('ek_schedule_rate_ibfk_4')->comment('lkp_act');
			$table->integer('schd_usercategory')->nullable()->comment('1- individu 2-company');
			$table->decimal('schd_amount', 15);
			$table->date('schd_startdate')->nullable();
			$table->date('schd_enddate')->nullable();
			$table->integer('schd_status')->nullable()->comment('1-active,0-inactive');
			$table->integer('created_by')->nullable();
			$table->dateTime('created_date')->nullable();
			$table->integer('modify_by')->nullable();
			$table->dateTime('modify_date')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->dateTime('deleted_date')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ek_schedule_rate');
	}

}
