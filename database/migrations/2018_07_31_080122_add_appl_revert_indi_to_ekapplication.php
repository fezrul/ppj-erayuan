<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApplRevertIndiToEkapplication extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ek_application', function (Blueprint $table) {
            $table->unsignedInteger('appl_revert_indi')->after('appl_gst')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ek_application', function (Blueprint $table) {
            $table->dropColumn('appl_revert_indi');
        });
    }
}
