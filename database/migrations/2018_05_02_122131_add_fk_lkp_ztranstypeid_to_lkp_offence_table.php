<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkLkpZtranstypeidToLkpOffenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('lkp_offence', function (Blueprint $table) {
            $table->unsignedInteger('fk_lkp_ztranstypeid')->index('fk_ztranstypeid')->after('offence_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
