<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSemakSapTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('semak_sap', function(Blueprint $table)
		{
			$table->foreign('fk_applid', 'semak_sap_ibfk_1')->references('appl_id')->on('ek_application')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('fk_lkp_taskid', 'semak_sap_ibfk_2')->references('task_id')->on('lkp_task')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('fk_lkp_parameterid', 'semak_sap_ibfk_3')->references('param_id')->on('lkp_search_parameter')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('fk_userid', 'semak_sap_ibfk_4')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('semak_sap', function(Blueprint $table)
		{
			$table->dropForeign('semak_sap_ibfk_1');
			$table->dropForeign('semak_sap_ibfk_2');
			$table->dropForeign('semak_sap_ibfk_3');
			$table->dropForeign('semak_sap_ibfk_4');
		});
	}

}
