<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEkScheduleRateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ek_schedule_rate', function(Blueprint $table)
		{
			$table->foreign('fk_lkp_period', 'ek_schedule_rate_ibfk_1')->references('period_id')->on('lkp_period')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('fk_lkp_offence', 'ek_schedule_rate_ibfk_2')->references('offence_id')->on('lkp_offence')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('fk_lkp_vehicle', 'ek_schedule_rate_ibfk_3')->references('vehicle_id')->on('lkp_vehicle')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('fk_lkp_act', 'ek_schedule_rate_ibfk_4')->references('act_id')->on('lkp_act')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ek_schedule_rate', function(Blueprint $table)
		{
			$table->dropForeign('ek_schedule_rate_ibfk_1');
			$table->dropForeign('ek_schedule_rate_ibfk_2');
			$table->dropForeign('ek_schedule_rate_ibfk_3');
			$table->dropForeign('ek_schedule_rate_ibfk_4');
		});
	}

}
