<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRawSapTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('raw_sap', function(Blueprint $table)
		{
			$table->foreign('semak_sapid', 'FK_raw_sap')->references('id')->on('semak_sap')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('raw_sap', function(Blueprint $table)
		{
			$table->dropForeign('FK_raw_sap');
		});
	}

}
