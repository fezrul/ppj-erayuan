<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApplSapRecieptIndi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ek_application', function (Blueprint $table) {
            $table->unsignedInteger('appl_reciept_indi')->after('appl_revert_indi')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ek_application', function (Blueprint $table) {
            $table->dropColumn('appl_reciept_indi');
        });
    }
}
