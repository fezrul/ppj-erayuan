<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRawSapTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('raw_sap', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('semak_sapid')->index('FK_raw_sap');
			$table->string('result', 5000)->nullable();
			$table->dateTime('created_date')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('raw_sap');
	}

}
