<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeOffenceidType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ek_campaign_detail', function (Blueprint $table) {
            $table->text('cdet_offenceid')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ek_campaign_detail', function (Blueprint $table) {
            $table->string('cdet_offenceid', 500)->comment('lkp_offence')->nullable()->change();
        });
    }
}
