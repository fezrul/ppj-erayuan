<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTranCompoundmountPrevToEktransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ek_transaction', function (Blueprint $table) {
            $table->decimal('tran_compoundamount_prev')->after('tran_compoundamount')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ek_transaction', function (Blueprint $table) {
            $table->dropColumn('tran_compoundamount_prev');
        });
    }
}
