<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLkpOffenceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lkp_offence', function(Blueprint $table)
		{
			$table->increments('offence_id');
			$table->unsignedInteger('fk_lkp_sectionid')->index('fk_sectionid')->comment('lkp_section');
			$table->unsignedInteger('fk_lkp_actid')->index('fk_actid')->comment('lkp_act');
			$table->string('offence_code', 15);
			$table->string('offence_name', 500)->index('legaltype');
			$table->integer('offence_status')->comment('0-inactive,1-active');
			$table->integer('created_by')->nullable();
			$table->dateTime('created_date')->nullable();
			$table->integer('modify_by')->nullable();
			$table->dateTime('modify_date')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->dateTime('deleted_date')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lkp_offence');
	}

}
