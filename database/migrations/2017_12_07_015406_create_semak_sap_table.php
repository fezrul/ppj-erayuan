<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSemakSapTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('semak_sap', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('status')->nullable()->comment('1-hantar 2-return');
			$table->unsignedInteger('fk_lkp_taskid')->nullable()->index('fk_lkp_taskid')->comment('lkp_task');
			$table->unsignedInteger('fk_userid')->unsigned()->nullable()->index('FK_semak_sap')->comment('users');
			$table->unsignedInteger('fk_lkp_parameterid')->index('semak_sap_ibfk_3')->comment('lkp_search_parameter');
			$table->string('parameter_value', 50)->nullable();
			$table->unsignedInteger('fk_applid')->nullable()->index('fk_applid')->comment('ek_application');
			$table->integer('created_by')->nullable();
			$table->dateTime('created_date')->nullable();
			$table->integer('modify_by')->nullable();
			$table->dateTime('modify_date')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->dateTime('deleted_date')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('semak_sap');
	}

}
