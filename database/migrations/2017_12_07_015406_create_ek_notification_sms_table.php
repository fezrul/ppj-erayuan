<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEkNotificationSmsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ek_notification_sms', function(Blueprint $table)
		{
			$table->increments('nsms_id');
			$table->string('nsms_mobileno', 20);
			$table->string('nsms_message', 160);
			$table->unsignedInteger('fk_applid')->index('ek_notification_sms_ibfk_1')->comment('ek_application');
			$table->unsignedInteger('fk_userid')->unsigned()->index('ek_notification_sms_ibfk_2')->comment('users');
			$table->integer('nsms_status')->nullable()->comment('0-not sent 1-sent');
			$table->string('nsms_response', 10)->nullable();
			$table->integer('created_by')->nullable();
			$table->dateTime('created_date')->nullable();
			$table->integer('modify_by')->nullable();
			$table->dateTime('modify_date')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ek_notification_sms');
	}

}
