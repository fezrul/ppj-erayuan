<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAssigndateToApplication extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ek_application', function (Blueprint $table) {
            $table->dateTime('appl_assigndate')->nullable()->after('appl_personincharge');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ek_application', function (Blueprint $table) {
            $table->dropColumn('appl_assigndate');
        });
    }
}
