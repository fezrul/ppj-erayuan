<?php

use Carbon\Carbon;
use Ekompaun\Systemconfig\Model\Holiday;
use Faker\Generator as Faker;

$factory->define(Holiday::class, function (Faker $faker) {

    $randomDate = Carbon::createFromFormat('Y-m-d', $faker->dateTimeThisYear()->format('Y-m-d'));

    return [
        'hday_name'      => $faker->sentence,
        'hday_date_from' => $randomDate->format('d-m-Y'),
        'hday_date_to'   => $randomDate->addDay(rand(0, 10))->format('d-m-Y'),
        'hday_status'    => $faker->randomElement([1, 2]),
    ];
});
