<?php

use Carbon\Carbon;
use Ekompaun\Payment\Data\Model\RunningNo;
use Faker\Generator as Faker;

$factory->define(RunningNo::class, function (Faker $faker) {

    return [
        'trans_no'       => 0,

    ];
});
