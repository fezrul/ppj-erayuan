<?php

use App\User;
use Faker\Generator as Faker;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Appeal\Model\Transaction;

$factory->define(Ekompaun\Appeal\Model\Payment::class, function (Faker $faker) {
    $date        = \Carbon\Carbon::createFromFormat('Y-m-d', $faker->dateTimeThisYear()->format('Y-m-d'));
    $transaction = Transaction::get()->random();
    $application = Application::get()->random();
    $pelanggan  = User::Customer()->get()->random();

    $amount = $faker->numberBetween(50, 100);

    return [
        'fk_paymentmethod'      => $faker->numberBetween(1, 6),
        'fk_applid'             => $application,
        'fk_users'              => $pelanggan,
        'fk_tranid'             => $transaction,
        'pymt_totalamount'      => $amount,
        'pymt_received'         => $amount - $faker->randomElement([0, 10, 50, $amount]),
        'pymt_date'             => $date,
        'pymt_receiptnumber'    => $faker->randomDigit,
        'pymt_refno'            => $faker->randomDigit,
    ];
});
