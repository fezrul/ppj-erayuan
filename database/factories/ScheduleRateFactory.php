<?php

use Faker\Generator as Faker;

$factory->define(
    \Ekompaun\Systemconfig\Model\ScheduleRate::class, function (Faker $faker) {
    $date = \Carbon\Carbon::createFromFormat('Y-m-d', $faker->dateTimeThisYear()->format('Y-m-d'));

    return [
        'fk_lkp_period'     => null,
        'fk_lkp_offence'    => null,
        'fk_lkp_vehicle'    => null,
        'fk_lkp_act'        => null,
        'schd_usercategory' => $faker->randomElement([1, 2]),
        'schd_amount'       => $faker->numberBetween(10, 100),
        'schd_startdate'    => $date,
        'schd_enddate'      => $date->addYear(),
        'schd_status'       => $faker->randomElement([0, 1]),
    ];
}
);
