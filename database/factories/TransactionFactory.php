<?php

use Faker\Generator as Faker;
use App\User;
use Ekompaun\Systemconfig\Model\Lookup\Offence;

$factory->define(
    \Ekompaun\Appeal\Model\Transaction::class, function (Faker $faker) {

    $pegawai = User::pegawai()->get();
    $offence = Offence::all()->pluck('offence_code');

    return [
        'fk_userid'             => null,
        'fk_lkp_sapstatus'      => null,
        'fk_lkp_offencecode'    => $offence->random(),
        'created_by'            => $pegawai->random(),
        'fk_lkp_vehicletype'    => $faker->randomElement([1, 2]),
        'tran_offendername'     => $faker->name,
        'tran_offendericno'     => $faker->numberBetween(1000000, 9999999),
        'tran_compoundno'       => strtoupper($faker->lexify('??')).$faker->numerify('####'),
        'tran_compounddate'     => $faker->dateTimeThisMonth()->format('Y-m-d'),
        'tran_compoundamount'   => $faker->numberBetween(10, 100),
        'tran_place'            => $faker->city,
        'tran_refno'            => strtoupper($faker->lexify('??')).$faker->numerify('####'),
        'tran_vehicleno'        => strtoupper($faker->lexify('???')).$faker->numerify('####'),
        'tran_vehiclecolor'     => $faker->safeColorName,
        'fk_rawsapid'           => $faker->numberBetween(10, 100),

    ];
}
);
