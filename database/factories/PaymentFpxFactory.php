<?php

use App\User;
use Faker\Generator as Faker;
use Ekompaun\Appeal\Model\Payment;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Appeal\Model\Transaction;

$factory->define(Ekompaun\Appeal\Model\PaymentFpx::class, function (Faker $faker) {
    $transaction = Transaction::get()->random();
    $application = Application::get()->random();
    $pelanggan   = User::Customer()->get()->random();
    $payment     = Payment::get()->random();
    $amount = $faker->numberBetween(50, 100);

    return [
        'fk_pymtid'             =>  $payment,
        'fk_applid'             =>  $application,
        'fk_users'              =>  $pelanggan,
        'fk_tranid'             =>  $transaction,
        'bank'                  =>  $faker->name,
        'pfpx_transid'          =>  $transaction,
        'pfpx_serialno'         =>  $faker->numerify('############').strtoupper($faker->lexify('????')),
        'pfpx_amountpaid'       =>  $amount - $faker->randomElement([0, 10, 50, $amount]),
    ];
});
