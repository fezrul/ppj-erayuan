<?php

use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Storage;
use Ekompaun\Systemconfig\Enum\AppealStatusType;
use Ekompaun\Systemconfig\Model\CampaignDetail;
use Ekompaun\Systemconfig\Model\CampaignMaster;
use Ekompaun\Systemconfig\Model\Lookup\Offence;
use Ekompaun\Systemconfig\Model\Lookup\Vehicle;
use Ekompaun\Systemconfig\Enum\CampaignRateType;

$factory->define(CampaignMaster::class, function (Faker $faker) {
    Storage::makeDirectory('campaign');

    $randomDate = Carbon::createFromFormat('Y-m-d', $faker->dateTimeThisYear()->format('Y-m-d'));
    return [
        'camp_name'             => $faker->sentence,
        'camp_ratetype'         => $faker->randomElement(CampaignRateType::toArray()),
        'camp_startdate'        => $randomDate->format('d-m-Y H:i'),
        'camp_enddate'          => $randomDate->addDay(rand(0, 10))->format('d-m-Y H:i'),
        'camp_filename'         => null,
        'camp_content'          => $faker->text(),
    ];
});

$factory->define(CampaignDetail::class, function (Faker $faker) {

    $randomDate = Carbon::createFromFormat('Y-m-d', $faker->dateTimeThisYear()->format('Y-m-d'));
    $offencesId = Offence::pluck('offence_id');
    $vehicleId = Vehicle::pluck('vehicle_id');

    return [
        'fk_campid'      			=> null,
        'cdet_offencedate_from' 	=> $randomDate->format('Y'),
        'cdet_offencedate_to' 		=> $randomDate->addYear(rand(0, 10))->format('Y'),
        'cdet_offenceid'    		=> $offencesId->random(rand(1, 10)),
        'cdet_vehicletype'    		=> $vehicleId->random(),
        'cdet_amount'    			=> $faker->randomElement([10, 50]),
        'cdet_compoundstatus'       => $faker->randomElement(AppealStatusType::toArray()),
        'cdet_status'     			=> $faker->randomElement(\Ekompaun\Systemconfig\Enum\Status::toArray()),
    ];
});
