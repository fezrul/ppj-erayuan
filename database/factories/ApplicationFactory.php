<?php

use Faker\Generator as Faker;
use Ekompaun\Appeal\Model\Transaction;
use Ekompaun\Systemconfig\Enum\AppealType;
use Ekompaun\Systemconfig\Enum\StatusKompaun;

$factory->define(Ekompaun\Appeal\Model\Application::class, function (Faker $faker) {
    $date = $faker->dateTimeBetween('-6 months', 'now');
    $channel = $faker->randomElement(\Ekompaun\Systemconfig\Enum\AppealChannel::toArray());
    $phonenumber = ($channel == \Ekompaun\Systemconfig\Enum\AppealChannel::SMS)?$faker->phoneNumber():null;
    $transaction = Transaction::get()->random();


    return [
        'appl_transid'          => $transaction,
        'appl_status_sap'       => null,
        'appl_reason'           => $faker->sentence,
        'appl_result'           => $faker->numberBetween(10, 100),
        'appl_status'           => StatusKompaun::MENUNGGU_KELULUSAN,
        'appl_type'             => $faker->randomElement(AppealType::toArray()),
        'appl_date'             => $date,
        'appl_channel'          => $channel,
        'appl_personincharge'   => null,
        'appl_remark'           => $faker->sentence,
        'appl_phonenumber'      => $phonenumber,
        'appl_approveamount'    => null,
        'appl_approveby'        => null,
        'fk_campid'             => null,
        'appl_campamount'       => null,
        'created_date'          => $date
    ];
});
