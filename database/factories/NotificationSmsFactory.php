<?php

use App\User;
use Faker\Generator as Faker;
use Ekompaun\Appeal\Model\Application;

$factory->define(Ekompaun\Systemconfig\Model\NotificationSms::class, function (Faker $faker) {
    $date        = \Carbon\Carbon::createFromFormat('Y-m-d', $faker->dateTimeThisYear()->format('Y-m-d'));
    $application = Application::get()->random();
    $pegawai     = User::Pegawai()->get()->random();

    return [
        'nsms_mobileno' 	=> $faker->e164PhoneNumber(),
        'nsms_message' 		=> $faker->sentence,
        'fk_applid' 		=> $application,
        'fk_userid' 		=> $application->user->id,
        'nsms_status' 		=> '1',
        'created_by' 		=> $pegawai,
        'created_date' 		=> $date,
        'modify_date' 		=> $date,
    ];
});
