<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'name'           => $faker->name,
        'email'          => $faker->unique()->safeEmail,
        'icno'           => $faker->unique()->numberBetween(10000, 99999),
        'password'       => $password ?: $password = bcrypt('asdf1234'),
        'status'         => config('laravolt.auth.registration.status'),
        'type'           => \Ekompaun\Systemconfig\Enum\UserType::STAF,
        'remember_token' => str_random(10),
    ];
});
