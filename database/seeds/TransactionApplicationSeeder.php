<?php

use App\User;
use Illuminate\Database\Seeder;
use Ekompaun\Appeal\Model\Transaction;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Systemconfig\Model\Lookup\StatusSap;
use Ekompaun\Systemconfig\Enum\AppealStatusType;

class TransactionApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// hanya ada 3 transaction yang sudah mempunyai application

    	//dapatkan data pelanggan
    	$customers = User::customer()->get();

    	$kesMahkamah  = StatusSap::byAppealType(AppealStatusType::KES_MAHKAMAH)->get();
    	$notisKompaun = StatusSap::byAppealType(AppealStatusType::NOTIS_KOMPAUN)->get();

    	foreach ($customers as $customer) {
    		factory(Transaction::class)
    		->times(5)
    		->make()
    		->each(function ($transaction, $key) use ($customer, $kesMahkamah, $notisKompaun) {
    			//setiap customer ada 5 transaction
    			if ($key == 0) {
    				//setiap customer ada 1 transaction dengan statusSap kes mahkamah
    				$statusSap = $kesMahkamah->random();
    				$transaction->statusSap()->associate($statusSap);
    			} else {
    				//setiap customer ada 4 transaction dengan statusSap notis kompaun
    				$statusSap = $notisKompaun->random();
    				$transaction->statusSap()->associate($statusSap);
    			}
    			$transaction->user()->associate($customer);
    			$transaction->save();

    			// transaction dengan statusSap = kesMahkamah tidak ada application
    			// ada satu transaction yang tidak punya application, supaya bisa melakukan rayuan ikut jadwal
    			// setiap customer mempunyai 3 application dengan status menunggu kelulusan

    			if ($key > 1) {
    				$application = factory(Application::class)->make();
    				$application->user()->associate($customer);
    				$application->transaction()->associate($transaction);
    				$application->statusSap()->associate($statusSap);
    				$application->save();
    			}


    		});
    	}


    }
}