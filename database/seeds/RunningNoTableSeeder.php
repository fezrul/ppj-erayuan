<?php

use Illuminate\Database\Seeder;
use Ekompaun\Payment\Data\Model\RunningNo;

class RunningNoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         factory(\Ekompaun\Payment\Data\Model\RunningNo::class)->times(1)->create();

    }
}
