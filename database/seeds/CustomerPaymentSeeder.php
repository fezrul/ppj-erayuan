<?php

use App\User;
use Illuminate\Database\Seeder;
use Ekompaun\Appeal\Model\Payment;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Appeal\Model\Transaction;
use Ekompaun\Systemconfig\Model\Lookup\StatusSap;
use Ekompaun\Systemconfig\Enum\AppealStatusType;
use Ekompaun\Systemconfig\Enum\StatusType;

class CustomerPaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $notisKompaun = StatusSap::byAppealType(AppealStatusType::NOTIS_KOMPAUN)->get()->random();
        $user  = User::customer()->where('name', 'Pelanggan')->first();

        factory(\Ekompaun\Appeal\Model\Transaction::class)->times(1)->create([
            'fk_userid'         => $user->id,
            'fk_lkp_sapstatus'  => $notisKompaun->status_id
        ])
        ->each(
            function ($transaction) {
                $application = factory(Application::class)->times(1)->create([
                    'appl_transid'      => $transaction->tran_id,
                    'created_by'        => $transaction->fk_userid,
                    'appl_status_sap'   => $transaction->fk_lkp_sapstatus,
                    'appl_status'       => StatusType::PAID,
                ]);
                $transaction->application()->saveMany($application);
                $payments = factory(Payment::class)->times(1)->create([
                    'fk_tranid' => $transaction->tran_id]);
                    // 'fk_applid' => $appl->getKey()]);
            }
        );
    }
}
