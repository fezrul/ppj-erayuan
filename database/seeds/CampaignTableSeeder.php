<?php

use Illuminate\Database\Seeder;

class CampaignTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\Ekompaun\Systemconfig\Model\CampaignMaster::class)
            ->times(10)
            ->create()
            ->each(
                function ($campaign) {
                    $campaignDetails = factory(\Ekompaun\Systemconfig\Model\CampaignDetail::class)
                        ->times(3)
                        ->make();

                    $campaign->details()->saveMany($campaignDetails);

                    $path = database_path('raw/campaign.jpg');
                    $originalName = 'campaign.jpg';
                    $picture = new \Illuminate\Http\UploadedFile($path, $originalName, 'image/jpeg');
                    $campaign->savePicture($picture);
                }
            );
    }
}
