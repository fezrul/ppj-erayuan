<?php

use Illuminate\Database\Seeder;
use Ekompaun\Appeal\Model\Payment;

class PaymentFpxTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\Ekompaun\Appeal\Model\PaymentFpx::class)->times(100)->create();
    }
}
