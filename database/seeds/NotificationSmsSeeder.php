<?php

use Illuminate\Database\Seeder;

class NotificationSmsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\Ekompaun\Systemconfig\Model\NotificationSms::class)->times(20)->create();
    }
}
