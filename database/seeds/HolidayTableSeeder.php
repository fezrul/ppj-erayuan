<?php

use Illuminate\Database\Seeder;

class HolidayTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\Ekompaun\Systemconfig\Model\Holiday::class)->times(100)->create();
    }
}
