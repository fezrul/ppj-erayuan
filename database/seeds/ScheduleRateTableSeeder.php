<?php

use Ekompaun\Systemconfig\Model\ScheduleRate;
use Illuminate\Database\Seeder;

class ScheduleRateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (\Ekompaun\Systemconfig\Model\Lookup\Offence::all()->random(10) as $offence) {
            foreach (\Ekompaun\Systemconfig\Model\Lookup\Period::all()->random(10) as $period) {
                foreach (\Ekompaun\Systemconfig\Model\Lookup\Vehicle::visible()->get() as $vehicle) {
                    factory(ScheduleRate::class)->create(
                        [
                            'fk_lkp_offence'    => $offence->getKey(),
                            'fk_lkp_period'     => $period->getKey(),
                            'fk_lkp_vehicle'    => $vehicle->getKey(),
                            'fk_lkp_act'        => $offence->fk_lkp_actid,
                        ]
                    );
                }
            }
        }
    }
}
