<?php

use App\User;
use Illuminate\Database\Seeder;
use Ekompaun\Appeal\Model\Application;
use Ekompaun\Systemconfig\Enum\StatusKompaun;

class ApplicationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pegawaiJu = User::pegawaiJu()->get()->random();
        factory(Application::class)->times(20)->create();
        factory(Application::class)->times(10)->create(
            [
                'appl_status' => StatusKompaun::ASSIGNMENT,
                'appl_personincharge' => $pegawaiJu,
            ]
        );
    }
}
