<?php

use Ekompaun\Systemconfig\Enum\UserType;
use Illuminate\Database\Seeder;

class UserAndRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = config('roles');

        foreach ($roles as $roleName => $permissions) {
            $role = app(config('laravolt.epicentrum.models.role'))->firstOrCreate(['name' => $roleName]);
            $role->syncPermission($permissions);

            // create user with predefined email, for login purpose
            $email = sprintf('%s@erayuan.dev', str_slug($roleName));
            $name = title_case($roleName);
            $type = ($roleName == 'Pelanggan') ? UserType::PELANGGAN : UserType::STAF;

            factory(\App\User::class)
                ->times(1)
                ->create(
                    [
                        'email' => $email,
                        'name'  => $name,
                        'type'  => $type,
                    ]
                )
                ->each( function($user) use ($role) {
                    $user->assignRole($role);
                }
            );

            // create more dummy users
            factory(\App\User::class)->times(10)->create(['type' => $type])->each( function($user) use ($role) {
                $user->assignRole($role);
            });
        }
    }
}
