<?php

use Illuminate\Database\Seeder;
use Ekompaun\Systemconfig\Model\Lookup\Task;
use Ekompaun\Systemconfig\Enum\TaskType;

class TaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Task $task)
    {
        $tasks = $this->dataSeed();
        foreach ($tasks as $data) {
            $task->create($data);   
        }
    }

    public function dataSeed()
    {
        $tasks = [
            [
                'task_name'     => 'Index Application',
                'task_status'   => '1',
                'task_type'     => TaskType::INDEX_APPLICATION,
                'created_by'    => '1'
            ],
            [
                'task_name'     => 'Show Application',
                'task_status'   => '1',
                'task_type'     => TaskType::SHOW_APPLICATION,
                'created_by'    => '1'
            ],
            [
                'task_name'     => 'Store Application',
                'task_status'   => '1',
                'task_type'     => TaskType::STORE_APPLICATION,
                'created_by'    => '1'
            ],
            [
                'task_name'     => 'Edit Application',
                'task_status'   => '1',
                'task_type'     => TaskType::EDIT_APPLICATION,
                'created_by'    => '1'
            ],
            [
                'task_name'     => 'Update Application',
                'task_status'   => '1',
                'task_type'     => TaskType::UPDATE_APPLICATION,
                'created_by'    => '1'
            ],
            [
                'task_name'     => 'Delete Application',
                'task_status'   => '1',
                'task_type'     => TaskType::DELETE_APPLICATION,
                'created_by'    => '1'
            ],
            [
                'task_name'     => 'Index Transaction',
                'task_status'   => '1',
                'task_type'     => TaskType::INDEX_TRANSACTION,
                'created_by'    => '1'
            ],
            [
                'task_name'     => 'Show Transaction',
                'task_status'   => '1',
                'task_type'     => TaskType::SHOW_TRANSACTION,
                'created_by'    => '1'
            ],
            [
                'task_name'     => 'Store Transaction',
                'task_status'   => '1',
                'task_type'     => TaskType::STORE_TRANSACTION,
                'created_by'    => '1'
            ],
            [
                'task_name'     => 'Edit Transaction',
                'task_status'   => '1',
                'task_type'     => TaskType::EDIT_TRANSACTION,
                'created_by'    => '1'
            ],
            [
                'task_name'     => 'Update Transaction',
                'task_status'   => '1',
                'task_type'     => TaskType::UPDATE_TRANSACTION,
                'created_by'    => '1'
            ],
            [
                'task_name'     => 'Delete Transaction',
                'task_status'   => '1',
                'task_type'     => TaskType::DELETE_TRANSACTION,
                'created_by'    => '1'
            ],
            [
                'task_name'     => 'Index Payment',
                'task_status'   => '1',
                'task_type'     => TaskType::INDEX_PAYMENT,
                'created_by'    => '1'
            ],
            [
                'task_name'     => 'Show Payment',
                'task_status'   => '1',
                'task_type'     => TaskType::SHOW_PAYMENT,
                'created_by'    => '1'
            ],
            [
                'task_name'     => 'Show Payment',
                'task_status'   => '1',
                'task_type'     => TaskType::STORE_PAYMENT,
                'created_by'    => '1'
            ],
            [
                'task_name'     => 'Edit Payment',
                'task_status'   => '1',
                'task_type'     => TaskType::EDIT_PAYMENT,
                'created_by'    => '1'
            ],
            [
                'task_name'     => 'Update Payment',
                'task_status'   => '1',
                'task_type'     => TaskType::UPDATE_PAYMENT,
                'created_by'    => '1'
            ],
            [
                'task_name'     => 'Delete Payment',
                'task_status'   => '1',
                'task_type'     => TaskType::DELETE_PAYMENT,
                'created_by'    => '1'
            ],
        ];

        return $tasks;
    }
}
