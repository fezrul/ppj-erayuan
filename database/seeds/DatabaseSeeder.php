<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app('platform.package')->sync();
        app('laravolt.acl')->syncPermission();
        $this->call(RunningNoTableSeeder::class);
        $this->call(LookupSeeder::class);
        $this->call(UserAndRoleSeeder::class);
        $this->call(TaskTableSeeder::class);
        $this->call(HolidayTableSeeder::class);
        $this->call(ScheduleRateTableSeeder::class);
        $this->call(CampaignTableSeeder::class);
        $this->call(TransactionApplicationSeeder::class);
        // $this->call(NotificationSmsSeeder::class);
        // $this->call(PaymentTableSeeder::class);
        // $this->call(CustomerPaymentSeeder::class);
        // $this->call(PaymentFpxTableSeeder::class);
        // $this->call(ApplicationTableSeeder::class);

    }
}
