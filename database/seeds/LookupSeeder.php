<?php

use Illuminate\Database\Seeder;

class LookupSeeder extends Seeder
{
    protected $tables = [
        'lkp_country',
        'lkp_custom_period',
        'lkp_offence',
        'lkp_payment_method',
        'lkp_period',
        'lkp_roles',
        'lkp_search_parameter',
        'lkp_section',
        'lkp_state',
        'lkp_status',
        'lkp_status_sap',
        'lkp_task',
        'lkp_vehicle',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncate();
        $this->insert();
    }

    protected function truncate()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        foreach ($this->tables as $table) {
            \DB::table($table)->truncate();
        }

        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    protected function insert()
    {
        $file = database_path('raw/lookup.sql');
        \DB::unprepared(file_get_contents($file));
    }
}
