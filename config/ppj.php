<?php

return [

 /*
    |--------------------------------------------------------------------------
    | SAP Config parameter
    |--------------------------------------------------------------------------
    |
    | SAP PPJ - With SAPFRC - PHP 7.0
    | 
    |
    */

    	'sap' =>env('SAPOPTION', '0'),
    	'host' =>env('SAPHOST', '0'),
		'clent' =>env('SAPCLIENT', '0'),
		'username' =>env('SAPUSER', '0'),
		'password' =>env('SAPPASS', '0'),



    /*
    |--------------------------------------------------------------------------
    | SMS Config parameter
    |--------------------------------------------------------------------------
    |
    | VISTA DAPAT SMS GATEWAYS FOR PPJ
    | HTTP_API_v1.5.1. | Gerbang SMS Kerajaan 15888 Bulk API v1.1 
    |
    */

    	'sms' =>env('SMSOPTION', '0'),
    	'smsdomain' =>env('SMSDOMAIN', '0'),
    	'smskey' =>env('SMSKEY', '0'),
    	'smsuser' =>env('SMSUSER', '0'),
    	'smspass' =>env('SMSPASS', '0'),
    	'smstype' =>env('SMSTYPE', '0'),

    ];