<?php
return [
    'layout'       => 'layouts.auth',
    'captcha'      => false,
    'registration' => [
        'enable' => false,
        'status' => 'ACTIVE'
    ],
    'activation'   => [
        'enable'        => false,
        'status_before' => 'PENDING',
        'status_after'  => 'ACTIVE',
    ],
    'router'       => [
        'middleware' => ['web'],
        'prefix'     => 'auth',
    ],
    'redirect'    => [
        'after_login' => 'home',
        'after_reset_password' => 'home',
    ],
];
