<?php

/*
 * Set specific configuration variables here
 */
return [
    'route'                 => [
        'enable'     => true,
        'middleware' => ['web', 'auth'],
        'prefix'     => 'epicentrum',
    ],
    'view'                  => [
        'layout' => 'layouts.app',
    ],
    'menu'                  => [
        'enable' => true,
    ],
    'role'                  => [
        'multiple' => false,
    ],
    'repository'            => [
        'criteria' => [
            \Prettus\Repository\Criteria\RequestCriteria::class,
            \Laravolt\Epicentrum\Repositories\Criteria\WithTrashedCriteria::class,
            \Ekompaun\Systemconfig\Criteria\StaffCriteria::class,
        ],
    ],
    'user_available_status' => [
        'PENDING' => 'PENDING',
        'ACTIVE'  => 'ACTIVE',
    ],
    'models'                => [
        'role' => \Laravolt\Acl\Models\Role::class,
    ],
];
