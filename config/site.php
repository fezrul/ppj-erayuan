<?php

return [
    'name'                      => 'E-Rayuan',
    'description'               => 'E-Rayuan Kompaun Perbadanan Putrajaya',
    'copyright'                 => sprintf('Copyright %s Perbadanan Putrajaya. All rights reserved.', date('Y')),
    'default_role'              => 6,
    'auto_assign_staff_number'  => 2,
    'auto_cancel_status_6' => env('DURATION_AUTOCANCEL_STATUS_6', 3),
    'fallback_assignee_role_id' => env('FALLBACK_ASSIGNEE_ROLE_ID', 2),
    'auto_assign'               => [
        'duration_1' => env('AUTO_ASSIGN_DURATION_1', 3),
        'duration_2' => env('AUTO_ASSIGN_DURATION_2', 6),
        'duration_3' => env('AUTO_ASSIGN_DURATION_3', 9),
    ],
    'max_saman'                 => 300,
];
